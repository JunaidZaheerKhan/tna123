﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TNAApp_ViewModel.Model;
using static TNA_ViewModel.Model.TNA_VM;

namespace BAL.Repository
{
    public class TrainerRepository : BaseRespoitory
    {
        public TrainerRepository()
            : base()
        { }


        public TrainerRepository(vt_TNAEntity ContextDB)
            : base(ContextDB)
        {
            DBContext = ContextDB;
        }


        public DataTable sp_GetAllSessions_Cordinator(int coordinatorId)
        {
            //var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllSessions_Cordinator");
            SqlParameter[] param = {new SqlParameter("@ID",  coordinatorId )};
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllSessions_byTrainerID",param);
            return dt;
        }

        
        public DataTable GetAllSessions_ByTrainer(int ID)
        {
            //List<vt_Session> SessionList = new List<vt_Session>();

            SqlParameter[] param = {
            new SqlParameter("@ID",  ID )
                               };


            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllSessions_byTrainerID", param);
            return dt;
            //if (dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        vt_Session session = new vt_Session();
            //        session.ID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["ID"].ToString()) ? dt.Rows[i]["ID"].ToString() : null);
            //        session.SessionName = dt.Rows[i]["SessionName"].ToString();
            //        session.Multimedia = Convert.ToBoolean(dt.Rows[i]["Multimedia"].ToString());
            //        session.Stationary = Convert.ToBoolean(dt.Rows[i]["Stationary"].ToString());
            //        session.Hall = Convert.ToBoolean(dt.Rows[i]["Hall"]);
            //        session.Other = dt.Rows[i]["Other"].ToString();
            //        session.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
            //        session.EndDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["EndDate"].ToString()) ? dt.Rows[i]["EndDate"].ToString() : null);
            //        session.CostPerHead = Convert.ToDecimal(!string.IsNullOrEmpty(dt.Rows[i]["CostPerHead"].ToString()) ? dt.Rows[i]["CostPerHead"].ToString() : null);
            //        session.Participants = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["Participants"].ToString()) ? dt.Rows[i]["Participants"].ToString() : null);

            //        SessionList.Add(session);
            //    }

            //}
            //else
            //{
            //    SessionList = null;
            //}



        }



        public List<AssignedUserGrid> GetAllSessionsAssignedUsers(int SessionID)
        {
            List<AssignedUserGrid> UserList = new List<AssignedUserGrid>();

            SqlParameter[] param = {
            new SqlParameter("@ID",  SessionID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAssignedUsers_bySessionID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AssignedUserGrid user = new AssignedUserGrid();
                    user.ID = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
                    user.UserID = Convert.ToInt32(dt.Rows[i]["UserID"].ToString());
                    user.SessionID = Convert.ToInt32(dt.Rows[i]["SessionID"].ToString());
                    user.Feedback = dt.Rows[i]["Feedback"].ToString();
                    user.UserName = dt.Rows[i]["UserName"].ToString().Trim();
                    user.StartDate = dt.Rows[i]["StartDate"] !=DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["StartDate"]): new Nullable<DateTime>();
                    user.EndDate = dt.Rows[i]["EndDate"] != DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["EndDate"]) : new Nullable<DateTime>();
                    user.PublishEmail = dt.Rows[i]["PublishEmail"] !=DBNull.Value ? Convert.ToBoolean(dt.Rows[i]["PublishEmail"]) : false;
                    user.SessionName = Convert.ToString(dt.Rows[i]["SessionName"]);
                    if (dt.Rows[i]["IsClosed"].ToString() != "" && dt.Rows[i]["IsClosed"] != DBNull.Value)
                        user.SessionClosed = Convert.ToBoolean(dt.Rows[i]["IsClosed"].ToString());
                    
                    if (dt.Rows[i]["Attendence"].ToString() != "" && dt.Rows[i]["Attendence"] != DBNull.Value)
                        user.Attendence = Convert.ToBoolean(dt.Rows[i]["Attendence"].ToString());

                    UserList.Add(user);
                }

            }
            else
            {
                UserList = null;
            }

            return UserList;
        }

        public DataTable UpdateAttendence(AssignedUserGrid Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@SessionID",  Parameters.SessionID ),
            new SqlParameter("@UserID",  Parameters.UserID ),
            new SqlParameter("@Attendence",  Parameters.Attendence ),
            new SqlParameter("@Feedback",  Parameters.Feedback!=null?Parameters.Feedback: "" ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_UpdateSessionUsers_AttendenceFeedback", param);

            return dt;

        }
        public DataTable getTrainingCalendar_Cordinator()
        {
            
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "getTrainingCalendar_Cordinator");

            return dt;
        }
        public DataTable GetDataforTrainingCalendar(int TrainerID) {
            SqlParameter[] param = {
            new SqlParameter("@TrainerID",  TrainerID),
           
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "getTrainingCalendar", param);

            return dt;
        }
        public DataTable CloseSession_ByID(int SessionID)
        {
            SqlParameter[] param = {
            new SqlParameter("@SessionID",  SessionID ),
            new SqlParameter("@IsClosed",  true )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_ClosedSession_byID", param);

            return dt;
        }

        public DataTable AddorEditDetailsofSession(SessionForm Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID", Convert.ToInt32(Parameters.ID)),
            new SqlParameter("@StartDate", vt_Common.ConvertDateString(Parameters.StartDate)),
            new SqlParameter("@EndDate", vt_Common.ConvertDateString(Parameters.EndDate)),
            new SqlParameter("@FeedbackDate", vt_Common.ConvertDateString(Parameters.FeedbackDate)),
            new SqlParameter("@Venue", Parameters.Venue!=null?Parameters.Venue:""),
            new SqlParameter("@StartTime", Parameters.StartTime),
            new SqlParameter("@EndTime", Parameters.EndTime),
            
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_AddorEditDetailsofSession", param);

            return dt;
        }
    }
}
