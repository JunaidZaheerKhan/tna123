﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static TNA_ViewModel.Model.TNA_VM;

namespace BAL.Repository
{
    public class LineManagerRepository : BaseRespoitory
    {
        public LineManagerRepository()
            :base()
        { }

        public LineManagerRepository(vt_TNAEntity ContextDB)
            : base(ContextDB)
        {
            DBContext = ContextDB;
        }
        public DataTable GetParticpantsInSessionList(int DeptID,int EmpID) {
            SqlParameter[] param = {
                                    new SqlParameter("@DeptID", DeptID),
                                    new SqlParameter("@EmpID", EmpID),
                                    
                               };
          return Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetParticpantsInSessionList", param);
        }
        public void DeleteFromSessionandTraining(Remove_Participants_From_Session data)
        {
           
            vt_TNATrainee tnatrainee = DBContext.vt_TNATrainee.Where(x =>
                                          x.DepartmentID == data.DeptID &&
                                          x.TNAID == data.TNAID &&
                                          x.TrainingID == data.TrainingID &&
                                          x.UserID == data.ReporteeID &&
                                          x.SessionID == data.SessionID &&
                                          x.LM_ID == data.LMID
                                          ).FirstOrDefault();
             DBContext.vt_TNATrainee.Remove(tnatrainee);

            vt_SessionParticipant sessionparticipant = DBContext.vt_SessionParticipant.Where(x =>
                                          x.DeptID == data.DeptID &&
                                          x.TNAID == data.TNAID &&
                                          x.TrainingID == data.TrainingID &&
                                          x.UserID == data.ReporteeID &&
                                          x.SessionID == data.SessionID &&
                                          x.LM_ID == data.LMID
                                          ).FirstOrDefault();
            DBContext.vt_SessionParticipant.Remove(sessionparticipant);
            DBContext.SaveChanges();
                
        }

        public List<GetDepartUsers> GetLineManagerUsersByLoginID(int loginID, string departmentID,string Location , int RoleID)
        {//sp_GetUsersByLineManagerDepartment


            List<GetDepartUsers> allUserName = new List<GetDepartUsers>();

            SqlParameter[] param = {
                                    new SqlParameter("@LoginID", loginID),
                                    new SqlParameter("@DepartmentID", departmentID),
                                     new SqlParameter("@Location", Location),
                                    new SqlParameter("@RoleID", RoleID),
                               };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUsersByLineManagerDepartment", param);
     
            //var getData = ConfigurationManager.AppSettings["ConnStr"].vt_Heirarchy.Where(a => a.LoginID == loginID).Where(a => a.DepartmentName == departmentName).ToList();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    GetDepartUsers user = new GetDepartUsers();
                    user.ReporteeID = Convert.ToInt32(dt.Rows[i]["ReporteeID"]);
                    user.ReporteeName = dt.Rows[i]["ReporteeName"].ToString();
                    user.DepartmentName= dt.Rows[i]["DepartmentName"].ToString();
                    allUserName.Add(user);
                }

            }
            else
            {
                allUserName = null;
            }
            return allUserName;
        }

        public DataTable GetUsersByLoginID(string loginID, string departmentID, string Location, int RoleID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@LoginID", loginID),
                                    new SqlParameter("@DepartmentID", departmentID),                                   
                                     new SqlParameter("@Location", Location),
                                    new SqlParameter("@RoleID", RoleID),
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUsersByLineManagerDepartment", param);

            return dt;
        }

        public DataTable GetParticipantsWiseReport(int EmpID,int DeptID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@EmpID", EmpID),
                                    new SqlParameter("@DeptID", DeptID),
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "Sp_GetParticipantsWiseReport", param);

            return dt;
        }

        public DataTable GetClosedSessionList(int DeptID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@DeptID", DeptID)
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_ShowSessionDeptWise_LM", param);

            return dt;
        }
        public DataTable GetParticipantsList(SessionParticipants_mdl model)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@DeptID", model.DepartmentID),
                                    new SqlParameter("@TNAID", model.TNAID),
                                    new SqlParameter("@TrainingID", model.TrainingID),
                                    new SqlParameter("@SessionID", model.SessionID),
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetParticipantsList", param);

            return dt;
        }
        public DataTable SaveFeedback(Feedback model)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@DeptID", model.DeptID),
                                      new SqlParameter("@UserID", model.UserID),
                                    new SqlParameter("@TNAID", model.TNAID),
                                    new SqlParameter("@TrainingID", model.TrainingID),
                                    new SqlParameter("@SessionID", model.SessionID),
                                    new SqlParameter("@Rate1", model.Rate1),
                                     new SqlParameter("@Rate2", model.Rate2),
                                      new SqlParameter("@Rate3", model.Rate3),
                                       new SqlParameter("@Rate4", model.Rate4),
                                        new SqlParameter("@Rate5", model.Rate5),
                                        new SqlParameter("@Rate6", model.Rate6),
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_SaveFeedback", param);

            return dt;
        }

        public DataTable GetFeedbackValue(Feedback model)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@DeptID", model.DeptID),
                                     new SqlParameter("@UserID", model.UserID),
                                    new SqlParameter("@TNAID", model.TNAID),
                                    new SqlParameter("@TrainingID", model.TrainingID),
                                    new SqlParameter("@SessionID", model.SessionID),
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetFeedbackValue", param);

            return dt;
        }

        public DataTable SubmitSessionFeedback(int ID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@ID", ID),
                                   
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "SubmitSessionFeedback", param);

            return dt;
        }
        public List<GetAllTrainings_LM> GetTrainingsCurrentUser(string departName,int DepartmentID,int LM_ID)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@DepartName", departName),
                                     new SqlParameter("@DepartID", DepartmentID),
                                     new SqlParameter("@LM_ID",LM_ID)
                               };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_LM_GetTrainingsCurrentUser", param);

            List<GetAllTrainings_LM> lst_Trainings_LM = new List<GetAllTrainings_LM>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    GetAllTrainings_LM type = new GetAllTrainings_LM();
                    type.TnaId = Convert.ToInt32(dt.Rows[i]["TnaId"]);
                    type.TNAName = dt.Rows[i]["TNAName"].ToString();
                    type.TrainingId = Convert.ToInt32(dt.Rows[i]["TrainingId"]);
                    type.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    type.Description = dt.Rows[i]["Description"].ToString();
                    type.StartDate = Convert.ToDateTime(dt.Rows[i]["StartDate"]); //.ToString();
                    type.EndDate = Convert.ToDateTime(dt.Rows[i]["LastSubmissionDate"]); //.ToString();
                    type.DepartmentId = Convert.ToInt32(dt.Rows[i]["DepartmentId"]);
                    type.LastDateforLM = Convert.ToDateTime(dt.Rows[i]["LastDateforLM"]);
                    type.ParticipantsCount = Convert.ToInt32(dt.Rows[i]["ParticipantsCount"]);
                    type.LM_Limit = Convert.ToInt32(dt.Rows[i]["LM_Limit"]);
                    lst_Trainings_LM.Add(type);
                }

            }
            else
            {
                lst_Trainings_LM = null;
            }
            return lst_Trainings_LM;
        }

        public DataTable GetTNADeptWise(int DeptID)
        {
            SqlParameter[] para =  { new SqlParameter("@DeptID", DeptID) };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetActiveTNADeptWise", para);
            return dt;
        }

        //Search
        public List<GetAllTrainings_LM> GetSearchTrainings(string departName, string TNAName)
        {
            var valueArray = TNAName.Split(',');
            SqlParameter[] param = {
                                    new SqlParameter("@DepartName", departName),
                                    new SqlParameter("@TrainingName", Convert.ToString(valueArray[0])),
                                    new SqlParameter("@TNAName", Convert.ToString(valueArray[1]))
                                   
                               };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_LM_GetTrainingsCurrentUserbyTNAID", param);

            List<GetAllTrainings_LM> lst_Trainings_LM = new List<GetAllTrainings_LM>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    GetAllTrainings_LM type = new GetAllTrainings_LM();
                    type.TnaId = Convert.ToInt32(dt.Rows[i]["TnaId"]);
                    type.TNAName = dt.Rows[i]["TNAName"].ToString();
                    type.TrainingId = Convert.ToInt32(dt.Rows[i]["TrainingId"]);
                    type.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    type.Description = dt.Rows[i]["Description"].ToString();
                    type.StartDate = Convert.ToDateTime(dt.Rows[i]["StartDate"]); //.ToString();
                    type.EndDate = Convert.ToDateTime(dt.Rows[i]["LastSubmissionDate"]); //.ToString();
                    type.DepartmentId = Convert.ToInt32(dt.Rows[i]["DepartmentId"]);
                    type.LastDateforLM = Convert.ToDateTime(dt.Rows[i]["LastDateforLM"]);
                    type.ParticipantsCount = Convert.ToInt32(dt.Rows[i]["ParticipantsCount"]);
                    type.LM_Limit = Convert.ToInt32(dt.Rows[i]["LM_Limit"]);
                    lst_Trainings_LM.Add(type);
                }

            }
            else
            {
                lst_Trainings_LM = null;
            }
            return lst_Trainings_LM;
        }
        public string InsertUserToTNATrainee(Add_or_Remove_Participants data)
        {
            string msg = "";
            List<vt_TNATrainee> List = new List<vt_TNATrainee>();
            var previous_saved = DBContext.vt_TNATrainee.Where(x => x.DepartmentID == data.Department_Id
                                                 && x.TNAID == data.TNA_ID && x.TrainingID == data.Training_ID && x.LM_ID==data.LM_ID).Select(y => y.UserID).ToList();
            if (data.Participants_IDs==null )
            {
                if (previous_saved.Count > 0)
                {
                    var deleteList = DBContext.vt_TNATrainee.Where(x =>
                                                    x.TNAID == data.TNA_ID &&
                                                    x.TrainingID == data.Training_ID &&
                                                    x.DepartmentID == data.Department_Id &&
                                                    x.LM_ID==data.LM_ID
                                                   ).ToList();
                    DBContext.vt_TNATrainee.RemoveRange(deleteList);
                    msg = "Changes Saved Successfully";
                   
                }

                else
                {
                    msg = "Please checked the check box before submit.";
                   
                }

            }

            else
            {
                
                var deletedList = (from previous_participants in previous_saved
                                   where !data.Participants_IDs.Any(
                                                     x => x == previous_participants.Value
                                                         )
                                   select previous_participants).ToList();

                var notChangeList = (from previous_participants in previous_saved
                                     where data.Participants_IDs.Any(
                                                       x => x == previous_participants.Value
                                                           )
                                     select previous_participants).ToList();

                var newaddList = (from new_participants in data.Participants_IDs
                                  where !previous_saved.Any(
                                                    x => x.Value == new_participants
                                                        )
                                  select new_participants).ToList();

                var deleteList = DBContext.vt_TNATrainee.Where(x => x.TNAID == data.TNA_ID && x.TrainingID == data.Training_ID && x.DepartmentID == data.Department_Id && x.LM_ID==data.LM_ID
                 && deletedList.Contains(x.UserID)
                ).ToList();

                DBContext.vt_TNATrainee.RemoveRange(deleteList);

                foreach (int Participant_ID in newaddList)
                {
                    vt_TNATrainee obj = new vt_TNATrainee();
                    obj.DepartmentID = data.Department_Id;
                    obj.TNAID = data.TNA_ID;
                    obj.TrainingID = data.Training_ID;
                    obj.UserID = Participant_ID;
                    obj.Created = DateTime.Now;
                    obj.LM_ID = data.LM_ID;
                    List.Add(obj);
                }

                DBContext.vt_TNATrainee.AddRange(List);
                DBContext.SaveChanges();

                if (previous_saved.Count>0)
                {
                    msg = "Changes Saved Successfully.";
                }

                else
                {
                    msg = "User Added Successfully.";
                }
                //foreach (var item in deleteList)
                //{
                //    var remove_from_session = DBContext.vt_SessionParticipant.Where(x => x.DeptID == item.DepartmentID &&
                //    x.TNAID == item.TNAID && x.TrainingID == item.TrainingID && x.UserID == item.UserID).FirstOrDefault();
                //    DBContext.vt_SessionParticipant.Remove(remove_from_session);
                //    DBContext.SaveChanges();
                //}

                //SqlParameter[] para = {
                //new SqlParameter("@TNA_ID", Convert.ToInt32(data.TNA_ID)),
                //new SqlParameter("@Training_ID", Convert.ToInt32(data.Training_ID))};
                //var dt_1 = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "Add_Participants_using_FIFO", para);
            }
            var check_for_session = DBContext.vt_Session.Where(x => x.TNAID == data.TNA_ID && x.TrainingID == data.Training_ID).ToList();
            if (check_for_session.Count>0)
            {
                SqlParameter[] para = {
                new SqlParameter("@TNA_ID", Convert.ToInt32(data.TNA_ID)),
                new SqlParameter("@Training_ID", Convert.ToInt32(data.Training_ID))};
                var dt_1 = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "Add_Participants_using_FIFO", para);
            }

            return msg;
        }
        

       public DataTable CheckUserisExistinSession(TNATrainee_mdl data)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  data.TNAID ),
            new SqlParameter("@TrainingID",  data.TrainingID ),
            new SqlParameter("@UserID",  data.UserID ),
            new SqlParameter("@DepartmentID",  data.DepartmentId )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "CheckUserisExistinSession", param);

            return dt;
        }
        public DataTable DeleteUserTNATrainee(TNATrainee_mdl data)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  data.TNAID ),
            new SqlParameter("@TrainingID",  data.TrainingID ),
            new SqlParameter("@UserID",  data.UserID ),
            new SqlParameter("@DepartmentID",  data.DepartmentId )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_LM_DeleteUserFromTrainingTNA", param);

            return dt;
        }

        public DataTable GetAssignUsersTrainingOnPageLoad(TNATrainee_mdl data)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  data.TNAID ),
            new SqlParameter("@TrainingID",  data.TrainingID ),            
            new SqlParameter("@EmpID",  data.EmpID ),
                               };

            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_LM_GetAssignUsersTrainingOnPageLoad", param);

            //List<TNATrainee_mdl> lst_Trainings_LM = new List<TNATrainee_mdl>();
            //if (dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        TNATrainee_mdl type = new TNATrainee_mdl();
            //        type.TNAID = Convert.ToInt32(dt.Rows[i]["TNAID"]);                    
            //        type.TrainingID = Convert.ToInt32(dt.Rows[i]["TrainingId"]);
            //        type.UserID = Convert.ToInt32(dt.Rows[i]["UserID"].ToString());                    
            //        type.DepartmentId = Convert.ToInt32(dt.Rows[i]["DepartmentId"]);
            //        type.UserName = dt.Rows[i]["ReporteeName"].ToString();
            //        type.SessionID = dt.Rows[i]["SessionID"] != DBNull.Value ? Convert.ToInt32(dt.Rows[i]["SessionID"]) : 0;
            //        lst_Trainings_LM.Add(type);
            //    }

            //}
            //else
            //{
            //    lst_Trainings_LM = null;
            //}
            return dt;
        }

        public string GetDepartName_byDepartID(int DepartID)
        {
            SqlParameter[] param = {
                                    new SqlParameter("@DepartID", Convert.ToInt32(DepartID))
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDepartName_byDepartID", param);

            string departName = string.Empty;

            if (dt.Rows.Count > 0)
            {
                departName = dt.Rows[0]["DepartmentName"].ToString();
            }
            
            return departName;
        }
    }
}
