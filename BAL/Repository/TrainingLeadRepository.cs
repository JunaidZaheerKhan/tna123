﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TNAApp_ViewModel.Model;
using static TNA_ViewModel.Model.TNA_VM;
using TNA_ViewModel.Model;
using System.Configuration;


namespace BAL.Repository
{
    public class TrainingLeadRepository : BaseRespoitory
    {

        public TrainingLeadRepository()
            : base()
        { }


        public TrainingLeadRepository(vt_TNAEntity ContextDB)
            : base(ContextDB)
        {
            DBContext = ContextDB;
        }

        public EmailToTrainer GetTrainerDetail(int SessionId) {
           // vt_UserProfile data = DBContext.vt_UserProfile.Where(x => x.ID == TrainerId).FirstOrDefault();
            SqlParameter [] param =  {new SqlParameter("@SessionId",SessionId)};
            var data = DBContext.Database.SqlQuery<EmailToTrainer>("exec sp_TrainerDetailforEmail @SessionId", param).FirstOrDefault();
            return data;

        }
        public vt_EmailManagement getEmailTemplate(string emailType)
        {
            vt_EmailManagement data = DBContext.vt_EmailManagement.Where(x => x.EmailType == emailType).FirstOrDefault();
            return data;

        }
        //Dashboard

        public DataTable GetDataforTrainingCalendar()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DashboardCalendar");
            return dt;
        }

        public DataTable getTraining()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Dashboard_Training");
            return dt;
        }
        public DataTable GetCounterValue()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DashboardCounter");
            return dt;
        }

        public DataTable CurrentMonthTraining()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CurrentMonthTraining");
            return dt;
        }
        public DataTable CurrentMonthSession()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CurrentMonthSession");
            return dt;
        }

        //GetAllActiveTnaActiveTrainings

        public DataTable GetTNAStartandEndDate(int TNAID) {

            SqlParameter [] param =  { new SqlParameter("@TNAID", TNAID) };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTNAStartandEndDate", param);
            return dt;
        }
        public List<ActiveTnaAndTrainings_mdl> GetAllActiveTnaActiveTrainings()
        {
            List<ActiveTnaAndTrainings_mdl> lst = new List<ActiveTnaAndTrainings_mdl>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTnaActiveTrainings");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ActiveTnaAndTrainings_mdl mdl_activeTna_ActiveTrainings = new ActiveTnaAndTrainings_mdl();
                    mdl_activeTna_ActiveTrainings.TNAID = Convert.ToInt32(dt.Rows[i]["TNAID"]);
                    mdl_activeTna_ActiveTrainings.TNAName = Convert.ToString(dt.Rows[i]["TNAName"]);
                    mdl_activeTna_ActiveTrainings.TrainingName = Convert.ToString(dt.Rows[i]["TrainingName"]);
                    mdl_activeTna_ActiveTrainings.Descriptions = Convert.ToString(dt.Rows[i]["Description"]);
                    mdl_activeTna_ActiveTrainings.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                    mdl_activeTna_ActiveTrainings.TrainingID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    lst.Add(mdl_activeTna_ActiveTrainings);
                }
            }
            else
            {
                lst = null;
            }

            return lst;
        }

        public List<ActiveTrainingsSessions_mdl> GetAllActiveTrainingsSessions(int trainingID , int tnaID )
        {
            List<ActiveTrainingsSessions_mdl> lst_activeSessions = new List<ActiveTrainingsSessions_mdl>();

            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  trainingID),
            new SqlParameter("@TNAID",  tnaID),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTrainingsSessions", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ActiveTrainingsSessions_mdl mdl_activeTna_ActiveTrainings = new ActiveTrainingsSessions_mdl();
                    mdl_activeTna_ActiveTrainings.TNAID = Convert.ToInt32(dt.Rows[i]["TNAID"]);
                    mdl_activeTna_ActiveTrainings.TNAName = Convert.ToString(dt.Rows[i]["TNAName"]);
                    mdl_activeTna_ActiveTrainings.TrainingID = Convert.ToInt32(dt.Rows[i]["TrainingID"]);
                    mdl_activeTna_ActiveTrainings.TrainingName = Convert.ToString(dt.Rows[i]["TrainingName"]);
                    mdl_activeTna_ActiveTrainings.Descriptions = Convert.ToString(dt.Rows[i]["Description"]);
                    mdl_activeTna_ActiveTrainings.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                    mdl_activeTna_ActiveTrainings.SessionID = Convert.ToInt32(dt.Rows[i]["SessionID"]);
                    mdl_activeTna_ActiveTrainings.SessionName = Convert.ToString(dt.Rows[i]["SessionName"]);
                    mdl_activeTna_ActiveTrainings.SessionStart = dt.Rows[i]["SessionStart"]!=DBNull.Value ? Convert.ToDateTime(dt.Rows[i]["SessionStart"]): new Nullable<DateTime>();
                    mdl_activeTna_ActiveTrainings.SessionEnd = dt.Rows[i]["SessionEnd"]!=DBNull.Value? Convert.ToDateTime(dt.Rows[i]["SessionEnd"]): new Nullable<DateTime>();
                    mdl_activeTna_ActiveTrainings.Participants = Convert.ToInt32(dt.Rows[i]["Participants"]);
                    mdl_activeTna_ActiveTrainings.StartTime  = Convert.ToString((dt.Rows[i]["StartTime"]!= null ? dt.Rows[i]["StartTime"]:"" ));
                    mdl_activeTna_ActiveTrainings.EndTime = Convert.ToString((dt.Rows[i]["StartTime"]!=null ? dt.Rows[i]["EndTime"]:""));
                    lst_activeSessions.Add(mdl_activeTna_ActiveTrainings);
                }
            }
            else
            {
                lst_activeSessions = null;
            }

            return lst_activeSessions;
        }

        public List<ActiveAssignedDepartmentsEmployee_mdl> GetAllDepartmentsAssignedUsers(int tnaId, int trainingId)
        {
            List<ActiveAssignedDepartmentsEmployee_mdl> lst_activeDepartmentsAssignedUsers = new List<ActiveAssignedDepartmentsEmployee_mdl>();

            SqlParameter[] param = {
            new SqlParameter("@TnaID",  tnaId),
            new SqlParameter("@TrainingID",  trainingId)
            };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllDepartmentsAssignedUsers", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ActiveAssignedDepartmentsEmployee_mdl mdl_activeTna_ActiveTrainings = new ActiveAssignedDepartmentsEmployee_mdl();
                    mdl_activeTna_ActiveTrainings.DepartmentID = Convert.ToInt32(dt.Rows[i]["DepartmentID"]);
                    mdl_activeTna_ActiveTrainings.DepartmentName = Convert.ToString(dt.Rows[i]["DepartmentName"]);
                    mdl_activeTna_ActiveTrainings.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    mdl_activeTna_ActiveTrainings.EmployeeName = Convert.ToString(dt.Rows[i]["ReporteeName"]);
                    mdl_activeTna_ActiveTrainings.TotalCount = Convert.ToInt32(dt.Rows[i]["TotalCount"]);
                    lst_activeDepartmentsAssignedUsers.Add(mdl_activeTna_ActiveTrainings);
                }
            }
            else
            {
                lst_activeDepartmentsAssignedUsers = null;
            }

            return lst_activeDepartmentsAssignedUsers;
        }

        public DataTable GetClosedSessionsDetailbyID(int sessionID)
        {
            DataTable dt = new DataTable();
            try
            {
               

                SqlParameter[] param = {
            new SqlParameter("@SessionID",  sessionID)

                               };

                dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetClosedSessionsDetailbyID", param);

               
            }
            catch (Exception)
            {
                dt = null;
                
            }
            

            return dt;
        }

        //Search
        public DataTable GetDept(int DeptID, int TNAID, int TrainingID) {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  TNAID),
            new SqlParameter("@TrainingID",  TrainingID),
         
            };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDeptListviaTNAnotEnd", param);
            return dt;
        }
        public List<ActiveAssignedDepartmentsEmployee_mdl> SearchDepartmentsbyNameforAssignedEmployees(int tnaId, int trainingId,string DName)
        {
            List<ActiveAssignedDepartmentsEmployee_mdl> lst_activeDepartmentsAssignedUsers = new List<ActiveAssignedDepartmentsEmployee_mdl>();

            SqlParameter[] param = {
            new SqlParameter("@TnaID",  tnaId),
            new SqlParameter("@TrainingID",  trainingId),
            new SqlParameter("@DName",DName)
            };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDepartmentsAssignedUsersbyName", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ActiveAssignedDepartmentsEmployee_mdl mdl_activeTna_ActiveTrainings = new ActiveAssignedDepartmentsEmployee_mdl();
                    mdl_activeTna_ActiveTrainings.DepartmentID = Convert.ToInt32(dt.Rows[i]["DepartmentID"]);
                    mdl_activeTna_ActiveTrainings.DepartmentName = Convert.ToString(dt.Rows[i]["DepartmentName"]);
                    mdl_activeTna_ActiveTrainings.UserID = Convert.ToInt32(dt.Rows[i]["UserID"]);
                    mdl_activeTna_ActiveTrainings.EmployeeName = Convert.ToString(dt.Rows[i]["ReporteeName"]);
                    mdl_activeTna_ActiveTrainings.TotalCount = Convert.ToInt32(dt.Rows[i]["TotalCount"]);
                    lst_activeDepartmentsAssignedUsers.Add(mdl_activeTna_ActiveTrainings);
                }
            }
            else
            {
                lst_activeDepartmentsAssignedUsers = null;
            }

            return lst_activeDepartmentsAssignedUsers;
        }
        //End Search
        public static bool time(TimeSpan userStart, TimeSpan sessionStart, TimeSpan userEnd, TimeSpan sessionEnd)
        {
            bool correcttime = true;

            if (userStart >= sessionStart && userStart < sessionEnd)
            { return false; }
            //end time is in between
            if (userEnd > sessionStart && userEnd <= sessionEnd)
            { return false; }
            //start time and end time is greatar then compared meeting time 
            if (userStart < sessionStart && userEnd > sessionEnd)
            { return false; }

            return correcttime;
        }
        public DataTable InsertUsersSessions(vt_SessionParticipant data)
        {
            DataTable dta = new DataTable();
            try
            {
            SqlParameter[] param = 
            {
            new SqlParameter("@TNAID",  data.TNAID ),
            new SqlParameter("@TrainingID",  data.TrainingID ),
            new SqlParameter("@UserID",  data.UserID ),
            new SqlParameter("@DeptID",  data.DeptID),
            new SqlParameter("@SessionID",  data.SessionID),
            };

            dta = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertsUserToSession", param);
            return dta;
            }
            catch (Exception ex)
            {
                return dta = null;

            }

        }

        //public DataTable InsertUsersSessions(vt_SessionParticipant data)
        //{
        //    DataTable dta = new DataTable();
        //    try
        //    {
        //        SqlParameter[] para = {
        //    new SqlParameter("@SessionID",  data.SessionID)
        //                       };

        //        var GetsessionDateTimebySessionID = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetsessDateTimebySessionID", para);

        //        DateTime SessionStartDate = Convert.ToDateTime(GetsessionDateTimebySessionID.Rows[0]["StartDate"]).Date;
        //        DateTime SessionEndDate = Convert.ToDateTime(GetsessionDateTimebySessionID.Rows[0]["EndDate"]).Date;
        //        var SessionStartTime = Convert.ToDateTime(GetsessionDateTimebySessionID.Rows[0]["StartTime"]).TimeOfDay;
        //        var SessionEndTime = Convert.ToDateTime(GetsessionDateTimebySessionID.Rows[0]["EndTime"]).TimeOfDay;

        //        List<DateTime> SessionallDates = new List<DateTime>();
        //        for (DateTime date = SessionStartDate; date <= SessionEndDate; date = date.AddDays(1))
        //        {
        //            SessionallDates.Add(date);
        //        }

        //        SqlParameter[] param1 = {new SqlParameter("@UserID",  data.UserID )};
        //        var GetsessDateTimebyUserID = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetsessDateTimebyUserID", param1);

        //        bool GOTo = true;
        //        for (int i = 0; i < GetsessDateTimebyUserID.Rows.Count; i++)
        //        {
        //            var user_startdate = Convert.ToDateTime(GetsessDateTimebyUserID.Rows[i]["StartDate"]).Date;
        //            var user_enddate = Convert.ToDateTime(GetsessDateTimebyUserID.Rows[i]["EndDate"]).Date;
        //            var user_StartTime = Convert.ToDateTime(GetsessDateTimebyUserID.Rows[i]["StartTime"]).TimeOfDay;
        //            var user_EndTime = Convert.ToDateTime(GetsessDateTimebyUserID.Rows[i]["EndTime"]).TimeOfDay;

        //            List<DateTime> UsersallDates = new List<DateTime>();
        //            for (DateTime dates = user_startdate; dates <= user_enddate; dates = dates.AddDays(1))
        //            {
        //                UsersallDates.Add(dates);
        //            }

        //            for (int j = 0; j < SessionallDates.Count; j++)
        //            {
        //                for (int k = 0; k < UsersallDates.Count; k++)
        //                {
        //                    if (SessionallDates[j].Date == UsersallDates[k].Date)
        //                    {

        //                           GOTo= time(user_StartTime, SessionStartTime, user_EndTime, SessionEndTime);
        //                        if (GOTo==false)
        //                        {
        //                            break;
        //                        }

        //                    }
        //                }
        //                if (GOTo == false)
        //                {
        //                    break;
        //                }

        //            }
        //            if (GOTo == false)
        //            {
        //                break;
        //            }

        //        }
        //        if (GOTo == true)
        //        {
        //            SqlParameter[] param = {
        //    new SqlParameter("@TNAID",  data.TNAID ),
        //    new SqlParameter("@TrainingID",  data.TrainingID ),
        //    new SqlParameter("@UserID",  data.UserID ),
        //    new SqlParameter("@DeptID",  data.DeptID),
        //    new SqlParameter("@SessionID",  data.SessionID),

        //                       };

        //            dta = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertsUserToSession", param);

        //        }

        //        else
        //        {
        //            dta.Columns.Add("checkIsExist", typeof(bool));
        //            dta.Rows.Add(true);

        //        }
        //        return dta;
        //    }
        //    catch (Exception ex)
        //    {
        //        return dta = null;

        //    }

        //}
        public List<SessionParticipants_mdl> GetAssignSessionsUsersOnLoad(int? tnaid, int? trainingid)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  tnaid),
            new SqlParameter("@TrainingID",  trainingid)
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAssignSessionsUsersOnLoad", param);

            List<SessionParticipants_mdl> lst_AssignSessionUsers = new List<SessionParticipants_mdl>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SessionParticipants_mdl type = new SessionParticipants_mdl();
                    type.TNAID = Convert.ToInt32(dt.Rows[i]["TNAID"]);
                    type.TrainingID = Convert.ToInt32(dt.Rows[i]["TrainingId"]);
                    type.UserID = Convert.ToInt32(dt.Rows[i]["UserID"].ToString());
                    type.DepartmentID = Convert.ToInt32(dt.Rows[i]["DeptId"]);
                    type.EmployeeName = dt.Rows[i]["ReporteeName"].ToString();
                    type.SessionID = Convert.ToInt32(dt.Rows[i]["SessionID"]);
                    type.DepartmentName= Convert.ToString(dt.Rows[i]["DepartmentName"]);
                   // type.PublishEmail = Convert.ToBoolean(dt.Rows[i]["PublishEmail"]);
                    lst_AssignSessionUsers.Add(type);
                }

            }
            else
            {
                lst_AssignSessionUsers = null;
            }
            return lst_AssignSessionUsers;
        }

        public DataTable GetdetailclosedSessionsUsers(int tnaid, int trainingid,int SessionID)
        {
            DataTable dt = new DataTable();
            try
            {
              SqlParameter[] param = {
                    new SqlParameter("@TNAID",  tnaid),
                    new SqlParameter("@TrainingID",  trainingid),
                    new SqlParameter("@SessionID",  SessionID)
                };

                dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetclosedSessionsUsers", param);
            }
            catch (Exception ex)
            {

                dt = null;
            }
                       
            return dt;
        }

        public DataTable DeleteUserFromSession(vt_SessionParticipant data)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  data.TNAID ),
            new SqlParameter("@TrainingID",  data.TrainingID ),
            new SqlParameter("@UserID",  data.UserID ),
            new SqlParameter("@DeptID",  data.DeptID ),
            new SqlParameter("@SessionID",  data.SessionID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteUserFromSessions", param);

            return dt;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        public List<vt_Trainings> GetAllTrainings()
        {
            List<vt_Trainings> TrainingList = new List<vt_Trainings>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTrainings");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Trainings training = new vt_Trainings();
                    training.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    training.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    training.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
                    training.CourseOwnerID = Convert.ToInt32(dt.Rows[i]["CourseOwnerID"]);
                    training.ModeID = Convert.ToInt32(dt.Rows[i]["ModeID"]);
                    training.iLearnURL = dt.Rows[i]["iLearnURL"].ToString();
                    //training.TrainingTypeID = Convert.ToInt32(dt.Rows[i]["TrainingTypeID"]);
                    training.CostPerHead = Convert.ToDecimal(dt.Rows[i]["CostPerHead"]);
                    training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                    training.Deleted = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Deleted"].ToString()) ? dt.Rows[i]["Deleted"].ToString() : null);
                    training.CreatedBy = Convert.ToInt32(dt.Rows[i]["CreatedBy"]);
                    training.Description = dt.Rows[i]["Description"].ToString();
                    training.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    training.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

                    TrainingList.Add(training);
                }

            }
            else
            {
                TrainingList = null;
            }

            return TrainingList;

        }

        public DataTable GetAllActiveTrainings()
        {
           // List<vt_Trainings> TrainingList = new List<vt_Trainings>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTrainings");
            return dt;
            //if (dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        vt_Trainings training = new vt_Trainings();
            //        training.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
            //        training.TrainingName = dt.Rows[i]["TrainingName"].ToString();
            //        training.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
            //        training.CourseOwnerID = Convert.ToInt32(dt.Rows[i]["CourseOwnerID"]);
            //        training.ModeID = Convert.ToInt32(dt.Rows[i]["ModeID"]);
            //        training.iLearnURL = dt.Rows[i]["iLearnURL"].ToString();
            //        training.TrainingTypeID = Convert.ToInt32(dt.Rows[i]["TrainingTypeID"]);
            //        training.CostPerHead = Convert.ToDecimal(dt.Rows[i]["CostPerHead"]);
            //        training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
            //        training.Deleted = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Deleted"].ToString()) ? dt.Rows[i]["Deleted"].ToString() : null);
            //        training.CreatedBy = Convert.ToInt32(dt.Rows[i]["CreatedBy"]);
            //        training.Description = dt.Rows[i]["Description"].ToString();
            //        training.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
            //        training.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

            //        TrainingList.Add(training);
            //    }

            //}
            //else
            //{
            //    TrainingList = null;
            //}

            //return TrainingList;

        }

        public List<TNA_Trainings> GetAllTrainingsWithDepart()
        {

            List<TNA_Trainings> List = new List<TNA_Trainings>();

            //List<GetTrainingDepartList> List = new List<GetTrainingDepartList>();

            var dtActiveTrainings = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTrainings");





            if (dtActiveTrainings.Rows.Count > 0)
            {
                for (int i = 0; i < dtActiveTrainings.Rows.Count; i++)
                {
                    TNA_Trainings ObjTraining = new TNA_Trainings();

                    ObjTraining.TrainingID = Convert.ToInt32(dtActiveTrainings.Rows[i]["ID"].ToString());
                    ObjTraining.TrainingName = dtActiveTrainings.Rows[i]["TrainingName"].ToString();
                    ObjTraining.Training_IsSelected = false;
                    //        List.Add(ObjTraining);

                    SqlParameter[] param = {
                                    new SqlParameter("@TrainingID", Convert.ToInt32(ObjTraining.TrainingID) ),
                                       };

                    var dtDepartments = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDeparts_ByTrainingID", param);

                    if (dtDepartments.Rows.Count > 0)
                    {
                        List<TNA_TrainingDepartment> ListObjDepartment = new List<TNA_TrainingDepartment>();
                        for (int j = 0; j < dtDepartments.Rows.Count; j++)
                        {
                            TNA_TrainingDepartment ObjDepartment = new TNA_TrainingDepartment();
                            ObjDepartment.DepartmentID = Convert.ToInt32(dtDepartments.Rows[j]["DepartmentID"].ToString());
                            ObjDepartment.DepartmentName = dtDepartments.Rows[j]["DepartmentName"].ToString();
                            ObjDepartment.Department_IsLocked = false;
                            ListObjDepartment.Add(ObjDepartment);
                        }

                        ObjTraining.Departs = ListObjDepartment;

                    }
                    List.Add(ObjTraining);
                }
            }


            return List;
        }

        public List<TNA_Trainings> GetAllTrainingsWithDeparts_Saved( int TNA_ID)
        {

            List<TNA_Trainings> List = new List<TNA_Trainings>();

            SqlParameter[] param = {
            new SqlParameter("@ID",  TNA_ID ),
                               };
            var dtSavedTrainings = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTNASavedTrainings",param);

            if (dtSavedTrainings.Rows.Count > 0)
            {
                for (int i = 0; i < dtSavedTrainings.Rows.Count; i++)
                {
                    TNA_Trainings ObjTraining = new TNA_Trainings();
                    ObjTraining.TrainingID = Convert.ToInt32(dtSavedTrainings.Rows[i]["TrainingID"].ToString());
                    ObjTraining.TrainingName = dtSavedTrainings.Rows[i]["TrainingName"].ToString();
                    ObjTraining.Training_IsSelected = true;

                    SqlParameter[] paramDepart = {
                                    new SqlParameter("@TNAID", Convert.ToInt32(TNA_ID) ),
                                    new SqlParameter("@TrainingID", Convert.ToInt32(ObjTraining.TrainingID) )
                                       };

                    var dtDepartments = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDeparts_ByTNATrainingID", paramDepart);

                    if (dtDepartments.Rows.Count > 0)
                    {
                        List<TNA_TrainingDepartment> ListObjDepartment = new List<TNA_TrainingDepartment>();

                        for (int j = 0; j < dtDepartments.Rows.Count; j++)
                        {
                            TNA_TrainingDepartment ObjDepartment = new TNA_TrainingDepartment();
                            ObjDepartment.DepartmentID = Convert.ToInt32(dtDepartments.Rows[j]["DepartmentID"].ToString());
                            ObjDepartment.DepartmentName = dtDepartments.Rows[j]["DepartmentName"].ToString();
                            ObjDepartment.Department_IsLocked = Convert.ToBoolean(dtDepartments.Rows[j]["IsActive"]); 
                            ListObjDepartment.Add(ObjDepartment);
                        }

                        ObjTraining.Departs = ListObjDepartment;

                    }

                    List.Add(ObjTraining);
                }

                
            }
            

            return List;
        }

        public List<TNA_TrainingDepartment> GetDepartsByTrainingID(int ID)
        {
            List<TNA_TrainingDepartment> DeaprtList = new List<TNA_TrainingDepartment>();
            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  ID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDeparts_ByTrainingID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TNA_TrainingDepartment Depart = new TNA_TrainingDepartment();
                    Depart.DepartmentID = Convert.ToInt32(dt.Rows[i]["DepartmentID"]);
                    Depart.DepartmentName = dt.Rows[i]["DepartmentName"].ToString();
                    Depart.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    //Depart.IsLocked = false;

                    DeaprtList.Add(Depart);
                }

            }
            else
            {
                DeaprtList = null;
            }

            return DeaprtList;
        }

        public int[] GetDepartsByTNATrainingID(int TNAID, int TrainingID)
        {
            int[] DepartIDs;

            SqlParameter[] param = {
            new SqlParameter("@TNAID",  TNAID ),
            new SqlParameter("@TrainingID",  TrainingID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTrainingLockedDepartments", param);

            if (dt.Rows.Count > 0)
            {
                DepartIDs = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DepartIDs[i] = Convert.ToInt32(dt.Rows[i]["DepartmentID"].ToString());
                }

            }
            else
            {
                DepartIDs = null;
            }

            return DepartIDs;
        }

        public DataTable AddTNA(TNAForm tna)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  tna.ID ),
            new SqlParameter("@TNAName",  tna.TNAName ),
            new SqlParameter("@StartDate",Convert.ToDateTime(tna.StartDate).ToString("MM/dd/yyyy")),
            new SqlParameter("@EndDate", Convert.ToDateTime(tna.EndDate).ToString("MM/dd/yyyy")),
            new SqlParameter("@LastDateforLM", Convert.ToDateTime(tna.LastDateforLM).ToString("MM/dd/yyyy")),
            new SqlParameter("@IsActive", tna.IsActive),
            new SqlParameter("@Created", DateTime.Now),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_TNA", param);

            return dt;
        }

        public string GetLastTNAID()
        {
            SqlParameter[] param = { };
            var TopicID = Entity_Common.get_Scalar(ConfigurationManager.AppSettings["ConnStr"], "sp_GetLastTNAID", param);
            return TopicID;
        }

        public bool AddNewTNADepatDetail(List<TNA_Details> tna, int TNAID, int TrainingID) {
            try
            {
                bool isExist = false;
                var listExistDeprt = DBContext.vt_TNADetails.Where(x => x.TrainingID == TrainingID && x.TNAID == TNAID).ToList();
             
                for (int i = 0; i < tna.Count; i++){
                    for (int j = 0; j < listExistDeprt.Count; j++)
                    {
                        if (tna[i].DepartmentID == listExistDeprt[j].DepartmentID)
                        {
                            isExist = true;
                            break;
                        }
                    }

                    if (isExist==false){
                        DBContext.vt_TNADetails.Add(new vt_TNADetails
                        {
                            TNAID = TNAID,
                            TrainingID = TrainingID,
                            DepartmentID = tna[i].DepartmentID,
                            CreatedAt = DateTime.Now,
                            IsActive = tna[i].Department_IsLocked
                        });
                    }
                }

               
                DBContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        public bool AddNewTNADetailDept(List<TNA_Details> tna,int TNAID,int TrainingID)
        {
            try
            {
                for (int i = 0; i < tna.Count; i++)
                {
                    DBContext.vt_TNADetails.Add(new vt_TNADetails
                    {
                        TNAID = TNAID,
                        TrainingID = TrainingID,
                        DepartmentID = tna[i].DepartmentID,
                        CreatedAt = DateTime.Now,
                        IsActive = tna[i].Department_IsLocked
                    });
                }

                DBContext.SaveChanges();

                return true;
            }
            catch (Exception e)
            {

                return false;
            }
            
        }
        public DataTable AddNewTNADetail(TNADetails tna)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  tna.TNA_ID ),
            new SqlParameter("@TrainingID",  tna.Training_ID ),
            new SqlParameter("@DepartmentID",  tna.Department_ID ),
            new SqlParameter("@CreatedAt",  DateTime.Now ),
            new SqlParameter("@IsActive",  tna.Department_IsLocked )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Insert_TNADetail", param);

            return dt;
        }

        public DataTable UpdateTNADetail(TNADetails tna)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  tna.TNA_ID ),
            new SqlParameter("@TrainingID",  tna.Training_ID ),
            new SqlParameter("@DepartmentID",  tna.Department_ID ),
            new SqlParameter("@CreatedAt",  DateTime.Now ),
            new SqlParameter("@IsActive",  tna.Department_IsLocked )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_update_TNADetail", param);

            return dt;
        }

        public DataTable GetselectedDepartmentfromTNAforEmail(int TNAID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  TNAID )
          
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDeptofTNAforEmail", param);

            return dt;
        }

        public DataTable UpdateEmailColumn(string EmailSenttoDep )
        {
            DataTable dt = new DataTable();
            var arrayofIDs = EmailSenttoDep.Split(',');           
            for (int i = 0; i < arrayofIDs.Length; i++)
            {
                int ID = Convert.ToInt32(arrayofIDs[i]);
                SqlParameter[] param = {
                new SqlParameter("@ID",  ID ) };
                dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_UpdateEmailColumn", param);
            }
            

            return dt;
        }
        public DataTable GetTNAEmailTemplate()
        {
            var  dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetEmailTemplatesofTNAbyID");
            return dt;
        }
        public DataTable CheckforParticpantwasAssign(int TNAID, int TrainingID,int DeptID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",TNAID ),
            new SqlParameter("@TrainingID",TrainingID ),
             new SqlParameter("@DeptID",DeptID )

                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CheckforParticpantwasAssign", param);

            return dt;
        }
        
        public DataTable GetAllPreviousDepByTNA(int TNAID, int TrainingID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  TNAID ),
            new SqlParameter("@TrainingID", TrainingID )
           
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllPreviousDepByTNA", param);

            return dt;
        }
        public DataTable DeleteTNADetail(int TNAID, int TrainingID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID",  TNAID ),
            new SqlParameter("@TrainingID", TrainingID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Delete_TNADetail", param);

            return dt;
        }

        public List<vt_TNA> GetAllTNAs()
        {
            List<vt_TNA> TNAList = new List<vt_TNA>();
            //Get All Active TNAs
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTNAs");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_TNA tna = new vt_TNA();
                    tna.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    tna.TNAName = dt.Rows[i]["TNAName"].ToString();
                    tna.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
                    tna.LastSubmissionDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["LastSubmissionDate"].ToString()) ? dt.Rows[i]["LastSubmissionDate"].ToString() : null);
                    tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    tna.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

                    TNAList.Add(tna);
                }

            }
            else
            {
                TNAList = null;
            }

            return TNAList;

        }

        public TNAForm GetTNA_byID(int TNAID)
        {
            TNAForm tna = new TNAForm();

            SqlParameter[] param = {
            new SqlParameter("@TNAID",  TNAID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTNAby_ID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    tna.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    tna.TNAName = dt.Rows[i]["TNAName"].ToString();
                    tna.StartDate = Convert.ToDateTime(dt.Rows[i]["StartDate"]).ToString("MM/dd/yyyy");
                    tna.EndDate = Convert.ToDateTime(dt.Rows[i]["LastSubmissionDate"]).ToString("MM/dd/yyyy");
                    tna.LastDateforLM= Convert.ToDateTime(dt.Rows[i]["LastDateforLM"]).ToString("MM/dd/yyyy");
                    tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                }

            }
            else
            {
                tna = null;
            }

            return tna;
        }

        public int[] GetTrainings_byTNAID(int TNAID)
        {
            //List<vt_TopicTypes> TopicDepartList = new List<vt_TopicTypes>();
            int[] TrainingID_Array;

            SqlParameter[] param = {
            new SqlParameter("@ID",  TNAID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTNATrainings", param);

            if (dt.Rows.Count > 0)
            {
                TrainingID_Array = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TrainingID_Array[i] = Convert.ToInt32(dt.Rows[i]["TrainingID"].ToString());
                }

            }
            else
            {
                TrainingID_Array = null;
            }

            return TrainingID_Array;

        }

        public DataTable DeleteTrainings_byTNAID(int TNAID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  TNAID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteAll_TNATrainings", param);

            return dt;
        }

        public DataTable DeleteTNA(int TNAID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  TNAID ),
            new SqlParameter("@Deleted" , DateTime.Now),
            new SqlParameter("@IsActive",  false ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteTNA_byID", param);

            return dt;
        }

        public DataTable GetActiveTNAs()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetActiveTNA");

            return dt;
        }

        public DataTable GetAllSessions()
        {
            List<vt_Session> SessionList = new List<vt_Session>();
            DataTable dtable = new DataTable();
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllSessions");

            if (dt.Rows.Count > 0)
            {
                dtable = dt;
                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    vt_Session session = new vt_Session();
                //    session.ID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["ID"].ToString()) ? dt.Rows[i]["ID"].ToString() : null);
                //    session.SessionName = dt.Rows[i]["SessionName"].ToString();
                //    session.Multimedia = Convert.ToBoolean(dt.Rows[i]["Multimedia"].ToString());
                //    session.Stationary = Convert.ToBoolean(dt.Rows[i]["Stationary"].ToString());
                //    session.Hall = Convert.ToBoolean(!string.IsNullOrEmpty(dt.Rows[i]["Hall"].ToString()) ? dt.Rows[i]["Hall"].ToString() : null);
                //    session.Other = dt.Rows[i]["Other"].ToString();
                //    session.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
                //    session.EndDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["EndDate"].ToString()) ? dt.Rows[i]["EndDate"].ToString() : null);
                //    session.CostPerHead = Convert.ToDecimal(!string.IsNullOrEmpty(dt.Rows[i]["CostPerHead"].ToString()) ? dt.Rows[i]["CostPerHead"].ToString() : null);
                //    session.Participants = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["Participants"].ToString()) ? dt.Rows[i]["Participants"].ToString() : null);
                //    session.IsClosed = Convert.ToBoolean(!string.IsNullOrEmpty(dt.Rows[i]["IsClosed"].ToString()) ? dt.Rows[i]["IsClosed"].ToString() : null);
                //    SessionList.Add(session);
                //}

            }
            else
            {
                //SessionList = null;
                dtable = null;
            }

            return dtable;

        }

        public DataTable GetAllClosedSessions()
        {
            List<vt_Session> SessionList = new List<vt_Session>();
            DataTable dtable = new DataTable();
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllClosedSessions");

            if (dt.Rows.Count > 0)
            {
                dtable = dt;               

            }
            else
            {
                //SessionList = null;
                dtable = null;
            }

            return dtable;

        }
        public DataTable GetAllParticipants_bySession(int SessionID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  SessionID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllSessionParticipants", param);

            return dt;
        }
        public DataTable DeleteSessionandAllParticipants(int SessionID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  SessionID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteSessionandAllParticipants", param);

            return dt;
        }
        public DataTable DeleteSession(int SessionID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  SessionID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteSession_byID", param);

            return dt;
        }

        public SessionForm GetSession_byID(int SessionID)
        {
            SessionForm session = new SessionForm();

            SqlParameter[] param = {
            new SqlParameter("@ID",  SessionID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetSession_byID", param);
          
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    session.ID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["ID"].ToString()) ? dt.Rows[0]["ID"].ToString() : null);
                    session.TNAID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["TNAID"].ToString()) ? dt.Rows[0]["TNAID"].ToString() : null);
                    session.TrainingID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["TrainingID"].ToString()) ? dt.Rows[0]["TrainingID"].ToString() : null);
                    session.SessionName = dt.Rows[0]["SessionName"].ToString();
                    session.Multimedia = Convert.ToBoolean(dt.Rows[0]["Multimedia"].ToString());
                    session.Stationary = Convert.ToBoolean(dt.Rows[0]["Stationary"].ToString());
                    session.Hall = Convert.ToBoolean(dt.Rows[0]["Hall"]);
                    session.Other = dt.Rows[0]["Other"].ToString();
                    session.TrainerID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["TrainerID"].ToString()) ? dt.Rows[0]["TrainerID"].ToString() : null);
                    session.StartDate = dt.Rows[0]["StartDate"]!=DBNull.Value && dt.Rows[0]["StartDate"]!= null ? Convert.ToDateTime(dt.Rows[0]["StartDate"].ToString()).ToString("yyyy/MM/dd"):"";
                    session.EndDate = dt.Rows[0]["EndDate"]!=DBNull.Value && dt.Rows[0]["EndDate"]!=null? Convert.ToDateTime(dt.Rows[0]["EndDate"].ToString()).ToString("yyyy/MM/dd"):"";
                    session.FeedbackDate = dt.Rows[0]["FeedbackDate"]!=DBNull.Value && dt.Rows[0]["FeedbackDate"]!=null ? Convert.ToDateTime(dt.Rows[0]["FeedbackDate"].ToString()).ToString("yyyy/MM/dd"):"";
                    session.Venue = dt.Rows[0]["Venue"].ToString();
                    session.CostPerhead = Convert.ToDecimal(!string.IsNullOrEmpty(dt.Rows[0]["CostPerHead"].ToString()) ? dt.Rows[0]["CostPerHead"].ToString() : null);
                    session.GeographicID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["GeographyID"].ToString()) ? dt.Rows[0]["GeographyID"].ToString() : null);
                    session.Participants = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["Participants"].ToString()) ? dt.Rows[0]["Participants"].ToString() : null);
                    session.IsClosed = Convert.ToBoolean(!string.IsNullOrEmpty(dt.Rows[i]["IsClosed"].ToString()) ? dt.Rows[i]["IsClosed"].ToString() : null);
                    session.StartTime = dt.Rows[0]["StartTime"] !=DBNull.Value ?  Convert.ToDateTime(dt.Rows[0]["StartTime"]): new Nullable<DateTime>();
                    session.EndTime = dt.Rows[0]["EndTime"]!=DBNull.Value ? Convert.ToDateTime(dt.Rows[0]["EndTime"]): new Nullable<DateTime>();
                    session.SessionType =dt.Rows[0]["SessionType"]!=DBNull.Value ? Convert.ToInt32(dt.Rows[0]["SessionType"]) : 0;
                    session.TrainerName = dt.Rows[0]["TrainerName"] != DBNull.Value ? Convert.ToString(dt.Rows[0]["TrainerName"]) : "";
                }

            }
            else
            {
                session = null;
            }

            return session;
        }

        public List<vt_TNA> GetAllActiveTNAs()
        {
            List<vt_TNA> TNAList = new List<vt_TNA>();
            //Get All Active TNAs
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTNAs");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_TNA tna = new vt_TNA();
                    tna.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    tna.TNAName = dt.Rows[i]["TNAName"].ToString();
                    tna.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
                    tna.LastSubmissionDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["LastSubmissionDate"].ToString()) ? dt.Rows[i]["LastSubmissionDate"].ToString() : null);
                    tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    tna.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

                    TNAList.Add(tna);
                }

            }
            else
            {
                TNAList = null;
            }

            return TNAList;

        }

        public List<vt_Trainings> GetAllActiveTrainings_byTNAID(int TNAID)
        {
            List<vt_Trainings> TrainingList = new List<vt_Trainings>();

            SqlParameter[] param = {
            new SqlParameter("@ID",  TNAID ),
                               };
            //Get All Active Trainings
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTrainingsby_TNAID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Trainings train = new vt_Trainings();
                    train.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    train.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    //tna.StartDate = vt_Common.ConvertDateString(dt.Rows[i]["StartDate"].ToString()).ToString("yyyy/MM/dd");
                    //tna.EndDate = vt_Common.ConvertDateString(dt.Rows[i]["LastSubmissionDate"].ToString()).ToString("yyyy/MM/dd");
                    //tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);

                    TrainingList.Add(train);
                }

            }
            else
            {
                TrainingList = null;
            }

            return TrainingList;
        }

        public List<vt_Geography> GetAllGeography()
        {
            List<vt_Geography> AreaList = new List<vt_Geography>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllAreaGeography");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Geography area = new vt_Geography();
                    area.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    area.Geography = dt.Rows[i]["Geography"].ToString();

                    AreaList.Add(area);
                }

            }
            else
            {
                AreaList = null;
            }

            return AreaList;
        }

        public List<vt_UserProfile> GetAllTrainers()
        {
            List<vt_UserProfile> TrainerList = new List<vt_UserProfile>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTrainers");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_UserProfile user = new vt_UserProfile();
                    user.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    user.FullName = dt.Rows[i]["FullName"].ToString();

                    TrainerList.Add(user);
                }

            }
            else
            {
                TrainerList = null;
            }

            return TrainerList;
        }
        public List<vt_UserProfile> GetAllCoordinator()
        {
            List<vt_UserProfile> TrainerList = new List<vt_UserProfile>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllCoordinator");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_UserProfile user = new vt_UserProfile();
                    user.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    user.FullName = dt.Rows[i]["FullName"].ToString();

                    TrainerList.Add(user);
                }

            }
            else
            {
                TrainerList = null;
            }

            return TrainerList;
        }
        public int AddUpdateSession(SessionForm Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID", Convert.ToInt32(Parameters.ID)),
            new SqlParameter("@TNAID", Convert.ToInt32(Parameters.TNAID)),
            new SqlParameter("@TrainingID", Convert.ToInt32(Parameters.TrainingID)),
            new SqlParameter("@SessionName", Parameters.SessionName),
            new SqlParameter("@Multimedia", Convert.ToBoolean(Parameters.Multimedia)),
            new SqlParameter("@Staitionary", Convert.ToBoolean(Parameters.Stationary)),
            new SqlParameter("@Hall", Convert.ToBoolean(Parameters.Hall)),
            new SqlParameter("@Other", Parameters.Other!=null?Parameters.Other:""),
            new SqlParameter("@TrainerID", Convert.ToInt32(Parameters.TrainerID)),            
            new SqlParameter("@CostPerHead", Convert.ToDecimal(Parameters.CostPerhead!=null?Parameters.CostPerhead:0)),
            new SqlParameter("@GeographyID", Convert.ToInt32(Parameters.GeographicID)),
            new SqlParameter("@Participants", Convert.ToInt32(Parameters.Participants)),
            new SqlParameter("@SessionType", Convert.ToInt32(Parameters.SessionType)),
            new SqlParameter("@TrainerName", Convert.ToString(Parameters.TrainerName!=null?Parameters.TrainerName:""))

                               };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_Session", param);

            SqlParameter[] para = {
            new SqlParameter("@TNA_ID", Convert.ToInt32(Parameters.TNAID)),
            new SqlParameter("@Training_ID", Convert.ToInt32(Parameters.TrainingID))
            };
            var dt_1 = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "Add_Participants_using_FIFO", para);

            int lastSessionID = DBContext.Database.SqlQuery<int>("select Top 1 ID From vt_session order by ID desc").FirstOrDefault();
            return lastSessionID;
        }


        public DataTable DeleteTNADetailTraining(int TNA_ID, int Training_ID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID", Convert.ToInt32(TNA_ID)),
            new SqlParameter("@TrainingID", Convert.ToInt32(Training_ID)),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteTNADetailTraining", param);

            return dt;
        }

        public DataTable GetSessionEmailTemplate()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetSessionEmailTemplate");
            return dt;
        }
        public DataTable GetCancelSessionEmailTemplate()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetCancleSessionEmailTemplate");
            return dt;
        }
        public DataTable setPublishEmailTrue(int ID)
        {
            SqlParameter[] param = {new SqlParameter("@ID", ID)};
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_SetPublishEmailTrue", param);
            return dt;
        }
        public DataTable GetSessionAssignedUsers(SessionParticipants_mdl Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@TNAID", Convert.ToInt32(Parameters.TNAID)),
            new SqlParameter("@TrainingID", Convert.ToInt32(Parameters.TrainingID)),
            new SqlParameter("@SessionID", Convert.ToInt32(Parameters.SessionID)),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetSessionPublishAssignedUsers", param);

            return dt;
        }


        #region Commented Code

        //public SessionForm GetSession_byID(int SessionID)
        //{
        //    SessionForm session = new SessionForm();

        //    SqlParameter[] param = {
        //    new SqlParameter("@ID",  SessionID ),
        //                       };

        //    var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetSessionby_ID", param);

        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {

        //            session.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
        //            session.TNAName = dt.Rows[i]["TNAName"].ToString();
        //            session.StartDate = vt_Common.ConvertDateString(dt.Rows[i]["StartDate"].ToString()).ToString("yyyy/MM/dd");
        //            session.EndDate = vt_Common.ConvertDateString(dt.Rows[i]["LastSubmissionDate"].ToString()).ToString("yyyy/MM/dd");
        //            session.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
        //        }

        //    }
        //    else
        //    {
        //        session = null;
        //    }

        //    return session;
        //}
        #endregion

        public List<vt_Trainings> GetActiveAllTrainings()
        {
            List<vt_Trainings> TrainingList = new List<vt_Trainings>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTrainings");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Trainings training = new vt_Trainings();
                    training.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    training.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    training.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
                    training.CourseOwnerID = Convert.ToInt32(dt.Rows[i]["CourseOwnerID"]);
                    training.ModeID = Convert.ToInt32(dt.Rows[i]["ModeID"]);
                    training.iLearnURL = dt.Rows[i]["iLearnURL"].ToString();
                    //training.TrainingTypeID = Convert.ToInt32(dt.Rows[i]["TrainingTypeID"]);
                    training.CostPerHead = Convert.ToDecimal(dt.Rows[i]["CostPerHead"]);
                    training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                    training.Deleted = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Deleted"].ToString()) ? dt.Rows[i]["Deleted"].ToString() : null);
                    training.CreatedBy = Convert.ToInt32(dt.Rows[i]["CreatedBy"]);
                    training.Description = dt.Rows[i]["Description"].ToString();
                    training.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    training.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

                    TrainingList.Add(training);
                }

            }
            else
            {
                TrainingList = null;
            }

            return TrainingList;

        }


    }
}





//using DAL.DBEntities;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using TNAApp_ViewModel.Model;
//using static TNA_ViewModel.Model.TNA_VM;

//namespace BAL.Repository
//{
//    public class TrainingLeadRepository : BaseRespoitory
//    {

//        public TrainingLeadRepository()
//            :base()
//        { }


//        public TrainingLeadRepository(vt_TNAEntity ContextDB)
//            :base(ContextDB)
//        {
//            DBContext = ContextDB;
//        }

//        public List<vt_Trainings> GetAllTrainings()
//        {
//            List<vt_Trainings> TrainingList = new List<vt_Trainings>();

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllTrainings");

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_Trainings training = new vt_Trainings();
//                    training.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    training.TrainingName = dt.Rows[i]["TrainingName"].ToString();
//                    training.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
//                    training.CourseOwnerID = Convert.ToInt32(dt.Rows[i]["CourseOwnerID"]);
//                    training.ModeID = Convert.ToInt32(dt.Rows[i]["ModeID"]);
//                    training.iLearnURL = dt.Rows[i]["iLearnURL"].ToString();
//                    training.TrainingTypeID = Convert.ToInt32(dt.Rows[i]["TrainingTypeID"]);
//                    training.CostPerHead = Convert.ToDecimal(dt.Rows[i]["CostPerHead"]);
//                    training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
//                    training.Deleted = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Deleted"].ToString()) ? dt.Rows[i]["Deleted"].ToString() : null);
//                    training.CreatedBy = Convert.ToInt32(dt.Rows[i]["CreatedBy"]);
//                    training.Description = dt.Rows[i]["Description"].ToString();
//                    training.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
//                    training.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

//                    TrainingList.Add(training);
//                }

//            }
//            else
//            {
//                TrainingList = null;
//            }

//            return TrainingList;

//        }

//        public DataTable AddTNA(TNAForm tna)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@ID",  tna.ID ),
//            new SqlParameter("@TNAName",  tna.TNAName ),
//            new SqlParameter("@StartDate", vt_Common.ConvertDateString(tna.StartDate)),
//            new SqlParameter("@EndDate", vt_Common.ConvertDateString(tna.EndDate)),
//            new SqlParameter("@IsActive", tna.IsActive),
//            new SqlParameter("@Created", DateTime.Now),
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_InsertUpdate_TNA", param);

//            return dt;
//        }

//        public string GetLastTNAID()
//        {
//            SqlParameter[] param = { };
//            var TopicID = Entity_Common.get_Scalar(DBContext, "sp_GetLastTNAID", param);
//            return TopicID;
//        }

//        public DataTable AddNewTNADetail(TNADetails tna)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@TNAID",  tna.TNA_ID ),
//            new SqlParameter("@TrainingID",  tna.Training_ID )
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_Insert_TNADetail", param);

//            return dt;
//        }

//        public List<vt_TNA> GetAllTNAs()
//        {
//            List<vt_TNA> TNAList = new List<vt_TNA>();
//            //Get All Active TNAs
//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllTNAs");

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_TNA tna = new vt_TNA();
//                    tna.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    tna.TNAName = dt.Rows[i]["TNAName"].ToString();
//                    tna.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
//                    tna.LastSubmissionDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["LastSubmissionDate"].ToString()) ? dt.Rows[i]["LastSubmissionDate"].ToString() : null);
//                    tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
//                    tna.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

//                    TNAList.Add(tna);
//                }

//            }
//            else
//            {
//                TNAList = null;
//            }

//            return TNAList;

//        }

//        public TNAForm GetTNA_byID(int TNAID)
//        {
//            TNAForm tna = new TNAForm();

//            SqlParameter[] param = {
//            new SqlParameter("@TNAID",  TNAID ),
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetTNAby_ID", param);

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {

//                    tna.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    tna.TNAName = dt.Rows[i]["TNAName"].ToString();
//                    tna.StartDate = vt_Common.ConvertDateString(dt.Rows[i]["StartDate"].ToString()).ToString("yyyy/MM/dd");
//                    tna.EndDate = vt_Common.ConvertDateString(dt.Rows[i]["LastSubmissionDate"].ToString()).ToString("yyyy/MM/dd");
//                    tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
//                }

//            }
//            else
//            {
//                tna = null;
//            }

//            return tna;
//        }

//        public int[] GetTrainings_byTNAID(int TNAID)
//        {
//            //List<vt_TopicTypes> TopicDepartList = new List<vt_TopicTypes>();
//            int[] TrainingID_Array;

//            SqlParameter[] param = {
//            new SqlParameter("@ID",  TNAID ),
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetTNATrainings", param);

//            if (dt.Rows.Count > 0)
//            {
//                TrainingID_Array = new int[dt.Rows.Count];

//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    TrainingID_Array[i] = Convert.ToInt32(dt.Rows[i]["TrainingID"].ToString());
//                }

//            }
//            else
//            {
//                TrainingID_Array = null;
//            }

//            return TrainingID_Array;

//        }

//        public DataTable DeleteTrainings_byTNAID(int TNAID)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@ID",  TNAID )
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_DeleteAll_TNATrainings", param);

//            return dt;
//        }

//        public DataTable DeleteTNA(int TNAID)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@ID",  TNAID ),
//            new SqlParameter("@Deleted" , DateTime.Now)
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_DeleteTNA_byID", param);

//            return dt;
//        }

//        public DataTable GetActiveTNAs()
//        {
//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetActiveTNA");

//            return dt;
//        }

//        public List<vt_Session> GetAllSessions()
//        {
//            List<vt_Session> SessionList = new List<vt_Session>();

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllSessions");

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_Session session = new vt_Session();
//                    session.ID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["ID"].ToString()) ? dt.Rows[i]["ID"].ToString() : null);
//                    session.SessionName = dt.Rows[i]["SessionName"].ToString();
//                    session.Multimedia = Convert.ToBoolean(dt.Rows[i]["Multimedia"].ToString());
//                    session.Stationary =Convert.ToBoolean(dt.Rows[i]["Stationary"].ToString());
//                    session.Hall = Convert.ToBoolean(dt.Rows[i]["Hall"]);
//                    session.Other = dt.Rows[i]["Other"].ToString();
//                    session.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
//                    session.EndDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["EndDate"].ToString()) ? dt.Rows[i]["EndDate"].ToString() : null);
//                    session.CostPerHead = Convert.ToDecimal(!string.IsNullOrEmpty(dt.Rows[i]["CostPerHead"].ToString()) ? dt.Rows[i]["CostPerHead"].ToString() : null);
//                    session.Participants = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["Participants"].ToString()) ? dt.Rows[i]["Participants"].ToString() : null);

//                    SessionList.Add(session);
//                }

//            }
//            else
//            {
//                SessionList = null;
//            }

//            return SessionList;

//        }


//        public DataTable GetAllParticipants_bySession(int SessionID)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@ID",  SessionID )
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllSessionParticipants",param);

//            return dt;
//        }

//        public DataTable DeleteSession(int SessionID)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@ID",  SessionID )
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_DeleteSession_byID", param);

//            return dt;
//        }

//        public SessionForm GetSession_byID(int SessionID)
//        {
//            SessionForm session = new SessionForm();

//            SqlParameter[] param = {
//            new SqlParameter("@ID",  SessionID )
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetSession_byID",param);

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {

//                    session.ID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["ID"].ToString()) ? dt.Rows[0]["ID"].ToString() : null);
//                    session.TNAID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["TNAID"].ToString()) ? dt.Rows[0]["TNAID"].ToString() : null);
//                    session.TrainingID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["TrainingID"].ToString()) ? dt.Rows[0]["TrainingID"].ToString() : null);
//                    session.SessionName = dt.Rows[0]["SessionName"].ToString();
//                    session.Multimedia = Convert.ToBoolean(dt.Rows[0]["Multimedia"].ToString());
//                    session.Stationary = Convert.ToBoolean(dt.Rows[0]["Stationary"].ToString());
//                    session.Hall = Convert.ToBoolean(dt.Rows[0]["Hall"]);
//                    session.Other = dt.Rows[0]["Other"].ToString();
//                    session.TrainerID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["TrainerID"].ToString()) ? dt.Rows[0]["TrainerID"].ToString() : null);
//                    session.StartDate = Convert.ToDateTime(dt.Rows[0]["StartDate"].ToString()).ToString("yyyy/MM/dd");
//                    session.EndDate = Convert.ToDateTime(dt.Rows[0]["EndDate"].ToString()).ToString("yyyy/MM/dd");
//                    session.FeedbackDate = Convert.ToDateTime(dt.Rows[0]["FeedbackDate"].ToString()).ToString("yyyy/MM/dd");
//                    session.Venue = dt.Rows[0]["Venue"].ToString();
//                    session.CostPerhead = Convert.ToDecimal(!string.IsNullOrEmpty(dt.Rows[0]["CostPerHead"].ToString()) ? dt.Rows[0]["CostPerHead"].ToString() : null);
//                    session.GeographicID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["GeographyID"].ToString()) ? dt.Rows[0]["GeographyID"].ToString() : null);
//                    session.Participants = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["Participants"].ToString()) ? dt.Rows[0]["Participants"].ToString() : null);

//                }

//            }
//            else
//            {
//                session = null;
//            }

//            return session;
//        }

//        public List<vt_TNA> GetAllActiveTNAs()
//        {
//            List<vt_TNA> TNAList = new List<vt_TNA>();
//            //Get All Active TNAs
//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllActiveTNAs");

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_TNA tna = new vt_TNA();
//                    tna.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    tna.TNAName = dt.Rows[i]["TNAName"].ToString();
//                    tna.StartDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["StartDate"].ToString()) ? dt.Rows[i]["StartDate"].ToString() : null);
//                    tna.LastSubmissionDate = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["LastSubmissionDate"].ToString()) ? dt.Rows[i]["LastSubmissionDate"].ToString() : null);
//                    tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
//                    tna.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

//                    TNAList.Add(tna);
//                }

//            }
//            else
//            {
//                TNAList = null;
//            }

//            return TNAList;

//        }

//        public List<vt_Trainings> GetAllActiveTrainings_byTNAID(int TNAID)
//        {
//            List<vt_Trainings> TrainingList = new List<vt_Trainings>();

//            SqlParameter[] param = {
//            new SqlParameter("@ID",  TNAID ),
//                               };
//            //Get All Active Trainings
//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllActiveTrainingsby_TNAID", param);

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_Trainings train = new vt_Trainings();
//                    train.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    train.TrainingName = dt.Rows[i]["TrainingName"].ToString();
//                    //tna.StartDate = vt_Common.ConvertDateString(dt.Rows[i]["StartDate"].ToString()).ToString("yyyy/MM/dd");
//                    //tna.EndDate = vt_Common.ConvertDateString(dt.Rows[i]["LastSubmissionDate"].ToString()).ToString("yyyy/MM/dd");
//                    //tna.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);

//                    TrainingList.Add(train);
//                }

//            }
//            else
//            {
//                TrainingList = null;
//            }

//            return TrainingList;
//        }

//        public List<vt_Geography> GetAllGeography()
//        {
//            List<vt_Geography> AreaList = new List<vt_Geography>();

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllAreaGeography");

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_Geography area = new vt_Geography();
//                    area.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    area.Geography = dt.Rows[i]["Geography"].ToString();

//                    AreaList.Add(area);
//                }

//            }
//            else
//            {
//                AreaList = null;
//            }

//            return AreaList;
//        }

//        public List<vt_UserProfile> GetAllTrainers()
//        {
//            List<vt_UserProfile> TrainerList = new List<vt_UserProfile>();

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAllTrainers");

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    vt_UserProfile user = new vt_UserProfile();
//                    user.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//                    user.FullName = dt.Rows[i]["FullName"].ToString();

//                    TrainerList.Add(user);
//                }

//            }
//            else
//            {
//                TrainerList = null;
//            }

//            return TrainerList;
//        }

//        public DataTable AddUpdateSession(SessionForm Parameters)
//        {
//            SqlParameter[] param = {
//            new SqlParameter("@ID", Convert.ToInt32(Parameters.ID)),
//            new SqlParameter("@TNAID", Convert.ToInt32(Parameters.TNAID)),
//            new SqlParameter("@TrainingID", Convert.ToInt32(Parameters.TrainingID)),
//            new SqlParameter("@SessionName", Parameters.SessionName),
//            new SqlParameter("@Multimedia", Convert.ToBoolean(Parameters.Multimedia)),
//            new SqlParameter("@Staitionary", Convert.ToBoolean(Parameters.Stationary)),
//            new SqlParameter("@Hall", Convert.ToBoolean(Parameters.Hall)),
//            new SqlParameter("@Other", Parameters.Other),
//            new SqlParameter("@TrainerID", Convert.ToInt32(Parameters.TrainerID)),
//            new SqlParameter("@StartDate", vt_Common.ConvertDateString(Parameters.StartDate)),
//            new SqlParameter("@EndDate", vt_Common.ConvertDateString(Parameters.EndDate)),
//            new SqlParameter("@FeedbackDate", vt_Common.ConvertDateString(Parameters.FeedbackDate)),
//            new SqlParameter("@Venue", Parameters.Venue),
//            new SqlParameter("@CostPerHead", Convert.ToDecimal(Parameters.CostPerhead)),
//            new SqlParameter("@GeographyID", Convert.ToInt32(Parameters.GeographicID)),
//            new SqlParameter("@Participants", Convert.ToInt32(Parameters.Participants)),
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_InsertUpdate_Session", param);

//            return dt;
//        }


//        public List<AssignedUserGrid> GetAllSessionsAssignedUsers(int SessionID)
//        {
//            List<AssignedUserGrid> UserList = new List<AssignedUserGrid>();

//            SqlParameter[] param = {
//            new SqlParameter("@ID",  SessionID )
//                               };

//            var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetAssignedUsers_bySessionID", param);

//            if (dt.Rows.Count > 0)
//            {
//                for (int i = 0; i < dt.Rows.Count; i++)
//                {
//                    AssignedUserGrid user = new AssignedUserGrid();
//                    //user.ID = Convert.ToInt32(dt.Rows[i]["ID"].ToString());
//                    user.UserID = Convert.ToInt32(dt.Rows[i]["UserID"].ToString());
//                    user.SessionID = Convert.ToInt32(SessionID);
//                    user.UserName = dt.Rows[i]["UserName"].ToString().Trim();
//                    if (dt.Rows[i]["Attendence"].ToString() !="" && dt.Rows[i]["Attendence"]!=DBNull.Value)
//                        user.Attendence = Convert.ToBoolean(dt.Rows[i]["Attendence"].ToString());
//                    user.Feedback = dt.Rows[i]["Feedback"].ToString();

//                    UserList.Add(user);
//                }

//            }
//            else
//            {
//                UserList = null;
//            }

//            return UserList;
//        }




//        #region Commented Code

//        //public SessionForm GetSession_byID(int SessionID)
//        //{
//        //    SessionForm session = new SessionForm();

//        //    SqlParameter[] param = {
//        //    new SqlParameter("@ID",  SessionID ),
//        //                       };

//        //    var dt = Entity_Common.get_SP_DataTable(DBContext, "sp_GetSessionby_ID", param);

//        //    if (dt.Rows.Count > 0)
//        //    {
//        //        for (int i = 0; i < dt.Rows.Count; i++)
//        //        {

//        //            session.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
//        //            session.TNAName = dt.Rows[i]["TNAName"].ToString();
//        //            session.StartDate = vt_Common.ConvertDateString(dt.Rows[i]["StartDate"].ToString()).ToString("yyyy/MM/dd");
//        //            session.EndDate = vt_Common.ConvertDateString(dt.Rows[i]["LastSubmissionDate"].ToString()).ToString("yyyy/MM/dd");
//        //            session.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
//        //        }

//        //    }
//        //    else
//        //    {
//        //        session = null;
//        //    }

//        //    return session;
//        //}
//        #endregion

//    }
//}
