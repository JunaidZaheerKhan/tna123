﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using TNAApp_ViewModel.Model;
using static TNA_ViewModel.Model.TNA_VM;

namespace BAL.Repository
{
    public class AdminRepository : BaseRespoitory
    {
        public AdminRepository()
            : base()
        { }


        public AdminRepository(vt_TNAEntity ContextDB)
            : base(ContextDB)
        {
            DBContext = ContextDB;
        }

        public DataTable AddEmailTemplate(vt_EmailManagement model) {

            SqlParameter[] para = {
                 new SqlParameter("@ID",model.ID),
                new SqlParameter("@Subject",model.Subject),
                new SqlParameter("@Body",model.Body),
                new SqlParameter("@EmailType",model.EmailType),
            };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_SaveChangesEmail", para);
            return dt;
        }
        public DataTable GetHeirarchy()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetHeirarchy");
            return dt;
        }
        public DataTable GetLineManagers()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetLineManagersList");
            return dt;
        }

        public bool CreateLineManagers(List<LineManagers> model) {

            foreach (var item in model)
            {
                SqlParameter[] param = { new SqlParameter("@EmpId", item.EmpID) };
                var IsExist = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CheckUserAlreadyExist", param);
                if (IsExist.Rows.Count == 0)
                {
                    SqlParameter[] para = { new SqlParameter("@RoleID", 3),
                        new SqlParameter("@EmployeeID", item.EmpID),
                        new SqlParameter("@LoginID", item.LoginID),
                        new SqlParameter("@DepartmentID", item.DeptID),
                        new SqlParameter("@FullName", item.Name),
                        new SqlParameter("@Email",item.Email),
                        new SqlParameter("@IsApproved", true),
                        new SqlParameter("@IsActive", true),
                        new SqlParameter("@Created", DateTime.Now),
                        new SqlParameter("@Location", item.Location),
                       
                    };
                    var CreateLineManagers = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CreateLineManagers", para);
                }
               
            }
            bool Status = true;
            return Status;



        }
        public DataTable UpdateHeirarchy() {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_UpdateHeirarchy");
            return dt;
        }
        public DataTable SetIsActiveFalseRemaining()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_SetIsActiveFalseRemaining");
            return dt;
        }
        public DataTable AutoUpdateLocationandDept()
        {
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_AutoUpdateLoctionandDeptID");
            return dt;
        }
        //public DataTable autoCreateLineManager()
        //{
        //    var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetallDeparment");
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        SqlParameter[] para = { new SqlParameter("DepartmentName") };
        //    }
        //    return 
        //}

        public DataTable GetEmailTemplates() {
           
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetEmailTemplates");
            return dt;
        }
        public DataTable GetDataforTrainingCalendar()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DashboardCalendar");
            return dt;
        }
       
        public DataTable getTraining()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Dashboard_Training");
            return dt;
        }
        public DataTable GetCounterValue()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DashboardCounter");
            return dt;
        }
        
        public DataTable CurrentMonthTraining()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CurrentMonthTraining");
            return dt;
        }
        public DataTable CurrentMonthSession()
        {
            DataTable dt = new DataTable();
            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CurrentMonthSession");
            return dt;
        }
        public List<vt_Departments> GetAllDeparts()
        {
            List<vt_Departments> DepartList = new List<vt_Departments>();
            //var dt = Entity_Common.get_SP_DataTable(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllDepartments");
            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllDepartments");
            //var dt = Entity_Common.get_SP_DataTable(b, "sp_GetAllDepartments");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Departments depart = new vt_Departments();
                    depart.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    depart.DepartmentName = dt.Rows[i]["DepartmentName"].ToString();

                    DepartList.Add(depart);
                }

            }
            else
            {
                DepartList = null;
            }

            return DepartList;
        }

        public List<vt_Topics> GetAllTopics()
        {
            List<vt_Topics> TopicList = new List<vt_Topics>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTopics");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Topics topic = new vt_Topics();
                    topic.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    topic.TopicName = dt.Rows[i]["TopicName"].ToString();
                    topic.Description = dt.Rows[i]["Description"].ToString();
                    topic.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    topic.Created = Convert.ToDateTime(dt.Rows[i]["Created"]);

                    TopicList.Add(topic);
                }

            }
            else
            {
                TopicList = null;
            }

            return TopicList;

        }

        public List<vt_Topics> GetAllActiveTopics()
        {
            List<vt_Topics> TopicList = new List<vt_Topics>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllActiveTopics");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Topics topic = new vt_Topics();
                    topic.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    topic.TopicName = dt.Rows[i]["TopicName"].ToString();
                    topic.Description = dt.Rows[i]["Description"].ToString();
                    topic.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    topic.Created = Convert.ToDateTime(dt.Rows[i]["Created"]);

                    TopicList.Add(topic);
                }

            }
            else
            {
                TopicList = null;
            }

            return TopicList;

        }

        public DataTable AddTopic(TopicForm topic)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  topic.ID ),
            new SqlParameter("@TopicName",  topic.TopicName ),
            new SqlParameter("@Description", topic.Description),
            new SqlParameter("@IsActive", topic.IsActive),
            new SqlParameter("@CreatedAt", DateTime.Now),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_Topic", param);

            return dt;
        }

        public string GetLastTopicID()
        {
            SqlParameter[] param = { };
            var TopicID = Entity_Common.get_Scalar(ConfigurationManager.AppSettings["ConnStr"], "sp_GetLastTopicID", param);
            return TopicID;
        }

        public DataTable AddNewTopicJunc(int TopicID, int DepartID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TopicID",  TopicID ),
            new SqlParameter("@DepartID",  DepartID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Insert_TopicJunc", param);
            return dt;
        }

        public TopicForm GetTopic_byID(int TopicID)
        {
            TopicForm topic = new TopicForm();

            SqlParameter[] param = {
            new SqlParameter("@TopicID",  TopicID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTopicby_ID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    topic.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    topic.TopicName = dt.Rows[i]["TopicName"].ToString();
                    topic.Description = dt.Rows[i]["Description"].ToString();
                    topic.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    //topic.Created = Convert.ToDateTime(dt.Rows[i]["Created"]);
                }

            }
            else
            {
                topic = null;
            }

            return topic;
        }

        public int[] GetTopicDeparts_byID(int TopicID)
        {
            //List<vt_TopicTypes> TopicDepartList = new List<vt_TopicTypes>();
            int[] DepartIDs;

            SqlParameter[] param = {
            new SqlParameter("@ID",  TopicID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTopicDepartments", param);

            if (dt.Rows.Count > 0)
            {
                DepartIDs = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //vt_TopicTypes topicdepart = new vt_TopicTypes();
                    //topicdepart.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    //topicdepart.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
                    //topicdepart.DepartmentID = Convert.ToInt32(dt.Rows[i]["DepartmentID"]);
                    //TopicDepartList.Add(topicdepart);
                    DepartIDs[i] = Convert.ToInt32(dt.Rows[i]["DepartmentID"].ToString());
                }

            }
            else
            {
                DepartIDs = null;
            }

            return DepartIDs;

        }

        public DataTable UpdateTopic(TopicForm topic)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  topic.ID ),
            new SqlParameter("@TopicName",  topic.TopicName ),
            new SqlParameter("@Description", topic.Description),
            new SqlParameter("@IsActive", topic.IsActive),
            new SqlParameter("@CreatedAt", DateTime.Now),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_Topic", param);

            return dt;
        }

        public DataTable DeleteAllTopicDeparts(int TopicID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  TopicID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteAll_TopicDeparts", param);

            return dt;
        }

        public DataTable DeleteTopic(int TopicID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TopicID",  TopicID ),
            new SqlParameter("@Deleted" , DateTime.Now),
            new SqlParameter("@IsActive",  false ),
                               };

            //var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteTopic_byID", param);
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteTopicHeirarchy", param);
            return dt;
        }

        public List<vt_Training_Type> GetAllTypes()
        {
            List<vt_Training_Type> TypeList = new List<vt_Training_Type>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTypes");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Training_Type type = new vt_Training_Type();
                    type.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    type.TrainingType = dt.Rows[i]["TrainingType"].ToString();

                    TypeList.Add(type);
                }

            }
            else
            {
                TypeList = null;
            }

            return TypeList;
        }

        public List<vt_Training_Mode> GetAllModes()
        {
            List<vt_Training_Mode> ModeList = new List<vt_Training_Mode>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllModes");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Training_Mode mode = new vt_Training_Mode();
                    mode.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    mode.ModeOfTraining = dt.Rows[i]["ModeOfTraining"].ToString();

                    ModeList.Add(mode);
                }

            }
            else
            {
                ModeList = null;
            }

            return ModeList;
        }

        public List<vt_CourseMaterial> GetAllCourseMaterial()
        {
            List<vt_CourseMaterial> ModeList = new List<vt_CourseMaterial>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllCourseMaterial");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_CourseMaterial mode = new vt_CourseMaterial();
                    mode.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    mode.CourseMaterial = dt.Rows[i]["CourseMaterial"].ToString();

                    ModeList.Add(mode);
                }

            }
            else
            {
                ModeList = null;
            }

            return ModeList;
        }

        public DataTable AddTraining(TrainingForm Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  Parameters.ID ),
            new SqlParameter("@TopicID",  Parameters.TopicID ),
            new SqlParameter("@TrainingName",  Parameters.TrainingName ),
            new SqlParameter("@CourseOwnerID",  Parameters.CourseOwnerID ),
            new SqlParameter("@Description", Parameters.Description!=null?Parameters.Description:""),
            new SqlParameter("@ModeID",  Parameters.ModeID ),
            new SqlParameter("@iLearnURL",  Parameters.iLearnURL!=null?Parameters.iLearnURL:""),
            //new SqlParameter("@TrainingTypeID",  Parameters.TypeID ),
            new SqlParameter("@CostPerHead",  Parameters.CostPer!=null ?Parameters.CostPer:0 ),
            new SqlParameter("@Days",  Parameters.Days!=null ? Parameters.Days:0 ),
            new SqlParameter("@IsActive", Parameters.IsActive),
            new SqlParameter("@Created", DateTime.Now),
            new SqlParameter("@CreatedBy", Parameters.CreatedBy),
            new SqlParameter("@LM_Limit",Parameters.LM_Limit)
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_Training", param);

            return dt;
        }

        public DataTable UpdateTraining(TrainingForm Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  Parameters.ID ),
            new SqlParameter("@TopicID",  Parameters.TopicID ),
            new SqlParameter("@TrainingName",  Parameters.TrainingName ),
            new SqlParameter("@CourseOwnerID",  Parameters.CourseOwnerID ),
            new SqlParameter("@Description", Parameters.Description),
            new SqlParameter("@ModeID",  Parameters.ModeID ),
            new SqlParameter("@iLearnURL",  Parameters.iLearnURL ),
            new SqlParameter("@TrainingTypeID",  Parameters.TypeID ),
            new SqlParameter("@CostPerHead",  Parameters.CostPer ),
            new SqlParameter("@Days",  Parameters.Days ),
            new SqlParameter("@IsActive", Parameters.IsActive),
            new SqlParameter("@Created", DateTime.Now),
            new SqlParameter("@CreatedBy", Parameters.CreatedBy),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_Training", param);

            return dt;
        }

        public string GetLastTrainingID()
        {
            SqlParameter[] param = { };
            var TopicID = Entity_Common.get_Scalar(ConfigurationManager.AppSettings["ConnStr"], "sp_GetLastTrainingID", param);
            return TopicID;
        }

        public DataTable AddNewTrainingAttachments(TrainingsAttachments Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  Parameters.TrainingID ),
            new SqlParameter("@Attachment",  Parameters.Attachment ),
            new SqlParameter("@DocType",  Parameters.DocType ),
            new SqlParameter("@Path", Parameters.Path),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Insert_TrainingAttachments", param);

            return dt;
        }

        public DataTable AddNewTrainingCourseMaterial(TrainingsCourseMaterial Parameters)
        {
            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  Parameters.TrainingID ),
            new SqlParameter("@CourseMaterialID",  Parameters.CourseMaterialID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Insert_TrainingCourseMaterial", param);

            return dt;
        }

        public List<vt_Trainings> GetAllTrainings()
        {
            List<vt_Trainings> TrainingList = new List<vt_Trainings>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllTrainings");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Trainings training = new vt_Trainings();
                    training.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    training.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    training.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
                    training.CourseOwnerID = Convert.ToInt32(dt.Rows[i]["CourseOwnerID"]);
                    training.ModeID = Convert.ToInt32(dt.Rows[i]["ModeID"]);
                    training.iLearnURL = dt.Rows[i]["iLearnURL"].ToString();
                    //training.TrainingTypeID = Convert.ToInt32(dt.Rows[i]["TrainingTypeID"]);
                    training.CostPerHead = Convert.ToDecimal(dt.Rows[i]["CostPerHead"]);
                    training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                    training.Deleted = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Deleted"].ToString()) ? dt.Rows[i]["Deleted"].ToString() : null);
                    training.CreatedBy = Convert.ToInt32(dt.Rows[i]["CreatedBy"]);
                    training.Description = dt.Rows[i]["Description"].ToString();
                    training.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    training.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

                    TrainingList.Add(training);
                }

            }
            else
            {
                TrainingList = null;
            }

            return TrainingList;

        }
        public DataTable GetAllLog()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllLogs");
                
            }
            catch (Exception ex)
            {
                dt = null;

            }
            return dt;

        }
        public DataTable DeleteTraining(int TrainingID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  TrainingID ),
            new SqlParameter("@Deleted" , DateTime.Now),
            new SqlParameter("@IsActive",  false )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteTrainingHeirarchy", param);

            return dt;
        }


        public TrainingForm GetTraining_byID(int TrainingID)
        {
            TrainingForm training = new TrainingForm();

            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  TrainingID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTrainingby_ID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    training.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    training.TrainingName = dt.Rows[i]["TrainingName"].ToString();
                    training.TopicID = Convert.ToInt32(dt.Rows[i]["TopicID"]);
                    training.CourseOwnerID = Convert.ToInt32(dt.Rows[i]["CourseOwnerID"]);
                    training.ModeID = Convert.ToInt32(dt.Rows[i]["ModeID"]);
                    training.iLearnURL = dt.Rows[i]["iLearnURL"].ToString();
                    //training.TypeID = Convert.ToInt32(dt.Rows[i]["TrainingTypeID"]);
                    training.CostPer = Convert.ToDecimal(dt.Rows[i]["CostPerHead"]);
                    training.Days = Convert.ToInt32(dt.Rows[i]["Days"]);
                    training.CreatedBy = Convert.ToInt32(dt.Rows[i]["CreatedBy"]);
                    training.Description = dt.Rows[i]["Description"].ToString();
                    training.IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"]);
                    training.LM_Limit = Convert.ToInt32(dt.Rows[i]["LM_Limit"]);
                }

            }
            else
            {
                training = null;
            }

            return training;
        }

        public string[] GetTrainingAttachments_byID(int TrainingID)
        {
            //List<vt_TopicTypes> TopicDepartList = new List<vt_TopicTypes>();
            string[] AttachmentIDs;

            SqlParameter[] param = {
            new SqlParameter("@ID",  TrainingID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTrainingAttachments", param);

            if (dt.Rows.Count > 0)
            {
                AttachmentIDs = new string[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var filepath = dt.Rows[i]["Path"].ToString();
                    string[] filepathArray = filepath.Split('\\');
                    AttachmentIDs[i] = filepathArray[filepathArray.Count() - 1];
                }

            }
            else
            {
                AttachmentIDs = null;
            }

            return AttachmentIDs;
        }

        public int[] GetTrainingCourseMaterial_byID(int TrainingID)
        {
            //List<vt_TopicTypes> TopicDepartList = new List<vt_TopicTypes>();
            int[] CourseMaterialIDs;

            SqlParameter[] param = {
            new SqlParameter("@ID",  TrainingID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTrainingCourseMaterial", param);

            if (dt.Rows.Count > 0)
            {
                CourseMaterialIDs = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CourseMaterialIDs[i] = Convert.ToInt32(dt.Rows[i]["CourseMaterialID"].ToString());
                }

            }
            else
            {
                CourseMaterialIDs = null;
            }

            return CourseMaterialIDs;

        }

        public DataTable DeleteAttachments_byID(int TrainingID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  TrainingID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteAll_TrainingAttachments", param);

            return dt;
        }

        public DataTable DeleteCourseOwner_byID(int TrainingID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  TrainingID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteAll_TrainingCourseOwners", param);

            return dt;
        }

        public List<vt_Roles> GetAllRoles()
        {
            List<vt_Roles> RoleList = new List<vt_Roles>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllRoles");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Roles role = new vt_Roles();
                    role.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    role.RoleName = dt.Rows[i]["RoleName"].ToString();

                    RoleList.Add(role);
                }

            }
            else
            {
                RoleList = null;
            }

            return RoleList;
        }

        public List<vt_Departments> GetAllDepartments()
        {
            List<vt_Departments> DepartmentList = new List<vt_Departments>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllDeparts");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Departments department = new vt_Departments();
                    department.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    department.DepartmentName = dt.Rows[i]["DepartmentName"].ToString();

                    DepartmentList.Add(department);
                }

            }
            else
            {
                DepartmentList = null;
            }

            return DepartmentList;
        }

        public List<vt_Geography> GetAllGeography()
        {
            List<vt_Geography> AreaList = new List<vt_Geography>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllAreaGeography");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_Geography area = new vt_Geography();
                    area.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    area.Geography = dt.Rows[i]["Geography"].ToString();

                    AreaList.Add(area);
                }

            }
            else
            {
                AreaList = null;
            }

            return AreaList;
        }


        public HarmonyUser GetHarmonyUser(string loginID)
        {
            HarmonyUser harmonyuser = new HarmonyUser();

            SqlParameter[] param = {
            new SqlParameter("@LoginID",  loginID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetHarmonyUser_byLoginID", param);


            if (dt.Rows.Count > 0)
            {

                harmonyuser.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"]);
                harmonyuser.DepartmentID = Convert.ToInt32(dt.Rows[0]["DepartmentID"]);
                harmonyuser.FullName = dt.Rows[0]["FullName"].ToString();
                harmonyuser.Email= dt.Rows[0]["Email"].ToString();
                harmonyuser.LineManagerName = dt.Rows[0]["LineManagerName"].ToString();
                harmonyuser.Location = dt.Rows[0]["Location"].ToString();
            }
            else
            {
                harmonyuser = null;
            }

            return harmonyuser;
        }


        public DataTable GetAllUsers()
        {
            DataTable dt = new DataTable();

            dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllUsers");
            return dt;

        }


        public DataTable AddUpdateUser(UserForm parameters)
        {
            if (parameters.TypeID == null)
                parameters.TypeID = 0;

            SqlParameter[] param = {
            new SqlParameter("@ID",  parameters.ID ),
            new SqlParameter("@LoginID",  parameters.LoginID ),
            new SqlParameter("@TypeID", parameters.TypeID),
            new SqlParameter("@DepartmentID",  parameters.DepartmentID ),
            new SqlParameter("@EmployeeID",  parameters.EmployeeID ),
            new SqlParameter("@FullName",  parameters.FullName ),
            new SqlParameter("@Location",  parameters.Location ),
            new SqlParameter("@RoleID",  parameters.RoleID ),
            new SqlParameter("@Email",  parameters.Email ),
            new SqlParameter("@IsApproved",  true ),
            new SqlParameter("@IsActive",  parameters.IsActive ),
            new SqlParameter("@Created",  DateTime.Now )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdate_User", param);

            return dt;
        }

        public string GetLastUserID()
        {
            SqlParameter[] param = { };
            var UserID = Entity_Common.get_Scalar(ConfigurationManager.AppSettings["ConnStr"], "sp_GetLastUserID", param);
            return UserID;
        }

        public DataTable AddNewGeographyJunc(int UserID, int GeographyID)
        {
            SqlParameter[] param = {
            new SqlParameter("@UserID",  UserID ),
            new SqlParameter("@GeographyID",  GeographyID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_Insert_UserGeography", param);

            return dt;
        }

        public UserForm GetUser_byID(int UserID)
        {
            UserForm user = new UserForm();

            SqlParameter[] param = {
            new SqlParameter("@UserID",  UserID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUserby_ID", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    user.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    user.RoleID = Convert.ToInt32(dt.Rows[i]["RoleID"]);
                    user.EmployeeID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["EmployeeID"].ToString()) ? dt.Rows[i]["EmployeeID"].ToString() : null);
                    user.LoginID = dt.Rows[i]["LoginID"].ToString();
                    user.DepartmentID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["DepartmentID"].ToString()) ? dt.Rows[i]["DepartmentID"].ToString() : null);
                    user.FullName = dt.Rows[i]["FullName"].ToString();
                    user.LineManagerName = dt.Rows[i]["LineManagerName"].ToString();
                    user.Location = dt.Rows[i]["Location"].ToString();
                    user.Email = dt.Rows[i]["Email"].ToString();
                    user.TypeID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["TypeID"].ToString()) ? dt.Rows[i]["TypeID"].ToString() : null);
                    user.HarmonyID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["TypeID"].ToString()) ? dt.Rows[i]["TypeID"].ToString() : null);
                    user.WorkdayID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["TypeID"].ToString()) ? dt.Rows[i]["TypeID"].ToString() : null);
                    user.IsActive = Convert.ToBoolean(!string.IsNullOrEmpty(dt.Rows[i]["IsActive"].ToString()) ? dt.Rows[i]["IsActive"].ToString() : null);
                }

            }
            else
            {
                user = null;
            }

            return user;
        }

        public int[] GetUserGeography_byID(int UserID)
        {
            int[] GeoIDs;

            SqlParameter[] param = {
            new SqlParameter("@ID",  UserID ),
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUserGeographys", param);

            if (dt.Rows.Count > 0)
            {
                GeoIDs = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    GeoIDs[i] = Convert.ToInt32(dt.Rows[i]["GeographyID"].ToString());
                }

            }
            else
            {
                GeoIDs = null;
            }

            return GeoIDs;

        }

        public DataTable DeleteAllUserGeography(int UserID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  UserID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteAll_UserGeography", param);

            return dt;
        }

        public DataTable DeleteUser(int UserID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  UserID ),
            new SqlParameter("@Deleted" , DateTime.Now)
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeleteUser_byID", param);

            return dt;
        }


        public List<vt_UserProfile> GetAllCourseOwner()
        {
            List<vt_UserProfile> UsersList = new List<vt_UserProfile>();

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAllCourseOwner");

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_UserProfile user = new vt_UserProfile();
                    user.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    user.RoleID = Convert.ToInt32(dt.Rows[i]["RoleID"]);
                    user.EmployeeID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["EmployeeID"].ToString()) ? dt.Rows[i]["EmployeeID"].ToString() : null);
                    user.LoginID = dt.Rows[i]["LoginID"].ToString();
                    user.DepartmentID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["DepartmentID"].ToString()) ? dt.Rows[i]["DepartmentID"].ToString() : null);
                    user.FullName = dt.Rows[i]["FullName"].ToString();
                    user.FirstName = dt.Rows[i]["FirstName"].ToString();
                    user.LastName = dt.Rows[i]["LastName"].ToString();
                    user.UserName = dt.Rows[i]["UserName"].ToString();
                    user.Password = dt.Rows[i]["Password"].ToString();
                    user.Email = dt.Rows[i]["Email"].ToString();
                    user.Phone = dt.Rows[i]["Phone"].ToString();
                    user.TypeID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[i]["TypeID"].ToString()) ? dt.Rows[i]["TypeID"].ToString() : null);
                    user.HamonyID = dt.Rows[i]["HamonyID"].ToString();
                    user.WorkdayID = dt.Rows[i]["WorkdayID"].ToString();
                    user.IsApproved = Convert.ToBoolean(!string.IsNullOrEmpty(dt.Rows[i]["IsApproved"].ToString()) ? dt.Rows[i]["IsApproved"].ToString() : null);
                    user.IsActive = Convert.ToBoolean(!string.IsNullOrEmpty(dt.Rows[i]["IsActive"].ToString()) ? dt.Rows[i]["IsActive"].ToString() : null);
                    user.Created = Convert.ToDateTime(!string.IsNullOrEmpty(dt.Rows[i]["Created"].ToString()) ? dt.Rows[i]["Created"].ToString() : null);

                    UsersList.Add(user);
                }

            }
            else
            {
                UsersList = null;
            }

            return UsersList;

        }


        #region For Upload Heirarchy Excel

        public DataTable CheckDepartmentExsist(string DepartName)
        {
            SqlParameter[] param = {
            new SqlParameter("@DepartName",  DepartName )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CheckDepartmentExsist", param);

            return dt;
        }

        public DataTable AddDepartment(string DepartName)
        {
            SqlParameter[] param = {
            new SqlParameter("@DepartName",  DepartName )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertDepartment", param);

            return dt;
        }

        public DataTable CheckEmployeeExsist(string LoginID)
        {
            SqlParameter[] param = {
            new SqlParameter("@LoginID",  LoginID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_CheckEmployeeExsist", param);

            return dt;
        }
        public DataTable GetUsersByDeptID(int DeptID)
        {
            SqlParameter[] param = {
            new SqlParameter("@DeptID",  DeptID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUsersByDeptID", param);

            return dt;
        }

        public DataTable GetDeptListbyTrID(int TrID)
        {
            SqlParameter[] param = {
            new SqlParameter("@TrainingID",  TrID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDeptListbyTrID", param);

            return dt;
        }
        public List<vt_UserProfile> GetAllTrainer()
        {
            return DBContext.vt_UserProfile.Where(x => x.RoleID == 4 && x.IsActive==true).ToList();
        }
        public DataTable GetSessionbyTrainerID(int trainerId)
        {
            SqlParameter[] param = {
              new SqlParameter("@TrainerID",  trainerId )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTrainerWiseReport", param);

            return dt;
        }
        public DataTable GetCalendarWiseReport(Nullable<DateTime> startdate, Nullable<DateTime> EndDate)
        {
            SqlParameter[] param = {
              new SqlParameter("@Startdate",  startdate ),
               new SqlParameter("@EndDate",  @EndDate )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetCalendarWiseReport", param);

            return dt;
        }
        public DataTable GetTrainingandDeptWiseReport(int TrID,int DeptID)
        {
            SqlParameter[] param = {
              new SqlParameter("@DeptID",  DeptID ),
              new SqlParameter("@TrainingID",  TrID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetTrainingandDeptWiseReport", param);

            return dt;
        }
        
        public DataTable GetParticipantsWiseReport(int EmpID, int DeptID)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = {
                                    new SqlParameter("@EmpID", EmpID),
                                    new SqlParameter("@DeptID", DeptID),
                               };
            dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "Sp_GetParticipantsWiseReportForAdmin", param);

            return dt;
        }

        public void updateEmployee(vt_Heirarchy model)
        {
            DBContext.Entry(model).State = EntityState.Modified;
            DBContext.SaveChanges();

        }
        public void addEmployee(List<vt_Heirarchy> model)
        {
            DBContext.vt_Heirarchy.AddRange(model);
            DBContext.SaveChanges();

        }
        public DataTable AddUpdateEmployee(HarmonyUsers User)
        {
            SqlParameter[] param = {
                new SqlParameter("@ID",  User.ID ),
                new SqlParameter("@EmpID",  User.EmpID ),
                new SqlParameter("@Name",  User.Name ),
                new SqlParameter("@ReporteeID",  User.ReporteeID ),
                new SqlParameter("@ReporteeName",  User.ReporteeName ),
                new SqlParameter("@DepartmentName",  User.DepartmentName ),
                new SqlParameter("@Department",  User.Department ),
                new SqlParameter("@LoginID",  User.LoginID ),
                new SqlParameter("@Email",  User.Email ),
                new SqlParameter("@Grade",  User.Grade ),
                new SqlParameter("@Designation",  User.Designation ),
                new SqlParameter("@Location",  User.Location ),
                new SqlParameter("@OrgUnit",  User.OrgUnit ),
                new SqlParameter("@Division",  User.Division ),
                new SqlParameter("@IsUpdate",  User.IsUpdate ),
                new SqlParameter("@IsActive",  User.IsActive ),

                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertUpdateEmployee", param);

            return dt;
        }


        #endregion


    }
}
