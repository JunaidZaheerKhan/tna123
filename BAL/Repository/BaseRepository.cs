﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BAL.Repository
{
    public class BaseRespoitory
    {
        public vt_TNAEntity DBContext;

        public BaseRespoitory()
        {
            DBContext = new vt_TNAEntity();
        }

        public BaseRespoitory(vt_TNAEntity ContextDB)
        {
            DBContext = ContextDB;
        }

        public void SaveChanges()
        {
            DBContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DBContext != null)
                {
                    DBContext.Dispose();
                    DBContext = null;
                }
            }
        }
        ~BaseRespoitory()
        {
            Dispose(false);
        }
    }
}
