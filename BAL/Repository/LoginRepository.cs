﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.Repository
{
    public class LoginRepository : BaseRespoitory
    {
        public LoginRepository()
            : base()
        { }


        public LoginRepository(vt_TNAEntity ContextDB)
            : base(ContextDB)
        {
            DBContext = ContextDB;
        }
        public DataTable checkDeptID(string UserName)
        {
            SqlParameter[] param = {new SqlParameter("@LoginID",  UserName ),};
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_checkDeptID", param);
            return dt;
        }
        public vt_UserProfile UserExsist(string UserName)
        {
            vt_UserProfile user = new vt_UserProfile();

            SqlParameter[] param = {
            new SqlParameter("@LoginID",  UserName ),
            //new SqlParameter("@Password", Password)
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "CheckalreadyRoleAssigned", param);

            if (dt.Rows.Count > 0)
            {
               
                user.ID = Convert.ToInt32(dt.Rows[0]["ID"]);
                user.RoleID = Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["RoleID"].ToString()) ? dt.Rows[0]["RoleID"].ToString() : null);
                user.EmployeeID = Convert.ToInt32(Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["EmployeeID"].ToString()) ? dt.Rows[0]["EmployeeID"].ToString() : null));
                user.DepartmentID = Convert.ToInt32(Convert.ToInt32(!string.IsNullOrEmpty(dt.Rows[0]["DepartmentID"].ToString()) ? dt.Rows[0]["DepartmentID"].ToString() : null));
                user.LoginID = dt.Rows[0]["LoginID"].ToString();
                user.UserName = dt.Rows[0]["UserName"].ToString();
                user.FullName = dt.Rows[0]["FullName"].ToString();
                user.FirstName = dt.Rows[0]["FirstName"].ToString();
                user.LastName = dt.Rows[0]["LastName"].ToString();
                user.IsActive = Convert.ToBoolean(dt.Rows[0]["IsActive"]);
                user.Location= dt.Rows[0]["Location"].ToString();
                //}

            }
            else
            {
                user = null;
            }

            return user;
        }

        public DataTable LineManagerExsist(string Location,int RoleID)
        {
            SqlParameter[] param = {new SqlParameter("@Location",  Location ), new SqlParameter("@RoleID", RoleID) };
            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_checkSameLocationLM", param);
            return dt;
        }

        public List<vt_RoleMenu> GetMenuList_byRole(int Role)
        {
            List<vt_RoleMenu> menulist = new List<vt_RoleMenu>();

            SqlParameter[] param = {
            new SqlParameter("@RoleID",  Role )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUserMenu_ByRole", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_RoleMenu menu = new vt_RoleMenu();
                    menu.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    menu.RoleID = Convert.ToInt32(dt.Rows[i]["RoleID"]);
                    menu.Menu = dt.Rows[i]["Menu"].ToString();
                    menu.Icon = dt.Rows[i]["Icon"].ToString();
                    menu.URL = dt.Rows[i]["URL"].ToString();
                    menu.MenuOrder = Convert.ToInt32(dt.Rows[i]["MenuOrder"]);
                    menu.Grouped=dt.Rows[i]["Grouped"]!=DBNull.Value?Convert.ToInt32(dt.Rows[i]["Grouped"]):0;

                    menulist.Add(menu);
                }

            }
            else
            {
                menulist = null;
            }

            return menulist;
        }

        public List<vt_RolePages> GetPageList_byRole(int Role)
        {
            List<vt_RolePages> pagelist = new List<vt_RolePages>();

            SqlParameter[] param = {
            new SqlParameter("@RoleID",  Role )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetUserPages_ByRole", param);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    vt_RolePages page = new vt_RolePages();
                    page.ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                    page.RoleID = Convert.ToInt32(dt.Rows[i]["RoleID"]);
                    page.PageName = dt.Rows[i]["PageName"].ToString();
                    page.PageURL = dt.Rows[i]["PageURL"].ToString();

                    pagelist.Add(page);
                }

            }
            else
            {
                pagelist = null;
            }

            return pagelist;
        }

        public DataTable GetActiveTNA()
        {

            var dt = Entity_Common.get_DataTable_WithoutPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetActiveTNA");

            return dt;
        }

        public DataTable DeactivateTNA(int TNA_ID)
        {
            SqlParameter[] param = {
            new SqlParameter("@ID",  TNA_ID )
                               };

            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_DeactivateTNA_byTNAID",param);
            return dt;
        }
    }
}
