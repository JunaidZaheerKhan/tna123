﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for Entity_Common
/// </summary>
public class Entity_Common
{
   
    public Entity_Common()
    {
        
        //
        // TODO: Add constructor logic here
        //
    }

    //public static DataTable get_sp_datatable(vt_TNAEntity context, string sp_name)
    //{
    //    return get_sp_datatable(context, sp_name, null);
    //}
    //public static DataTable get_datatable(vt_TNAEntity context, string sp_name)
    //{
    //    return get_datatable(context, sp_name, null);
    //}
    //public static DataTable get_datatable(vt_TNAEntity context, string query, SqlParameter[] param)
    //{
    //    DataTable dt = new DataTable();
    //    var conn = context.Database.Connection;
    //    var connectionstate = conn.State;
    //    try
    //    {

    //        {
    //            if (connectionstate != ConnectionState.Open)
    //                conn.Open();
    //            using (var cmd = conn.CreateCommand())
    //            {
    //                cmd.CommandText = query;
    //                cmd.CommandType = CommandType.Text;

    //                if (param != null)
    //                    foreach (var item in param)
    //                    {
    //                        cmd.Parameters.Add(item);
    //                    }

    //                using (var reader = cmd.ExecuteReader())
    //                {
    //                    dt.Load(reader);
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        if (connectionstate != ConnectionState.Open)
    //            conn.Close();
    //    }
    //    return dt;
    //}
    //public static DataTable get_sp_datatable(vt_TNAEntity context, string sp_name, SqlParameter[] param)
    //{
    //    DataTable dt = new DataTable();
    //    var conn = context.Database.Connection;
    //    var connectionstate = conn.State;
    //    try
    //    {

    //        {
    //            if (connectionstate != ConnectionState.Open)
    //                conn.Open();
    //            using (var cmd = conn.CreateCommand())
    //            {
    //                cmd.CommandText = sp_name;
    //                cmd.CommandType = CommandType.StoredProcedure;

    //                if (param != null)
    //                    foreach (var item in param)
    //                    {
    //                        cmd.Parameters.Add(item);
    //                    }

    //                using (var reader = cmd.ExecuteReader())
    //                {
    //                    dt.Load(reader);
    //                }
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //    finally
    //    {
    //        if (connectionstate != ConnectionState.Open)
    //            conn.Close();
    //    }
    //    return dt;
    //}

    //public static datatable get_sp_datatable(string connstr, string sp_name)
    //{

    //    sqlconnection con = new sqlconnection(connstr);
    //    return get_sp_datatable(con, sp_name, null);
    //}
    //public static datatable get_datatable(string connstr, string sp_name)
    //{
    //    sqlconnection con = new sqlconnection(connstr);
    //    return get_datatable(con, sp_name, null);
    //}

   
    public static DataTable get_DataTable_WithPara(string connstr, string sp_name, SqlParameter[] param)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = new SqlConnection(connstr);
        //var conn = context;
        var connectionState = conn.State;
        try
        {

            {
                if (connectionState != ConnectionState.Open)
                    conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sp_name;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (param != null)
                        foreach (var item in param)
                        {
                            cmd.Parameters.Add(item);
                        }

                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connectionState != ConnectionState.Open)
                conn.Close();
        }
        return dt;
    }
    public static DataTable get_DataTable_WithoutPara(string connstr, string sp_Name)
    {
        DataTable dt = new DataTable();
        SqlConnection conn = new SqlConnection(connstr);
        //var conn = context;
        var connectionState = conn.State;
        try
        {

            {
                if (connectionState != ConnectionState.Open)
                    conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sp_Name;
                    cmd.CommandType = CommandType.StoredProcedure;

                    //if (param != null)
                    //    foreach (var item in param)
                    //    {
                    //        cmd.Parameters.Add(item);
                    //    }

                    using (var reader = cmd.ExecuteReader())
                    {
                        dt.Load(reader);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connectionState != ConnectionState.Open)
                conn.Close();
        }
        return dt;
    }

    public static string get_Scalar(string connstr, string sp_Name, SqlParameter[] param)
    {
        object Value = null;
        SqlConnection conn = new SqlConnection(connstr);
        //var conn = context.Database.Connection;
        var connectionState = conn.State;
        try
        {

            {
                if (connectionState != ConnectionState.Open)
                    conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sp_Name;
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (param != null)
                        foreach (var item in param)
                        {
                            cmd.Parameters.Add(item);
                        }

                    Value = cmd.ExecuteScalar();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connectionState != ConnectionState.Open)
                conn.Close();
        }
        return Value.ToString();
    }

    public static bool get_Scalar(string connstr, string sp_Name)
    {
        object Value = null;
        SqlConnection conn = new SqlConnection(connstr);
        //var conn = context.Database.Connection;
        var connectionState = conn.State;
        try
        {

            {
                if (connectionState != ConnectionState.Open)
                    conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sp_Name;
                    cmd.CommandType = CommandType.StoredProcedure;                   

                    Value = cmd.ExecuteScalar();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connectionState != ConnectionState.Open)
                conn.Close();
        }
        return Convert.ToBoolean(Value);
    }

    public static void ExecuteSql(vt_TNAEntity context, string sql)
    {


        var conn = context.Database.Connection;
        var connectionState = conn.State;
        try
        {

            {
                if (connectionState != ConnectionState.Open)
                    conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = sql;
                    cmd.ExecuteReader();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connectionState != ConnectionState.Open)
                conn.Close();
        }
    }
    public static void ExecuteSql(SqlConnection context, string Command, SqlParameter[] param)
    {


        var conn = context;
        var connectionState = conn.State;
        try
        {

            {
                if (connectionState != ConnectionState.Open)
                    conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = Command;
                    cmd.CommandTimeout = 60;
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (param != null)
                        foreach (var item in param)
                        {
                            cmd.Parameters.Add(item);
                        }
                    cmd.ExecuteNonQuery();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (connectionState != ConnectionState.Open)
                conn.Close();
        }
    }


}