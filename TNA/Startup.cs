﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TNA.Startup))]
namespace TNA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
