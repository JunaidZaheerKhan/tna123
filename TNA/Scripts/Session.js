﻿//Common Method for Binding DevExtreme DropDown
function getTNAEndDate() {
    var URL = "";
    if ($('#tna_prefix').val() == "") {
        URL = "/TrainingLead/getTNAEndDate";
    }
    else {
        URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetTNAEndDate";
    }
    $.ajax({
       
        url: URL,
        type: "GET",
        data: {TNAID: $("#TNAID").val()},
        success: function (response) {
            debugger
            if (response.Status) {
            
                var JsonResult = JSON.parse(response.data);
                $('#TNA_StartTime').val(moment(JsonResult[0].StartDate).format('DD/MM/YYYY'));
                $('#TNA_EndTime').val(moment(JsonResult[0].EndDate).format('DD/MM/YYYY'));
                $("#dxStartDate").dxDateBox('instance').option('max', new Date(JsonResult[0].EndDate));
                $("#dxStartDate").dxDateBox('instance').option('min', new Date(JsonResult[0].StartDate));
                $("#dxEndDate").dxDateBox('instance').option('min', new Date(JsonResult[0].StartDate));
                $("#dxEndDate").dxDateBox('instance').option('max', new Date(JsonResult[0].EndDate));
            }
            
        },
        error: function (xhr) {
            console.log(xhr.status + ' : ' + xhr.statusText);
        }
    });
}

function BinddxSelectBox(id, data, selectedValue) {
    //debugger
    var selectValue = "";

    if ((selectedValue != null || selectedValue != "") && data != undefined && data.length > 0) {
        selectValue = selectedValue;
    }
    else if ((selectedValue == null || selectedValue == "") && data != undefined && data.length > 0) {
        selectValue = null;
    }

    $("#" + id).dxSelectBox({
        dataSource: data,
        value: selectValue,
        displayExpr: "Value",
        valueExpr: "ID",
        disabled: false,       
        onValueChanged: function (data) {
            debugger
            var controlID = data.element[0].id;

            switch (controlID) {
                case "TNAList":

                    if (data.value != "") {

                        $("#TNAID").val(data.value);
                        getAllTrainings(null);
                        getTNAEndDate();
                    }
                    break;
                case "TrainingList":

                    if (data.value != "") {
                       
                        $("#TrainingID").val(data.value);
                    }
                    
                    break;
                case "TypeList":
                    debugger
                    if (data.value != "") {

                        $("#SessionType").val(data.value);
                        if (data.value == 1) {
                            DisableSelectBox("TrainerList", false);
                            $("#TrainerList").find(".dx-texteditor-input").attr('validate', 'SessionForm');
                            $("#TrainerList").find(".dx-texteditor-input").attr('require', '*Please Select Trainers.');
                           

                            $("#TrainerName").attr("disabled", true);
                            $("#TrainerName").removeAttr('validate');
                            $("#TrainerName").removeAttr('require');
                             
                            DisableSelectBox("Co_ordinator", true);
                            $("#Co_ordinator").find(".dx-texteditor-input").removeAttr('validate');
                            $("#Co_ordinator").find(".dx-texteditor-input").removeAttr('require');

                            $("#Co_ordinator").dxSelectBox("instance").reset();
                            $("#TrainerName").val("");

                        } else {
                            DisableSelectBox("Co_ordinator", false);
                            $("#Co_ordinator").find(".dx-texteditor-input").attr('validate', 'SessionForm');
                            $("#Co_ordinator").find(".dx-texteditor-input").attr('require', '*Please Select Co-ordinator.');

                            $("#TrainerName").attr("disabled", false);
                            $("#TrainerName").attr('validate', 'SessionForm');
                            $("#TrainerName").attr('require', '*Please Enter Trainer Name.');

                            DisableSelectBox("TrainerList", true);
                            $("#TrainerList").find(".dx-texteditor-input").removeAttr('validate');
                            $("#TrainerList").find(".dx-texteditor-input").removeAttr('require');
                           
                            $("#TrainerList").dxSelectBox("instance").reset();
                        }
                    }
                    break;
                case "GeographyList":

                    if (data.value != "") {
                        $("#GeographicID").val(data.value);
                    }
                    break
                case "TrainerList":

                    if (data.value != "") {
                        $("#TrainerID").val(data.value);
                    }
                    //var ins = $("#TrainerList").dxSelectBox("instance");
                    //ins.option("disabled", "true");
                    break
                    
                    case "Co_ordinator":

                    if (data.value != "") {

                        $("#TrainerID").val(data.value);
                    }
                    //var ins = $("#TrainerList").dxSelectBox("instance");
                    //ins.option("disabled", "true");
                    break
            }

           
        }
    });
}


//Common Method for Enable/Disable DevExtreme DropDown
function DisableSelectBox(id, disableValue) {
    $("#" + id).dxSelectBox('instance').option('disabled', disableValue);
}



//Selected Value of Select Box
//getSelectBoxSelectedValue("example")
function getSelectBoxSelectedValue(ControlID) {
    var value = $("#" + ControlID).dxSelectBox("instance").option("value");
    return value;
}

//Selected Value of Select Box
//getSelectBoxSelectedText("example")
function getSelectBoxSelectedText(ControlID) {
    var value = $("#" + ControlID).dxSelectBox("instance").option("text");
    return value;
}




function getAllTNAs(selected) {

    try {
        //debugger

        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllActiveTNAs_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllActiveTNAs_Select";
        }

        $.ajax({
            //url: "/TrainingLead/GetAllActiveTNAs_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        BinddxSelectBox("TNAList", response.data, selected);
                    else
                        BinddxSelectBox("TNAList", response.data, "");

                }
                else {
                    AlertToast('warning', response.Message);
                    BinddxSelectBox("TNAList", response.data, null);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}

function getAllTNAs_Edit(selected) {

    //debugger
    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllTNAs_SelectEdit";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllTNAs_SelectEdit";
        }

        $.ajax({
            //url: "/TrainingLead/GetAllActiveTNAs_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        BinddxSelectBox("TNAList", response.data, selected);
                    else
                        BinddxSelectBox("TNAList", response.data, "");

                }
                else {
                    AlertToast('warning', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}

function getAllTrainings_Edit(selected) {

    //debugger

    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllTrainings_SelectEdit";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllTrainings_SelectEdit";
        }


        $.ajax({
            //url: "/TrainingLead/GetAllActiveTrainings_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        BinddxSelectBox("TrainingList", response.data, selected);
                    else
                        BinddxSelectBox("TrainingList", response.data, "");

                }
                else {
                    AlertToast('error', response.Message);
                   // BinddxSelectBox("TrainingList", null, null);
                   // DisableSelectBox("TrainingList", true);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }
}

function getAllTrainings(selected) {

    try {
        //debugger

        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllActiveTrainings_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllActiveTrainings_Select";
        }

        if ($("#TNAID").val() > 0) {

            $.ajax({
                //url: "/TrainingLead/GetAllActiveTrainings_Select",
                url: URL,
                type: "GET",
                data: {
                    ID: $("#TNAID").val()
                },
                success: function (response) {
                    //debugger
                    if (response.Status) {
                        if (selected != "" || selected != null)
                            BinddxSelectBox("TrainingList", response.data, selected);
                        else
                            BinddxSelectBox("TrainingList", response.data, "");

                    }
                    else {
                        AlertToast('error', response.Message);
                        var emptydatasource = [];
                        BinddxSelectBox("TrainingList", emptydatasource, null);
                        DisableSelectBox("TrainingList", true);
                    }
                },
                error: function (xhr) {
                    console.log(xhr.status + ' : ' + xhr.statusText);
                }
            });
        }
        else {
            AlertToast('error', 'Please select TNA first');
        }
    }
    catch (err) {
        console.log(err.message);
    }



}


function getAllTrainers(selected) {

    try {
        //debugger

        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllTrainers_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllTrainers_Select";
        }

        $.ajax({
            //url: "/TrainingLead/GetAllTrainers_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                debugger
                if (response.Status) {
                    if (selected != null) {
                        BinddxSelectBox("TrainerList", response.data, selected);
                        if ($("#TypeID").val() == 2) {
                            DisableSelectBox("TrainerList", true);
                        }
                    }
                   
                    else {
                        BinddxSelectBox("TrainerList", response.data, "");
                        DisableSelectBox("TrainerList", true);
                    }
                        


                    
                }
                else {
                    AlertToast('error', response.Message);
                    BinddxSelectBox("TrainerList", null, null);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}

function getAllCo_Ordinator(selected) {

    try {
        //debugger

        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllCo_Ordinator_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllCo_Ordinator_Select";
        }

        $.ajax({
            //url: "/TrainingLead/GetAllTrainers_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                debugger
                if (response.Status) {
                    if (selected != null) {
                        BinddxSelectBox("Co_ordinator", response.data, selected);
                        if ($("#TypeID").val() == 1) {
                            DisableSelectBox("Co_ordinator", true);
                        }
                    }
                   
                    else {
                        BinddxSelectBox("Co_ordinator", response.data, "");
                            DisableSelectBox("Co_ordinator", true);
                       
                       
                    }
                        


                   
                }
                else {
                    AlertToast('error', response.Message);
                    BinddxSelectBox("Co_ordinator", null, null);
                }
            },
            error: function (xhr) {
               
            }
        });
    }
    catch (err) {
      
    }

}

function getAllGeography(selected) {

    try {
        //debugger

        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/TrainingLead/GetAllAreaGeography_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/TrainingLead/GetAllAreaGeography_Select";
        }

        $.ajax({
            //url: "/TrainingLead/GetAllAreaGeography_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != null)
                        BinddxSelectBox("GeographyList", response.data, selected);
                    else
                        BinddxSelectBox("GeographyList", response.data, "");

                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}
function ValidateFields(Start, End, Feedback) {
    debugger
    var _ID = $('#ID').val();

    var _Start = new Date(Start);
    var _End = new Date(End);
    var _Feedback= new Date(Feedback);

    var isvalid = false;
    if (_End < _Start) {
        isvalid = false;
    }
    else {
        if (_Feedback < _End || _Feedback < _Start) {
            isvalid = false;
        }

        else {
            isvalid = true;
        }
    }
    if (isvalid == false) {
        if (_ID>0) {
            $('#btnUpdate').attr("disabled", true);
            $('#btnUpdate').css("cursor", "not-allowed");
        }
        else {
            $('#btnSave').attr("disabled", true);
            $('#btnSave').css("cursor", "not-allowed");
        }
       
    }

    else {
        if (_ID > 0) {
            $('#btnUpdate').attr("disabled", false);
            $('#btnUpdate').css("cursor", "pointer");
        }
        else {
            $('#btnSave').attr("disabled", false);
            $('#btnSave').css("cursor", "pointer");
        }
        
    }
}
function dxStartDatePicker(date,min,max) {
    try {
        $("#dxStartDate").dxDateBox({
            type: "date",
            pickerType: "calendar",
            showClearButton: false,
            value: date,
            min: new Date(min),
            max: new Date(max),
            displayFormat: "dd/MM/yyyy",
            onValueChanged: function (data) {
                debugger
                var dt = data.value;
                var end = $('#dxEndDate').dxDateBox('instance')._options.value;
                var feedback = $('#dxFeedbackDate').dxDateBox('instance')._options.value;
                ValidateFields(dt, end, feedback);
                var endDate = moment(dt).format('MM/DD/YYYY');
                dt = moment(dt).format('DD/MM/YYYY');

                $('#StartDate').val(dt);
                $("#dxEndDate").dxDateBox('instance').option('min', new Date(endDate));
              //  $("#dxFeedbackDate").dxDateBox('instance').option('min', new Date(endDate).setDate(new Date(endDate).getDate() + 2));
            },
            onContentReady: function () {
                $('#dxStartDate').find('.dx-texteditor-input').attr('readonly', true);

            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}

function dxStartTimePicker(now) {
    try {
        $("#dxStartTime").dxDateBox({
            type: "time",            
                       
            onValueChanged: function (data) {
                debugger
                var dt = data.value;
                //var feedbackDate = moment(dt).format('MM/DD/YYYY');
                //dt = moment(dt).format('DD/MM/YYYY');
                var a = $("#dxEndTime").dxDateBox('instance');
                //var endTime = moment(a._options.value).format('LT');
                var endTime = moment(a._options.value).format();
                //var startTime = moment(dt).format('LT');
                  var startTime = moment(dt).format();
                if (startTime < endTime) {
                    
                }
                else {
                    AlertToast('error', 'End Time Must be greater than Start Time.');
                }
                $('#StartTime').val(startTime);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }
}
function dxEndTimePicker(now, start) {
    try {
        $("#dxEndTime").dxDateBox({
            type: "time",           
           
            onValueChanged: function (data) {
                var dt = data.value;
                debugger
                //var feedbackDate = moment(dt).format('MM/DD/YYYY');
                //dt = moment(dt).format('DD/MM/YYYY');
                var a = $("#dxStartTime").dxDateBox('instance');
                //var startTime = moment(a._options.value).format('LT');
                //var endTime = moment(dt).format('LT');
                var startTime = moment(a._options.value).format();
                var endTime = moment(dt).format();
                if (startTime < endTime) {
                   
                }
                else {

                    AlertToast('error', 'End Time Must be less than Start Time.');
                }
                $('#EndTime').val(endTime);
            }
        });
    }
    catch (err) {
        console.log(err.message);
        }
}


function dxStartTimePicker_Edit(now) {
    try {
        $("#dxStartTime").dxDateBox({
            type: "time",
            value: now,
            onValueChanged: function (data) {
                debugger
                var dt = data.value;
                //var feedbackDate = moment(dt).format('MM/DD/YYYY');
                //dt = moment(dt).format('DD/MM/YYYY');
                var a = $("#dxEndTime").dxDateBox('instance');
                //var endTime = moment(a._options.value).format('LT');
                var endTime = moment(a._options.value).format();
                //var startTime = moment(dt).format('LT');
                var startTime = moment(dt).format();
                if (startTime < endTime) {

                }
                else {
                    AlertToast('error', 'End Time Must be greater than Start Time.');
                }
                $('#StartTime').val(startTime);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }
}
function dxEndTimePicker_Edit(now) {
    try {
        $("#dxEndTime").dxDateBox({
            type: "time",
            value: now,
            onValueChanged: function (data) {
                var dt = data.value;
                debugger
                //var feedbackDate = moment(dt).format('MM/DD/YYYY');
                //dt = moment(dt).format('DD/MM/YYYY');
                var a = $("#dxStartTime").dxDateBox('instance');
                //var startTime = moment(a._options.value).format('LT');
                var startTime = moment(a._options.value).format();
                //var endTime = moment(dt).format('LT');
                var endTime = moment(dt).format();
                if (startTime < endTime) {

                }
                else {

                    AlertToast('error', 'End Time Must be greater than Start Time.');
                }
                $('#EndTime').val(endTime);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }
}

function dxEndDatePicker(date,min,max) {
    try {
        $("#dxEndDate").dxDateBox({
            type: "date",
            pickerType: "calendar",
            showClearButton: false,
            value: date,
            min: new Date(min),
            max: new Date(max),
            displayFormat: "dd/MM/yyyy",
            onValueChanged: function (data) {
                var dt = data.value;
                var start = $('#dxStartDate').dxDateBox('instance')._options.value;
                var feedback = $('#dxFeedbackDate').dxDateBox('instance')._options.value;
                ValidateFields(start, dt, feedback);
                var feedbackDate = moment(dt).format('MM/DD/YYYY');
                dt = moment(dt).format('DD/MM/YYYY');
                $('#EndDate').val(dt); 
                $("#dxFeedbackDate").dxDateBox('instance').option('min', new Date(feedbackDate).setDate(new Date(feedbackDate).getDate() + 1));
               // $('#dxFeedbackDate').find('.dx-texteditor-buttons-container').css({ "display": "block" });
            },
            onContentReady: function () {
                $('#dxEndDate').find('.dx-texteditor-input').attr('readonly', true);
               
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}

function dxFeedbackDatePicker(date,min) {
    try {
        $("#dxFeedbackDate").dxDateBox({
            type: "date",
            pickerType: "calendar",
            showClearButton: false,
            value: date,
            min: new Date(min),
            //max: new Date(max),
            displayFormat: "dd/MM/yyyy",
            onValueChanged: function (data) {
                var dt = data.value;
                var start = $('#dxStartDate').dxDateBox('instance')._options.value;
                var end = $('#dxEndDate').dxDateBox('instance')._options.value;
                ValidateFields(start, end, dt);
                
                dt = moment(dt).format('DD/MM/YYYY');
                $('#FeedbackDate').val(dt);
            },
            onContentReady: function () {
                $('#dxFeedbackDate').find('.dx-texteditor-input').attr('readonly', true);
               // $('#dxFeedbackDate').find('.dx-texteditor-buttons-container').css({ "display": "none" });
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

}

function CheckBoxes(id) {
    switch (id) {
        case "multimedia":
            if ($("#Multimedia").val() == "True") {
                $('#' + id + '').prop('checked', true);
            }
            else {
                $('#' + id + '').prop('checked', false);
            }
            break;

        case "stationary":
            if ($("#Stationary").val() == "True") {
                $('#' + id + '').prop('checked', true);
            }
            else {
                $('#' + id + '').prop('checked', false);
            }
            break;
        case "hall":
            if ($("#Hall").val() == "True") {
                $('#' + id + '').prop('checked', true);
            }
            else {
                $('#' + id + '').prop('checked', false);
            }
            break;

        default:
            break;

    }
}