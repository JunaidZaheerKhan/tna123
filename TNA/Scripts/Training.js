﻿//Common Method for Binding DevExtreme DropDown
function BinddxDropdown(id, data, selectedValue) {
    debugger
    var selectValue = "";

    if (selectedValue != "" && data != undefined && data.length > 0) {
        selectValue = selectedValue;
    }
    else if ((selectedValue == null || selectedValue == "") && data != undefined && data.length > 0)
    {
        selectValue = null;
    }

    $("#" + id).dxSelectBox({
        items: data,
        value: selectValue,
        displayExpr: "Value",
        valueExpr: "ID",
        disabled: false,
        onValueChanged: function (data) {

            var controlID = data.element[0].id;

            switch (controlID) {
                case "TopicsList":

                    if (data.value != "") {

                        $("#TopicID").val(data.value);
                    }
                    break;
                case "TypeList":

                    if (data.value != "") {

                        $("#TypeID").val(data.value);
                    }
                    break;
                case "CourseOwnerList":

                    if (data.value != "") {

                        $("#CourseOwnerID").val(data.value);
                    }
                    break;
                case "ModeList":

                    if (data.value != "") {

                        $("#ModeID").val(data.value);
                    }
                    break;
            }

            console.log(data.value);
        }
    });
}

//DropDown MultiSelect DevExtreme
//START
function DDMulti_CourseMaterial(dataSource, selectedValues) {

    var makeAsyncDataSource = function () {
        return new DevExpress.data.CustomStore({
            loadMode: "raw",
            key: "ID",
            load: function () {
                return dataSource;
            }
        });
    };

    $("#CourseMaterialList").dxDropDownBox({
        value: selectedValues,
        valueExpr: "ID",
        deferRendering: false,
        placeholder: "Select a value...",
        showClearButton: true,
        //cssclass: "form-control frm-cstm",
        displayExpr: "Value",
        //displayExpr: function (item) {
        //    return item.kol.trim();
        //},
        showClearButton: true,
        dataSource: makeAsyncDataSource(),
        contentTemplate: function (e) {
            var value = e.component.option("value"),
                $dataGrid = $("<div>").dxDataGrid({
                    dataSource: e.component.option("dataSource"),
                    columns: ["Value"],
                    filterRow: { visible: true },
                    scrolling: { mode: "infinite" },
                    selection: { mode: "multiple" },
                    selectedRowKeys: value,
                    height: "100%",
                    onSelectionChanged: function (selectedItems) {
                        var keys = selectedItems.selectedRowKeys;
                        BindCourseMaterial = keys;
                        e.component.option("value", keys);
                    }
                });

            dataGrid = $dataGrid.dxDataGrid("instance");

            e.component.on("valueChanged", function (args) {
                var value = args.value;
                dataGrid.selectRows(value, false);
            });

            return $dataGrid;
        }
    });
}
//END


//Common Method for Enable/Disable DevExtreme DropDown
function DisableDropDown(id, disableValue) {
    $("#" + id).dxSelectBox('instance').option('disabled',disableValue);
}



//Selected Value of Select Box
//getSelectBoxSelectedValue("example")
function getSelectBoxSelectedValue(ControlID) {
    var value = $("#" + ControlID).dxSelectBox("instance").option("value");
    return value;
}

//Selected Value of Select Box
//getSelectBoxSelectedText("example")
function getSelectBoxSelectedText(ControlID) {
    var value = $("#" + ControlID).dxSelectBox("instance").option("text");
    return value;
}

function getAllTopics(selected)
{
    try {
     //debugger
    var URL = "";
    if ($('#tna_prefix').val() == "") {
        URL = "/Admin/GetAllTopics_Select";
    }
    else {
        URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllTopics_Select";
    }

        $.ajax({
            //url: "/Admin/GetAllTopics_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        BinddxDropdown("TopicsList", response.data, selected);
                    else
                        BinddxDropdown("TopicsList", response.data, "");

                }
                else {
                    BinddxDropdown("TopicsList", null, "");
                    //AlertToast('error', response.Message);
                }
            },
            error: function (xhr)
            {
	            console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err)
    {
    console.log(err.message);
    }
   
}


function getAllTypes(selected) {

    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllTypes_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllTypes_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllTypes_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        BinddxDropdown("TypeList", response.data, selected);
                    else
                        BinddxDropdown("TypeList", response.data, "");
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

    
}

function getAllCourseOwner(selected) {

    try {
        debugger
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllCourseOwner_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllCourseOwner_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllCourseOwner_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                debugger
                if (response.Status) {
                    if (selected != "" || selected != null) {
                        BinddxDropdown("CourseOwnerList", response.data, selected);
                    }
                    else {
                        BinddxDropdown("CourseOwnerList", response.data, "");
                    }
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

    
}


function getAllModes(selected) {

    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllModes_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllModes_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllModes_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        BinddxDropdown("ModeList", response.data, selected);
                    else
                        BinddxDropdown("ModeList", response.data, "");
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }

    
}

function getAllCourseMaterial(selected) {

    try {
        var URL = "";
    if ($('#tna_prefix').val() == "") {
        URL = "/Admin/GetAllCourseMaterial_Select";
    }
    else {
        URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllCourseMaterial_Select";
    }

    $.ajax({
        //url: "/Admin/GetAllCourseMaterial_Select",
        url: URL,
        type: "GET",
        data: {},
        success: function (response) {
            //debugger
            if (response.Status) {
                if (selected != "" || selected != null)
                    DDMulti_CourseMaterial(response.data, BindCourseMaterial);
                else
                    DDMulti_CourseMaterial(response.data, null);
            }
            else {
                AlertToast('error', response.Message);
            }
        },
        error: function (xhr) {
            console.log(xhr.status + ' : ' + xhr.statusText);
        }
    });
    }
    catch (err)
    {
        console.log(err.message);
    }

    
}

//Funtions For Filling and Removing Value from Jquery Array 

function showSelectedFiles(array) {
    debugger
    $(".file-wrap").html("");

    for (var i = 0; i < array.length; i++) {
        //var html = "<div class='file-child'><label>"+ i +". " + array[i] + "</label><div class='file-close-icon' ><img src='/assets/images/close-icon.png' /></div></div>";
     var html = "<div class='file-child'><label>" + i + ". " + array[i] + "</label><div class='file-close-icon' ><img src='/TNA/assets/images/close-icon.png' /></div></div>";
        $(".file-wrap").append(html);
    }

    $(".file-child").on("click", ".file-close-icon", function () {

        debugger
        $(this).closest(".file-child").remove(); //Removing "div" Uploaded Files 
        var filename = $(this).parent().find("label").html(); //filename which need to be remove from BindAttachments[]
        removeArrayValue(filename);
    });


}

function ValueExsist(value, arr) {
    var status = false;

    for (var i = 0; i < arr.length; i++) {
        var name = arr[i];
        if (name == value) {
            status = true;
            break;
        }
    }
    return status;
}

function removeArrayValue(filename) {
    debugger
    if (filename == undefined || filename == "") {
    }
    else {
        BindAttachments.splice($.inArray(filename.split(' ')[1], BindAttachments), 1);
    }
}

//$(".file-child").on("click", ".file-close-icon", function () {

//    debugger
//    $(this).closest(".file-child").remove();
//    var filename = $(this).parent().find("label").html();
//    removeArrayValue(filename);
//});