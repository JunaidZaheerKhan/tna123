﻿var BindKOL = [];



function MultiSelectDropDown(id,dataSource,SelectedValues,keyName,valueExp,displayExp)
{

    var makeAsyncDataSource = function () {
        return new DevExpress.data.CustomStore({
            loadMode: "raw",
            key: "" + keyName + "",
            load: function () {
                return data.kol;
            }
        });
    };


    $("#"+ id +"").dxDropDownBox({
        value: SelectedValues,
        valueExpr: "" + valueExp + "",
        deferRendering: false,
        placeholder: "Select a value...",
        showClearButton: true,
        cssclass: "form-control frm-cstm",
        displayExpr: "" + displayExp + "",
        //displayExpr: function (item) {
        //    return item.kol.trim();
        //},
        showClearButton: true,
        dataSource: makeAsyncDataSource(),
        contentTemplate: function (e) {
            var value = e.component.option("value"),
                $dataGrid = $("<div>").dxDataGrid({
                    dataSource: e.component.option("dataSource"),
                    columns: ["" + displayExp + ""],
                    filterRow: { visible: true },
                    scrolling: { mode: "infinite" },
                    selection: { mode: "multiple" },
                    selectedRowKeys: value,
                    height: "100%",
                    onSelectionChanged: function (selectedItems) {
                        var keys = selectedItems.selectedRowKeys;
                        BindKOL = keys;
                        e.component.option("value", keys);
                    }
                });

            dataGrid = $dataGrid.dxDataGrid("instance");

            e.component.on("valueChanged", function (args) {
                var value = args.value;
                dataGrid.selectRows(value, false);
            });

            return $dataGrid;
        }
    });
}

