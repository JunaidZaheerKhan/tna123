﻿//Common Method for Binding DevExtreme DropDown
function User_BinddxDropdown(id, data, selectedValue) {

    var selectValue = "";

    if (selectedValue != "" && data != undefined && data.length > 0) {
        selectValue = selectedValue;
    }
    else if ((selectedValue == null || selectedValue == "") && data != undefined && data.length > 0) {
        selectValue = null;
    }

    $("#" + id).dxSelectBox({
        items: data,
        value: selectValue,
        displayExpr: "Value",
        valueExpr: "ID",
        disabled: false,
        onValueChanged: function (data) {
            debugger
            var controlID = data.element[0].id;

            switch (controlID) {
                case "DepartmentList":

                    if (data.value != "") {

                        $("#DepartmentID").val(data.value);
                    }
                    break;
                case "RoleList":

                    if (data.value != "") {

                        $("#RoleID").val(data.value);

                        if (data.value == "4") {
                            DisableSelectBox("TypeList", false);
                            DisableDropDown("AreaList", false);
                        }
                        else {
                            DisableSelectBox("TypeList", true);
                            DisableDropDown("AreaList", true);
                        }
                    }
                    break;
                case "TypeList":

                    if (data.value != "") {

                        $("#TypeID").val(data.value);
                    }
                    break;
            }

            console.log(data.value);
        }
    });
}

//DropDown MultiSelect DevExtreme
//START
function User_DDMulti_Geography(dataSource, selectedValues) {
    //debugger
    var makeAsyncDataSource = function () {
        return new DevExpress.data.CustomStore({
            loadMode: "raw",
            key: "ID",
            load: function () {
                return dataSource;
            }
        });
    };

    $("#AreaList").dxDropDownBox({
        value: selectedValues,
        valueExpr: "ID",
        deferRendering: false,
        placeholder: "Select a value...",
        showClearButton: true,
        //cssclass: "form-control frm-cstm",
        displayExpr: "Value",
        //displayExpr: function (item) {
        //    return item.kol.trim();
        //},
        showClearButton: true,
        dataSource: makeAsyncDataSource(),
        contentTemplate: function (e) {
            var value = e.component.option("value"),
                $dataGrid = $("<div>").dxDataGrid({
                    dataSource: makeAsyncDataSource(),//e.component.option("dataSource"),
                    columns: ["Value"],
                    filterRow: { visible: true },
                    scrolling: { mode: "infinite" },
                    selection: { mode: "multiple" },
                    selectedRowKeys: value,
                    height: "100%",
                    onSelectionChanged: function (selectedItems) {

                        var keys = selectedItems.selectedRowKeys;
                        BindGeography = keys;
                        e.component.option("value", keys);
                    }
                });

            dataGrid = $dataGrid.dxDataGrid("instance");

            e.component.on("valueChanged", function (args) {
                var value = args.value;
                dataGrid.selectRows(value, false);
            });

            return $dataGrid;
        }
    });
}
//END


//Common Method for Enable/Disable DevExtreme DropDown
function DisableSelectBox(id, disableValue) {
    debugger
    $("#" + id).dxSelectBox('instance').option('disabled', disableValue);
}

//Common Method for Enable/Disable DevExtreme DropDown
function DisableDropDown(id, disableValue) {
    debugger
    $("#" + id).dxDropDownBox('instance').option('disabled', disableValue);
}




//Selected Value of Select Box
//getSelectBoxSelectedValue("example")
function getSelectBoxSelectedValue(ControlID) {
    var value = $("#" + ControlID).dxSelectBox("instance").option("value");
    return value;
}

//Selected Value of Select Box
//getSelectBoxSelectedText("example")
function getSelectBoxSelectedText(ControlID) {
    var value = $("#" + ControlID).dxSelectBox("instance").option("text");
    return value;
}


function getAllRoles(selected) {

    try {
        //debugger
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllRoles_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllRoles_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllRoles_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                    { User_BinddxDropdown("RoleList", response.data, selected); }
                    

                else{User_BinddxDropdown("RoleList", response.data, "");}
                        
                        $('#RoleList .dx-texteditor-input').attr("validate", "UserForm");
                        $('#RoleList .dx-texteditor-input').attr("require", "*Role is required.");  
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }


}

function getAllTypes(selected) {

    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllTypes_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllTypes_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllTypes_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        User_BinddxDropdown("TypeList", response.data, selected);
                    else
                        User_BinddxDropdown("TypeList", response.data, "");
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }



}

function getAllDepartments(selected) {

    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllDepartments_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllDepartments_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllDepartments_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        User_BinddxDropdown("DepartmentList", response.data, selected);
                    else
                        User_BinddxDropdown("DepartmentList", response.data, "");
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }


}

function getAllAreaGeography(selected) {

    try {
        var URL = "";
        if ($('#tna_prefix').val() == "") {
            URL = "/Admin/GetAllAreaGeography_Select";
        }
        else {
            URL = "/" + $('#tna_prefix').val() + "/Admin/GetAllAreaGeography_Select";
        }

        $.ajax({
            //url: "/Admin/GetAllAreaGeography_Select",
            url: URL,
            type: "GET",
            data: {},
            success: function (response) {
                //debugger
                if (response.Status) {
                    if (selected != "" || selected != null)
                        User_DDMulti_Geography(response.data, BindGeography);
                    else
                        User_DDMulti_Geography(response.data, null);
                }
                else {
                    AlertToast('error', response.Message);
                }
            },
            error: function (xhr) {
                console.log(xhr.status + ' : ' + xhr.statusText);
            }
        });
    }
    catch (err) {
        console.log(err.message);
    }


}