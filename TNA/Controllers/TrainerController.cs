﻿using BAL.Repository;
using DAL.DBEntities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static TNA_ViewModel.Model.TNA_VM;

namespace TNA.Controllers
{
    public class TrainerController : BaseController
    {


        TrainerRepository TrainerRepo;

        public TrainerController()
        {
            TrainerRepo = new TrainerRepository(new vt_TNAEntity());
        }


        // GET: Trainer
        public ActionResult Index()
        {
            return View();
        }

        #region Session Assigned Users


        public ActionResult Sessions()
        {
            return View();
        }

        public JsonResult GetAllSessions()
        {
            string jsonSessionList = string.Empty;

            try
            {
                var TrainerID = Convert.ToInt32(CurrentUser.User.ID);
                
                var SessionList = TrainerRepo.GetAllSessions_ByTrainer(TrainerID);

                if (SessionList.Rows.Count > 0)
                {
                    jsonSessionList = JsonConvert.SerializeObject(SessionList);

                    //return Json(jsonSessionList, JsonRequestBehavior.AllowGet);
                    return Json(new { Status = true, Message = "Sessions Found.", DataList = jsonSessionList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("Sessions Not Found.");
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message , DataList = jsonSessionList }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public ActionResult AssignedSessionUsers(int ID)
        {
            return View();
        }

        public JsonResult GetSessionAssignedUsers()
        {
            List<AssignedUserGrid> UserList = new List<AssignedUserGrid>();
            try
            {
                var url_array = Request.UrlReferrer.ToString().Split('/');
                var sessionID = Convert.ToInt32(url_array.LastOrDefault());

                UserList = TrainerRepo.GetAllSessionsAssignedUsers(sessionID);

                if (UserList != null)
                {
                    return Json(new { Status = true, Message = "Users Found.", DataList = UserList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Users Not Found.", DataList = UserList }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, DataList = UserList }, JsonRequestBehavior.AllowGet);
            }


        }

        //public ActionResult SubmitAttendenceFeedback(List<AssignedUserGrid> UsersList)
        [HttpPost]
        public ActionResult SubmitAttendenceFeedback(List<AssignedUserGrid> Data)
        {
            ResponseHeader Response = new ResponseHeader();
            
            try
            {
                
                if (Data.Count > 0)
                {
                    foreach (var item in Data)
                        {
                            var sessionUser = TrainerRepo.UpdateAttendence(item);
                        }

                        Response.Status = true;
                        Response.Message = "Attendance and Feedback Submitted Successfully.";

                        return Json(Response, JsonRequestBehavior.AllowGet);
                    
                }
                else
                {
                    throw new Exception("There are no records form submit.");
                }
                    

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                return Json(Response, JsonRequestBehavior.AllowGet);
                
            }
        }


        public JsonResult CloseSession()
        {
            try
            {
                var url_array = Request.UrlReferrer.ToString().Split('/');
                var sessionID = Convert.ToInt32(url_array.LastOrDefault());


                if (sessionID > 0)
                {
                    var dt = TrainerRepo.CloseSession_ByID(sessionID);

                    return Json(new { Status = true, Message = "Session Closed Successfully." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Session ID cannot be null." }, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }


        }

        #endregion

        public ActionResult TrainingCalendar()
        {
            return View();
        }
        public ActionResult getTrainingCalendar()
        {

            DataTable dt = new DataTable();
            
            try
            {
                var TrainerID = Convert.ToInt32(CurrentUser.User.ID);
                dt = TrainerRepo.GetDataforTrainingCalendar(TrainerID);
                var data = JsonConvert.SerializeObject(dt); 
                if (dt != null)
                {
                    return Json(new { Status = true, Message = "Data Found.", DataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data Not Found.", DataList = data }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, DataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult AddorEditDetailsofSession(SessionForm Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {
                        var Session = TrainerRepo.AddorEditDetailsofSession(Header);
                        Response.Status = true;
                        Response.Message = "Session Details Saved successfully.";
                }
                else
                {
                    var error = ModelState.Select(x => x.Value.Errors)
                          .Where(y => y.Count > 0)
                          .FirstOrDefault();
                    throw new Exception(error.FirstOrDefault().ErrorMessage);
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }
    }
}