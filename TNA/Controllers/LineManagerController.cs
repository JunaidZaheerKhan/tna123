﻿using TNA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static TNA_ViewModel.Model.TNA_VM;
using BAL.Repository;
using DAL.DBEntities;
using System.Data;
using System.IO;
using TNAApp_ViewModel.Model;
using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;
using System.IO;
using Newtonsoft.Json;
using System.Collections;

namespace TNA.Controllers
{
    public class LineManagerController : BaseController
    {

        LineManagerRepository LineManagerRepo;
        public LineManagerController()
        {
            LineManagerRepo = new LineManagerRepository(new vt_TNAEntity());
        }

        public ActionResult ParticipantsInSession() {

            return View();
        }
        public ActionResult GetParticpantsInSessionList() {
            try
            {
            return Json(new {Success=true, data= JsonConvert.SerializeObject(LineManagerRepo.GetParticpantsInSessionList(Convert.ToInt32(CurrentUser.User.DepartmentID), Convert.ToInt32(CurrentUser.User.EmployeeID))) },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
          
        }

        [HttpPost]
        public ActionResult DeleteFromSessionandTraining(Remove_Participants_From_Session model)
        {
            try
            {
                model.LMID = Convert.ToInt32(CurrentUser.User.EmployeeID);
                model.DeptID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                LineManagerRepo.DeleteFromSessionandTraining(model);
                return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        // GET: LineManager
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AssignUser()
        {
            List<GetDepartUsers> getListOfUsers = new List<GetDepartUsers>();
            try
            {
                var get_DepartmentID = CurrentUser.User.DepartmentID.ToString();
                var get_LoginID = Convert.ToInt32(CurrentUser.User.EmployeeID);
                var Location = Convert.ToString(CurrentUser.User.Location);
                var RoleID = Convert.ToInt32(CurrentUser.User.RoleID);
                getListOfUsers = LineManagerRepo.GetLineManagerUsersByLoginID(get_LoginID, get_DepartmentID,Location, RoleID);
                
                

                return View(getListOfUsers);
            }
            catch (Exception ex)
            {
                getListOfUsers = null;
                return View(getListOfUsers);
            }

                       
        }
        
        public JsonResult GetAllTrainingsCurrentUser()
        {
            List<GetAllTrainings_LM> lst_LM = new List<GetAllTrainings_LM>();
            try
            {
                //string departmentName = "CHC Team - North";
                lst_LM = LineManagerRepo.GetTrainingsCurrentUser
                    (
                    LineManagerRepo.GetDepartName_byDepartID(Convert.ToInt32(CurrentUser.User.DepartmentID)),
                    Convert.ToInt32(CurrentUser.User.DepartmentID),
                    Convert.ToInt32(CurrentUser.User.EmployeeID)
                    );
                return Json(new { lst_LM }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                lst_LM = null;
                return Json(new { lst_LM }, JsonRequestBehavior.AllowGet);
            }           
            
        }
        public JsonResult GetTNADeptWise()
        {
            try
            {
                int departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                var TNAList = LineManagerRepo.GetTNADeptWise(departID);
                if (TNAList.Rows.Count > 0)
                {
                    return Json(new { Status = true, Message = "Record found.", TNAList = JsonConvert.SerializeObject(TNAList) }, JsonRequestBehavior.AllowGet);
                }

                else
                {
                    return Json(new { Status = false, Message = "Record not found.", TNAList ="" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {

                return Json(new { Status = false, Message = ex.Message, TNAList = "" }, JsonRequestBehavior.AllowGet);
            }
            
           
        }

        //Search 
        public JsonResult GetSearchTrainings(string TNAName)
        {
            List<GetAllTrainings_LM> lst_LM = new List<GetAllTrainings_LM>();
            try
            {
               
                
                int departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                var departName = LineManagerRepo.GetDepartName_byDepartID(departID);
                //string departmentName = "CHC Team - North";
                lst_LM = LineManagerRepo.GetSearchTrainings(departName, TNAName);
                return Json(new { lst_LM }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                lst_LM = null;
                return Json(new { lst_LM }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult InsertUsersToTraining(Add_or_Remove_Participants obj)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                var departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                obj.Department_Id = departID;
                obj.LM_ID = Convert.ToInt32(CurrentUser.User.EmployeeID);

                Response.Message = LineManagerRepo.InsertUserToTNATrainee(obj);
                Response.Status = true;
              

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = "During process unknown error occur. Please try again.";
            }
            return Json(Response, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult DeleteUserFromTnaTraining(TNATrainee_mdl obj)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                var departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                obj.DepartmentId = departID;
                //int userID = Convert.ToInt32(obj);
                var usersisexistinsess = LineManagerRepo.CheckUserisExistinSession(obj);
                if (usersisexistinsess.Rows.Count>0)
                {
                    if (usersisexistinsess == null || usersisexistinsess.Rows[0]["SessionID"] == DBNull.Value)
                    {
                        var DataInsertion = LineManagerRepo.DeleteUserTNATrainee(obj);
                        Response.Status = true;
                        Response.Message = "User Deleted Successfully.";
                    }

                    else
                    {
                        Response.Status = false;
                        Response.Message = "User could not be deleted because he/she assigned in session.";
                    }
                }

                else
                {
                    Response.Status = false;
                    Response.Message = "User could not be deleted because he/she assigned in session.";
                }





            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = "There is some error please try again.";
            }
            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAssignUsersOnLoad(TNATrainee_mdl obj)
        {
            var departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
            obj.EmpID = Convert.ToInt32(CurrentUser.User.EmployeeID);
            obj.DepartmentId = departID;
            obj.Location= Convert.ToString(CurrentUser.User.Location);
            obj.RoleID = Convert.ToInt32(CurrentUser.User.RoleID);
            var jsondata = LineManagerRepo.GetAssignUsersTrainingOnPageLoad(obj);
            if (jsondata.Rows.Count>0)
            {
                var getData = JsonConvert.SerializeObject(jsondata);
                return Json(new { getData }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<TNATrainee_mdl> list = new List<TNATrainee_mdl>();
                return Json(new { getData= list }, JsonRequestBehavior.AllowGet);
            }
            
                     
        }

        //Report
        public ActionResult ParticipantsWiseReport()
        {
            return View();
        }

        public ActionResult GetEmployeeList()
        {
            System.Data.DataTable getListOfUsers = new System.Data.DataTable();
            try
            {

                var get_DepartmentID = CurrentUser.User.DepartmentID.ToString();
                var get_LoginID = CurrentUser.User.EmployeeID.ToString();
                var roleID = Convert.ToInt32(CurrentUser.User.RoleID);
                var location = Convert.ToString(CurrentUser.User.Location);

                if (get_DepartmentID != null & get_LoginID != null)
                {
                   getListOfUsers = LineManagerRepo.GetUsersByLoginID(get_LoginID, get_DepartmentID,location, roleID);
                }
                if (getListOfUsers.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(getListOfUsers);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record Not Found", data = getListOfUsers }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = getListOfUsers }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetParticipantsWiseReport(int EmpID)
        {
            System.Data.DataTable participantReport = new System.Data.DataTable();
            try
            {
                int get_DepartmentID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                participantReport = LineManagerRepo.GetParticipantsWiseReport(EmpID, get_DepartmentID);
               
                if (participantReport.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(participantReport);
                    return Json(new { Status = true, Message = "Data is available.", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data is not available.", data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //Feedback
        public ActionResult AllClosedSession()
        {
            return View();
        }
        public ActionResult GetClosedSessionList()
        {
            System.Data.DataTable sessionList = new System.Data.DataTable();
            try
            {
                int get_DepartmentID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                sessionList = LineManagerRepo.GetClosedSessionList(get_DepartmentID);
                
                if (sessionList.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(sessionList);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record Not Found", data = sessionList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = sessionList }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Participants(int SessionID,int TNAID, int TrainingID)
        {
            SessionParticipants_mdl model = new SessionParticipants_mdl();
            model.SessionID = SessionID;
            model.TNAID = TNAID;
            model.TrainingID = TrainingID;
            return View(model);
        }

        [HttpPost]
        public ActionResult GetParticipantsList(SessionParticipants_mdl data)
        {
            System.Data.DataTable sessionList = new System.Data.DataTable();
            try
            {
                data.DepartmentID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                sessionList = LineManagerRepo.GetParticipantsList(data);

                if (sessionList.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(sessionList);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record Not Found", data = sessionList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = sessionList }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveFeedback(Feedback model)
        {
            System.Data.DataTable response = new System.Data.DataTable();
            try
            {
                model.DeptID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                response = LineManagerRepo.SaveFeedback(model);
               
                if (response.Rows.Count==0)
                {
                    return Json(new { Status = true, Message = "Success"}, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record Not Found"}, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetFeedbackValue(Feedback model)
        {
            System.Data.DataTable response = new System.Data.DataTable();
            try
            {
                 model.DeptID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                response = LineManagerRepo.GetFeedbackValue(model);

                if (response.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(response);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record Not Found", data = response }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = response }, JsonRequestBehavior.AllowGet);
            }
           
        }

        [HttpGet]
        public ActionResult SubmitSessionFeedback(int ID)
        {
            System.Data.DataTable response = new System.Data.DataTable();
            try
            {
               
                response = LineManagerRepo.SubmitSessionFeedback(ID);

                if (response.Rows.Count == 0)
                {
                    return Json(new { Status = true, Message = "Success submit feedback." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Failed to submit try again." }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}