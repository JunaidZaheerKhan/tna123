﻿using BAL.Repository;
using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using TNA_ViewModel.Model;
using TNAApp_ViewModel.Model;
using static TNA_ViewModel.Model.TNA_VM;

namespace TNA.Controllers
{
    public class LoginController : Controller
    {

        LoginRepository LoginRepo;
        public LoginController()
        {
            LoginRepo = new LoginRepository(new vt_TNAEntity());
        }

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginForm Header)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Header.Status = true;
                    if (Header.Status)
                    {
                        //Valdiate From Active Directory
                        bool IsValid_AD;

                        var UserExist = LoginRepo.UserExsist(Header.UserName);

                        if (UserExist != null)
                        {
                            
                           LoginRepo.checkDeptID(Header.UserName);

                            if (ConfigurationManager.AppSettings["IsLive"].ToString().Contains("1"))
                            {
                                //IsValid_AD = vt_Common.ValidateUser(WebConfigurationManager.AppSettings["ActiveDirectoryPath"].ToString(), Header.UserName, Header.Password, UserExist.LoginID);
                                IsValid_AD = vt_Common.ValidateUser(WebConfigurationManager.AppSettings["ActiveDirectoryPath"].ToString(), WebConfigurationManager.AppSettings["ActiveDirectory_ID"].ToString(), WebConfigurationManager.AppSettings["ActiveDirectory_Pass"].ToString(), UserExist.LoginID);
                            }
                            else
                            {
                                IsValid_AD = true;
                            }

                            if (IsValid_AD)
                            {


                                #region Auto Disable Active TNA


                                AutoDeactivateTNA();

                                #endregion


                                //Check User Exsist in vt_UserProfile
                                //var UserExist = LoginRepo.UserExsist(Header.UserName);

                                if (UserExist != null)
                                {
                                    var Menus = LoginRepo.GetMenuList_byRole(Convert.ToInt32(UserExist.RoleID));

                                    if (Menus != null)
                                    {
                                        TNA_Session session = new TNA_Session();
                                        session.User = UserExist;
                                        session.MenuList = Menus;
                                        session.PageList = LoginRepo.GetPageList_byRole(Convert.ToInt32(UserExist.RoleID));
                                        Session.Add("TNASession", session); //Add Session

                                        var redirectlink = Menus.Where(x => x.MenuOrder == 1).Select(x => x.URL).FirstOrDefault();
                                        string[] link = redirectlink.Split('/');

                                        return RedirectToAction(link[2], link[1]);
                                    }
                                    else
                                    {
                                        throw new Exception("No Menu Found.");
                                    }
                                }
                                else
                                {
                                    throw new Exception("User not Exsist.");
                                }
                            }
                            else
                            {
                                throw new Exception("User not Exist in active Directory.");
                            }
                        }
                        else
                        {
                            throw new Exception("User not Exsist.");
                        }


                    }
                    else
                    {
                        throw new Exception("Incorrect UserName or Password.");
                    }
                }
                else
                {
                    throw new Exception("Fill the Required Fields.");
                }
            }
            catch (Exception ex)
            {
                Header.Status = false;
                Header.Message = ex.Message;
                return View(Header);
            }

        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            Session.Add("TNASession", null);
            
            return View("Index");
        }

        public void AutoDeactivateTNA()
        {
            var ActiveTNA = LoginRepo.GetActiveTNA();

            if (ActiveTNA.Rows.Count > 0)
            {
                for (int i = 0; i < ActiveTNA.Rows.Count; i++)
                {
                    var strd1 = DateTime.Now.Date; //Current DateTime
                    var strd2 = Convert.ToDateTime(ActiveTNA.Rows[i]["LastSubmissionDate"]); //Session Start DateTime


                    if (strd2 < strd1)
                    {
                        //IsActive "false" the TNA
                        int TNAID = Convert.ToInt32(ActiveTNA.Rows[i]["ID"].ToString());
                        var DeactivateTNA = LoginRepo.DeactivateTNA(TNAID);
                    }
                    else
                    { }
                }
               
            }
        }
    }
}