﻿using BAL.Repository;
using DAL.DBEntities;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using TNA_ViewModel.Model;
using TNAApp_ViewModel.Model;
using static TNA_ViewModel.Model.TNA_VM;

namespace TNA.Controllers
{
    public class TrainingLeadController : BaseController
    {
        TrainingLeadRepository TrainingLeadRepo;

        public TrainingLeadController()
        {
            TrainingLeadRepo = new TrainingLeadRepository(new vt_TNAEntity());
        }

        #region Dashboard
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult getTraining()
        {
            try
            {
                var dataList = TrainingLeadRepo.getTraining();

                if (dataList != null)
                {
                    var data = JsonConvert.SerializeObject(dataList);
                    return Json(new { Status = true, Message = "Data found.", dataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", dataList = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, dataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult TrainingCalendar()
        {
            return View();
        }
        public ActionResult getTrainingCalendar()
        {

            System.Data.DataTable dt = new System.Data.DataTable();

            try
            {
                var TrainerID = Convert.ToInt32(CurrentUser.User.ID);
                dt = TrainingLeadRepo.GetDataforTrainingCalendar();
                var data = JsonConvert.SerializeObject(dt);
                if (dt != null)
                {
                    return Json(new { Status = true, Message = "Data found.", DataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", DataList = data }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, DataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetCounterValue()
        {
            try
            {
                var result = TrainingLeadRepo.GetCounterValue();
                if (result.Rows.Count > 0)
                {
                    var data = JsonConvert.SerializeObject(result);
                    return Json(new { Status = true, Message = "Record found.", data = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Null", data = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = "Record not found.", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CurrentMonthTraining()
        {
            try
            {
                var dataList = TrainingLeadRepo.CurrentMonthTraining();

                if (dataList != null)
                {
                    var data = JsonConvert.SerializeObject(dataList);
                    return Json(new { Status = true, Message = "Data found.", dataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", dataList = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, dataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CurrentMonthSession()
        {
            try
            {
                var dataList = TrainingLeadRepo.CurrentMonthSession();

                if (dataList != null)
                {
                    var data = JsonConvert.SerializeObject(dataList);
                    return Json(new { Status = true, Message = "Data found.", dataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", dataList = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, dataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }
       
        #endregion

        #region Add TNA Page
        public ActionResult AddTNA()
        {
            return View(new TNAForm());
        }

        public JsonResult GetAllTrainings()
        {
            //List<TNA_Trainings> TrainingList = new List<TNA_Trainings>();
           // List<vt_Trainings> TrainingList = new List<vt_Trainings>();
            try
            {
               var TrainingList = TrainingLeadRepo.GetAllActiveTrainings();
                var JSON_TrainingList = JsonConvert.SerializeObject(TrainingList);
                //TrainingList = TrainingLeadRepo.GetAllTrainingsWithDepart();

                if (TrainingList != null)
                {
                    return Json(new { Status = true, Message = "Trainings found.", Trainings = JSON_TrainingList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                   
                    return Json(new { Status = false, Message = "No trainings found.", Trainings = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                
                return Json(new { Status = false, Message = ex.Message, Trainings = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //public JsonResult GetAllTrainings_Edit(int ID)
        //{
        //    List<TNA_Trainings> ActiveTrainingList = new List<TNA_Trainings>();
        //    List<TNA_Trainings> SavedTrainingList = new List<TNA_Trainings>(); 
        //    List<TNA_Trainings> FinalDataSource = new List<TNA_Trainings>();

        //    try
        //    {
        //        //TrainingList = TrainingLeadRepo.GetAllTrainings();

        //        ActiveTrainingList = TrainingLeadRepo.GetAllTrainingsWithDepart();
        //        SavedTrainingList = TrainingLeadRepo.GetAllTrainingsWithDeparts_Saved(Convert.ToInt32(ID));

        //        foreach (var activetraining in ActiveTrainingList)
        //        {
        //            foreach (var savedtraining in SavedTrainingList)
        //            {
        //                if (activetraining.TrainingID == savedtraining.TrainingID)
        //                {
        //                    //Make Training Selected
        //                    activetraining.Training_IsSelected = savedtraining.Training_IsSelected;

        //                    if (activetraining.Departs.Count > 0)
        //                    {
        //                        foreach (var activedepart in activetraining.Departs)
        //                        {
        //                            foreach (var savedepart in savedtraining.Departs)
        //                            {
        //                                if (activedepart.DepartmentID == savedepart.DepartmentID)
        //                                {
        //                                    activedepart.Department_IsLocked = savedepart.Department_IsLocked;
        //                                }
        //                            }
        //                        }
        //                    }

        //                }
        //            }
        //        }

        //        FinalDataSource = ActiveTrainingList;

        //        if (FinalDataSource != null)
        //        {
        //            return Json(new { Status = true, Message = "Trainings Found.", Trainings = FinalDataSource }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            FinalDataSource = null;
        //            return Json(new { Status = false, Message = "No Trainings Found.", Trainings = FinalDataSource }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        FinalDataSource = null;
        //        return Json(new { Status = false, Message = ex.Message, Trainings = FinalDataSource }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public JsonResult GetDepartmentByTrainingID(int ID)
        //{
        //    List<GetTrainingDepartList> DepartList = new List<GetTrainingDepartList>();
        //    try
        //    {
        //        DepartList = TrainingLeadRepo.GetDepartsByTrainingID(Convert.ToInt32(ID));

        //        if (DepartList != null)
        //        {
        //            return Json(new { Status = true, Message = "Departments Found.", DataList = DepartList }, JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //        {
        //            DepartList = null;
        //            return Json(new { Status = false, Message = "No Departments Found.", DataList = DepartList }, JsonRequestBehavior.AllowGet);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        DepartList = null;
        //        return Json(new { Status = false, Message = ex.Message, DataList = DepartList }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult CreateTNA(TNAForm Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {
                    var TNA = TrainingLeadRepo.AddTNA(Header);
                    string LastTNAID = TrainingLeadRepo.GetLastTNAID();
                    int ID = Convert.ToInt32(LastTNAID);


                   


                    Response.Status = true;
                    Response.Message = "TNA added successfully.";

                }
                else
                {
                    throw new Exception("Enter valid fields");
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTrainingDepartments(GetTrainingDepartment Header)
        {
            List<TNA_TrainingDepartment> Data = new List<TNA_TrainingDepartment>();
            int[] array = new int[0];
            try
            {
                if (Header.TNAID > 0 && Header.TrainingID > 0)
                {
                    var departs = TrainingLeadRepo.GetDepartsByTrainingID(Convert.ToInt32(Header.TrainingID));

                    if (departs != null)
                    {
                        var selectedDeparts = TrainingLeadRepo.GetDepartsByTNATrainingID(Convert.ToInt32(Header.TNAID), Convert.ToInt32(Header.TrainingID));

                        if (selectedDeparts!=null)
                        {
                            for (int i = 0; i < selectedDeparts.Length; i++)
                            {
                                foreach (var item in departs)
                                {
                                    if (item.DepartmentID == selectedDeparts[i])
                                    {
                                        item.Department_IsLocked = true;
                                    }
                                }
                            }
                        }

                        return Json(new { Status = true, Message = "Department found successfully.", DataSource = departs, Selected = selectedDeparts }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        throw new Exception("Department records not found.");
                    }
                }
                else
                {
                    throw new Exception("TNA and training Id cannot be null.");
                }

            }
            catch (Exception ex)
            {
                Data = null;
                array = null;
                return Json(new { Status = false, Message = ex.Message, DataSource = Data, Selected = array }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SubmitLockedTrainingDepartments(SubmitTrainingDeaprts Data)
        {
            try
            {
                if (Data.DepartGrid.Count>0)
                {
                    bool isExist = false;
                    //var del_submitDeparts = TrainingLeadRepo.DeleteTNADetail(Data.TNAID, Data.TrainingID);
                    var previousdep = TrainingLeadRepo.GetAllPreviousDepByTNA(Data.TNAID, Data.TrainingID);
                    if (previousdep.Rows.Count>0)
                    {
                        for (int i = 0; i < Data.DepartGrid.Count; i++)
                        {
                           isExist = false;
                            for (int j = 0; j < previousdep.Rows.Count; j++)
                            {
                                if (Data.DepartGrid[i].DepartmentID==Convert.ToInt32(previousdep.Rows[j]["DepartmentID"]))
                                {
                                    isExist = true;
                                    if (Data.DepartGrid[i].Department_IsLocked!= Convert.ToBoolean(previousdep.Rows[j]["IsActive"]))
                                    {
                                        if (Convert.ToBoolean(previousdep.Rows[j]["IsActive"]))
                                        {
                                            var CheckforParticpantwasAssign = TrainingLeadRepo.CheckforParticpantwasAssign(Data.TNAID, Data.TrainingID, Data.DepartGrid[i].DepartmentID);
                                            if (CheckforParticpantwasAssign.Rows.Count>0)
                                            {
                                                throw new Exception("This " + Convert.ToString(CheckforParticpantwasAssign.Rows[0]["DepartmentName"]) + " can't remove from this Training and TNA because Line Manager of that department has assigned users.");
                                            }
                                        }
                                        TNADetails detail = new TNADetails();
                                        detail.TNA_ID = Data.TNAID;
                                        detail.Training_ID = Data.TrainingID;
                                        detail.Department_ID = Data.DepartGrid[i].DepartmentID;
                                        detail.Department_Name = Data.DepartGrid[i].DepartmentName;
                                        detail.Department_IsLocked = Data.DepartGrid[i].Department_IsLocked;
                                        //update
                                        var submitDeparts = TrainingLeadRepo.UpdateTNADetail(detail);
                                    }
                                    break;
                                }
                                
                            }

                            if (isExist==false)
                            {
                                TNADetails detail = new TNADetails();
                                detail.TNA_ID = Data.TNAID;
                                detail.Training_ID = Data.TrainingID;
                                detail.Department_ID = Data.DepartGrid[i].DepartmentID;
                                detail.Department_Name = Data.DepartGrid[i].DepartmentName;
                                detail.Department_IsLocked = Data.DepartGrid[i].Department_IsLocked;
                                var submitDeparts = TrainingLeadRepo.AddNewTNADetail(detail);
                            }

                        }
                    }
                    else
                    {
                        foreach (var item in Data.DepartGrid)
                        {
                            TNADetails detail = new TNADetails();
                            detail.TNA_ID = Data.TNAID;
                            detail.Training_ID = Data.TrainingID;
                            detail.Department_ID = item.DepartmentID;
                            detail.Department_Name = item.DepartmentName;
                            detail.Department_IsLocked = item.Department_IsLocked;
                            var submitDeparts = TrainingLeadRepo.AddNewTNADetail(detail);
                        }
                    }
                    
                }


                return Json(new { Status = true, Message = "Departments submitted successfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult PublishEmailDeptWise(int TNAID)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {

               

                if (ConfigurationManager.AppSettings["IsLiveEmail"].ToString().Contains("1"))
                {
                    var emailTemplate = TrainingLeadRepo.getEmailTemplate("Email-1");
                    string subject = emailTemplate.Subject;
                    string body = emailTemplate.Body;
                    var dt = TrainingLeadRepo.GetselectedDepartmentfromTNAforEmail(TNAID);
                    int  deptId=0;
                    int cont = 0;
                    string LoginID = "";
                    string TrainingName="";
                    string EmailSenttoDep = "";
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count;)
                        {
                            if (i==0)
                            {
                                EmailSenttoDep = Convert.ToString(dt.Rows[i]["ID"]);
                                deptId =Convert.ToInt32( dt.Rows[i]["DeptID"]);
                                LoginID = Convert.ToString(dt.Rows[i]["LoginID"]);
                                TrainingName = Convert.ToString(dt.Rows[i]["TrainingName"]);
                            i++;
                            if (dt.Rows.Count == i)
                            {
                                i--;
                                
                                var emailBody = vt_Common.SessionPublishEmailBodyDept(dt.Rows[i],TrainingName,body);

                                if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                                {
                                    vt_Common.SendingEmailToParticipants(dt.Rows[i]["Email"].ToString(), subject, emailBody);
                                    var UpdateEmailColumnSendTrue = TrainingLeadRepo.UpdateEmailColumn(EmailSenttoDep);
                                }
                                break;
                            }
                               
                            }
                            else
                            {
                                if (Convert.ToInt32(dt.Rows[i]["DeptID"]) == deptId && Convert.ToString(dt.Rows[i]["LoginID"])== LoginID)
                                {
                                   
                                    EmailSenttoDep +=","+ Convert.ToString(dt.Rows[i]["ID"]);
                                    TrainingName += "," +Convert.ToString(dt.Rows[i]["TrainingName"]);
                                    i++;
                                if (dt.Rows.Count==i)
                                {
                                    i--;
                                    var emailBody = vt_Common.SessionPublishEmailBodyDept(dt.Rows[i], TrainingName, body);

                                    if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                                    {
                                        vt_Common.SendingEmailToParticipants(dt.Rows[i]["Email"].ToString(), subject, emailBody);
                                        var UpdateEmailColumnSendTrue = TrainingLeadRepo.UpdateEmailColumn(EmailSenttoDep);
                                    }
                                    break;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                                else
                                {
                                    if (cont != dt.Rows.Count)
                                    {

                                        i--;
                                    }
                                    var emailBody = vt_Common.SessionPublishEmailBodyDept(dt.Rows[i], TrainingName,body);

                                    if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                                    {
                                        vt_Common.SendingEmailToParticipants(dt.Rows[i]["Email"].ToString(), subject, emailBody);
                                        var UpdateEmailColumnSendTrue = TrainingLeadRepo.UpdateEmailColumn(EmailSenttoDep);
                                        i++;
                                        if (i != dt.Rows.Count)
                                        {
                                            TrainingName = "";
                                            EmailSenttoDep = "";
                                            EmailSenttoDep = Convert.ToString(dt.Rows[i]["ID"]);
                                            deptId = Convert.ToInt32(dt.Rows[i]["DeptID"]);
                                            LoginID = Convert.ToString(dt.Rows[i]["LoginID"]);
                                            TrainingName = Convert.ToString(dt.Rows[i]["TrainingName"]);
                                            i++;
                                            cont = i;
                                            if (i == dt.Rows.Count)
                                            {
                                                LoginID = "";
                                                i--;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                        
                                    }
                                }
                               
                            }
                           
                        }
                    }
               }              

                else
                {
                    string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                    var emailTemplate = TrainingLeadRepo.getEmailTemplate("Email-1");
                    string subject = emailTemplate.Subject;
                    string body = emailTemplate.Body;
                    var dt = TrainingLeadRepo.GetselectedDepartmentfromTNAforEmail(TNAID);
                    int cont = 0;
                    int deptId = 0;
                    string LoginID = "";
                    string TrainingName = "";
                    string EmailSenttoDep = "";
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count;)
                        {
                            if (i == 0)
                            {
                                EmailSenttoDep = Convert.ToString(dt.Rows[i]["ID"]);
                                deptId = Convert.ToInt32(dt.Rows[i]["DeptID"]);
                                LoginID = Convert.ToString(dt.Rows[i]["LoginID"]);
                                TrainingName = Convert.ToString(dt.Rows[i]["TrainingName"]);
                                i++;
                                if (dt.Rows.Count == i)
                                {
                                    i--;

                                    var emailBody = vt_Common.SessionPublishEmailBodyDept(dt.Rows[i], TrainingName, body);

                                    if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                                    {
                                        vt_Common.SendingEmailToParticipants(ToEmail, subject, emailBody);
                                        var UpdateEmailColumnSendTrue = TrainingLeadRepo.UpdateEmailColumn(EmailSenttoDep);
                                    }
                                    break;
                                }

                            }
                            else
                            {
                                if (Convert.ToInt32(dt.Rows[i]["DeptID"]) == deptId &&  Convert.ToString(dt.Rows[i]["LoginID"]) == LoginID)
                                {

                                    EmailSenttoDep += "," + Convert.ToString(dt.Rows[i]["ID"]);
                                    TrainingName += "," + Convert.ToString(dt.Rows[i]["TrainingName"]);
                                    i++;
                                    if (dt.Rows.Count == i)
                                    {
                                        i--;
                                        var emailBody = vt_Common.SessionPublishEmailBodyDept(dt.Rows[i], TrainingName, body);

                                        if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                                        {
                                            vt_Common.SendingEmailToParticipants(ToEmail, subject, emailBody);
                                            var UpdateEmailColumnSendTrue = TrainingLeadRepo.UpdateEmailColumn(EmailSenttoDep);
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                else
                                {
                                    if (cont != dt.Rows.Count)
                                    {
                                       
                                        i--;
                                    }
                                

                                    var emailBody = vt_Common.SessionPublishEmailBodyDept(dt.Rows[i], TrainingName, body);

                                    if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                                    {
                                        vt_Common.SendingEmailToParticipants(ToEmail, subject, emailBody);
                                        var UpdateEmailColumnSendTrue = TrainingLeadRepo.UpdateEmailColumn(EmailSenttoDep);
                                        i++;
                                        if (i!=dt.Rows.Count)
                                        {

                                            TrainingName = "";
                                            EmailSenttoDep = "";
                                            EmailSenttoDep = Convert.ToString(dt.Rows[i]["ID"]);
                                            deptId = Convert.ToInt32(dt.Rows[i]["DeptID"]);
                                            LoginID = Convert.ToString(dt.Rows[i]["LoginID"]);
                                            TrainingName = Convert.ToString(dt.Rows[i]["TrainingName"]);
                                            i++;
                                            cont = i;
                                            if (i == dt.Rows.Count)
                                            {
                                                LoginID = "";                                               
                                                i--;
                                            }
                                        }
                                        else
                                        {
                                            break;
                                        }
                                       
                                    }
                                }

                            }

                        }
                    }
                }

                Response.Status = true;
                Response.Message = "TNA published successfully.";

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
            }

            return Json(Response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteTNADetailTraining(SubmitTrainingDeaprts Header)
        {
            try
            {

                var deltraining = TrainingLeadRepo.DeleteTNADetailTraining(Header.TNAID, Header.TrainingID);
                if (deltraining.Rows.Count>0)
                {
                    return Json(new { Status = false, Message = "This training can't remove from this TNA because line manager has assigned their employee's in this Training." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = true, Message = "Training deleted and unselected Successfully." }, JsonRequestBehavior.AllowGet);
                }

               
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Create TNA Commented
        
        #endregion

        public ActionResult UpdateTNA(TNAForm Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {
                    

                    var TNA = TrainingLeadRepo.AddTNA(Header);
                    int ID = Convert.ToInt32(Header.ID);
                    //var del = TrainingLeadRepo.DeleteTrainings_byTNAID(ID);

                    if (Header.Trainings != null)
                    {
                        TNADetails TNADetail = new TNADetails();

                        foreach (var item in Header.Trainings)
                        {
                            if (item.Training_IsSelected == true)
                            {
                                TNADetail.TNA_ID = ID;
                                TNADetail.Training_ID = item.TrainingID;

                                foreach (var depart in item.Departs)
                                {
                                    TNADetail.Department_ID = depart.DepartmentID;
                                    TNADetail.Department_IsLocked = depart.Department_IsLocked;

                                    DataTable dt = TrainingLeadRepo.AddNewTNADetail(TNADetail);
                                }

                            }
                        }
                    }


                   


                    Response.Status = true;
                    Response.Message = "TNA updated successfully.";

                }
                else
                {
                    throw new Exception("Enter valid fields");
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region TNAs Page


        public ActionResult TNAs()
        {
            return View();
        }

        public ActionResult NewTNAForm()
        {
            try
            {
                return PartialView("_TNAForm", new TNAForm());
            }
            catch (Exception ex)
            {
                return PartialView("_TNAForm", new TNAForm());
            }
        }

        public JsonResult AllTNA()
        {
            List<vt_TNA> TNAList = new List<vt_TNA>();
            try
            {
                TNAList = TrainingLeadRepo.GetAllTNAs();

                if (TNAList != null)
                {
                    var jsonList = JsonConvert.SerializeObject(TNAList, Formatting.Indented, new JsonSerializerSettings() { DateFormatString = "dd/MM/yyyy" });
                    return Json(new { Status = true, Message = "TNAs found.", TNAs = jsonList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No TNAs found.");
                }
            }
            catch (Exception ex)
            {
                TNAList = null;
                return Json(new { Status = false, Message = ex.Message, TNAs = TNAList }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditTNA(int ID)
        {
            TNAForm TNA = new TNAForm();

            try
            {
                if (ID > 0)
                {
                    var vtTNA = TrainingLeadRepo.GetTNA_byID(ID);
                    if (vtTNA != null)
                    {
                        TNA = vtTNA;
                        var TrainingIDArray = TrainingLeadRepo.GetTrainings_byTNAID(ID);
                        if (TrainingIDArray != null)
                        {
                            TNA.TrainingIDs = TrainingIDArray;
                        }
                        else
                        {
                            TNA.TrainingIDs = null;
                        }
                    }

                    TNA.Status = true;
                    TNA.Message = "TNA found.";

                }
                else
                {
                    TNA.Status = false;
                    TNA.Message = "TNA not found.";
                }


                return View("AddTNA", TNA);
            }
            catch (Exception ex)
            {
                TNA.Status = false;
                TNA.Message = ex.Message;
                return View("AddTraining", TNA);
            }
        }

        

        public ActionResult DeleteTNA(int ID)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ID > 0)
                {

                    var tna = TrainingLeadRepo.DeleteTNA(ID);

                    Response.Status = true;
                    Response.Message = "TNA deleted successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("ID can not be null.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }


        #endregion

        #region Session Page

        public ActionResult Sessions()
        {
            return View();
        }

        public ActionResult NewSessionForm()
        {
            try
            {
                return PartialView("_SessionForm", new SessionForm());
            }
            catch (Exception ex)
            {
                return PartialView("_SessionForm", new SessionForm());
            }
        }

        public JsonResult GetAllSessions()
        {
            try
            {
                var SessionList = TrainingLeadRepo.GetAllSessions();

                var jsonSessionList = JsonConvert.SerializeObject(SessionList, Formatting.Indented, new JsonSerializerSettings() { DateFormatString = "dd/MM/yyyy" });

                return Json(jsonSessionList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult GetAllActiveTNAs_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var TNAs = TrainingLeadRepo.GetAllActiveTNAs();

                if (TNAs != null)
                {

                    foreach (var item in TNAs)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.TNAName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "TNA record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No TNA record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllActiveTrainings_Select(int ID)
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var TNAsList = TrainingLeadRepo.GetAllActiveTrainings_byTNAID(Convert.ToInt32(ID));

                if (TNAsList != null)
                {

                    foreach (var item in TNAsList)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.TrainingName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "TNA record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No TNA record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllTNAs_SelectEdit()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var TNAs = TrainingLeadRepo.GetAllTNAs();

                if (TNAs != null)
                {

                    foreach (var item in TNAs)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.TNAName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "TNA record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No TNA record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllTrainings_SelectEdit()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var TrainingsList = TrainingLeadRepo.GetAllTrainings();

                if (TrainingsList != null)
                {

                    foreach (var item in TrainingsList)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.TrainingName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "TNA record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No Training against this TNA.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllTrainers_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var Trainers = TrainingLeadRepo.GetAllTrainers();

                if (Trainers != null)
                {

                    foreach (var item in Trainers)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.FullName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Co-ordinator record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No Co-ordinator record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllCo_Ordinator_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var Trainers = TrainingLeadRepo.GetAllCoordinator();

                if (Trainers != null)
                {

                    foreach (var item in Trainers)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.FullName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Trainers record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No trainers record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllAreaGeography_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var areas = TrainingLeadRepo.GetAllGeography();

                if (areas != null)
                {

                    foreach (var item in areas)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.Geography;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Geography record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No geography record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult CancelSession(int ID)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ID > 0)
                {
                    var sessionParticipants = TrainingLeadRepo.DeleteSessionandAllParticipants(Convert.ToInt32(ID));
                    if (sessionParticipants.Rows.Count > 0)
                    {
                        sendCancelEmailToTrainer_Cooridnator(ID);
                        var emailTemplate = TrainingLeadRepo.GetCancelSessionEmailTemplate();
                        if (emailTemplate.Rows.Count>0)
                        {
                            string subject = Convert.ToString(emailTemplate.Rows[0]["Subject"]);
                            string body = Convert.ToString(emailTemplate.Rows[0]["Body"]);
                            if (ConfigurationManager.AppSettings["IsLiveEmail"].ToString().Contains("1"))
                            {


                                for (int i = 0; i < sessionParticipants.Rows.Count; i++)
                                {
                                    var emailBody = vt_Common.SessionCancelEmailBody(sessionParticipants.Rows[i], body);

                                    if (sessionParticipants.Rows[i]["Email"] != null && sessionParticipants.Rows[i]["Email"] != DBNull.Value)
                                    {
                                        vt_Common.SendingEmailToParticipants(sessionParticipants.Rows[i]["Email"].ToString(), subject, emailBody);
                                    }
                                }

                            }

                            else
                            {
                               
                                string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();                               
                                foreach (DataRow row in sessionParticipants.Rows)
                                {
                                    row.SetField("Email", ToEmail);

                                }
                                for (int i = 0; i < sessionParticipants.Rows.Count; i++)
                                {
                                    var emailBody = vt_Common.SessionCancelEmailBody(sessionParticipants.Rows[i],body);

                                    if (sessionParticipants.Rows[i]["Email"] != null && sessionParticipants.Rows[i]["Email"] != DBNull.Value)
                                    {
                                        vt_Common.SendingEmailToParticipants(sessionParticipants.Rows[i]["Email"].ToString(), subject, emailBody);
                                    }
                                }

                            }

                            Response.Status = true;
                            Response.Message = "Session canceled email has sent to all participants.";
                        }
                        else
                        {
                            throw new Exception("Email template is empty.");
                        }
                        
                    }

                    else
                    {

                        // Response.Status = false;
                        // Response.Message = "There is no participants in this session.";
                        sendCancelEmailToTrainer_Cooridnator(ID);





                    }


                }
                else
                {
                    Response.Status = false;
                    Response.Message = "Session not available.";

                }
                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                return Json(Response, JsonRequestBehavior.AllowGet);
            }          

           

        }

        public void sendCancelEmailToTrainer_Cooridnator(int sessionId) {

            var trainerDetail = TrainingLeadRepo.GetTrainerDetail(sessionId);
            if (trainerDetail != null)
            {
                var emailTemplate = TrainingLeadRepo.getEmailTemplate("Email-5");
                if (emailTemplate != null)
                {
                    if (ConfigurationManager.AppSettings["IsLiveEmail"].ToString().Contains("1"))
                    {
                        var EmailBody = vt_Common.EmailToTrainer(trainerDetail, emailTemplate.Body);
                        vt_Common.SendingEmailToParticipants(trainerDetail.Email, emailTemplate.Subject, EmailBody);
                    }
                    else
                    {
                        string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                        var EmailBody = vt_Common.EmailToTrainer(trainerDetail, emailTemplate.Body);
                        vt_Common.SendingEmailToParticipants(ToEmail, emailTemplate.Subject, EmailBody);
                    }
                }
                else
                {
                    throw new Exception("Email-5 Template not found.");
                }
            }

            else
            {
                throw new Exception("Trainer/Co-ordinator not found.");
            }
        }
        public ActionResult DeleteSession(int ID)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ID > 0)
                {
                    var sessionParticipants = TrainingLeadRepo.GetAllParticipants_bySession(Convert.ToInt32(ID));

                    if (sessionParticipants.Rows.Count > 0)
                        throw new Exception("Delete Participents of this Session first.");

                    var sessionRecord = TrainingLeadRepo.GetSession_byID(Convert.ToInt32(ID));

                    if (sessionRecord.StartDate != null && sessionRecord.StartDate != "")
                    {
                        var strd1 = DateTime.Now.ToShortDateString(); //Current DateTime
                        var strd2 = Convert.ToDateTime(sessionRecord.StartDate).ToShortDateString(); //Session Start DateTime
                        var today = DateTime.Parse(strd1);
                        var start = DateTime.Parse(strd2);

                        //Check Session StartDate is passed date with Todays date
                        if (start <= today)
                        {
                            throw new Exception("Session cannot be delete.");
                        }
                        else
                        {
                            var DelSession = TrainingLeadRepo.DeleteSession(Convert.ToInt32(ID));
                        }

                        //var d2 = vt_Common.ConvertDateString(sessionRecord.StartDate.ToString()).ToString("mm/DD/yyyy"); //Session Start DateTime
                        //d2 = d2.ToShortDateString("mm/DD/yyyy");
                        //var d1 = vt_Common.GetCurrentDateTime();  //Current DateTime
                        //var d2 = (DateTime)sessionRecord.StartDate; //Session Start DateTime

                        //if (d2 <= d1)
                        //{
                        //    throw new Exception("Session cannot be delete.");
                        //}
                        //else
                        //{
                        //    var DelSession = TrainingLeadRepo.DeleteSession(Convert.ToInt32(ID));
                        //}
                    }
                    else {
                        var DelSession = TrainingLeadRepo.DeleteSession(Convert.ToInt32(ID));
                        //throw new Exception("Session Record not found.");
                    }




                    Response.Status = true;
                    Response.Message = "Session Deleted Successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("ID can not be null.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CreateSession(SessionForm Header)
        {
           // var arr=Header.StartTime.Split('(');
           // DateTime dt = DateTime.Parse(Convert.ToDateTime(arr[0]).ToString());
           //var a = dt.ToString("hh:mm tt");
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {

                    var Session = TrainingLeadRepo.AddUpdateSession(Header);
                    if (Header.ID==0)
                    {
                     var trainerDetail= TrainingLeadRepo.GetTrainerDetail(Session);

                        if (trainerDetail != null)
                        {
                            var emailTemplate = TrainingLeadRepo.getEmailTemplate("Email-3");
                            if (emailTemplate != null)
                            {
                                if (ConfigurationManager.AppSettings["IsLiveEmail"].ToString().Contains("1"))
                                {
                                    var EmailBody = vt_Common.EmailToTrainer(trainerDetail, emailTemplate.Body);
                                    vt_Common.SendingEmailToParticipants(trainerDetail.Email, emailTemplate.Subject, EmailBody);
                                }
                                else
                                {
                                    string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                                    var EmailBody = vt_Common.EmailToTrainer(trainerDetail, emailTemplate.Body);
                                    vt_Common.SendingEmailToParticipants(ToEmail, emailTemplate.Subject, EmailBody);
                                }
                            }
                            else
                            {
                                throw new Exception("Email-3 Template not found.");
                            }
                        }

                        else
                        {
                            throw new Exception("Trainer/Co-ordinator not found.");
                        }
                    }
                    Response.Status = true;
                    Response.Message = "Session Added Successfully.";

                }
                else
                {
                    var error = ModelState.Select(x => x.Value.Errors)
                           .Where(y => y.Count > 0)
                           .FirstOrDefault();
                    throw new Exception(error.FirstOrDefault().ErrorMessage);
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult EditSession(int ID)
        {
            SessionForm Session = new SessionForm();

            try
            {
                if (ID > 0)
                {
                    var vtSession = TrainingLeadRepo.GetSession_byID(ID);

                    //if (vtSession != null)
                    //{
                    Session = vtSession;

                    //}
                    //else
                    //{
                    //    Session = null;
                    //}

                }
                else
                {
                    Session.Status = false;
                    Session.Message = "SessionID can't be null";
                }


                return PartialView("_SessionForm", Session);
            }
            catch (Exception ex)
            {
                Session.Status = false;
                Session.Message = ex.Message;
                return PartialView("_SessionForm", Session);
            }
        }

        public ActionResult UpdateSession(SessionForm Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Header.ID > 0)
                    {
                        var Session = TrainingLeadRepo.AddUpdateSession(Header);

                        Response.Status = true;
                        Response.Message = "Session updated successfully.";
                    }
                    else
                    {
                        Response.Status = false;
                        Response.Message = "SessionID can't be null.";
                    }

                }
                else
                {
                    var error = ModelState.Select(x => x.Value.Errors)
                          .Where(y => y.Count > 0)
                          .FirstOrDefault();
                    throw new Exception(error.FirstOrDefault().ErrorMessage);
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Assign Users To Sessions

        public ActionResult ActiveTrainings()
        {
            return View();
        }

        public ActionResult GetTNAEndDate(int TNAID) {
            try
            {
                var TNAsDate = TrainingLeadRepo.GetTNAStartandEndDate(TNAID);
                var Jsonresult = JsonConvert.SerializeObject(TNAsDate);
                return Json(new { Status = true, Message = "Record found.", data = Jsonresult }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = "" }, JsonRequestBehavior.AllowGet);

               
            }
        }
        public JsonResult ActiveTnaTrainings()
        {
            List<ActiveTnaAndTrainings_mdl> lst_Trainings = new List<ActiveTnaAndTrainings_mdl>();
            try
            {
                lst_Trainings = TrainingLeadRepo.GetAllActiveTnaActiveTrainings();

                if (lst_Trainings != null)
                {
                    return Json(new { Status = true, Message = "Active Tna Active Trainings.", TNAs = lst_Trainings }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = true, Message = "Active Tna Active Trainings Not Found.", TNAs = lst_Trainings }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                lst_Trainings = null;
                return Json(new { Status = false, Message = ex.Message, TNAs = lst_Trainings }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AssignSessionUsers(string id)
        {
            List<ActiveTrainingsSessions_mdl> lst_ActiveTrainingSession = new List<ActiveTrainingsSessions_mdl>();
            try
            {
                var idsArray = id.Split('^');
                int TrainingID = Convert.ToInt32(idsArray[0]);
                int TNAID = Convert.ToInt32(idsArray[1]);

                lst_ActiveTrainingSession = TrainingLeadRepo.GetAllActiveTrainingsSessions(TrainingID, TNAID);
                if (lst_ActiveTrainingSession != null)
                {
                    ViewBag.TrainingID = TrainingID;
                    ViewBag.TNAID = TNAID;
                    //return Json(new { Status = true, Message = "Active Trainings Sessions.", TNAs = lst_ActiveTrainingSession }, JsonRequestBehavior.AllowGet);
                    return View(lst_ActiveTrainingSession);
                }
                else
                {
                    //return Json(new { Status = true, Message = "Active Trainings Sessions Not Found.", TNAs = lst_ActiveTrainingSession }, JsonRequestBehavior.AllowGet);
                    ViewBag.TrainingID = TrainingID;
                    ViewBag.TNAID = TNAID;
                    return View(lst_ActiveTrainingSession);
                }
            }
            catch (Exception ex)
            {
                lst_ActiveTrainingSession = null;
                //return Json(new { Status = false, Message = ex.Message, TNAs = lst_ActiveTrainingSession }, JsonRequestBehavior.AllowGet);
                return View(lst_ActiveTrainingSession);
            }

            //return View();
        }
        [HttpPost]
        public JsonResult GetDept(int TnaID, int TrainingID)
        {
            int departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
            var DeptList = TrainingLeadRepo.GetDept(departID,TnaID,TrainingID);
            return Json(new { DeptList = JsonConvert.SerializeObject(DeptList) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllDepartmentsAssignedEmployees(int TnaID, int TrainingID)
        {
            List<ActiveAssignedDepartmentsEmployee_mdl> lst_SesssionParticipants = new List<ActiveAssignedDepartmentsEmployee_mdl>();
            try
            {
                lst_SesssionParticipants = TrainingLeadRepo.GetAllDepartmentsAssignedUsers(TnaID, TrainingID);
            }
            catch (Exception ex)
            {

            }


            return Json(lst_SesssionParticipants, JsonRequestBehavior.AllowGet);
        }

        //Search 
        public JsonResult SearchDepartmentsbyNameforAssignedEmployees(int TnaID, int TrainingID,string DName)
        {
            List<ActiveAssignedDepartmentsEmployee_mdl> lst_SesssionParticipants = new List<ActiveAssignedDepartmentsEmployee_mdl>();
            try
            {
                lst_SesssionParticipants = TrainingLeadRepo.SearchDepartmentsbyNameforAssignedEmployees(TnaID, TrainingID,DName);
            }
            catch (Exception ex)
            {

            }


            return Json(new { lst_SesssionParticipants }, JsonRequestBehavior.AllowGet);
        }

        //End Search
        public JsonResult InsertUsersToSessions(vt_SessionParticipant obj)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                var departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                //obj.DeptID = departID;
                var DataInsertion = TrainingLeadRepo.InsertUsersSessions(obj);
                if (DataInsertion!=null && DataInsertion.Rows.Count>0)
                {
                    if (Convert.ToBoolean(DataInsertion.Rows[0]["checkIsExist"]))
                    {
                        Response.Status = false;
                        Response.Message = "User Already Exist same Time.";
                    }
                    else
                    {
                        Response.Status = true;
                        Response.Message = "Participants Added Successfully.";
                    }
                }

                else
                {
                    Response.Status = true;
                    Response.Message = "Participants Added Successfully.";
                }
                
               

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = "There is some error please try again.";
            }
            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSessionUsersOnLoad(vt_SessionParticipant obj)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                var tnaid = obj.TNAID;
                var trainingid = obj.TrainingID;

                if (obj.TNAID != null && obj.TrainingID != null)
                {
                    var getData = TrainingLeadRepo.GetAssignSessionsUsersOnLoad(tnaid, trainingid);
                    return Json(new { Status = true, Message = "Found Records.", data = getData }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //var getData = TrainingLeadRepo.GetAssignSessionsUsersOnLoad(tnaid, trainingid);
                    //return Json(new { Status = false, Message = "Found Not Records.", data =  }, JsonRequestBehavior.AllowGet);
                    throw new Exception("Records Not Found.");
                }
            }
            catch (Exception ex)
            {
                var getdata = "";
                return Json(new { Status = false, Message = ex.Message, data = getdata }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult DeleteUserFromSession(vt_SessionParticipant obj)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                //var departID = Convert.ToInt32(CurrentUser.User.DepartmentID);
                //obj.DepartmentId = departID;
                //int userID = Convert.ToInt32(obj);

                var DataInsertion = TrainingLeadRepo.DeleteUserFromSession(obj);

                Response.Status = true;
                Response.Message = "User Deleted Successfully.";

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = "There is some error please try again.";
            }
            return Json(Response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult PublishSession(SessionParticipants_mdl Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                var emailTemplate = TrainingLeadRepo.GetSessionEmailTemplate();
                string subject = Convert.ToString(emailTemplate.Rows[0]["Subject"]);
                string body = Convert.ToString(emailTemplate.Rows[0]["Body"]);

                if (ConfigurationManager.AppSettings["IsLiveEmail"].ToString().Contains("1"))
                {
                    var dt = TrainingLeadRepo.GetSessionAssignedUsers(Header);
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var emailBody = vt_Common.SessionPublishEmailBody(dt.Rows[i], body);

                            if (dt.Rows[i]["Email"] != null && dt.Rows[i]["Email"] != DBNull.Value)
                            {
                                int ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                                vt_Common.SendingEmailToParticipants(dt.Rows[i]["Email"].ToString(), subject, emailBody);
                                TrainingLeadRepo.setPublishEmailTrue(ID);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("No participant available for sending session email.");
                    }
                }

                else 
                {
                    string ToEmail = ConfigurationManager.AppSettings["ToEmail"].ToString();
                    var dt = TrainingLeadRepo.GetSessionAssignedUsers(Header);
                    //string[] ListofEmailAdd = ConfigurationManager.AppSettings["OffEmailAddress"].ToString().Split(',');
                    foreach (DataRow row in dt.Rows)
                    {
                        row.SetField("Email", ToEmail);

                    }
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            var emailBody = vt_Common.SessionPublishEmailBody(dt.Rows[i], body);
                            int ID = Convert.ToInt32(dt.Rows[i]["ID"]);
                            vt_Common.SendingEmailToParticipants(dt.Rows[i]["Email"].ToString(), subject, emailBody);
                            TrainingLeadRepo.setPublishEmailTrue(ID);
                        }
                    }

                    else
                    {
                        throw new Exception("No participant available for sending session email.");
                    }

                }
               

               
               

                Response.Status = true;
                Response.Message = "Session published successfully.";

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
            }

            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Closed Session
        public ActionResult ClosedSessions()
        {
            return View();
        }

        public JsonResult GetAllClosedSessions()
        {
            try
            {
                var SessionList = TrainingLeadRepo.GetAllClosedSessions();

                var jsonSessionList = JsonConvert.SerializeObject(SessionList, Formatting.Indented, new JsonSerializerSettings() { DateFormatString = "dd/MM/yyyy" });

                return Json(jsonSessionList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult ClosedSessionDetail(int id)
        {
            ActiveTrainingsSessions_mdl obj = new ActiveTrainingsSessions_mdl();
            obj.SessionID = id;
            return View(obj);
           
        }

        [HttpPost]
        public ActionResult GetDetailofSessionbyID(int SessionID)
        {
            try
            {
                var SessionList = TrainingLeadRepo.GetClosedSessionsDetailbyID(SessionID);

                var jsonSessionList = JsonConvert.SerializeObject(SessionList, Formatting.Indented, new JsonSerializerSettings() { DateFormatString = "dd/MM/yyyy" });

                return Json(jsonSessionList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public JsonResult GetUsersDetailofSession(vt_SessionParticipant obj)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                var tnaid =(int) obj.TNAID;
                var trainingid = (int)obj.TrainingID;
                var sessionid = (int)obj.SessionID;

                if (obj.TNAID != null && obj.TrainingID != null)
                {
                    var getData = TrainingLeadRepo.GetdetailclosedSessionsUsers(tnaid, trainingid, sessionid);
                    var jsonSessionList = JsonConvert.SerializeObject(getData, Formatting.Indented);
                    return Json(new { Status = true, Message = "Found Records.", data = jsonSessionList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                   
                    return Json(new { Status = false, Message = "Found Not Records."}, JsonRequestBehavior.AllowGet);
                    
                }
            }
            catch (Exception ex)
            {
                var getdata = "";
                return Json(new { Status = false, Message = ex.Message, data = getdata }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult downloadInvoice(string Ids)
        {
            var data = JsonConvert.DeserializeObject<ActiveTrainingsSessions_mdlForPDF>(Ids);
            var SessionList = TrainingLeadRepo.GetClosedSessionsDetailbyID(data.SessionID);
            var _userDetail = TrainingLeadRepo.GetdetailclosedSessionsUsers(data.TNAID, data.TrainingID, data.SessionID);



            try
            {
                LocalReport localreport = new LocalReport();
                ReportParameterCollection reportParameters = new ReportParameterCollection();
                if (_userDetail.Rows.Count > 0)
                {
                    DataTable DataTable1 = new DataTable("DataTable1");
                    DataTable1.Columns.Add("EmployeeName");
                    DataTable1.Columns.Add("Department");
                    DataTable1.Columns.Add("Feedback");
                    DataTable1.Columns.Add("Attendance");
                    for (int i = 0; i < _userDetail.Rows.Count; i++)
                    {
                             DataTable1.Rows.Add(
                            _userDetail.Rows[i]["ReporteeName"] != DBNull.Value ? Convert.ToString(_userDetail.Rows[i]["ReporteeName"]) : "",
                            _userDetail.Rows[i]["DepartmentName"] != DBNull.Value ? Convert.ToString(_userDetail.Rows[i]["DepartmentName"]) : "",
                            _userDetail.Rows[i]["Feedback"] != DBNull.Value ? Convert.ToString(_userDetail.Rows[i]["Feedback"]) : "",
                            _userDetail.Rows[i]["Attendence"] != DBNull.Value ? (Convert.ToBoolean(_userDetail.Rows[i]["Attendence"]) == true ? 1 : 0 ) : 0);

                           // _userDetail.Rows[i]["Attendence"] != DBNull.Value ? (Convert.ToString(_userDetail.Rows[i]["Attendence"])=="true" ? 1 : 0);

                    }
                    DataSet DataSet1 = new DataSet("DataSet1");
                    DataSet1.Tables.Add(DataTable1);
                    ReportDataSource reportDS = new ReportDataSource();
                    reportDS.Name = "DataSet1";
                    reportDS.Value = DataSet1.Tables[0];
                    localreport.DataSources.Add(reportDS);
                }
                

                reportParameters.Add(new ReportParameter("ReportParameter1", Convert.ToString(SessionList.Rows[0]["Days"])));
                reportParameters.Add(new ReportParameter("ReportParameter2", Convert.ToString(SessionList.Rows[0]["Description"])));
                reportParameters.Add(new ReportParameter("ReportParameter3", Convert.ToString(SessionList.Rows[0]["SessionEnd"]) + "  " + Convert.ToString(SessionList.Rows[0]["EndTime"])));
                reportParameters.Add(new ReportParameter("ReportParameter4", Convert.ToString(SessionList.Rows[0]["Participants"])));
                reportParameters.Add(new ReportParameter("ReportParameter5", Convert.ToString(SessionList.Rows[0]["SessionName"])));
                reportParameters.Add(new ReportParameter("ReportParameter6", Convert.ToString(SessionList.Rows[0]["SessionStart"]) + "  " + Convert.ToString(SessionList.Rows[0]["StartTime"])));
                reportParameters.Add(new ReportParameter("ReportParameter7", Convert.ToString(SessionList.Rows[0]["TNAName"])));
                reportParameters.Add(new ReportParameter("ReportParameter8", Convert.ToString(SessionList.Rows[0]["TrainingName"])));
                reportParameters.Add(new ReportParameter("ReportParameter9", Convert.ToString(SessionList.Rows[0]["FullName"])));
                localreport.ReportPath = Server.MapPath("~/Pdf/SessionUser.rdlc");
                localreport.SetParameters(reportParameters);
                string mimeType;
                string encoding;
                string FileNameExtention;
                FileNameExtention = "pdf";
                string[] streams;
                Warning[] warnings;
                byte[] renderbyte;
                renderbyte = localreport.Render("pdf", "", out mimeType, out encoding, out FileNameExtention, out streams, out warnings);
                string fullPath = Request.MapPath("~/Content/Upload/SessionPDF.pdf");
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                string path = AppDomain.CurrentDomain.BaseDirectory + "/Content/Upload/";
                System.IO.File.WriteAllBytes(Path.Combine(path, "SessionPDF." + FileNameExtention), renderbyte);
                Response.AddHeader("content-disposition", "attachment;filename=" +Convert.ToString(SessionList.Rows[0]["SessionName"]) + "_" + DateTime.Now.ToString("dd-MM/yyyy") + "." + FileNameExtention);
                return File(renderbyte, FileNameExtention);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);

            }
           
        }

        #endregion

        
    }
}



//using BAL.Repository;
//using DAL.DBEntities;
//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using TNAApp_ViewModel.Model;
//using static TNA_ViewModel.Model.TNA_VM;

//namespace TNA.Controllers
//{
//    public class TrainingLeadController : BaseController
//    {
//        TrainingLeadRepository TrainingLeadRepo;

//        public TrainingLeadController()
//        {
//            TrainingLeadRepo = new TrainingLeadRepository(new vt_TNAEntity());
//        }

//        #region Dashboard Page

//        // GET: TrainingLead
//        public ActionResult Index()
//        {
//            return View();
//        }

//        #endregion

//        #region Add TNA Page
//        public ActionResult AddTNA()
//        {
//            return View(new TNAForm());
//        }

//        public JsonResult GetAllTrainings()
//        {
//            List<vt_Trainings> TrainingList = new List<vt_Trainings>();
//            try
//            {
//                TrainingList = TrainingLeadRepo.GetAllTrainings();

//                if (TrainingList != null)
//                {
//                    return Json(new { Status = true, Message = "Trainings Found.", Trainings = TrainingList }, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    TrainingList = null;
//                    return Json(new { Status = false, Message = "No Trainings Found.", Trainings = TrainingList }, JsonRequestBehavior.AllowGet);
//                }
//            }
//            catch (Exception ex)
//            {
//                TrainingList = null;
//                return Json(new { Status = false, Message = ex.Message, Trainings = TrainingList }, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public ActionResult CreateTNA(TNAForm Header)
//        {
//            ResponseHeader Response = new ResponseHeader();
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    if (Header.TrainingIDs == null)
//                        throw new Exception("Select any one Training against a TNA.");

//                    if (Header.IsActive)
//                    {
//                        var activeTNA = TrainingLeadRepo.GetActiveTNAs();

//                        if (activeTNA.Rows.Count > 0)
//                            throw new Exception("Only one TNA can be active at one time.");
//                    }


//                    var TNA = TrainingLeadRepo.AddTNA(Header);
//                    string LastTNAID = TrainingLeadRepo.GetLastTNAID();
//                    int ID = Convert.ToInt32(LastTNAID);

//                    TNADetails TNADetail = new TNADetails();
//                    TNADetail.TNA_ID = Convert.ToInt32(ID);

//                    if (Header.TrainingIDs.Length > 0)
//                    {
//                        //HttpFileCollectionBase[] file = Header.filesattachment;
//                        //var files = file;

//                        for (int i = 0; i < Header.TrainingIDs.Length; i++)
//                        {
//                            TNADetail.Training_ID = Header.TrainingIDs[i];

//                            DataTable dt = TrainingLeadRepo.AddNewTNADetail(TNADetail);
//                        }
//                    }


//                    Response.Status = true;
//                    Response.Message = "TNA Added Successfully.";

//                }
//                else
//                {
//                    throw new Exception("Enter valid fields");
//                }

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public ActionResult UpdateTNA(TNAForm Header)
//        {
//            ResponseHeader Response = new ResponseHeader();
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    if (Header.TrainingIDs == null)
//                        throw new Exception("Select any one Training against a TNA.");

//                    var TNA = TrainingLeadRepo.AddTNA(Header);
//                    int ID = Convert.ToInt32(Header.ID);
//                    var del = TrainingLeadRepo.DeleteTrainings_byTNAID(ID);

//                    TNADetails TNADetail = new TNADetails();
//                    TNADetail.TNA_ID = Convert.ToInt32(ID);

//                    if (Header.TrainingIDs.Length > 0)
//                    {

//                        for (int i = 0; i < Header.TrainingIDs.Length; i++)
//                        {
//                            TNADetail.Training_ID = Header.TrainingIDs[i];

//                            DataTable dt = TrainingLeadRepo.AddNewTNADetail(TNADetail);
//                        }
//                    }


//                    Response.Status = true;
//                    Response.Message = "TNA Updated Successfully.";

//                }
//                else
//                {
//                    throw new Exception("Enter valid fields");
//                }

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }


//        #endregion

//        #region TNAs Page


//        public ActionResult TNAs()
//        {
//            return View();
//        }

//        public JsonResult AllTNA()
//        {
//            List<vt_TNA> TNAList = new List<vt_TNA>();
//            try
//            {
//                TNAList = TrainingLeadRepo.GetAllTNAs();

//                if (TNAList != null)
//                {
//                    return Json(new { Status = true, Message = "TNAs Found.", TNAs = TNAList }, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    TNAList = null;
//                    return Json(new { Status = false, Message = "No TNAs Found.", TNAs = TNAList }, JsonRequestBehavior.AllowGet);
//                }
//            }
//            catch (Exception ex)
//            {
//                TNAList = null;
//                return Json(new { Status = false, Message = ex.Message, TNAs = TNAList }, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public ActionResult EditTNA(int ID)
//        {
//            TNAForm TNA = new TNAForm();

//            try
//            {
//                if (ID > 0)
//                {
//                    var vtTNA = TrainingLeadRepo.GetTNA_byID(ID);
//                    if (vtTNA != null)
//                    {
//                        TNA = vtTNA;
//                        var TrainingIDArray = TrainingLeadRepo.GetTrainings_byTNAID(ID);
//                        if (TrainingIDArray != null)
//                        {
//                            TNA.TrainingIDs = TrainingIDArray;
//                        }
//                        else
//                        {
//                            TNA.TrainingIDs = null;
//                        }
//                    }

//                    TNA.Status = true;
//                    TNA.Message = "TNA Found Successfully.";

//                }
//                else
//                {
//                    TNA.Status = false;
//                    TNA.Message = "TNA not Found Successfully.";
//                }


//                return View("AddTNA", TNA);
//            }
//            catch (Exception ex)
//            {
//                TNA.Status = false;
//                TNA.Message = ex.Message;
//                return View("AddTraining", TNA);
//            }
//        }

//        public ActionResult DeleteTNA(int ID)
//        {
//            ResponseHeader Response = new ResponseHeader();

//            try
//            {

//                if (ID > 0)
//                {

//                    var tna = TrainingLeadRepo.DeleteTNA(ID);

//                    Response.Status = true;
//                    Response.Message = "TNA Deleted Successfully.";

//                    return Json(Response, JsonRequestBehavior.AllowGet);

//                }
//                else
//                {
//                    throw new Exception("ID can not be null.");
//                }
//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }

//        }


//        #endregion

//        #region Session Page

//        public ActionResult Sessions()
//        {
//            return View();
//        }

//        public ActionResult NewSessionForm()
//        { 
//            try
//            {
//                return PartialView("_SessionForm", new SessionForm());
//            }
//            catch (Exception ex)
//            {
//                return PartialView("_SessionForm", new SessionForm());
//            }
//        }

//        public JsonResult GetAllSessions()
//        {
//            var SessionList = TrainingLeadRepo.GetAllSessions();

//            var jsonSessionList = JsonConvert.SerializeObject(SessionList, Formatting.Indented, new JsonSerializerSettings() { DateFormatString = "dd/MM/yyyy" });

//            return Json(jsonSessionList, JsonRequestBehavior.AllowGet);
//        }

//        public JsonResult GetAllActiveTNAs_Select()
//        {

//            SelectBoxResponse Response = new SelectBoxResponse();
//            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
//            try
//            {
//                var TNAs = TrainingLeadRepo.GetAllActiveTNAs();

//                if (TNAs != null)
//                {

//                    foreach (var item in TNAs)
//                    {
//                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
//                        SelectSingle.ID = item.ID;
//                        SelectSingle.Value = item.TNAName;
//                        SelectList.Add(SelectSingle);
//                    }

//                    Response.Status = true;
//                    Response.Message = "TNA Record Found.";
//                    Response.data = SelectList;
//                    return Json(Response, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    throw new Exception("No TNA Record Found.");
//                }

//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;
//                Response.data = null;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public JsonResult GetAllActiveTrainings_Select(int ID)
//        {

//            SelectBoxResponse Response = new SelectBoxResponse();
//            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
//            try
//            {
//                var TNAsList = TrainingLeadRepo.GetAllActiveTrainings_byTNAID(Convert.ToInt32(ID));

//                if (TNAsList != null)
//                {

//                    foreach (var item in TNAsList)
//                    {
//                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
//                        SelectSingle.ID = item.ID;
//                        SelectSingle.Value = item.TrainingName;
//                        SelectList.Add(SelectSingle);
//                    }

//                    Response.Status = true;
//                    Response.Message = "TNA Record Found.";
//                    Response.data = SelectList;
//                    return Json(Response, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    throw new Exception("No TNA Record Found.");
//                }

//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;
//                Response.data = null;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public JsonResult GetAllTrainers_Select()
//        {

//            SelectBoxResponse Response = new SelectBoxResponse();
//            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
//            try
//            {
//                var Trainers = TrainingLeadRepo.GetAllTrainers();

//                if (Trainers != null)
//                {

//                    foreach (var item in Trainers)
//                    {
//                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
//                        SelectSingle.ID = item.ID;
//                        SelectSingle.Value = item.FullName;
//                        SelectList.Add(SelectSingle);
//                    }

//                    Response.Status = true;
//                    Response.Message = "Trainers Record Found.";
//                    Response.data = SelectList;
//                    return Json(Response, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    throw new Exception("No Trainers Record Found.");
//                }

//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;
//                Response.data = null;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public JsonResult GetAllAreaGeography_Select()
//        {

//            SelectBoxResponse Response = new SelectBoxResponse();
//            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
//            try
//            {
//                var areas = TrainingLeadRepo.GetAllGeography();

//                if (areas != null)
//                {

//                    foreach (var item in areas)
//                    {
//                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
//                        SelectSingle.ID = item.ID;
//                        SelectSingle.Value = item.Geography;
//                        SelectList.Add(SelectSingle);
//                    }

//                    Response.Status = true;
//                    Response.Message = "Geography Record Found.";
//                    Response.data = SelectList;
//                    return Json(Response, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    throw new Exception("No Geography Record Found.");
//                }

//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;
//                Response.data = null;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }

//        public ActionResult DeleteSession(int ID)
//        {
//            ResponseHeader Response = new ResponseHeader();

//            try
//            {

//                if (ID > 0)
//                {
//                    var sessionParticipants = TrainingLeadRepo.GetAllParticipants_bySession(Convert.ToInt32(ID));

//                    if (sessionParticipants.Rows.Count > 0)
//                        throw new Exception("Delete Participents of this Session first.");

//                    var sessionRecord = TrainingLeadRepo.GetSession_byID(Convert.ToInt32(ID));

//                    if (sessionRecord.StartDate != null)
//                    {
//                        var strd1 = vt_Common.ConvertDateString(DateTime.Now.ToString()).ToString("dd/MM/yyyy"); //Current DateTime
//                        var strd2 = vt_Common.ConvertDateString(sessionRecord.StartDate.ToString()).ToString("dd/MM/yyyy"); //Session Start DateTime
//                        var d1 = Convert.ToDateTime(strd1);
//                        var d2 = Convert.ToDateTime(strd2);

//                        //Check Session StartDate is passed date with Todays date
//                        if (d2 <= d1)
//                        {
//                            throw new Exception("Session cannot be delete.");
//                        }
//                        else
//                        {
//                            var DelSession = TrainingLeadRepo.DeleteSession(Convert.ToInt32(ID));
//                        }

//                        //var d2 = vt_Common.ConvertDateString(sessionRecord.StartDate.ToString()).ToString("mm/DD/yyyy"); //Session Start DateTime
//                        //d2 = d2.ToShortDateString("mm/DD/yyyy");
//                        //var d1 = vt_Common.GetCurrentDateTime();  //Current DateTime
//                        //var d2 = (DateTime)sessionRecord.StartDate; //Session Start DateTime

//                        //if (d2 <= d1)
//                        //{
//                        //    throw new Exception("Session cannot be delete.");
//                        //}
//                        //else
//                        //{
//                        //    var DelSession = TrainingLeadRepo.DeleteSession(Convert.ToInt32(ID));
//                        //}
//                    }
//                    else
//                        throw new Exception("Session Record not found.");



//                    Response.Status = true;
//                    Response.Message = "Session Deleted Successfully.";

//                    return Json(Response, JsonRequestBehavior.AllowGet);

//                }
//                else
//                {
//                    throw new Exception("ID can not be null.");
//                }
//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }

//        }

//        public ActionResult CreateSession(SessionForm Header)
//        {
//            ResponseHeader Response = new ResponseHeader();
//            try
//            {
//                if (ModelState.IsValid)
//                { 

//                    var Session = TrainingLeadRepo.AddUpdateSession(Header);


//                    Response.Status = true;
//                    Response.Message = "Session Added Successfully.";

//                }
//                else
//                {
//                    throw new Exception("Enter valid fields");
//                }

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }


//        public ActionResult EditSession(int ID)
//        {
//            SessionForm Session = new SessionForm();

//            try
//            {
//                if (ID > 0)
//                {
//                    var vtSession = TrainingLeadRepo.GetSession_byID(ID);

//                    //if (vtSession != null)
//                    //{
//                        Session = vtSession;

//                    //}
//                    //else
//                    //{
//                    //    Session = null;
//                    //}

//                }
//                else
//                {
//                    Session.Status = false;
//                    Session.Message = "SessionID can't be null";
//                }


//                return PartialView("_SessionForm", Session);
//            }
//            catch (Exception ex)
//            {
//                Session.Status = false;
//                Session.Message = ex.Message;
//                return PartialView("_SessionForm", Session);
//            }
//        }

//        public ActionResult UpdateSession(SessionForm Header)
//        {
//            ResponseHeader Response = new ResponseHeader();
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    if (Header.ID > 0)
//                    {
//                        var Session = TrainingLeadRepo.AddUpdateSession(Header);

//                        Response.Status = true;
//                        Response.Message = "Session Updated Successfully.";
//                    }
//                    else
//                    {
//                        Response.Status = false;
//                        Response.Message = "SessionID can't be null.";
//                    }

//                }
//                else
//                {
//                    throw new Exception("Enter valid fields");
//                }

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//            catch (Exception ex)
//            {
//                Response.Status = false;
//                Response.Message = ex.Message;

//                return Json(Response, JsonRequestBehavior.AllowGet);
//            }
//        }
//        #endregion

//        #region Session Assigned Users

//        [HttpGet]
//        public ActionResult AssignedSessionUsers(int ID)
//        {
//            return View();
//        }

//        public JsonResult GetSessionAssingedUsers()
//       {
//            List<AssignedUserGrid> UserList = new List<AssignedUserGrid>();
//            try
//            {
//                var url_array = Request.UrlReferrer.ToString().Split('/');
//                var sessionID = Convert.ToInt32(url_array.LastOrDefault());

//                UserList = TrainingLeadRepo.GetAllSessionsAssignedUsers(sessionID);

//                if (UserList != null)
//                {
//                    return Json(new { Status = true, Message = "Users Found.", DataList = UserList }, JsonRequestBehavior.AllowGet);
//                }
//                else
//                {
//                    return Json(new { Status = false, Message = "Users Not Found.", DataList = UserList }, JsonRequestBehavior.AllowGet);
//                }


//            }
//            catch (Exception ex)
//            {
//                return Json(new { Status = false, Message = ex.Message, DataList = UserList }, JsonRequestBehavior.AllowGet);
//            }


//        }

//        #endregion

//    }
//} 