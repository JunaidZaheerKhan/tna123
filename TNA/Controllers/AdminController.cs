﻿using TNA.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static TNA_ViewModel.Model.TNA_VM;
using BAL.Repository;
using DAL.DBEntities;
using System.Data;
using System.IO;
using TNAApp_ViewModel.Model;
using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;
using System.Configuration;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace TNA.Controllers
{
    public class AdminController : BaseController
    {
       
        AdminRepository AdminRepo;
        LoginRepository LoginRepo;
        public AdminController()
        {
            AdminRepo = new AdminRepository(new vt_TNAEntity());
            LoginRepo = new LoginRepository(new vt_TNAEntity());
        }

        #region Dashboard
        public ActionResult getTraining()
        {
            try
            {
                var dataList = AdminRepo.getTraining();
            
                if (dataList!=null)
                {
                    var data = JsonConvert.SerializeObject(dataList);
                    return Json(new { Status = true, Message = "Data found.", dataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", dataList = "" }, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, dataList="" }, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult TrainingCalendar()
        {
            return View();
        }
        public ActionResult getTrainingCalendar()
        {

            System.Data.DataTable dt = new System.Data.DataTable();

            try
            {
                var TrainerID = Convert.ToInt32(CurrentUser.User.ID);
                dt = AdminRepo.GetDataforTrainingCalendar();
                var data = JsonConvert.SerializeObject(dt);
                if (dt != null)
                {
                    return Json(new { Status = true, Message = "Data found.", DataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", DataList = data }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, DataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetCounterValue()
        {
            try
            {
                var result = AdminRepo.GetCounterValue();
                if (result.Rows.Count>0)
                {
                    var data = JsonConvert.SerializeObject(result);
                    return Json(new { Status = true, Message = "Record found.", data = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Null", data = "" }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = "Record not found.", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        
        public ActionResult CurrentMonthTraining()
        {
            try
            {
                var dataList = AdminRepo.CurrentMonthTraining();

                if (dataList != null)
                {
                    var data = JsonConvert.SerializeObject(dataList);
                    return Json(new { Status = true, Message = "Data found.", dataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", dataList = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, dataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult CurrentMonthSession()
        {
            try
            {
                var dataList = AdminRepo.CurrentMonthSession();

                if (dataList != null)
                {
                    var data = JsonConvert.SerializeObject(dataList);
                    return Json(new { Status = true, Message = "Data found.", dataList = data }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data not found.", dataList = "" }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, dataList = "" }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        public ActionResult GetLineManagers() {
            return View();
        }
        public ActionResult GetHeirarchy() {
            var Result = string.Empty;
            try
            {
                var dt = AdminRepo.GetHeirarchy();
                if (dt.Rows.Count>0)
                {
                    Result = JsonConvert.SerializeObject(dt);
                    return Json(new {data= Result, Status=true, Message="Record found." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("Record not found.");
                }
            }
            catch (Exception ex)
            {

                return Json(new { data = Result, Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
     
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }


        #region Topic Page Functionality

        public ActionResult Topics()
        {
            return View();
        }

        public JsonResult GetAllDepartments()
        {
            var DepartList = AdminRepo.GetAllDeparts();

            return Json(new { DepartList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NewTopicForm()
        {
            //ViewBag.selectGroupList = new SelectList(GetAllGroupNames(), "GroupId", "Name");
            try
            {
                return PartialView("_TopicForm", new TopicForm());
            }
            catch (Exception ex)
            {
                return PartialView("_TopicForm", new TopicForm());
            }
        }


        public JsonResult GetAllTopics()
        {
            var TopicList = AdminRepo.GetAllTopics();

            return Json(TopicList, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddTopic(TopicForm Header)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ModelState.IsValid)
                {
                    if (Header.Departments == null)
                        throw new Exception("Select any one department against a topic.");

                    var Topic = AdminRepo.AddTopic(Header);
                    string ID = AdminRepo.GetLastTopicID();

                    TopicDepartments topicDeparts = new TopicDepartments();
                    topicDeparts.TopicID = Convert.ToInt32(ID);
                    topicDeparts.DepartmentID = Header.Departments;

                    if (topicDeparts.DepartmentID.Length > 0)
                    {


                        for (int i = 0; i < topicDeparts.DepartmentID.Length; i++)
                        {
                            System.Data.DataTable dtConversion = AdminRepo.AddNewTopicJunc(topicDeparts.TopicID, topicDeparts.DepartmentID[i]);

                        }
                    }

                    Response.Status = true;
                    Response.Message = "Topic added successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("Please enter valid fields.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EditTopic(int ID)
        {
            TopicForm Topic = new TopicForm();

            try
            {
                if (ID > 0)
                {
                    var vtTopic = AdminRepo.GetTopic_byID(ID);
                    if (vtTopic != null)
                    {
                        Topic = vtTopic;
                        var TopicDeparts = AdminRepo.GetTopicDeparts_byID(ID);
                        if (TopicDeparts != null)
                        {
                            Topic.Departments = TopicDeparts;
                            Topic.Status = true;
                            Topic.Message = "Topic load successfully.";
                        }
                        else
                        {
                            Topic.Departments = null;
                        }
                    }

                }
                else
                {
                    Topic.Status = false;
                    Topic.Message = "Topic not load successfully.";
                }


                return PartialView("_TopicForm", Topic);
            }
            catch (Exception ex)
            {
                Topic.Status = false;
                Topic.Message = ex.Message;
                return PartialView("_TopicForm", Topic);
            }
        }

        public ActionResult UpdateTopic(TopicForm Header)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {
                var serialize = JsonConvert.SerializeObject(Header);
                vt_Common.CreateAuditLog(CurrentUser.User.ID, "EditTopic", serialize, "vt_Topic");

                if (ModelState.IsValid)
                {
                    if (Header.Departments == null)
                        throw new Exception("Select any one department against a topic.");

                    var Topic = AdminRepo.UpdateTopic(Header);
                    var deletedDeparts = AdminRepo.DeleteAllTopicDeparts(Header.ID);

                    TopicDepartments topicDeparts = new TopicDepartments();
                    topicDeparts.TopicID = Convert.ToInt32(Header.ID);
                    topicDeparts.DepartmentID = Header.Departments;

                    if (topicDeparts.DepartmentID.Length > 0)
                    {


                        for (int i = 0; i < topicDeparts.DepartmentID.Length; i++)
                        {
                            System.Data.DataTable dtConversion = AdminRepo.AddNewTopicJunc(topicDeparts.TopicID, topicDeparts.DepartmentID[i]);

                        }
                    }

                    Response.Status = true;
                    Response.Message = "Topic updated successfully.";
                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("Please enter valid fields.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeleteTopic(int ID)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ID > 0)
                {

                    var Topic = AdminRepo.DeleteTopic(ID);

                    Response.Status = true;
                    Response.Message = "Topic deleted successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("ID can not be null.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Training Pages Functionality

        public ActionResult Trainings()
        {
            //Training List Page
            return View();
        }

        public ActionResult NewTrainingForm()
        {
            return PartialView("_TrainingForm", new TrainingForm());
        }

        public JsonResult GetAllTopics_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var topics = AdminRepo.GetAllActiveTopics();

                if (topics != null)
                {

                    foreach (var item in topics)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.TopicName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Topic record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No topic record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetAllTypes_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var types = AdminRepo.GetAllTypes();

                if (types != null)
                {

                    foreach (var item in types)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.TrainingType;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Type record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No type record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllCourseOwner_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                List<vt_UserProfile> course_owner = AdminRepo.GetAllCourseOwner();

                if (course_owner != null)
                {

                    foreach (var item in course_owner)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.FullName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Couser owner record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No couser owner record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllModes_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var modes = AdminRepo.GetAllModes();

                if (modes != null)
                {

                    foreach (var item in modes)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.ModeOfTraining;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Modes record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No modes record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllCourseMaterial_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var courses = AdminRepo.GetAllCourseMaterial();

                if (courses != null)
                {

                    foreach (var item in courses)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.CourseMaterial;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Course owner record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No course owner record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

       

        public ActionResult AddTraining()
        {
            return View("AddTraining", new TrainingForm());
        }

        public JsonResult UploadDocuments()
        {
            ResponseHeader Reponse = new ResponseHeader();
            try
            {
                
                string FileURL = "";
                HttpFileCollectionBase attachments = Request.Files;
                string[] BindFiles = new string[attachments.Count];

                string guid = Guid.NewGuid().ToString();
                int userid =Convert.ToInt32(CurrentUser.User.ID);
                // string ImageName = ;

                //string dir = ConfigurationManager.AppSettings["HostDomain"].ToString()+"/Images/";
                //string dir = string.Empty;
                //dir = Server.MapPath("~/"+ vt_Common.GetPathPrefix() +"/Attachments/");
                //ErrorHandling.WriteError(dir);
                string dir = Server.MapPath("~/Attachments/");
                //ErrorHandling.WriteError(dir);

                //string dir = @"C:\inetpub\wwwroot\TNA\Attachments";

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                for (int i = 0; i < attachments.Count; i++)
                {
                    ErrorHandling.WriteError(attachments[i].FileName);
                    var filesplit = attachments[i].FileName.Split('.');
                    var filenamesplit = filesplit.FirstOrDefault().Split('/');

                    var filenamewithextension = Path.GetFileName(attachments[i].FileName);
                    var fname = filenamewithextension.Split('.');
                    var extension = Path.GetExtension(attachments[i].FileName);


                    var currenttime = DateTime.Now.ToString().Replace(" ", "").Replace("/", "-").Replace(":", "-");
                    //var filename = filesplit.LastOrDefault() + "_" + userid + "_" + currenttime + "." + filesplit[1];
                    //var filename = attachments[i] + "_" + userid + "_" + currenttime + "." + filesplit.LastOrDefault();
                    //var filename = fname + "_" + userid + "_" + currenttime + "." + extension;
                    var filename = fname[0] + "_" + userid + "_" + currenttime + extension;
                    ErrorHandling.WriteError(filename);
                    //filename = vt_Common.ReplaceSpecialCharacters(filename);
                    BindFiles[i] = filename;
                    FileURL = Path.Combine(dir, filename);
                    HttpPostedFileBase file = attachments[i];
                    ErrorHandling.WriteError(FileURL);
                    file.SaveAs(FileURL);
                }



                //Reponse.Status = true;
                //Reponse.Message = "Document Uploaded Susseccfully.";
                return Json(new { Status = true , Message = "Document uploaded susseccfully.", filesArray = BindFiles }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Reponse.Status = false;
                //Reponse.Message = ex.Message;
                return Json(new { Status = false, Message = "Document not uploaded susseccfully.", filesArray = "" }, JsonRequestBehavior.AllowGet);
            }

            
        }

        public ActionResult CreateTraining(TrainingForm Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Header.CourseMaterial == null)
                        throw new Exception("Select any one course material against a training.");
                    //if (Header.Attachments == null)
                    //    throw new Exception("Select any one attachment against a training.");

                    Header.CreatedBy = CurrentUser.User.ID;
                    var Training = AdminRepo.AddTraining(Header);
                    string TrainingID = AdminRepo.GetLastTrainingID();

                    TrainingsAttachments trainingAttach = new TrainingsAttachments();
                    trainingAttach.TrainingID = Convert.ToInt32(TrainingID);

                    if (Header.Attachments!=null)
                    {
                        //HttpFileCollectionBase[] file = Header.filesattachment;
                        //var files = file;

                        for (int i = 0; i < Header.Attachments.Length; i++)
                        {
                            //var files = Request.Files;
                            string dir = Server.MapPath("~/Attachments/");
                           // string filename = vt_Common.ReplaceSpecialCharacters(Header.Attachments[i]);
                            string filename = Header.Attachments[i].ToString();
                            string[] files = filename.Split('.');
                            trainingAttach.Attachment = files[0];
                            trainingAttach.DocType = files[1];

                            trainingAttach.Path = dir + filename;

                            System.Data.DataTable dt = AdminRepo.AddNewTrainingAttachments(trainingAttach);
                        }
                    }

                    if (Header.CourseMaterial.Length > 0)
                    {

                        for (int i = 0; i < Header.CourseMaterial.Length; i++)
                        {
                            TrainingsCourseMaterial CourseMaterial = new TrainingsCourseMaterial();
                            CourseMaterial.TrainingID = Convert.ToInt32(TrainingID);
                            CourseMaterial.CourseMaterialID = Convert.ToInt32(Header.CourseMaterial[i]);

                            System.Data.DataTable dt = AdminRepo.AddNewTrainingCourseMaterial(CourseMaterial);
                        }
                    }


                    Response.Status = true;
                    Response.Message = "Training added successfully.";

                }
                else
                {
                    var error = ModelState.Select(x => x.Value.Errors)
                          .Where(y => y.Count > 0)
                          .FirstOrDefault();
                    throw new Exception(error.FirstOrDefault().ErrorMessage);
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            

        }

        public JsonResult GetAllTrainings()
        {
            List<vt_Trainings> TrainingList = new List<vt_Trainings>();
            try
            {
                TrainingList = AdminRepo.GetAllTrainings();

                if (TrainingList != null)
                {
                    return Json(new { Status = true, Message = "Trainings found.", Trainings = TrainingList }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    TrainingList = null;
                    return Json(new { Status = false, Message = "No trainings found." , Trainings = TrainingList }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                TrainingList = null;
                return Json(new { Status = false, Message = ex.Message, Trainings = TrainingList }, JsonRequestBehavior.AllowGet);
            }

            


            
        }


        public ActionResult DeleteTraining(int ID)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ID > 0)
                {

                    var Training = AdminRepo.DeleteTraining(ID);

                    Response.Status = true;
                    Response.Message = "Training deleted successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("ID can not be null.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EditTraining(int ID)
        {
            TrainingForm Training = new TrainingForm();

            try
            {
                if (ID > 0)
                {
                    var vtTraining = AdminRepo.GetTraining_byID(ID);
                    if (vtTraining != null)
                    {
                        Training = vtTraining;
                        var TrainingAttachments = AdminRepo.GetTrainingAttachments_byID(ID);
                        if (TrainingAttachments != null)
                        {
                            Training.Attachments = TrainingAttachments;                         
                           
                        }
                        else
                        {
                            Training.Attachments = null;
                        }
                        var TrainingCourseMaterial = AdminRepo.GetTrainingCourseMaterial_byID(ID);
                        if (TrainingCourseMaterial != null)
                        {
                            Training.CourseMaterial = TrainingCourseMaterial;
                        }
                        else
                        {
                            Training.CourseMaterial = null;
                        }
                    }

                    Training.Status = true;
                    Training.Message = "Training found successfully.";

                }
                else
                {
                    Training.Status = false;
                    Training.Message = "Training not found successfully.";
                }


                return View("AddTraining",Training);
            }
            catch (Exception ex)
            {
                Training.Status = false;
                Training.Message = ex.Message;
                return View("AddTraining", Training);
            }
        }

        public ActionResult UpdateTraining(TrainingForm Header)
        {
            ResponseHeader Response = new ResponseHeader();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Header.CourseMaterial == null)
                        throw new Exception("Select any one course material against a training.");
                    //if (Header.Attachments == null)
                    //    throw new Exception("Select any one attachment against a training.");

                     var serializeObj = JsonConvert.SerializeObject(Header);
                    vt_Common.CreateAuditLog(CurrentUser.User.ID, "EditTraining", serializeObj, "vt_Trainings");
                    Header.CreatedBy = CurrentUser.User.ID;
                    var Training = AdminRepo.AddTraining(Header);
                    var deleteAttach = AdminRepo.DeleteAttachments_byID(Header.ID);
                    var deleteCourseOwner = AdminRepo.DeleteCourseOwner_byID(Header.ID);
                    //string TrainingID = Header.ID;

                    TrainingsAttachments trainingAttach = new TrainingsAttachments();
                    trainingAttach.TrainingID = Convert.ToInt32(Header.ID);

                    if (Header.Attachments!=null)
                    {
                        //HttpFileCollectionBase[] file = Header.filesattachment;
                        //var files = file;

                        for (int i = 0; i < Header.Attachments.Length; i++)
                        {
                            //var files = Request.Files;
                            string dir = Server.MapPath("~/Attachments/");
                            string filename = Header.Attachments[i].ToString();
                            string[] files = filename.Split('.');
                            trainingAttach.Attachment = files[0];
                            trainingAttach.DocType = files[1];

                            trainingAttach.Path = dir + filename;

                            System.Data.DataTable dt = AdminRepo.AddNewTrainingAttachments(trainingAttach);
                        }
                    }

                    if (Header.CourseMaterial.Length > 0)
                    {

                        for (int i = 0; i < Header.CourseMaterial.Length; i++)
                        {
                            TrainingsCourseMaterial CourseMaterial = new TrainingsCourseMaterial();
                            CourseMaterial.TrainingID = Convert.ToInt32(Header.ID);
                            CourseMaterial.CourseMaterialID = Convert.ToInt32(Header.CourseMaterial[i]);

                            System.Data.DataTable dt = AdminRepo.AddNewTrainingCourseMaterial(CourseMaterial);
                        }
                    }


                    Response.Status = true;
                    Response.Message = "Training updated successfully.";

                }
                else
                {
                  var error=  ModelState.Select(x => x.Value.Errors)
                          .Where(y => y.Count > 0)
                          .FirstOrDefault();
                    throw new Exception(error.FirstOrDefault().ErrorMessage);
                }

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }


        }

        #endregion

        #region User Management Page

        public ActionResult UserManagement()
        {
            //User Management Page
            return View();
        }

        public ActionResult NewUserForm()
        {
            return PartialView("_UserForm",new UserForm());
            
        }

        public JsonResult GetAllRoles_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var roles = AdminRepo.GetAllRoles();

                if (roles != null)
                {

                    foreach (var item in roles)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.RoleName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Role record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No role record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllDepartments_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var departs = AdminRepo.GetAllDepartments();

                if (departs != null)
                {

                    foreach (var item in departs)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.DepartmentName;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Department record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No department record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetAllAreaGeography_Select()
        {

            SelectBoxResponse Response = new SelectBoxResponse();
            List<SelectBoxProperties> SelectList = new List<SelectBoxProperties>();
            try
            {
                var areas = AdminRepo.GetAllGeography();

                if (areas != null)
                {

                    foreach (var item in areas)
                    {
                        SelectBoxProperties SelectSingle = new SelectBoxProperties();
                        SelectSingle.ID = item.ID;
                        SelectSingle.Value = item.Geography;
                        SelectList.Add(SelectSingle);
                    }

                    Response.Status = true;
                    Response.Message = "Geography record found.";
                    Response.data = SelectList;
                    return Json(Response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("No geography record found.");
                }

            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                Response.data = null;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult ValidateUser(string LoginID)
        {
            HarmonyUser Response = new HarmonyUser();

            try
            {
                bool AuthenticUser = false;
                if (ConfigurationManager.AppSettings["IsLive"].ToString().Contains("1"))
                {
                   AuthenticUser = vt_Common.ValidateUser("", "", "", LoginID.Trim());
                }
                else
                {
                    AuthenticUser = true;
                }
                if (AuthenticUser)
                {

                    var HarmonyUser = AdminRepo.GetHarmonyUser(LoginID);

                    if (HarmonyUser != null)
                    {
                        Response.EmployeeID = HarmonyUser.EmployeeID;
                        Response.DepartmentID = HarmonyUser.DepartmentID;
                        Response.FullName = HarmonyUser.FullName;
                        Response.Email = HarmonyUser.Email;
                        Response.LineManagerName = HarmonyUser.LineManagerName;
                        Response.Location = HarmonyUser.Location;
                    }


                    Response.Status = AuthenticUser;
                    Response.Message = "AD valid user";
                }
                else
                {
                    Response.Status = AuthenticUser;
                    Response.Message = "AD invalid user";
                }


                return Json(Response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;
                return Json(Response, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetAllUsers()
        {
          
            try
            {
                var UsersList = AdminRepo.GetAllUsers();
                var serilized = JsonConvert.SerializeObject(UsersList);
               

                if (UsersList.Rows.Count >0)
                {
                    return Json(new { Status = true, Message = "Users found.", Users = serilized }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    UsersList = null;
                    return Json(new { Status = false, Message = "No users found.", Users = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
             
                return Json(new { Status = false, Message = ex.Message, Users = "" }, JsonRequestBehavior.AllowGet);
            }





        }

        public ActionResult AddUser(UserForm Header)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ModelState.IsValid)
                {
                    //if (Header.RoleID == 4)
                    //{
                    //    if (Header.Geography == null)
                    //        throw new Exception("Geography is not selected.");
                    //}

                    //Check User Exsist
                    var UserExsist = LoginRepo.UserExsist(Header.LoginID);

                    if (Header.RoleID==0)
                    {
                        throw new Exception("Role is Required.");
                    }
                    if (UserExsist != null )
                    {
                        throw new Exception("User already exist.");
                    }
                    else
                    {
                        
                        if (Header.RoleID==3 || Header.RoleID == 6)
                        {
                            var LineManagerExsist = LoginRepo.LineManagerExsist(Header.Location,Header.RoleID);
                            if (LineManagerExsist.Rows.Count>0)
                            {
                                throw new Exception("There is another line manager of same location is exist.");
                            }

                            else
                            {
                                var User = AdminRepo.AddUpdateUser(Header);
                                string ID = AdminRepo.GetLastUserID();

                                UserGeography geography = new UserGeography();
                                geography.UserID = Convert.ToInt32(ID);
                            }
                            
                        }
                        else
                        {
                            var User = AdminRepo.AddUpdateUser(Header);
                            string ID = AdminRepo.GetLastUserID();

                            UserGeography geography = new UserGeography();
                            geography.UserID = Convert.ToInt32(ID);
                        }
                     
                       

                        //if (Header.RoleID == 4)
                        //{
                        //    for (int i = 0; i < Header.Geography.Length; i++)
                        //    {
                        //System.Data.DataTable dtConversion = AdminRepo.AddNewGeographyJunc(geography.UserID, Header.Geography[i]);

                        //    }
                        //}
                    }


                    


                    Response.Status = true;
                    Response.Message = "User added successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("Please enter valid fields.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult EditUser(int ID)
        {
            UserForm User = new UserForm();

            try
            {
                if (ID > 0)
                {
                    var vtUser = AdminRepo.GetUser_byID(ID);
                    if (vtUser != null)
                    {
                        User = vtUser;
                        var UserGeo = AdminRepo.GetUserGeography_byID(ID);
                        if (UserGeo != null)
                        {
                            User.Geography = UserGeo;
                            User.Status = true;
                            User.Message = "User load successfully.";
                        }
                        else
                        {
                            User.Geography = null;
                        }
                    }

                }
                else
                {
                    User.Status = false;
                    User.Message = "Topic not load successfully.";
                }


                return PartialView("_UserForm", User);
            }
            catch (Exception ex)
            {
                User.Status = false;
                User.Message = ex.Message;
                return PartialView("_UserForm", User);
            }
        }

        public ActionResult UpdateUser(UserForm Header)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ModelState.IsValid)
                {
                    if (Header.ID > 0)
                    {
                        //if (Header.RoleID == 4)
                        //{
                        //    if (Header.Geography.Length == 0)
                        //        throw new Exception("geography is not selected.");
                        //}

                        var User = AdminRepo.AddUpdateUser(Header);
                        var delUser = AdminRepo.DeleteAllUserGeography(Convert.ToInt32(Header.ID));
                        //string ID = Header.ID;

                        UserGeography geography = new UserGeography();
                        geography.UserID = Convert.ToInt32(Header.ID);

                        //if (Header.RoleID == 4)
                        //{
                        //    for (int i = 0; i < Header.Geography.Length; i++)
                        //    {
                        //        System.Data.DataTable dtConversion = AdminRepo.AddNewGeographyJunc(geography.UserID, Header.Geography[i]);

                        //    }
                        //}


                        Response.Status = true;
                        Response.Message = "User updated successfully.";

                        return Json(Response, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        throw new Exception("UserID can't be empty.");
                    }
                    

                }
                else
                {
                    throw new Exception("Please enter valid fields.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DeleteUser(int ID)
        {
            ResponseHeader Response = new ResponseHeader();

            try
            {

                if (ID > 0)
                {

                    var User = AdminRepo.DeleteUser(ID);

                    Response.Status = true;
                    Response.Message = "User deleted successfully.";

                    return Json(Response, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    throw new Exception("ID can not be null.");
                }
            }
            catch (Exception ex)
            {
                Response.Status = false;
                Response.Message = ex.Message;

                return Json(Response, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Upload Hermony Data Page

        public ActionResult Upload()
        {
            ResponseHeader response = new ResponseHeader();
            return View(response);
        }

       
        //[HttpPost]
        //public ActionResult Upload(HttpPostedFileBase excelUpload)
        //{
           
        //    try
        //    {
               
        //        string fileExtension = Path.GetExtension(excelUpload.FileName.ToString());
        //        if (fileExtension == ".xls" || fileExtension == ".xlsx" || fileExtension == ".csv")
        //        {

        //            var dt = AdminRepo.GetHeirarchy();
        //            if (dt.Rows.Count > 0)
        //            {
        //                 var output = AdminRepo.UpdateHeirarchy();
        //            }
                        
        //                    var fileName = Path.GetFileName(excelUpload.FileName);
        //                    var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);
        //                    excelUpload.SaveAs(path);

        //            Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
        //                    DataRow myNewRow;
        //                    System.Data.DataTable dt_Heirarchy;


        //                    Workbook excelBook = excelApp.Workbooks.Open(path);
        //                    _Worksheet excelSheet = excelBook.Sheets[1];
        //                    Range excelRange = excelSheet.UsedRange;

        //                    int rows = excelRange.Rows.Count;
        //                    int cols = excelRange.Columns.Count;

        //                    dt_Heirarchy = new System.Data.DataTable("MyDataTable");

        //                    dt_Heirarchy.Columns.Add("Line Manager", typeof(int)); //For EmpID
        //                    dt_Heirarchy.Columns.Add("Line Manger Name", typeof(string)); //For Name
        //                    dt_Heirarchy.Columns.Add("Emp. ID", typeof(int)); //For ReporteeID
        //                    dt_Heirarchy.Columns.Add("Name", typeof(string)); //For ReporteeName
        //                    dt_Heirarchy.Columns.Add("Unit", typeof(string)); // For DepartmentName
        //                    dt_Heirarchy.Columns.Add("Description", typeof(string)); //For Department
        //                    dt_Heirarchy.Columns.Add("NetworkID", typeof(string)); //For Login ID
        //                    dt_Heirarchy.Columns.Add("Email", typeof(string));
        //                    dt_Heirarchy.Columns.Add("Grade", typeof(string));
        //                    dt_Heirarchy.Columns.Add("Designation", typeof(string));
        //                    dt_Heirarchy.Columns.Add("Location", typeof(string));
        //                    dt_Heirarchy.Columns.Add("Org. Unit", typeof(string));
        //                    dt_Heirarchy.Columns.Add("Division", typeof(string));
        //                    dt_Heirarchy.Columns.Add("IsUpdate", typeof(bool));
        //                    dt_Heirarchy.Columns.Add("IsActive", typeof(bool));

        //                    for (int i = 2; i <= rows; i++)
        //                    {
        //                        myNewRow = dt_Heirarchy.NewRow();
        //                        myNewRow[0] = Convert.ToInt32(Convert.ToString(excelRange.Cells[i, 7].Value2));
        //                        myNewRow[1] = Convert.ToString(excelRange.Cells[i, 8].Value2);
        //                        myNewRow[2] = Convert.ToInt32(Convert.ToString(excelRange.Cells[i, 3].Value2));
        //                        myNewRow[3] = Convert.ToString(excelRange.Cells[i, 4].Value2);
        //                        myNewRow[4] = Convert.ToString(excelRange.Cells[i, 12].Value2);
        //                        myNewRow[5] = Convert.ToString(excelRange.Cells[i, 2].Value2);
        //                        myNewRow[6] = Convert.ToString(excelRange.Cells[i, 5].Value2);
        //                        myNewRow[7] = Convert.ToString(excelRange.Cells[i, 6].Value2);
        //                        myNewRow[8] = Convert.ToString(excelRange.Cells[i, 9].Value2);
        //                        myNewRow[9] = Convert.ToString(excelRange.Cells[i, 10].Value2);
        //                        myNewRow[10] = Convert.ToString(excelRange.Cells[i, 11].Value2);
        //                        myNewRow[11] = Convert.ToString(excelRange.Cells[i, 1].Value2);
        //                        myNewRow[12] = Convert.ToString(excelRange.Cells[i, 13].Value2);
        //                        myNewRow[13] = Convert.ToBoolean(true);
        //                        myNewRow[14] = Convert.ToBoolean(true);
        //                        dt_Heirarchy.Rows.Add(myNewRow);
        //                    }

        //                    System.Data.DataTable distinctDepartment = dt_Heirarchy.DefaultView.ToTable(true, "Unit");


        //                    for (int i = 0; i < distinctDepartment.Rows.Count; i++)
        //                    {
        //                        System.Data.DataTable DepartExsist = AdminRepo.CheckDepartmentExsist(distinctDepartment.Rows[i]["Unit"].ToString());

        //                        if (DepartExsist.Rows.Count > 0)
        //                        {
                                    
        //                        }
        //                        else
        //                        {
        //                            var AddDepart = AdminRepo.AddDepartment(distinctDepartment.Rows[i]["Unit"].ToString());
        //                        }
        //                    }


        //                    for (int i = 0; i < dt_Heirarchy.Rows.Count; i++)
        //                    {

        //                        HarmonyUsers user = new HarmonyUsers();
        //                        user.EmpID = Convert.ToInt32(dt_Heirarchy.Rows[i]["Line Manager"].ToString());
        //                        user.Name = dt_Heirarchy.Rows[i]["Line Manger Name"].ToString();
        //                        user.ReporteeID = Convert.ToInt32(dt_Heirarchy.Rows[i]["Emp. ID"].ToString());
        //                        user.ReporteeName = dt_Heirarchy.Rows[i]["Name"].ToString();
        //                        user.DepartmentName = dt_Heirarchy.Rows[i]["Unit"].ToString();
        //                        user.Department = dt_Heirarchy.Rows[i]["Description"].ToString();
        //                        user.LoginID = dt_Heirarchy.Rows[i]["NetworkID"].ToString();
        //                        user.Email = dt_Heirarchy.Rows[i]["Email"].ToString();
        //                        user.Grade = dt_Heirarchy.Rows[i]["Grade"].ToString();
        //                        user.Designation = dt_Heirarchy.Rows[i]["Designation"].ToString();
        //                        user.Location = dt_Heirarchy.Rows[i]["Location"].ToString();
        //                        user.OrgUnit = dt_Heirarchy.Rows[i]["Org. Unit"].ToString();
        //                        user.Division = dt_Heirarchy.Rows[i]["Division"].ToString();
        //                        user.IsUpdate=Convert.ToBoolean(dt_Heirarchy.Rows[i]["IsUpdate"]);
        //                        user.IsActive = Convert.ToBoolean(dt_Heirarchy.Rows[i]["IsActive"]);

        //                            if (dt.Rows.Count > 0)
        //                            {
        //                                System.Data.DataTable DepartExsist = AdminRepo.CheckEmployeeExsist(user.LoginID);

        //                                if (DepartExsist.Rows.Count == 0)
        //                                {
        //                                    user.ID = 0;
        //                                    var AddDepart = AdminRepo.AddUpdateEmployee(user);
        //                                }
        //                                else
        //                                {
        //                                    user.ID = Convert.ToInt32(DepartExsist.Rows[0]["ID"].ToString());
        //                                    var UpdateDepart = AdminRepo.AddUpdateEmployee(user);
        //                                }
        //                            }

        //                            else
        //                            {
        //                                user.ID = 0;
        //                                var AddDepart = AdminRepo.AddUpdateEmployee(user);
        //                            }

        //                            var IsActiveFalse = AdminRepo.SetIsActiveFalseRemaining();

        //                    }

        //            #region Bulk Insert
        //            //con.Open();
        //            //using (SqlBulkCopy bulkDepartment = new SqlBulkCopy(con))
        //            //{
        //            //    bulkDepartment.DestinationTableName =
        //            //        "dbo.vt_Departments";
        //            //    bulkDepartment.ColumnMappings.Add("DepartmentName", "DepartmentName");

        //            //    try
        //            //    {
        //            //        // Write from the source to the destination.
        //            //        bulkDepartment.WriteToServer(distinctDepartment);

        //            //    }
        //            //    catch (Exception ex)
        //            //    {
        //            //        Console.WriteLine(ex.Message);
        //            //    }
        //            //}

        //            //using (SqlBulkCopy bulkHeirarchy = new SqlBulkCopy(con))
        //            //{
        //            //    bulkHeirarchy.DestinationTableName =
        //            //        "dbo.vt_heirarchy";
        //            //    bulkHeirarchy.ColumnMappings.Add("EmpID", "EmpID");
        //            //    bulkHeirarchy.ColumnMappings.Add("Name", "Name");
        //            //    bulkHeirarchy.ColumnMappings.Add("LineManagerID", "LineManagerID");
        //            //    bulkHeirarchy.ColumnMappings.Add("LineManager", "LineManager");
        //            //    bulkHeirarchy.ColumnMappings.Add("ReporteeID", "ReporteeID");
        //            //    bulkHeirarchy.ColumnMappings.Add("ReporteeName", "ReporteeName");
        //            //    bulkHeirarchy.ColumnMappings.Add("DepartmentName", "DepartmentName");
        //            //    bulkHeirarchy.ColumnMappings.Add("DepartmentID", "Department");
        //            //    bulkHeirarchy.ColumnMappings.Add("LoginID", "LoginID");

        //            //    try
        //            //    {
        //            //        // Write from the source to the destination.
        //            //        bulkHeirarchy.WriteToServer(dt_Heirarchy);
        //            //    }
        //            //    catch (Exception ex)
        //            //    {
        //            //        Console.WriteLine(ex.Message);
        //            //    }
        //            //}
        //            //con.Close();

        //            #endregion

        //           // after reading, relaase the excel project
        //                    excelApp.Quit();
        //            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);

        //            ResponseHeader response = new ResponseHeader();
        //                    response.Status = true;
        //                    response.Message = "Employees and departments uploaded successfully.";
        //                    return View(response);
                   
        //        }

        //        else
        //        {
        //            ResponseHeader response = new ResponseHeader();
        //            response.Status = false;
        //            response.Message = "Please choose only excel format file.";
        //            return View(response);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //       // excelApp.Quit();
        //       //System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
        //        ResponseHeader response = new ResponseHeader();
        //        response.Status = false;
        //        response.Message = "Please choose correct file.";
        //        return View(response);
        //    }
            
        //}

        [HttpPost]
        public ActionResult ReadExcelFile()
        {
            try
            {
                var dt = AdminRepo.GetHeirarchy();
                if (dt.Rows.Count > 0)
                {
                    //Set All to IsUpdate=0;
                    var output = AdminRepo.UpdateHeirarchy();
                }
                HttpPostedFileBase files = Request.Files[0]; //Read the Posted Excel File  
                ISheet sheet; //Create the ISheet object to read the sheet cell values  
                string filename = Path.GetFileName(Server.MapPath(files.FileName)); //get the uploaded file name  
                var fileExt = Path.GetExtension(filename); //get the extension of uploaded excel file  
                if (fileExt == ".xls")
                {
                    HSSFWorkbook hssfwb = new HSSFWorkbook(files.InputStream); //HSSWorkBook object will read the Excel 97-2000 formats  
                    sheet = hssfwb.GetSheetAt(0); //get first Excel sheet from workbook  
                }
                else
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(files.InputStream); //XSSFWorkBook will read 2007 Excel format  
                    sheet = hssfwb.GetSheetAt(0); //get first Excel sheet from workbook   
                }

                System.Data.DataTable dt_Heirarchy = new System.Data.DataTable("MyDataTable");
                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
                IRow headerRow = sheet.GetRow(0);
                int cellCount = headerRow.LastCellNum;
                int rowCount = headerRow.RowNum;

                for (int j = 0; j < cellCount; j++)
                {
                    ICell cell = headerRow.GetCell(j);
                    dt_Heirarchy.Columns.Add(Convert.ToString(cell).ToLower().Replace(" ",""));
                    if (j == (cellCount - 1))
                    {
                        dt_Heirarchy.Columns.Add("IsUpdate", typeof(bool));
                        dt_Heirarchy.Columns.Add("IsActive", typeof(bool));
                    }

                }
                for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++)
                {
                    IRow row = sheet.GetRow(i);
                    DataRow dataRow = dt_Heirarchy.NewRow();
                    if (row == null)
                    {
                        break;
                    }
                    for (int j = row.FirstCellNum; j < dt_Heirarchy.Columns.Count; j++)
                    {
                        if (j>13)
                        {
                            dataRow[j] = Convert.ToBoolean(true);
                        }
                        else
                        {
                            dataRow[j] = Convert.ToString(row.GetCell(j)).TrimEnd();
                        }
                       
                    }
                    dt_Heirarchy.Rows.Add(dataRow);
                }

                System.Data.DataTable distinctDepartment = dt_Heirarchy.DefaultView.ToTable(true, "Unit");


                for (int i = 0; i < distinctDepartment.Rows.Count; i++)
                {
                    System.Data.DataTable DepartExsist = AdminRepo.CheckDepartmentExsist(distinctDepartment.Rows[i]["Unit"].ToString());

                    if (DepartExsist.Rows.Count > 0)
                    {

                    }
                    else
                    {
                        var AddDepart = AdminRepo.AddDepartment(distinctDepartment.Rows[i]["Unit"].ToString());
                    }
                }
                List<vt_Heirarchy> listHeirarchy = new List<vt_Heirarchy>();
                for (int i = 0; i < dt_Heirarchy.Rows.Count; i++)
                {
                   
                    vt_Heirarchy user = new vt_Heirarchy();
                    user.EmpID = Convert.ToInt32(dt_Heirarchy.Rows[i]["linemanager"]);
                    user.Name =Convert.ToString(dt_Heirarchy.Rows[i]["linemangername"]);
                    user.ReporteeID = Convert.ToInt32(dt_Heirarchy.Rows[i]["emp.id"]);
                    user.ReporteeName = Convert.ToString(dt_Heirarchy.Rows[i]["name"]);
                    user.DepartmentName = Convert.ToString(dt_Heirarchy.Rows[i]["unit"]);
                    user.Department = Convert.ToString(dt_Heirarchy.Rows[i]["description"]);
                    user.LoginID = Convert.ToString(dt_Heirarchy.Rows[i]["networkid"]);
                    user.Email = Convert.ToString(dt_Heirarchy.Rows[i]["email"]);
                    user.Grade = Convert.ToString(dt_Heirarchy.Rows[i]["grade"]);
                    user.Designation = Convert.ToString(dt_Heirarchy.Rows[i]["designation"]);
                    user.Location = Convert.ToString(dt_Heirarchy.Rows[i]["location"]);
                    user.OrgUnit = Convert.ToString(dt_Heirarchy.Rows[i]["org.unit"]);
                    user.Division = Convert.ToString(dt_Heirarchy.Rows[i]["division"]);
                    user.WorkDayId= Convert.ToString(dt_Heirarchy.Rows[i]["workdayid"]);
                    user.IsUpdate = Convert.ToBoolean(dt_Heirarchy.Rows[i]["isupdate"]);
                    user.IsActive = Convert.ToBoolean(dt_Heirarchy.Rows[i]["isactive"]);

                    if (dt.Rows.Count > 0)
                    {
                      
                        System.Data.DataTable empExist = AdminRepo.CheckEmployeeExsist(user.LoginID);

                        if (empExist.Rows.Count == 0)
                        {
                            user.ID = 0;
                            listHeirarchy.Add(user);
                          //  var AddDepart = AdminRepo.AddUpdateEmployee(user);
                        }
                        else
                        {
                            user.ID = Convert.ToInt32(empExist.Rows[0]["ID"].ToString());
                            AdminRepo.updateEmployee(user);
                        }
                    }

                    else
                    {
                        user.ID = 0;
                        listHeirarchy.Add(user);
                        // var AddDepart = AdminRepo.AddUpdateEmployee(user);
                    }
                   

                }
                AdminRepo.addEmployee(listHeirarchy);
                var AutoUpdateLocationandDept = AdminRepo.AutoUpdateLocationandDept();
                var IsActiveFalse = AdminRepo.SetIsActiveFalseRemaining();
                System.Data.DataTable NewDt = dt_Heirarchy;
                return Json(new { Status = true, Message = "Employees and departments uploaded successfully." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                return Json(new { Status=false,Message="Please select correct file."}, JsonRequestBehavior.AllowGet);
            }
           
           
            
           

           

           
            

           
           
          

            //for (int row = 0; row <= sheet.LastRowNum; row++) //Loop the records upto filled row  
            //{
            //    if (sheet.GetRow(row) != null) //null is when the row only contains empty cells   
            //    {
            //        string value = sheet.GetRow(row).GetCell(0).StringCellValue; //Here for sample , I just save the value in "value" field, Here you can write your custom logics...  
            //    }
            //}
            //return Json(true, JsonRequestBehavior.AllowGet); //return true to display the success message  
        }

        public ActionResult GetLineManagersList()
        {
            var Result = string.Empty;
            try
            {
                var dt = AdminRepo.GetLineManagers();
                if (dt.Rows.Count > 0)
                {
                    Result = JsonConvert.SerializeObject(dt);
                    return Json(new { data = Result, Status = true, Message = "Record found." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    throw new Exception("Record not found.");
                }
            }
            catch (Exception ex)
            {

                return Json(new { data = Result, Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpPost]
        public ActionResult CreateLineManagers(List<LineManagers> model)
        {
            try
            {

                var response = AdminRepo.CreateLineManagers(model);
                return Json(new { Status = true, Message = "Line Managers created successfully." },JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
                
            }
        }
        #endregion

        #region Report
        public ActionResult LogReport()
        {
            return View();
        }

        public JsonResult GetAllLogs()
        {
            var result = "";
            try
            {
                var response = AdminRepo.GetAllLog();
                result = JsonConvert.SerializeObject(response);
                if (response.Rows.Count>0)
                {
                    return Json(new { Status = true, Message = "Logs found.", Logs = result }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    result = null;
                    return Json(new { Status = false, Message = "No log found.", Logs = result }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = null;
                return Json(new { Status = false, Message = ex.Message, Logs = result }, JsonRequestBehavior.AllowGet);
            }





        }

        public ActionResult ParticipantsReport()
        {
            return View();
        }

        public ActionResult GetEmployeeList(int DeptId)
        {
            System.Data.DataTable getListOfUsers = new System.Data.DataTable();
            try
            {


                var get_LoginID = CurrentUser.User.EmployeeID.ToString();

                getListOfUsers = AdminRepo.GetUsersByDeptID(DeptId);

                if (getListOfUsers.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(getListOfUsers);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record not found", data = getListOfUsers }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = getListOfUsers }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult TrainingandDeptWiseReport() {
            return View();
        }
        public ActionResult GetParticipantsWiseReport(int EmpID,int Dept)
        {
            System.Data.DataTable participantReport = new System.Data.DataTable();
            try
            {
                participantReport = AdminRepo.GetParticipantsWiseReport(EmpID,Dept);

                if (participantReport.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(participantReport);
                    return Json(new { Status = true, Message = "Data is available.", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Data is not available.", data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetDeptListbyTrID(int TrainingID)
        {
            System.Data.DataTable getListOfDepts = new System.Data.DataTable();
            try
            {




                getListOfDepts = AdminRepo.GetDeptListbyTrID(TrainingID);

                if (getListOfDepts.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(getListOfDepts);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record not found", data = getListOfDepts }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = getListOfDepts }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetTrainingandDeptWiseReport(int TrainingID,int DeptID)
        {
            System.Data.DataTable data = new System.Data.DataTable();
            try
            {




                data = AdminRepo.GetTrainingandDeptWiseReport(TrainingID,DeptID);

                if (data.Rows.Count > 0)
                {
                    var serializeobj = JsonConvert.SerializeObject(data);
                    return Json(new { Status = true, Message = "Success", data = serializeobj }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "Record not found", data = data }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message, data = data }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult TrainerReport() {
            return View();
        }
        public ActionResult GetAllTrainers() {
            try
            {
               List<vt_UserProfile> TrainerList = AdminRepo.GetAllTrainer();
                string jsonList = JsonConvert.SerializeObject(TrainerList);
                return Json(new { Success = true, Message = "Data Found.", jsonList = jsonList }, JsonRequestBehavior.AllowGet); 
            }
            catch (Exception x)
            {

                return Json(new { Success = false, Message = x.Message, jsonList = "" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetSessionbyTrainerID(int trainerId)
        {
            try
            {
                var TrainerList = AdminRepo.GetSessionbyTrainerID(trainerId);
                string jsonList = JsonConvert.SerializeObject(TrainerList);
                return Json(new { Success = true, Message = "Data Found.", jsonList = jsonList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {

                return Json(new { Success = false, Message = x.Message, jsonList = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CalendarReport()
        {
            return View();
        }
        public ActionResult GetSessionbyStartandEndDate(Nullable<DateTime> Startdate, Nullable<DateTime> EndDate)
        {
            try
            {
                var SessionList = AdminRepo.GetCalendarWiseReport(Startdate,EndDate);
                string jsonList = JsonConvert.SerializeObject(SessionList);
                return Json(new { Success = true, Message = "Data Found.", jsonList = jsonList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception x)
            {

                return Json(new { Success = false, Message = x.Message, jsonList = "" }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Email Management
        public ActionResult EmailManagement()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetCurrentEmailTemplates()
        {
            try
            {
               var EmailTemplates = AdminRepo.GetEmailTemplates();

                if (EmailTemplates.Rows.Count> 0)
                {
                    var jsondata = JsonConvert.SerializeObject(EmailTemplates);
                    return Json(new { Status = true, Message = "Template found.", data = jsondata }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Status = false, Message = "No Template Found.", data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                
                return Json(new { Status = false, Message = ex.Message, data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveEmailTemplate(vt_EmailManagement model)
        {
            try
            {
                if (model.Subject==null)
                {
                    throw new Exception("Subject field is required.");
                }
                if (model.Body==null)
                {
                    throw new Exception("Body field is required.");
                }
                var Emailtemplate = AdminRepo.AddEmailTemplate(model);
                return Json(new { Status = true, Message = "Changes are saved successfully." },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Status = false, Message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
          
        }


        #endregion

        //public ActionResult autoCreateLineManager() {
        //    try
        //    {
        //        var response = AdminRepo.autoCreateLineManager();
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

    }

}
