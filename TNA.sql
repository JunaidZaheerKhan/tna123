USE [vt_TNADB]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetEmployeeDepart] (@input INT)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Depart VARCHAR(250)

    SET @Depart = (SELECT DepartmentName FROM [dbo].[vt_Heirarchy] WHERE ReporteeID =  @input and IsActive=1)

    RETURN @Depart
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeDepartID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetEmployeeDepartID] (@input INT)
RETURNS int
AS BEGIN
    DECLARE @DepartID int
    SET @DepartID =(select ID from [dbo].[vt_Departments] where DepartmentName = (SELECT DepartmentName FROM [dbo].[vt_Heirarchy] WHERE ReporteeID =  @input and IsActive=1))
    RETURN @DepartID
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeEmail]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetEmployeeEmail] (@input INT)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Email VARCHAR(250)

    SET @Email = (SELECT Email FROM [dbo].[vt_Heirarchy] WHERE ReporteeID =  @input and IsActive=1)

    RETURN @Email
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeExist]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[GetEmployeeExist](@input INT)
RETURNS bit
AS BEGIN
    DECLARE @IsActive bit
	declare @count int
    SET @count = (SELECT count(EmployeeID) FROM [dbo].[vt_UserProfile] WHERE EmployeeID =@input and IsActive=1 and Deleted is null)
	if @count > 0
	begin
	set @IsActive=1
	end
	else
	begin
	set @IsActive=0
	end
	
	return @IsActive
	 
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeLocation]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetEmployeeLocation] (@input INT)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @Location VARCHAR(250)

    SET @Location = (SELECT Location FROM [dbo].[vt_Heirarchy] WHERE ReporteeID =  @input and IsActive=1)

    RETURN @Location
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmployeeLoginID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetEmployeeLoginID] (@input INT)
RETURNS VARCHAR(250)
AS BEGIN
    DECLARE @LoginID VARCHAR(250)

    SET @LoginID = (SELECT LoginID FROM [dbo].[vt_Heirarchy] WHERE ReporteeID =  @input and IsActive=1)

    RETURN @LoginID
END
GO
/****** Object:  Table [dbo].[vt_AuditLog]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_AuditLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[ViewName] [varchar](100) NULL,
	[PreviousData] [varchar](8000) NULL,
	[ChangeData] [varchar](8000) NULL,
	[FieldName] [varchar](50) NULL,
	[TrainingID] [varchar](50) NULL,
	[TopicID] [varchar](50) NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_vt_AuditLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_CourseMaterial]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_CourseMaterial](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CourseMaterial] [varchar](100) NULL,
 CONSTRAINT [PK_vt_CourseMaterial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Departments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Departments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [varchar](200) NULL,
 CONSTRAINT [PK_vt_Departments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_EmailManagement]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_EmailManagement](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Subject] [varchar](500) NULL,
	[Body] [text] NULL,
	[EmailType] [int] NULL,
 CONSTRAINT [PK_vt_EmailManagement] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Geography]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Geography](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Geography] [varchar](200) NULL,
 CONSTRAINT [PK_vt_Geography] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_HarmonyUsers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_HarmonyUsers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LoginID] [varchar](100) NULL,
	[Department] [varchar](200) NULL,
	[Designation] [varchar](200) NULL,
	[Email] [varchar](200) NULL,
	[Phone] [int] NULL,
	[DateOfJoining] [datetime] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_vt_HarmonyUsers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Heirarchy]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Heirarchy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EmpID] [int] NULL,
	[Name] [varchar](200) NULL,
	[ReporteeID] [int] NULL,
	[ReporteeName] [varchar](200) NULL,
	[DepartmentName] [varchar](200) NULL,
	[Department] [varchar](70) NULL,
	[LoginID] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Grade] [varchar](100) NULL,
	[Designation] [varchar](100) NULL,
	[Location] [varchar](100) NULL,
	[OrgUnit] [varchar](100) NULL,
	[Division] [varchar](100) NULL,
	[IsUpdate] [bit] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_vt_Heirarchy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_RoleMenu]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_RoleMenu](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NULL,
	[Menu] [varchar](50) NULL,
	[URL] [varchar](100) NULL,
	[Icon] [varchar](80) NULL,
	[MenuOrder] [int] NULL,
	[Grouped] [int] NULL,
 CONSTRAINT [PK_vt_RoleMenu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_RolePages]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_RolePages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NULL,
	[PageName] [varchar](100) NULL,
	[PageURL] [varchar](200) NULL,
 CONSTRAINT [PK_vt_RolePages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Roles]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NULL,
 CONSTRAINT [PK_vt_Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Session]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Session](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TNAID] [int] NULL,
	[TrainingID] [int] NULL,
	[SessionName] [varchar](100) NULL,
	[Multimedia] [bit] NULL,
	[Stationary] [bit] NULL,
	[Hall] [bit] NULL,
	[Other] [varchar](5000) NULL,
	[TrainerID] [int] NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[FeedbackDate] [date] NULL,
	[Venue] [varchar](50) NULL,
	[CostPerHead] [decimal](18, 2) NULL,
	[GeographyID] [int] NULL,
	[Participants] [int] NULL,
	[IsClosed] [bit] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[LineManagerFeedback] [bit] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_vt_Session] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_SessionParticipant]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_SessionParticipant](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SessionID] [int] NULL,
	[TNAID] [int] NULL,
	[TrainingID] [int] NULL,
	[DeptID] [int] NULL,
	[UserID] [int] NULL,
	[Attendence] [bit] NULL,
	[Feedback] [varchar](500) NULL,
	[StartDateTime] [datetime] NULL,
	[EndDateTime] [datetime] NULL,
	[Rate1] [float] NULL,
	[Rate2] [float] NULL,
	[Rate3] [float] NULL,
	[Rate4] [float] NULL,
	[Rate5] [float] NULL,
	[Rate6] [float] NULL,
	[PublishEmail] [bit] NULL,
 CONSTRAINT [PK_vt_SessionParticipant] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_TNA]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_TNA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TNAName] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[LastSubmissionDate] [datetime] NULL,
	[LastDateforLM] [datetime] NULL,
	[Created] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Deleted] [datetime] NULL,
 CONSTRAINT [PK_vt_TNA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_TNADetails]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vt_TNADetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TNAID] [int] NULL,
	[TrainingID] [int] NULL,
	[DepartmentID] [int] NULL,
	[CreatedAt] [datetime] NULL,
	[IsActive] [bit] NULL,
	[PublishEmail] [bit] NULL,
 CONSTRAINT [PK_vt_TNADetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[vt_TNATrainee]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_TNATrainee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NULL,
	[TNAID] [int] NULL,
	[TrainingID] [int] NULL,
	[UserID] [int] NULL,
	[Feedback] [varchar](50) NULL,
	[SessionID] [int] NULL,
 CONSTRAINT [PK_vt_TNATrainee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Topics]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Topics](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TopicName] [varchar](100) NULL,
	[Description] [varchar](500) NULL,
	[IsActive] [bit] NULL,
	[Created] [datetime] NULL,
	[Deleted] [datetime] NULL,
	[Createdby] [int] NULL,
 CONSTRAINT [PK_vt_Topics] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_TopicTypes]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vt_TopicTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TopicID] [int] NULL,
	[DepartmentID] [int] NULL,
 CONSTRAINT [PK_vt_TopicTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[vt_Training_Attachments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Training_Attachments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingID] [int] NULL,
	[Attachment] [varchar](100) NULL,
	[DocType] [varchar](50) NULL,
	[Path] [varchar](1000) NULL,
 CONSTRAINT [PK_vt_TrainingAttachments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Training_CourseMaterial]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vt_Training_CourseMaterial](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingID] [int] NULL,
	[CourseMaterialID] [int] NULL,
 CONSTRAINT [PK_vt_Training_CourseMaterial] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[vt_Training_Mode]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Training_Mode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ModeOfTraining] [varchar](50) NULL,
 CONSTRAINT [PK_vt_TrainingMode] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Training_Type]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Training_Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingType] [varchar](100) NULL,
 CONSTRAINT [PK_vt_TrainingType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_Trainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_Trainings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TopicID] [int] NULL,
	[TrainingName] [varchar](100) NULL,
	[CourseOwnerID] [int] NULL,
	[Description] [varchar](1000) NULL,
	[ModeID] [int] NULL,
	[iLearnURL] [varchar](100) NULL,
	[TrainingTypeID] [int] NULL,
	[CostPerHead] [decimal](18, 0) NULL,
	[Days] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Created] [datetime] NULL,
	[Deleted] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_vt_Trainings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_UserProfile]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vt_UserProfile](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NULL,
	[EmployeeID] [int] NULL,
	[LoginID] [varchar](100) NULL,
	[DepartmentID] [int] NULL,
	[FullName] [varchar](100) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[UserName] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[TypeID] [int] NULL,
	[HamonyID] [varchar](100) NULL,
	[WorkdayID] [varchar](100) NULL,
	[IsApproved] [bit] NULL CONSTRAINT [DF_vt_UserProfile_IsApproved]  DEFAULT ((0)),
	[IsActive] [bit] NULL,
	[Created] [datetime] NULL,
	[Deleted] [datetime] NULL,
	[Location] [varchar](100) NULL,
 CONSTRAINT [PK_vt_UserProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vt_UserProfile_Geography]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vt_UserProfile_Geography](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[GeographyID] [int] NULL,
 CONSTRAINT [PK_vt_UserProfile_Geography] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[sp_AddorEditDetailsofSession]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_AddorEditDetailsofSession]
@ID int,
@StartDate date,
@EndDate date,
@FeedbackDate date,
@Venue varchar(50),
@StartTime datetime,
@EndTime datetime
AS
IF @ID > 0 
	Update vt_Session 
	SET StartDate=@StartDate,EndDate=@EndDate,FeedbackDate=@FeedbackDate,Venue=@Venue,StartTime=@StartTime,EndTime=@EndTime
	Where ID = @ID;
GO
/****** Object:  StoredProcedure [dbo].[sp_DeactivateTNA_byTNAID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeactivateTNA_byTNAID]
@ID int
AS
Update vt_TNA Set IsActive = 0 Where ID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAll_TNATrainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_DeleteAll_TNATrainings]
@ID int
AS
DELETE FROM vt_TNADetails WHERE TNAID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAll_TopicDeparts]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_DeleteAll_TopicDeparts]
@ID int
AS
DELETE FROM vt_TopicTypes WHERE TopicID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAll_TrainingAttachments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_DeleteAll_TrainingAttachments]
@ID int
AS
DELETE FROM vt_Training_Attachments WHERE  TrainingID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAll_TrainingCourseOwners]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_DeleteAll_TrainingCourseOwners]
@ID int
AS
DELETE FROM vt_Training_CourseMaterial WHERE  TrainingID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteAll_UserGeography]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeleteAll_UserGeography]
@ID int
AS
Delete from vt_UserProfile_Geography where UserID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteSession_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_DeleteSession_byID]
@ID int
AS
Delete from vt_Session where ID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTNA_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_DeleteTNA_byID]
@ID int,
@Deleted datetime,
@IsActive bit
AS
Update vt_TNA set Deleted = @Deleted , IsActive = @IsActive where ID = @ID
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTopic_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeleteTopic_byID]
@ID int,
@Deleted datetime,
@IsActive bit
AS
Update vt_Topics Set Deleted = @Deleted ,IsActive = @IsActive where ID = @ID;
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTraining_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeleteTraining_byID]
@ID int,
@Deleted datetime,
@IsActive bit
AS
Update vt_Trainings Set Deleted = @Deleted , IsActive = @IsActive where ID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUser_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_DeleteUser_byID]
@ID int,
@Deleted datetime
AS
Update vt_UserProfile Set Deleted = @Deleted where ID = @ID;

GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUserFromSessions]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeleteUserFromSessions]
@TNAID int,
@TrainingID int,
@UserID int,
@DeptID int,
@SessionID int
AS
DELETE FROM vt_SessionParticipant 
WHERE TNAID = @TNAID 
and TrainingID = @TrainingID 
and UserID = @UserID
and DeptID = @DeptID
and SessionID = @SessionID

Update [dbo].[vt_TNATrainee] set SessionID = null  where UserID=@UserID and TNAID =@TNAID and TrainingID=@TrainingID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetActiveTNA]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetActiveTNA]
AS
Select * from vt_TNA where IsActive = 1

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllActiveTnaActiveTrainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllActiveTnaActiveTrainings]
AS
select distinct TD.TrainingID, TD.TNAID, T.TNAName, TR.TrainingName, TR.Description, TR.Days,TR.ID from vt_TNA T
inner join vt_TNADetails TD 
on T.ID = TD.TNAID 
inner join vt_Trainings TR
on TR.ID = TD.TrainingID
where T.IsActive = 1 and TR.IsActive = 1 --and format (T.LastSubmissionDate,'MM/dd/yyyy') >= format (getdate(),'MM/dd/yyyy')
order by TD.TNAID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllActiveTNAs]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllActiveTNAs]
AS
Select * from vt_TNA where Deleted is null and IsActive = 1 
--and format (LastSubmissionDate,'MM/dd/yyyy') >= format (getdate(),'MM/dd/yyyy')
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllActiveTopics]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllActiveTopics]
AS
Select * from vt_Topics where Deleted is null and IsActive = 1 order by ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllActiveTrainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllActiveTrainings]
AS
Select tr.*,tp.TopicName from [dbo].[vt_Trainings] tr
inner join [dbo].[vt_Topics] tp on tp.ID = tr.TopicID
where tr.Deleted is null and tr.IsActive = 1 ORDER BY tr.ID DESC
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllActiveTrainingsby_TNAID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllActiveTrainingsby_TNAID]
@ID int
AS
Select distinct Train.TopicID, Train.* from vt_Trainings as Train 
inner join vt_TNADetails as TNA
on Train.ID = TNA.TrainingID
where TNA.TNAID = @ID and Train.IsActive = 1
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllActiveTrainingsSessions]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllActiveTrainingsSessions] 
@TrainingID int,
@TNAID int
AS
select S.TNAID, S.TrainingID, S.ID as SessionID, S.SessionName, S.StartDate as SessionStart, S.EndDate as SessionEnd, S.Participants, 
T.TNAName, TR.TrainingName, TR.Description, TR.[Days], FORMAT (S.StartTime, 'hh:mm tt') as StartTime ,FORMAT (S.EndTime, 'hh:mm tt') as EndTime from vt_Session S
inner join vt_TNA T
on S.TNAID = T.ID
inner join vt_Trainings TR
on S.TrainingID = TR.ID 
where T.IsActive = 1 and S.TrainingID = @TrainingID and S.TNAID = @TNAID and S.IsClosed is null
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllAreaGeography]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllAreaGeography]
AS
Select * from vt_Geography

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllClosedSessions]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllClosedSessions]
AS
Select TNA.TNAName,Train.TrainingName,Sess.*,Train.*,TNA.*,Sess.IsClosed as IsClose from vt_Session as Sess
inner join vt_Trainings as Train ON Sess.TrainingID = Train.ID
inner join vt_TNA as TNA ON Sess.TNAID = TNA.ID
where Sess.IsClosed is not null and Sess.Deleted is null
order by Sess.ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllCourseMaterial]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllCourseMaterial]
AS
Select * from vt_CourseMaterial

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllDepartments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllDepartments]
AS
Select * from vt_Departments

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllDepartmentsAssignedUsers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllDepartmentsAssignedUsers]
@TnaID int,
@TrainingID int
as
select TT.DepartmentID, D.DepartmentName, TT.UserID, H.ReporteeName,
(Select Count(ID) from vt_TNATrainee where DepartmentID= TT.DepartmentID and TNAID= @TnaID and TrainingID = @TrainingID and UserID not in 
(select UserID from vt_SessionParticipant  where TNAID = @TnaID and TrainingID = @TrainingID)) as TotalCount
 from vt_TNATrainee TT
inner join [dbo].[vt_Departments] D
on TT.DepartmentID = D.ID
inner join vt_Heirarchy H
on H.ReporteeID = TT.UserID
where TT.TNAID = @TnaID and TT.TrainingID = @TrainingID 
and TT.UserID Not In (select UserID from vt_SessionParticipant as Sess where Sess.TNAID = @TnaID and Sess.TrainingID = @TrainingID)
--and TT.UserID Not In (select UserID from vt_SessionParticipant)
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllDeparts]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllDeparts]
AS
Select * from vt_Departments

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllModes]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllModes]
AS
Select * from vt_Training_Mode

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllPreviousDepByTNA]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   create proc [dbo].[sp_GetAllPreviousDepByTNA]
   @TNAID int,
   @TrainingID int
   as

   select * from [dbo].[vt_TNADetails] where TNAID=@TNAID and TrainingID=@TrainingID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllRoles]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllRoles]
AS
Select * from vt_Roles

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllSessionParticipants]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllSessionParticipants]
@ID int
AS
Select * from vt_SessionParticipant where SessionID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllSessions]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllSessions]
AS
Select TNA.TNAName,Train.TrainingName,*,isnull(Sess.IsClosed,0) as IsClose from vt_Session as Sess
inner join vt_Trainings as Train ON Sess.TrainingID = Train.ID
inner join vt_TNA as TNA ON Sess.TNAID = TNA.ID
where Sess.IsClosed is null and Sess.Deleted is null
order by Sess.ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllSessions_byTrainerID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllSessions_byTrainerID]
@ID int
AS
Select TNA.StartDate as TNAStartDate, TNA.LastSubmissionDate as TNAEndDate,TNA.TNAName,Train.TrainingName,Sess.* from vt_Session as Sess
inner join vt_Trainings as Train ON Sess.TrainingID = Train.ID
inner join vt_TNA as TNA ON Sess.TNAID = TNA.ID
where Sess.TrainerID = @ID

order by Sess.ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllSessions_Cordinator]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllSessions_Cordinator]
AS
Select TNA.StartDate as TNAStartDate, TNA.LastSubmissionDate as TNAEndDate,TNA.TNAName,Train.TrainingName,Sess.* from vt_Session as Sess
inner join vt_Trainings as Train ON Sess.TrainingID = Train.ID
inner join vt_TNA as TNA ON Sess.TNAID = TNA.ID
order by Sess.ID desc

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTNAs]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllTNAs]
AS
Select * from vt_TNA where Deleted is null
Order by ID DESC
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTopics]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllTopics]
AS
Select * from vt_Topics where Deleted is null order by ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTrainers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllTrainers]
AS
Select * from vt_UserProfile where RoleID = 4 and Deleted is null and IsApproved = 1 and IsActive = 1

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTrainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllTrainings]
AS
Select * from vt_Trainings where Deleted is null ORDER BY ID DESC

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTypes]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAllTypes]
AS
Select * from vt_Training_Type

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllUsers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllUsers]
AS
Select up.*,r.RoleName,h.Location as Area,h.DepartmentName as DepartmentName, h.Name as LineManager,h.* from vt_UserProfile as up
inner join [dbo].[vt_Roles] as r on r.ID=up.RoleID
inner join [dbo].[vt_Heirarchy] h on h.ReporteeID = up.EmployeeID
 where up.Deleted is null ORDER BY up.ID DESC
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAssignedUsers_bySessionID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAssignedUsers_bySessionID]
@ID int
AS
Select SessPar.PublishEmail, SessPar.ID,Sess.SessionName,SessPar.UserID as UserID,SessPar.SessionID ,Sess.IsClosed, Heir.ReporteeName as UserName ,SessPar.Attendence, SessPar.Feedback,
format(Sess.StartDate,'d','en-us') StartDate, format(Sess.EndDate,'d','en-us') EndDate from vt_SessionParticipant  as SessPar
inner join vt_Heirarchy as Heir on SessPar.UserID = Heir.ReporteeID 
inner join vt_Session as Sess on Sess.ID = SessPar.SessionID
where SessPar.SessionID = @ID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAssignSessionsUsersOnLoad]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAssignSessionsUsersOnLoad]
@TNAID int,
@TrainingID int
as
select sp.*, H.ReporteeName,d.DepartmentName from dbo.vt_SessionParticipant SP
inner join dbo.vt_Heirarchy H on H.ReporteeID = SP.UserID
inner join dbo.vt_Session s on s.ID = sp.SessionID
inner join [dbo].[vt_Departments] d on d.ID=SP.DeptID
where sp.TNAID = @TNAID and sp.TrainingID = @TrainingID and (s.IsClosed is null or s.IsClosed = 0)
GO
/****** Object:  StoredProcedure [dbo].[sp_GetCalendarWiseReport]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetCalendarWiseReport] 
@Startdate datetime=null,
@EndDate datetime=null
as
select distinct s.*,tna.TnaName,tr.TrainingName,up.FullName as Trainer,tp.TopicName,format(s.StartTime,'hh:mm tt') as S_StartTime,format(s.EndTime,'hh:mm tt')  as S_EndTime,
(select Count(ID) from [dbo].[vt_SessionParticipant] where SessionID=s.ID) as NoOfParticipants 
from [dbo].[vt_Session] s 
left join [dbo].[vt_SessionParticipant] sp on sp.sessionID=s.ID 
left join [dbo].[vt_TNA] tna on tna.ID = s.TNAID 
left join [dbo].[vt_Trainings] tr on tr.ID = s.TrainingID
left join [dbo].[vt_UserProfile] up on up.ID = s.TrainerID
left join [dbo].[vt_Topics] tp on tp.ID = tr.TopicID

where  
(cast(s.StartDate as date) >= cast(@Startdate as date) or @Startdate is null )
 and  
(cast(s.EndDate as date) <= cast(@EndDate as date) or @EndDate is null )
GO
/****** Object:  StoredProcedure [dbo].[sp_GetClosedSessionsDetailbyID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetClosedSessionsDetailbyID] 
@SessionID int
AS
select S.TNAID,S.TrainingID, S.ID as SessionID, S.SessionName,FORMAT( S.StartDate,'dd-MM-yyy') as SessionStart, format(S.EndDate,'dd-MM-yyy') as SessionEnd, S.Participants, 
T.TNAName, TR.TrainingName, TR.Description, TR.[Days], FORMAT (S.StartTime, 'hh:mm tt') as StartTime ,FORMAT (S.EndTime, 'hh:mm tt') as EndTime,trainer.FullName

from vt_Session S inner join vt_TNA T on S.TNAID = T.ID
inner join vt_Trainings TR on S.TrainingID = TR.ID
inner join [dbo].[vt_UserProfile] trainer on  trainer.ID=S.TrainerID
where  S.IsClosed is not null and S.ID=@SessionID --and T.IsActive = 1 
GO
/****** Object:  StoredProcedure [dbo].[sp_GetclosedSessionsUsers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  proc [dbo].[sp_GetclosedSessionsUsers]
@TNAID int,
@TrainingID int,
@SessionID int
as
select sp.*, H.ReporteeName,d.DepartmentName from vt_SessionParticipant SP
inner join vt_Heirarchy H
on H.ReporteeID = SP.UserID
inner join vt_departments d on d.ID=SP.DeptID
where SP.TNAID = @TNAID and SP.TrainingID = @TrainingID and  SP.SessionID=@SessionID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDepartmentsAssignedUsersbyName]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetDepartmentsAssignedUsersbyName]
@TnaID int,
@TrainingID int,
@DName varchar(200)
AS
--declare @TnaID int=1
--declare  @TrainingID int=1
--declare  @DName varchar(200)='CHC'
select TT.DepartmentID, H.DepartmentName, TT.UserID, H.ReporteeName,
(Select Count(ID) from vt_TNATrainee where DepartmentID= TT.DepartmentID and TNAID= @TnaID and TrainingID = @TrainingID and UserID not in 
(select UserID from vt_SessionParticipant  where TNAID = @TnaID and TrainingID = @TrainingID)) as TotalCount from vt_TNATrainee TT
inner join vt_Departments D
on TT.DepartmentID = D.ID
inner join vt_Heirarchy H
on H.ReporteeID = TT.UserID
where TT.TNAID = @TnaID and TT.TrainingID = @TrainingID 
and TT.UserID Not In (select UserID from vt_SessionParticipant as Sess where Sess.TNAID = @TnaID and Sess.TrainingID = @TrainingID)
AND D.DepartmentName like '%'+@DName+'%'
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDeptListbyTrID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetDeptListbyTrID] 
@TrainingID int 
as
select distinct dep.ID,dep.DepartmentName  from [dbo].[vt_Departments] dep
inner join [dbo].[vt_SessionParticipant] sp on sp.DeptID=dep.ID
where sp.TrainingID=@TrainingID or @TrainingID = 0
GO
/****** Object:  StoredProcedure [dbo].[sp_GetHarmonyUser_byLoginID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetHarmonyUser_byLoginID]
@LoginID varchar(100)
AS
select H.Name as LineManagerName,H.ReporteeName as FullName,H.ReporteeID as EmployeeID,D.ID as DepartmentID,H.Email,H.Location from vt_Heirarchy H
inner join vt_Departments D on
D.DepartmentName = H.DepartmentName
where LoginID = @LoginID 
GO
/****** Object:  StoredProcedure [dbo].[sp_GetLastTNAID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetLastTNAID]
AS
SELECT ID FROM vt_TNA ORDER BY ID DESC

GO
/****** Object:  StoredProcedure [dbo].[sp_GetLastTopicID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetLastTopicID]
AS
SELECT ID FROM vt_Topics ORDER BY ID DESC

GO
/****** Object:  StoredProcedure [dbo].[sp_GetLastTrainingID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetLastTrainingID]
AS
SELECT ID FROM vt_Trainings ORDER BY ID DESC

GO
/****** Object:  StoredProcedure [dbo].[sp_GetLastUserID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetLastUserID]
AS
SELECT ID FROM vt_UserProfile ORDER BY ID DESC

GO
/****** Object:  StoredProcedure [dbo].[sp_GetLineManagerByLoginIDAndDepartment]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[sp_GetLineManagerByLoginIDAndDepartment]
@LoginID varchar(MAX),
@DepartmentID int
As
select * from vt_UserProfile
where LoginID = @LoginID and DepartmentID = @DepartmentID


GO
/****** Object:  StoredProcedure [dbo].[sp_GetSession_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetSession_byID]
@ID int
AS
Select * from vt_Session where ID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTNAby_ID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_GetTNAby_ID]
@TNAID int
AS
Select * from vt_TNA where ID = @TNAID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTNAStartandEndDate]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetTNAStartandEndDate]
@TNAID int
as
select *, LastSubmissionDate as EndDate from [dbo].[vt_TNA] where ID = @TNAID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetTNATrainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetTNATrainings]
@ID int
AS
Select * from vt_TNADetails where TNAID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTopicby_ID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetTopicby_ID]
@TopicID int
AS
Select * from vt_Topics where ID = @TopicID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTopicDepartments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetTopicDepartments]
@ID int
AS
Select * from vt_TopicTypes where TopicID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTrainerWiseReport]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetTrainerWiseReport] --0,0
@TrainerID int 
as
select distinct s.*,tna.TnaName,tr.TrainingName,up.FullName as Trainer,tp.TopicName,format(s.StartTime,'hh:mm tt') as S_StartTime,format(s.EndTime,'hh:mm tt')  as S_EndTime,
(select Count(ID) from [dbo].[vt_SessionParticipant] where SessionID=s.ID) as NoOfParticipants 
from [dbo].[vt_Session] s 
left join [dbo].[vt_SessionParticipant] sp on sp.sessionID=s.ID 
left join [dbo].[vt_TNA] tna on tna.ID = s.TNAID 
left join [dbo].[vt_Trainings] tr on tr.ID = s.TrainingID
left join [dbo].[vt_UserProfile] up on up.ID = s.TrainerID
left join [dbo].[vt_Topics] tp on tp.ID = tr.TopicID

where up.ID=@TrainerID or @TrainerID = 0 
GO
/****** Object:  StoredProcedure [dbo].[sp_GetTrainingandDeptWiseReport]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetTrainingandDeptWiseReport]
@DeptID int,
@TrainingID int 
as
select distinct
 s.*,tna.TnaName,tr.TrainingName,up.FullName as Trainer,tp.TopicName,
 format(s.StartTime,'hh:mm tt') as S_StartTime,
 format(s.EndTime,'hh:mm tt')  as S_EndTime,
(select Count(ID) from [dbo].[vt_SessionParticipant] where SessionID=s.ID) as NoOfParticipants 
from [dbo].[vt_Session] s 
left join [dbo].[vt_SessionParticipant] sp on sp.sessionID=s.ID 
left join [dbo].[vt_TNA] tna on tna.ID = s.TNAID 
 left join [dbo].[vt_UserProfile] up on up.ID = s.TrainerID
left join [dbo].[vt_Trainings] tr on tr.ID = s.TrainingID
 left join [dbo].[vt_Topics] tp on tp.ID = tr.TopicID
--inner join [dbo].[vt_TNADetails] td on td.TNAID = tna.ID

where (sp.DeptID=@DeptID or @DeptID = 0 ) and (s.TrainingID=@TrainingID or @TrainingID = 0)
--where (td.DepartmentID=@DeptID or @DeptID = 0 ) and (td.TrainingID=@TrainingID or @TrainingID = 0)
GO
/****** Object:  StoredProcedure [dbo].[sp_GetTrainingAttachments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetTrainingAttachments]
@ID int
AS
Select * from vt_Training_Attachments where TrainingID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTrainingby_ID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetTrainingby_ID]
@TrainingID int
AS
Select * from vt_Trainings where ID = @TrainingID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetTrainingCourseMaterial]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetTrainingCourseMaterial]
@ID int
AS
Select * from vt_Training_CourseMaterial where TrainingID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserBy_Credentials]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetUserBy_Credentials]
@UserName varchar(100)
AS
Select * from vt_UserProfile where LoginID = @UserName and Deleted is null and IsActive=1
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserby_ID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetUserby_ID]
@UserID int
AS

Select *,h.Name as LineManagerName from vt_UserProfile u inner join vt_Heirarchy h on h.ReporteeID=u.EmployeeID where u.ID = @UserID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserGeographys]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetUserGeographys]
@ID int
AS
Select * from vt_UserProfile_Geography where UserID = @ID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserMenu_ByRole]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetUserMenu_ByRole]
@RoleID varchar(100)
AS
Select * from vt_RoleMenu where RoleID = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUserPages_ByRole]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_GetUserPages_ByRole]
@RoleID int
As
Select * from vt_RolePages where RoleID = @RoleID

GO
/****** Object:  StoredProcedure [dbo].[sp_GetUsersByLineManagerDepartment]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetUsersByLineManagerDepartment]
@LoginID int,
@DepartmentID int,
@Location varchar(100),
@RoleID int
AS
--declare @LoginID int=4039
--IF @RoleID=3
--BEGIN
declare @SecondLevelHeirarchy int =(select count(ReporteeID) from [dbo].[vt_Heirarchy] where EmpID in ( select H.ReporteeID from [dbo].[vt_Heirarchy] H where  H.EmpID = @LoginID))
if @SecondLevelHeirarchy  > 0
select EmpID as LMID,Name as LineManager, ReporteeID,ReporteeName,(Select DepartmentName from [dbo].[vt_Heirarchy] where ReporteeID = 4039 ) as DepartmentName from [dbo].[vt_Heirarchy] where EmpID in ( select H.ReporteeID from [dbo].[vt_Heirarchy] H where  H.EmpID = @LoginID)
group by EmpID,Name,ReporteeID,ReporteeName
else
select H.ReporteeID,H.ReporteeName , (Select DepartmentName from [dbo].[vt_Heirarchy] where ReporteeID = @LoginID ) as DepartmentName from [dbo].[vt_Heirarchy] H where  H.EmpID = @LoginID
--END
---------------------------------------------------------------------------
--ELSE IF @RoleID = 6
--BEGIN
--select H.ReporteeID,H.ReporteeName from vt_departments D
--inner join vt_heirarchy H 
--on D.DepartmentName = H.DepartmentName 
--where D.ID = @DepartmentID and H.EmpID = @LoginID
--END

GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_TNADetail]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_Insert_TNADetail]
@TNAID int,
@TrainingID int,
@DepartmentID int,
@CreatedAt datetime,
@IsActive bit
AS
Insert into vt_TNADetails(TNAID,TrainingID,DepartmentID,CreatedAt,IsActive) 
values (@TNAID,@TrainingID,@DepartmentID,@CreatedAt,@IsActive);
GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_TopicJunc]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[sp_Insert_TopicJunc]
@TopicID int,
@DepartID int
AS

Insert into vt_TopicTypes (TopicID , DepartmentID) 
values (@TopicID,@DepartID);

GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_TrainingAttachments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_Insert_TrainingAttachments]
@TrainingID int,
@Attachment varchar(100),
@DocType varchar(50),
@Path varchar(1000)
AS
Insert into vt_Training_Attachments (TrainingID,Attachment,DocType,[Path])
Values (@TrainingID,@Attachment,@DocType,@Path)

GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_TrainingCourseMaterial]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_Insert_TrainingCourseMaterial]
@TrainingID int,
@CourseMaterialID int
AS
Insert into vt_Training_CourseMaterial (TrainingID,CourseMaterialID)
Values (@TrainingID,@CourseMaterialID)

GO
/****** Object:  StoredProcedure [dbo].[sp_Insert_UserGeography]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_Insert_UserGeography]
@UserID int,
@GeographyID int
AS
Insert into vt_UserProfile_Geography (UserID,GeographyID)
Values (@UserID,@GeographyID)

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertsUserToSession]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_InsertsUserToSession]
@TNAID int,
@TrainingID int,
@DeptID int,
@UserID int,
@SessionID int
AS

Insert into vt_SessionParticipant(TNAID, TrainingID, DeptID, UserID, SessionID,StartDateTime,EndDateTime) 
values (@TNAID,@TrainingID,@DeptID,@UserID, @SessionID,(select [StartTime] from [dbo].[vt_Session] where ID=@SessionID),(select [EndTime] from [dbo].[vt_Session] where ID=@SessionID));

update [dbo].[vt_TNATrainee] set SessionID=@SessionID where UserID=@UserID and TNAID =@TNAID and TrainingID=@TrainingID
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUpdate_Session]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_InsertUpdate_Session]
@ID int,
@TNAID int,
@TrainingID int,
@SessionName varchar(200),
@Multimedia bit,
@Staitionary bit,
@Hall bit,
@Other varchar(500),
@TrainerID int,
@CostPerHead decimal(18,2),
@GeographyID int,
@Participants int

AS
IF @ID > 0 
	Update vt_Session 
	SET TNAID=@TNAID,TrainingID=@TrainingID,SessionName=@SessionName,Multimedia=@Multimedia,Stationary=@Staitionary,
	Hall=@Hall,Other=@Other,TrainerID=@TrainerID,CostPerHead=@CostPerHead,GeographyID=@GeographyID,Participants=@Participants
	Where ID = @ID;
ELSE
    Insert into vt_Session(TNAID,TrainingID,SessionName,Multimedia,Stationary,Hall,Other,TrainerID,CostPerHead,GeographyID,Participants) 
	values(@TNAID,@TrainingID,@SessionName,@Multimedia,@Staitionary,@Hall,@Other,@TrainerID,@CostPerHead,@GeographyID,@Participants);
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUpdate_TNA]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_InsertUpdate_TNA]
@ID int,
@TNAName varchar(100),
@StartDate datetime,
@EndDate datetime,
@LastDateforLM datetime,
@IsActive bit,
@Created datetime

AS

IF @ID > 0 
	Update vt_TNA SET TNAName = @TNAName , [StartDate] = @StartDate , LastSubmissionDate = @EndDate ,LastDateforLM=@LastDateforLM, IsActive = @IsActive
	Where ID = @ID;
ELSE
    Insert into vt_TNA (TNAName,[StartDate],LastSubmissionDate,LastDateforLM,IsActive, Created ) 
	values(@TNAName,@StartDate,@EndDate,@LastDateforLM,@IsActive, @Created);
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUpdate_Topic]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_InsertUpdate_Topic]
@ID int,
@TopicName varchar(100),
@Description varchar(500),
@IsActive bit,
@CreatedAt datetime

AS

IF @ID > 0 
	Update vt_Topics SET TopicName = @TopicName , [Description] = @Description , IsActive = @IsActive
	Where ID = @ID;
ELSE
    Insert into vt_Topics (TopicName,[Description], IsActive, Created ) 
	values(@TopicName,@Description,@IsActive, @CreatedAt);

GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUpdate_Training]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_InsertUpdate_Training]
@ID int,
@TopicID int,
@TrainingName varchar(100),
@CourseOwnerID int,
@Description varchar(1000),
@ModeID int,
@iLearnURL varchar(100),
@TrainingTypeID int,
@CostPerHead decimal(18,2),
@Days int,
@IsActive bit,
@Created datetime,
@CreatedBy int
AS

IF @ID > 0 
	Update vt_Trainings 
	Set TopicID= @TopicID,TrainingName=@TrainingName,CourseOwnerID=@CourseOwnerID,
	[Description]=@Description,ModeID=@ModeID,iLearnURL=@iLearnURL,TrainingTypeID=@TrainingTypeID,
	CostPerHead=@CostPerHead,[Days]=@Days,IsActive=@IsActive,Created=@Created,CreatedBy=@CreatedBy
	WHERE ID = @ID
ELSE
	Insert into vt_Trainings (TopicID,TrainingName,CourseOwnerID,[Description],ModeID,iLearnURL,TrainingTypeID,CostPerHead,[Days],IsActive,Created,CreatedBy)
	Values (@TopicID,@TrainingName,@CourseOwnerID,@Description,@ModeID,@iLearnURL,@TrainingTypeID,@CostPerHead,@Days,@IsActive,@Created,@CreatedBy)


GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUpdate_User]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_InsertUpdate_User]
@ID int,
@RoleID int,
@TypeID int,
@LoginID varchar(100),
@DepartmentID int,
@EmployeeID int,
@FullName varchar(200),
@Location varchar(200),
@Email varchar(200),
@IsApproved bit,
@IsActive bit,
@Created datetime
AS

IF @ID > 0
	Update vt_UserProfile 
	SET RoleID = @RoleID , DepartmentID = @DepartmentID, EmployeeID = @EmployeeID , 
	FullName = @FullName , Email = @Email ,IsActive = @IsActive , TypeID = @TypeID,Location=@Location
	Where ID = @ID;
ELSE
	Insert into vt_UserProfile (RoleID,TypeID,LoginID,DepartmentID,EmployeeID,[FullName],Location,Email,IsApproved,IsActive,Created)
	Values(@RoleID,@TypeID,@LoginID,@DepartmentID,@EmployeeID,@FullName,@Location,@Email,@IsApproved,@IsActive,@Created)


GO
/****** Object:  StoredProcedure [dbo].[sp_LM_DeleteUserFromTrainingTNA]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_LM_DeleteUserFromTrainingTNA]
@TNAID int,
@TrainingID int,
@UserID int,
@DepartmentID int
AS
DELETE FROM vt_TNATrainee 
WHERE TNAID = @TNAID 
and TrainingID = @TrainingID 
and UserID = @UserID
and DepartmentID = @DepartmentID
GO
/****** Object:  StoredProcedure [dbo].[sp_LM_GetAssignUsersTrainingOnPageLoad]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_LM_GetAssignUsersTrainingOnPageLoad]
@TNAID int,
@TrainingID int,
--@DepartmentID int,
--@Location varchar(100),
--@RoleID int,
@EmpID int
AS

declare @SecondLevelHeirarchy int =(select count(ReporteeID) from [dbo].[vt_Heirarchy] where EmpID in ( select H.ReporteeID from [dbo].[vt_Heirarchy] H where  H.EmpID = @EmpID))
if @SecondLevelHeirarchy  > 0
select TT.TNAID, TT.TrainingID, TT.UserID, TT.DepartmentID, H.ReporteeName
From vt_TNATrainee TT inner join vt_Heirarchy H
on TT.UserID = H.ReporteeID
where TT.TNAID = @TNAID and TT.TrainingID = @TrainingID and H.EmpID in (select H.ReporteeID from [dbo].[vt_Heirarchy] H where  H.EmpID = @EmpID)--TT.DepartmentID = @DepartmentID and H.Location=@Location
else 
select TT.TNAID, TT.TrainingID, TT.UserID, TT.DepartmentID, H.ReporteeName
From vt_TNATrainee TT inner join vt_Heirarchy H
on TT.UserID = H.ReporteeID
where TT.TNAID = @TNAID and TT.TrainingID = @TrainingID and H.EmpID=@EmpID
GO
/****** Object:  StoredProcedure [dbo].[sp_LM_GetTrainingsCurrentUser]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_LM_GetTrainingsCurrentUser]
@DepartName varchar(250),
@DepartID int
AS
select td.TNAID,td.TrainingID,td.DepartmentID,tn.TNAName,tn.StartDate,tn.LastSubmissionDate,tn.LastDateforLM,tr.TrainingName,
tr.Description 
from [dbo].[vt_TNADetails] td inner join [dbo].[vt_TNA] tn 
on tn.ID = td.TNAID
inner join [dbo].[vt_Trainings] tr 
on tr.ID = td.TrainingID
where td.DepartmentID=@DepartID  and td.IsActive = 1 and tn.IsActive = 1 and tn.Deleted is null
 and cast(FORMAT(tn.LastDateforLM,'M/d/yyyy') as datetime) >= cast(format(getDate(),'M/d/yyyy') as datetime)

--declare @DepartName varchar(250) ='CHCTeam-North'
--select distinct TD.TrainingID,TD.TNAID as TnaId,TNA.TNAName,T.ID as TrainingId,T.TrainingName, TNA.StartDate, TNA.LastSubmissionDate,
--T.Description, D.ID as DepartmentId,TNA.LastDateforLM
--from vt_TopicTypes TT 
--inner join vt_Departments D
--on TT.DepartmentID = D.ID
--inner join vt_Trainings T
--on T.TopicID = TT.TopicID
--inner join vt_TNADetails TD
--on TD.TrainingID = T.ID
--inner join vt_TNA TNA
--on TNA.ID = TD.TNAID
--where D.DepartmentName = @DepartName and TD.IsActive=1 and TNA.IsActive = 1 and TNA.Deleted is null
-- and cast(FORMAT(TNA.LastDateforLM,'M/d/yyyy') as datetime) >= cast(format(getDate(),'M/d/yyyy') as datetime)





GO
/****** Object:  StoredProcedure [dbo].[sp_LM_GetTrainingsCurrentUserbyTNAID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_LM_GetTrainingsCurrentUserbyTNAID]
@DepartName varchar(250),
@TNAName varchar(50),
@TrainingName varchar(50)
AS
select distinct TD.TrainingID, TNA.ID as TnaId, TNA.TNAName, T.ID as TrainingId, T.TrainingName, TNA.StartDate, 
TNA.LastSubmissionDate, T.Description, D.ID as DepartmentId,TNA.LastDateforLM from vt_TopicTypes TT 
inner join vt_Departments D
on TT.DepartmentID = D.ID
inner join vt_Trainings T
on T.TopicID = TT.TopicID
inner join vt_TNADetails TD
on TD.TrainingID = T.ID
inner join vt_TNA TNA
on TNA.ID = TD.TNAID
where D.DepartmentName = @DepartName and TNA.IsActive = 1 and TNA.Deleted is null and T.IsActive = 1 and T.Deleted is null and T.TrainingName like @TrainingName+'%' and TNA.TNAName like  @TNAName+'%' 
and CAST(FORMAT(TNA.LastDateforLM,'M/d/yyyy') AS DATETIME) >= CAST(format(getDAte(),'M/d/yyyy') AS datetime)
GO
/****** Object:  StoredProcedure [dbo].[sp_LM_InsertUsersToTnaTrainee]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_LM_InsertUsersToTnaTrainee]
@TNAID int,
@TrainingID int,
@DepartmentID int,
@UserID int
AS

Insert into vt_TNATrainee( TNAID, TrainingID, DepartmentID, UserID) 
values (@TNAID,@TrainingID,@DepartmentID,@UserID);

GO
/****** Object:  StoredProcedure [dbo].[sp_update_TNADetail]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE proc [dbo].[sp_update_TNADetail]
@TNAID int,
@TrainingID int,
@DepartmentID int,
@CreatedAt datetime,
@IsActive bit
AS
update vt_TNADetails set IsActive=@IsActive,PublishEmail=null
where  TNAID= @TNAID and TrainingID=@TrainingID and DepartmentID=@DepartmentID
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateSessionUsers_AttendenceFeedback]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_UpdateSessionUsers_AttendenceFeedback]
@SessionID int,
@UserID int,
@Attendence bit,
@Feedback varchar(500)
AS
Update vt_SessionParticipant
SET Attendence = @Attendence , Feedback = @Feedback
Where SessionID = @SessionID and UserID = @UserID

GO
/****** Object:  StoredProcedure [dbo].[CheckalreadyRoleAssigned]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CheckalreadyRoleAssigned]
@LoginID varchar(100)
as
select * from [dbo].[vt_UserProfile] where LoginID=@LoginID and Deleted is null and IsActive=1
GO
/****** Object:  StoredProcedure [dbo].[CheckUserisExistinSession]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CheckUserisExistinSession]
@TNAID int,
@TrainingID int,
@UserID int,
@DepartmentID int
as
select SessionID from [dbo].[vt_TNATrainee] WHERE TNAID = @TNAID and TrainingID =@TrainingID and UserID = @UserID and DepartmentID = @DepartmentID
GO
/****** Object:  StoredProcedure [dbo].[getTrainingCalendar]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getTrainingCalendar]

  @TrainerID int
  as

select s.ID,s.SessionName as [text], CONVERT(DATETIME, CONVERT(CHAR(8), s.StartDate, 112)
  + ' ' + CONVERT(CHAR(8), s.StartTime, 108)) as startDate,
  
   CONVERT(DATETIME, CONVERT(CHAR(8), s.StartDate, 112)
  + ' ' + CONVERT(CHAR(8), s.StartTime, 108)) as endDate,

  CONVERT(DATETIME, CONVERT(CHAR(8), s.EndDate, 112)
  + ' ' + CONVERT(CHAR(8), s.EndTime, 108)) as End_Date,
  
  s.Venue,tna.TNAName,t.TrainingName
from
[dbo].[vt_Session] s  inner join [dbo].[vt_TNA] tna on s.TNAID = tna.ID
inner join [dbo].[vt_Trainings] t on t.ID=s.TrainingID where s.TrainerID=@TrainerID and s.Deleted is null


GO
/****** Object:  StoredProcedure [dbo].[getTrainingCalendar_Cordinator]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getTrainingCalendar_Cordinator]


  as

select s.ID,s.SessionName as [text], CONVERT(DATETIME, CONVERT(CHAR(8), s.StartDate, 112)
  + ' ' + CONVERT(CHAR(8), s.StartTime, 108)) as startDate,
  
   CONVERT(DATETIME, CONVERT(CHAR(8), s.StartDate, 112)
  + ' ' + CONVERT(CHAR(8), s.StartTime, 108)) as endDate,

  CONVERT(DATETIME, CONVERT(CHAR(8), s.EndDate, 112)
  + ' ' + CONVERT(CHAR(8), s.EndTime, 108)) as End_Date,
  
  s.Venue,tna.TNAName,t.TrainingName
from
[dbo].[vt_Session] s  inner join [dbo].[vt_TNA] tna on s.TNAID = tna.ID
inner join [dbo].[vt_Trainings] t on t.ID=s.TrainingID where s.Deleted is null

GO
/****** Object:  StoredProcedure [dbo].[sp_AutoUpdateLoctionandDeptID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_AutoUpdateLoctionandDeptID]
as
UPDATE UP
SET DepartmentID =  (Select ID from [dbo].[vt_Departments] Where DepartmentName = Hr.DepartmentName ) 
, Location=Hr.Location
FROM [dbo].[vt_UserProfile] UP
JOIN [dbo].[vt_Heirarchy] Hr
    ON UP.LoginID = Hr.LoginID
	WHERE UP.RoleID =  3
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckDepartmentExsist]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_CheckDepartmentExsist]
@DepartName varchar(MAX)
AS
Select * from vt_Departments where DepartmentName = @DepartName
GO
/****** Object:  StoredProcedure [dbo].[sp_checkDeptID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_checkDeptID] 
@LoginID nvarchar(100)
as
declare @HeirarchyDeptID int
declare @UserProfileDeptID int
set @HeirarchyDeptID = (select ID  from [dbo].[vt_Departments] where DepartmentName = (select DepartmentName from [dbo].[vt_Heirarchy] where LoginID=@LoginID))
set @UserProfileDeptID = (select distinct DepartmentID from [dbo].[vt_UserProfile] where LoginID=@LoginID and IsActive=1 and Deleted is not null)
if @HeirarchyDeptID != @UserProfileDeptID
update [dbo].[vt_UserProfile] set DepartmentID=@HeirarchyDeptID where LoginID=@LoginID
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckEmployeeExsist]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_CheckEmployeeExsist]
@LoginID varchar(MAX)
AS
Select * from vt_Heirarchy where LoginID = @LoginID
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckforParticpantwasAssign]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_CheckforParticpantwasAssign]
@TNAID int,
@TrainingID int,
@DeptID int
as
select * , (select DepartmentName from  [dbo].[vt_Departments] where ID = @DeptID) as DepartmentName from [dbo].[vt_TNATrainee]  where TNAID = @TNAID and TrainingID=@TrainingID and DepartmentID=@DeptID 

GO
/****** Object:  StoredProcedure [dbo].[sp_checkSameLocationLM]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_checkSameLocationLM]
@Location varchar(100),
@RoleID INT
as
select * from [dbo].[vt_UserProfile] where RoleID=@RoleID and  Location=@Location
GO
/****** Object:  StoredProcedure [dbo].[sp_CheckUserAlreadyExist]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_CheckUserAlreadyExist]
@EmpId int
as
select * from [dbo].[vt_UserProfile] where EmployeeID=@EmpId and IsActive=0 and Deleted is null
GO
/****** Object:  StoredProcedure [dbo].[sp_ClosedSession_byID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_ClosedSession_byID]
@SessionID int,
@IsClosed bit
AS
Update vt_Session set IsClosed = @SessionID 
where ID = @SessionID
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateLineManagers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_CreateLineManagers]
@RoleID int,
@EmployeeID int,
@LoginID nvarchar(100),
@DepartmentID int,
@FullName varchar(200),
@Email varchar(100),
@IsApproved bit,
@IsActive bit,
@Created datetime,
@Location varchar(100)
as
insert into [dbo].[vt_UserProfile] (RoleID,EmployeeID,LoginID,DepartmentID,FullName,Email,IsApproved,IsActive,Created,Location) values (@RoleID,@EmployeeID,@LoginID,@DepartmentID,@FullName,@Email,@IsApproved,@IsActive,@Created,@Location)
GO
/****** Object:  StoredProcedure [dbo].[sp_CurrentMonthSession]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Proc [dbo].[sp_CurrentMonthSession]
as
select  format(StartDate,'dd','en-us') as CDay, SessionName from  [dbo].[vt_Session] where Format(StartDate,'MMMM','en-us') = format(getdate(),'MMMM','en-us')
order by CDay asc
GO
/****** Object:  StoredProcedure [dbo].[sp_CurrentMonthTraining]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sp_CurrentMonthTraining]
as
select  format(Created,'dd','en-us') as CDay, TrainingName from  [dbo].[vt_Trainings] where Format(Created,'MMMM','en-us') = format(getdate(),'MMMM','en-us') and Isactive=1 and deleted is null
order by CDay asc
GO
/****** Object:  StoredProcedure [dbo].[sp_Dashboard_Training]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  proc [dbo].[sp_Dashboard_Training]
as

select concat(t.TrainingName,', ',Format(t.Created,'MMMM','en-US'),' ',Format(getdate(),'yyyy','en-US')) as TrainingName,s.SessionName as SessionName,count(sp.ID) as NoOfParticipants from [dbo].[vt_Trainings] t
inner join [dbo].[vt_Session] s on t.ID = s.TrainingID
inner join [dbo].[vt_SessionParticipant] sp on sp.SessionID=s.ID 
where Format(s.StartDate,'MM/dd/yyyy') > format (DateADD(MM,-6,getdate()),'MM/dd/yyyy')

group by t.ID,t.TrainingName,t.Created,s.ID,s.SessionName
order by t.ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_DashboardCalendar]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DashboardCalendar]


  as
  select s.ID,s.SessionName as [text], CONVERT(DATETIME, CONVERT(CHAR(8), s.StartDate, 112) 
  + ' ' + CONVERT(CHAR(8), s.StartTime, 108)) as startDate,
  CONVERT(DATETIME, CONVERT(CHAR(8), s.StartDate, 112) 
  + ' ' + CONVERT(CHAR(8), s.StartTime, 108)) as endDate,

  CONVERT(DATETIME, CONVERT(CHAR(8), s.EndDate, 112)+ ' ' + CONVERT(CHAR(8), s.EndTime, 108)) as End_Date,
  s.Venue,tna.TNAName,t.TrainingName
from
[dbo].[vt_Session] s  inner join [dbo].[vt_TNA] tna on s.TNAID = tna.ID
inner join [dbo].[vt_Trainings] t on t.ID=s.TrainingID

where s.Deleted is null
GO
/****** Object:  StoredProcedure [dbo].[sp_DashboardCounter]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DashboardCounter]
as
Declare @TNA int
Declare @Training int 
Declare @Session int
set @TNA=(select Count(ID)   from [dbo].[vt_TNA] where  Deleted  is null  and IsActive =1)
set @Training=(select Count(ID)   from [dbo].[vt_Trainings] where Deleted  is null  and IsActive =1)
set @Session =(select Count(ID)   from [dbo].[vt_Session] where Deleted  is null)
select @TNA as TotalTNA, @Training as TotalTraining, @Session as TotalSession

GO
/****** Object:  StoredProcedure [dbo].[sp_Delete_TNADetail]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_Delete_TNADetail]
@TNAID int,
@TrainingID int 
AS
Delete from vt_TNADetails where TNAID = @TNAID and TrainingID = @TrainingID
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteSessionandAllParticipants]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeleteSessionandAllParticipants]
@ID int 
as 

select H.*, S.SessionName from [dbo].[vt_Heirarchy] H inner join 
[dbo].[vt_Session] S on S.ID=@ID
where H.ReporteeID in (select UserID from [dbo].[vt_SessionParticipant] where SessionID=@ID)

declare @Count int = (select count(H.ID) from [dbo].[vt_Heirarchy] H inner join 
[dbo].[vt_Session] S on S.ID=@ID
where H.ReporteeID in (select UserID from [dbo].[vt_SessionParticipant] where SessionID=@ID))
if @Count > 0
begin
delete from [dbo].[vt_SessionParticipant] where SessionID=@ID
delete from [dbo].[vt_Session] where ID=@ID
end
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTNADetailTraining]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_DeleteTNADetailTraining]
@TNAID int,
@TrainingID int
AS
begin
declare @checkUserExist int = (select Count(ID)  from [dbo].[vt_TNATrainee] where TNAID=@TNAID and TrainingID=@TrainingID)
if(@checkUserExist>0)
begin
select @checkUserExist
end
-- vt_TNADetails
else
begin
Delete from vt_TNADetails where TNAID = @TNAID and TrainingID = @TrainingID
end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTopicHeirarchy]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_DeleteTopicHeirarchy]
@TopicID int,
@Deleted datetime,
@IsActive bit
as
Update [dbo].[vt_Topics] Set Deleted = @Deleted ,IsActive = @IsActive where ID = @TopicID
Update [dbo].[vt_Trainings] Set Deleted = @Deleted ,IsActive = @IsActive where TopicID = @TopicID
---Update tna Set tna.Deleted = @Deleted, tna.IsActive = @IsActive from  [dbo].[vt_TNA] tna inner join [dbo].[vt_TNADetails] td on td.TNAID=tna.ID  where td.TrainingID in (select ID from [dbo].[vt_Trainings] where TopicID = @TopicID)
update [dbo].[vt_Session]   set Deleted = @Deleted where TrainingID in (select ID from [dbo].[vt_Trainings] where TopicID = @TopicID) --and TNAID in (select t.ID from [dbo].[vt_TNA] t inner join [dbo].[vt_TNADetails] td on td.TNAID=t.ID where td.TrainingID in  (select ID from [dbo].[vt_Trainings] where TopicID = @TopicID)) 
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteTrainingHeirarchy]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_DeleteTrainingHeirarchy]
@TrainingID int,
@Deleted datetime,
@IsActive bit
as
Update [dbo].[vt_Trainings] Set Deleted = @Deleted ,IsActive = @IsActive where ID = @TrainingID

--Update tna Set tna.Deleted = @Deleted, tna.IsActive = @IsActive from  [dbo].[vt_TNA] tna inner join [dbo].[vt_TNADetails] td on td.TNAID=tna.ID  where td.TrainingID=@TrainingID
update [dbo].[vt_Session] set Deleted = @Deleted where TrainingID =@TrainingID --and TNAID in (select t.ID from [dbo].[vt_TNA] t inner join [dbo].[vt_TNADetails] td on td.TNAID=t.ID where td.TrainingID=@TrainingID ) 
GO
/****** Object:  StoredProcedure [dbo].[sp_GetActiveTNADeptWise]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetActiveTNADeptWise]
@DeptID int
as
--declare @DeptID int=27
select  tr.TrainingName+','+ t.TNAName as Value,tr.TrainingName+' ('+ t.TNAName+')' as TrainingName,
tr.TrainingName as Training,t.TNAName as TNA  
from [dbo].[vt_TNADetails] td inner join [dbo].[vt_TNA] t on t.ID = td.TNAID
inner join [dbo].[vt_Trainings] tr on tr.ID = td.TrainingID
where t.IsActive = 1 and td.DepartmentID=@DeptID and td.IsActive=1 and t.Deleted is null 
and cast(FORMAT(t.LastDateforLM,'M/d/yyyy') as datetime) >= cast(format(getDate(),'M/d/yyyy') as datetime)
group by t.TNAName,tr.TrainingName

--select tr.TrainingName+','+ t.TNAName as Value,tr.TrainingName+' ('+ t.TNAName+')' as TrainingName ,tr.TrainingName as Training,t.TNAName as TNA  
-- from [dbo].[vt_Trainings] tr
-- inner join [dbo].[vt_TNADetails] td on td.TrainingID=tr.ID
-- inner join [dbo].[vt_TNA] t on td.TNAID=t.ID
----inner join [dbo].[vt_Departments] d on d.ID=@DeptID

--where t.IsActive = 1 and td.DepartmentID=@DeptID and td.IsActive=1 and t.Deleted is null and cast(FORMAT(t.LastDateforLM,'M/d/yyyy') as datetime) >= cast(format(getDate(),'M/d/yyyy') as datetime)
--group by t.TNAName,tr.TrainingName

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllCourseOwner]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllCourseOwner]
AS
Select * from vt_UserProfile where RoleID = 2

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllLogs]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllLogs]
as
select l.*,u.FullName from [dbo].[vt_AuditLog] l inner join [dbo].[vt_UserProfile] u on u.ID=l.UserID  order by ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTNASavedTrainings]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetAllTNASavedTrainings]
@ID int
AS
select distinct (Detail.TrainingId),Train.TrainingName from vt_TNADetails as Detail
left join vt_Trainings as Train
on Train.ID = Detail.TrainingID
where Detail.TNAID = @ID
group by Detail.TrainingID,Train.TrainingName

GO
/****** Object:  StoredProcedure [dbo].[sp_GetAttachmentNamebyAttachment]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetAttachmentNamebyAttachment]
@Attachment varchar(1000)
as
select Attachment from [dbo].[vt_Training_Attachments] where Attachment=@Attachment
GO
/****** Object:  StoredProcedure [dbo].[sp_GetCancleSessionEmailTemplate]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetCancleSessionEmailTemplate]
as
select * from [dbo].[vt_EmailManagement] where EmailType=3
GO
/****** Object:  StoredProcedure [dbo].[sp_GetCourseMaterialNamebyID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetCourseMaterialNamebyID]
@CourseID int
as
select * from [dbo].[vt_CourseMaterial] where ID=@CourseID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDepartmentNamebyID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetDepartmentNamebyID]
@DepartID int
as 
select [DepartmentName] from vt_Departments where ID=@DepartID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDepartName_byDepartID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_GetDepartName_byDepartID]
@DepartID int
AS
Select DepartmentName from vt_Departments
where ID = @DepartID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDeparts_ByTNATrainingID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetDeparts_ByTNATrainingID]
@TNAID int,
@TrainingID int
AS
Select DepartmentID,Depart.DepartmentName,IsActive from vt_TNADetails as Detail
inner join vt_Departments as Depart
on Depart.ID = Detail.DepartmentID
where TNAID = @TNAID and TrainingID = @TrainingID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDeparts_ByTrainingID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetDeparts_ByTrainingID]
@TrainingID int
AS
Select Depart.ID as DepartmentID,Depart.DepartmentName,Train.[TrainingName] from vt_Trainings as Train
inner join vt_TopicTypes as TT
on TT.TopicID = Train.TopicID
inner join vt_Departments as Depart
on Depart.ID = TT.DepartmentID
where Train.ID = @TrainingID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDeptListviaTNAnotEnd]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetDeptListviaTNAnotEnd]
@TNAID int,
@TrainingID int 
as 
select  distinct d.* from [dbo].[vt_Departments] d inner join [dbo].[vt_TNATrainee] tt on d.ID=tt.DepartmentID
inner join [dbo].[vt_TNA] t on t.ID=tt.TNAID
where tt.TrainingID=1 and tt.TNAID=1 and cast( format (t.LastSubmissionDate,'d','en-us') as datetime) >= format (getdate(),'d','en-us')
GO
/****** Object:  StoredProcedure [dbo].[sp_GetDeptofTNAforEmail]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetDeptofTNAforEmail]
@TNAID int
as 
select U.LoginID,TD.ID,TD.TNAID,Tr.TrainingName,T.TNAName,T.StartDate,T.LastSubmissionDate as EndDate,D.ID as DeptID,D.DepartmentName,U.Email,U.FullName as EmployeeName
from vt_TNADetails TD
inner join [dbo].[vt_TNA] T on TD.TNAID=T.ID
INNER JOIN [dbo].[vt_Trainings] Tr on Tr.ID =TD.TrainingID
INNER JOIN [dbo].[vt_Departments] D on D.ID=TD.DepartmentID
INNER JOIN [dbo].[vt_UserProfile] U on  U.DepartmentID=TD.DepartmentID
where TD.TNAID=@TNAID and TD.isActive=1 and TD.PublishEmail is null order by DeptID,U.LoginID asc
GO
/****** Object:  StoredProcedure [dbo].[sp_GetEmailTemplates]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetEmailTemplates]
as
select * from vt_EmailManagement
GO
/****** Object:  StoredProcedure [dbo].[sp_GetEmailTemplatesofTNAbyID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetEmailTemplatesofTNAbyID]
as
select * from vt_EmailManagement where EmailType=1
GO
/****** Object:  StoredProcedure [dbo].[sp_GetFeedbackValue]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetFeedbackValue]
@SessionID int,
@TNAID int,
@TrainingID int,
@UserID int,
@DeptID int
as
select * from [dbo].[vt_SessionParticipant] where 
SessionID=@SessionID and 
TNAID=@TNAID and 
TrainingID=@TrainingID and
DeptID=@DeptID and 
UserID=@UserID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetHeirarchy]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetHeirarchy]
as
select * from [dbo].[vt_Heirarchy]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetLineManagersList]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetLineManagersList]
as
begin
select distinct EmpID,Name,[dbo].[GetEmployeeEmail](EmpID) Email,[dbo].[GetEmployeeDepartID](EmpID) as DeptID,[dbo].[GetEmployeeLoginID](EmpID) as LoginID,dbo.GetEmployeeLocation(EmpID) as Location,dbo.GetEmployeeDepart(EmpID) as Department,[dbo].[GetEmployeeExist](EmpID) as IsExist from [dbo].[vt_Heirarchy] where EmpID in 
(select distinct ReporteeID from [dbo].[vt_Heirarchy] where DepartmentName IN (SELECT DISTINCT  DepartmentName FROM [dbo].[vt_Departments]))
ORDER BY Location,Department desc
end
GO
/****** Object:  StoredProcedure [dbo].[sp_GetParticipantsList]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetParticipantsList]
@TNAID int,
@SessionID int,
@TrainingID int,
@DeptID int
as
select * from [dbo].[vt_SessionParticipant] sp 
inner join [dbo].[vt_Heirarchy] h on h.ReporteeID=sp.UserID
where sp.TNAID=@TNAID and sp.TrainingID=@TrainingID and sp.SessionID=@SessionID and sp.DeptID=@DeptID
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetParticipantsWiseReport]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_GetParticipantsWiseReport]
@EmpID int,
@DeptID int
as
select TP.TopicName, TR.TrainingName, T.TNAName,S.SessionName ,U.FullName as TrainerName,isnull(SP.Feedback,'') as TrainerFeedback,isnull(SP.Attendence,0) as Attendance,S.StartDate,format(SP.StartDateTime,'hh:mm tt') as StartTime,S.EndDate,format(SP.EndDateTime,'hh:mm tt') as EndTime
from  [dbo].[vt_SessionParticipant] SP
inner join [dbo].[vt_Session] S on S.ID=SP.SessionID
inner join [dbo].[vt_TNA] T on T.ID= SP.TNAID
inner join [dbo].[vt_Trainings] TR on TR.ID =SP.TrainingID
inner join [dbo].[vt_Topics] TP on TP.ID=TR.TopicID
inner join [dbo].[vt_UserProfile] U on U.ID=S.TrainerID 
WHERE SP.UserID=@EmpID and SP.DeptID= @DeptID
GO
/****** Object:  StoredProcedure [dbo].[Sp_GetParticipantsWiseReportForAdmin]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Sp_GetParticipantsWiseReportForAdmin] --25,3086
@DeptID int,
@EmpID int
as
select H.ReporteeName,TP.TopicName, TR.TrainingName, T.TNAName,S.SessionName ,U.FullName as TrainerName,isnull(SP.Feedback,'') as TrainerFeedback,
isnull(SP.Attendence,0) as Attendance,S.StartDate,format(S.StartTime,'hh:mm tt') as StartTime,S.EndDate,
format(S.EndTime,'hh:mm tt') as EndTime 
from  [dbo].[vt_SessionParticipant] SP

inner join [dbo].[vt_Session] S on S.ID=SP.SessionID
inner join [dbo].[vt_TNA] T on T.ID= SP.TNAID
inner join [dbo].[vt_Trainings] TR on TR.ID =SP.TrainingID
inner join [dbo].[vt_Topics] TP on TP.ID=TR.TopicID
inner join [dbo].[vt_UserProfile] U on U.ID=S.TrainerID 
inner join [dbo].[vt_Heirarchy] H on H.ReporteeID = SP.UserID
WHERE SP.DeptID=@DeptID and SP.UserID=@EmpID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetsessDateTimebySessionID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sp_GetsessDateTimebySessionID]

@SessionID int

as
select * from vt_Session where ID =@SessionID
GO
/****** Object:  StoredProcedure [dbo].[sp_GetsessDateTimebyUserID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Proc [dbo].[sp_GetsessDateTimebyUserID]
@UserID int
as
select s.* from vt_SessionParticipant sp inner join 
[dbo].[vt_Session] s on  sp.SessionID=s.ID
where sp.UserID=@UserID and  sp.StartDateTime is not null
GO
/****** Object:  StoredProcedure [dbo].[sp_GetSessionEmailTemplate]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetSessionEmailTemplate]
as
select * from [dbo].[vt_EmailManagement] where EmailType=2
GO
/****** Object:  StoredProcedure [dbo].[sp_GetSessionPublishAssignedUsers]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetSessionPublishAssignedUsers]
@TNAID int,
@TrainingID int,
@SessionID int
AS
Select Sp.ID,H.ReporteeName as EmployeeName,H.Email,U.FullName as TrainerName,T.TrainingName as TrainingName,Format(S.StartDate,'MMM dd, yyyy') as StartDate,Format(S.StartTime,'hh:mm tt') as StartTime,Format(S.EndDate,'MMM dd, yyyy') as EndDate,Format(S.EndTime
,'hh:mm tt') as EndTime,S.Venue 
from vt_Session as S
inner join vt_SessionParticipant as SP
on S.ID = SP.SessionID
inner join vt_Trainings as T
on S.TrainingID = T.ID
inner join vt_UserProfile as U
on S.TrainerID = U.ID
inner join vt_Heirarchy as H
on SP.UserID = H.ReporteeID
where S.ID = @SessionID and S.TNAID = @TNAID and S.TrainingID = @TrainingID and SP.PublishEmail is null
GO
/****** Object:  StoredProcedure [dbo].[sp_GetTrainingLockedDepartments]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_GetTrainingLockedDepartments]
@TNAID int,
@TrainingID int
AS
Select DepartmentID from vt_TNADetails
where TNAID = @TNAID and TrainingID = @TrainingID and IsActive = 1
GO
/****** Object:  StoredProcedure [dbo].[sp_GetUsersByDeptID]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_GetUsersByDeptID]
@DeptID int
as
select H.ReporteeID,H.ReporteeName from vt_departments D
inner join vt_heirarchy H 
on D.DepartmentName = H.DepartmentName
where D.ID = @DeptID
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertAuditLog]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_InsertAuditLog]
@UserID int, 
@ViewName varchar(100),
@PreviousData varchar(8000),
@ChangeData varchar(8000),
@FieldName varchar(50),
@TrainingID varchar(50),
@TopicID varchar(50),
@CreatedAt datetime
as
insert into [dbo].[vt_AuditLog] (UserID,ViewName,PreviousData,ChangeData,FieldName,TrainingID,TopicID,CreatedAt)
values
(@UserID,@ViewName,@PreviousData,@ChangeData,@FieldName,@TrainingID,@TopicID,@CreatedAt)
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertDepartment]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[sp_InsertDepartment]
@DepartName varchar(MAX)
AS
Insert into vt_Departments (DepartmentName) Values (@DepartName)
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertUpdateEmployee]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_InsertUpdateEmployee]
@ID int ,
@EmpID int,
@Name varchar(MAX),
@ReporteeID int,
@ReporteeName varchar(MAX),
@DepartmentName varchar(MAX),
@Department varchar(MAX),
@LoginID varchar(MAX),
@Email varchar(MAX),
@Grade varchar(MAX),
@Designation varchar(MAX),
@Location varchar(MAX),
@OrgUnit varchar(MAX),
@Division varchar(MAX),
@IsUpdate bit,
@IsActive bit
AS

IF(@ID > 0)
	Update vt_Heirarchy
	SET EmpID = @EmpID ,Name = @Name ,ReporteeID = @ReporteeID ,ReporteeName = @ReporteeName,
	DepartmentName = @DepartmentName, Department = @Department , Email = @Email, 
	Grade = @Grade ,Designation = @Designation , Location = @Location,
	OrgUnit = @OrgUnit , Division = @Division , IsUpdate = @IsUpdate,IsActive= @IsActive where LoginID = @LoginID
ELSE
	Insert into vt_Heirarchy (EmpID,Name,ReporteeID,ReporteeName,DepartmentName,Department,LoginID,Email,
	Grade,Designation,Location,OrgUnit,Division,IsUpdate,IsActive)
	Values (@EmpID,@Name,@ReporteeID,@ReporteeName,@DepartmentName,@Department,@LoginID,@Email,
	@Grade,@Designation,@Location,@OrgUnit,@Division,@IsUpdate,@IsActive)


GO
/****** Object:  StoredProcedure [dbo].[sp_SaveChangesEmail]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_SaveChangesEmail]
@ID int,
@Subject varchar(500),
@EmailType int,
@Body text
as 
if @ID>0
	begin
		update vt_EmailManagement set [Subject]=@Subject, Body=@Body where ID=@ID
	end

else
	begin
		insert into vt_EmailManagement ([Subject],Body,EmailType) values(@Subject,@Body,@EmailType)
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_SaveFeedback]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_SaveFeedback]
@SessionID int,
@TNAID int,
@TrainingID int,
@UserID int,
@DeptID int,
@Rate1 float,
@Rate2 float,
@Rate3 float,
@Rate4 float,
@Rate5 float,
@Rate6 float
as
update [dbo].[vt_SessionParticipant] set Rate1=@Rate1, Rate2=@Rate2,Rate3=@Rate3,Rate4=@Rate4,Rate5=@Rate5,Rate6=@Rate6
where SessionID=@SessionID and TNAID=@TNAID and TrainingID=@TrainingID and DeptID=@DeptID and UserID=@UserID
GO
/****** Object:  StoredProcedure [dbo].[sp_SetIsActiveFalseRemaining]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_SetIsActiveFalseRemaining]
as
update [dbo].[vt_Heirarchy] set IsActive=0 where IsUpdate=0
GO
/****** Object:  StoredProcedure [dbo].[sp_SetPublishEmailTrue]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_SetPublishEmailTrue]
@ID int
as

update [dbo].[vt_SessionParticipant] set PublishEmail = 1  where ID=@ID
GO
/****** Object:  StoredProcedure [dbo].[sp_ShowSessionDeptWise_LM]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_ShowSessionDeptWise_LM] --25
@DeptID int
as
select distinct (select Count(ID) from [dbo].[vt_SessionParticipant] where SessionID=s.ID) Participants, 
s.ID as SessionID,
s.TNAID,
s.TrainingID,
tna.TNAName,
t.TrainingName,
s.SessionName,
s.FeedbackDate as FeedbackSubmitDate,
s.StartDate,
s.EndDate,
format(s.StartTime,'hh:mm tt') as StartTime ,format(s.EndTime,'hh:mm tt') as EndTime 
from [dbo].[vt_Session] s
left outer join [dbo].[vt_SessionParticipant] sp on sp.SessionID=s.ID
inner join [dbo].[vt_Trainings] t on t.ID = sp.TrainingID
inner join [dbo].[vt_TNA] tna on tna.ID=sp.TNAID
where 
sp.DeptID=@DeptID and 
s.LineManagerFeedback is null and 
CAST(s.FeedbackDate as date) >= CAST(getDate() as date) and
s.IsClosed=1 
--group by s.ID 
order by S.ID desc
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateEmailColumn]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_UpdateEmailColumn]
 @ID int
 as
 update vt_TNADetails set PublishEmail=1 where ID=@ID
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateHeirarchy]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_UpdateHeirarchy]
as
update [dbo].[vt_Heirarchy] set IsUpdate=0;
GO
/****** Object:  StoredProcedure [dbo].[SubmitSessionFeedback]    Script Date: 10/11/2019 2:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[SubmitSessionFeedback]
@ID int
as
update [dbo].[vt_Session] set  LineManagerFeedback=1 where ID=@ID
GO
USE [vt_TNADB]
GO
ALTER DATABASE [vt_TNADB] SET  READ_WRITE 
GO
