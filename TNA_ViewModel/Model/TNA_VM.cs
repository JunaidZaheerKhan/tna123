﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TNA_ViewModel.Model
{
    public class TNA_VM
    {
        public class ResponseHeader
        {
            public bool Status { get; set; }
            public string Message { get; set; }
        }
        public class LoginForm
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            [Required(ErrorMessage = "User Name is Required")]
            public string UserName { get; set; }
            [Required(ErrorMessage = "Password is Required")]
            public string Password { get; set; }
        }

        public class TopicForm
        {
            public int ID { get; set; }
            public string TopicName { get; set; }
            public string Description { get; set; }
            public bool IsActive { get; set; }
            public int[] Departments { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
        }
        public class TopicDepartments
        {
            public int TopicID { get; set; }
            public int[] DepartmentID { get; set; }
        }

        public class TrainingForm
        {
            public int ID { get; set; }
            public int TopicID { get; set; }
            public string TrainingName { get; set; }
            public int CourseOwnerID { get; set; }
            public string Description { get; set; }
            public int ModeID { get; set; }
            public string iLearnURL { get; set; }
            public int TypeID { get; set; }
            public Nullable<decimal> CostPer { get; set; }
            public Nullable<int> Days { get; set; }
            public bool IsActive { get; set; }
            public int CreatedBy { get; set; }
            public int[] CourseMaterial { get; set; }
            public string[] Attachments { get; set; }
            public HttpFileCollectionBase[] filesattachment { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
            public int LM_Limit { get; set; }
        }
        public class TrainingsAttachments
        {
            public int TrainingID { get; set; }
            public string Attachment { get; set; }
            public string DocType { get; set; }
            public string Path { get; set; }
        }
        public class TrainingsCourseMaterial
        {
            public int TrainingID { get; set; }
            public int CourseMaterialID { get; set; }
        }


        public class UserForm
        {
            public int ID { get; set; }
            public string LoginID { get; set; }
            public int RoleID { get; set; }
            public int? TypeID { get; set; }
            public int[] Geography { get; set; }
            public string Email { get; set; }
            public string FullName { get; set; }
            public int EmployeeID { get; set; }
            public int DepartmentID { get; set; }
            public int HarmonyID { get; set; }
            public int WorkdayID { get; set; }
            public bool IsActive { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
           public string LineManagerName { get; set; }
            public string Location { get; set; }
        }
        public class GetLineManagers {
            public List<LineManagers> Values { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
           
        }
        public class LineManagers
        {
            
            public int EmpID { get; set; }
            public string Name { get; set; }
            public string Email { get; set; }
            public int DeptID { get; set; }
            public string LoginID { get; set; }
            public string Location { get; set; }
            public string Department { get; set; }
            public bool IsExist { get; set; }
            public int RoleID { get; set; }
            public bool IsActive { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
            public bool IsApproved { get; set; }
            public DateTime Created { get; set; }

        }

        public class HarmonyUser
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public int EmployeeID { get; set; }
            public int DepartmentID { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string LineManagerName { get; set; }
            public string Location { get; set; }
        }

        public class UserGeography
        {
            public int UserID { get; set; }
            public int GeographyID { get; set; }
        }

        public class TNAForm
        {
            public int ID { get; set; }
            public string TNAName { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public string LastDateforLM { get; set; }
            public int[] TrainingIDs { get; set; }
            public List<TNA_Trainings> Trainings { get; set; }
            public bool IsActive { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
        }

        public class TNADetails
        {
            public int TNA_ID { get; set; }
            public int Training_ID { get; set; }
            public string Training_Name { get; set; }
            public int Department_ID { get; set; }
            public string Department_Name { get; set; }
            public bool Department_IsLocked { get; set; }
        }

        public class SessionForm
        {
            public int ID { get; set; }
            public int TNAID { get; set; }
            public int TrainingID { get; set; }
            public string SessionName { get; set; }
            public bool Multimedia { get; set; }
            public bool Stationary { get; set; }
            public bool Hall { get; set; }
            public string Other { get; set; }
            public int TrainerID { get; set; }
            public string StartDate { get; set; }
            public string EndDate { get; set; }
            public Nullable<DateTime> StartTime { get; set; }
            public Nullable<DateTime> EndTime { get; set; }
            public DateTime TNA_StartTime { get; set; }
            public DateTime TNA_EndTime { get; set; }
            public string FeedbackDate { get; set; }
            public string Venue { get; set; }
            public Nullable<decimal> CostPerhead { get; set; }
            public int GeographicID { get; set; }
            public int Participants { get; set; }
            public bool IsClosed { get; set; }
            public bool Status { get; set; }
            public string Message { get; set; }
            public int SessionType { get; set; }
            public string TrainerName { get; set; }
        }

        public class AssignedUserGrid
        {
            public int ID { get; set; }
            public int SessionID { get; set; }
            public bool SessionClosed { get; set; }
            public int UserID { get; set; }
            public string UserName { get; set; }
            public bool Attendence { get; set; }
            public string Feedback { get; set; }
            public Nullable<DateTime> StartDate { get; set; }
            public Nullable<DateTime> EndDate { get; set; }
            public string SessionName { get; set; }
            public bool PublishEmail { get; set; }
        }



        #region Select Box Classes

        public class SelectBoxResponse
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public List<SelectBoxProperties> data { get; set; }

        }

        public class SelectBoxProperties
        {
            public int ID { get; set; }
            public string Value { get; set; }
        }




        #endregion


        public class ActiveTnaAndTrainings_mdl
        {
            public int TNAID { get; set; }
            public string TNAName { get; set; }
            public string TrainingName { get; set; }
            public string Descriptions { get; set; }
            public int Days { get; set; }
            public int TrainingID { get; set; }
        }

        public class ActiveTrainingsSessions_mdl
        {
            public int TNAID { get; set; }
            public string TNAName { get; set; }
            public int TrainingID { get; set; }
            public string TrainingName { get; set; }
            public string Descriptions { get; set; }
            public int Days { get; set; }
            public int SessionID { get; set; }
            public string SessionName { get; set; }
            public Nullable<DateTime> SessionStart { get; set; }
            public Nullable<DateTime> SessionEnd { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public int Participants { get; set; }
            public string Session_Start { get; set; }
            public string Session_End { get; set; }

           

        }

        public partial class userdetailforpdf
        {


            public string ReporteeName { get; set; }        
           public Nullable<bool> Attendence { get; set; }
            public string Feedback { get; set; }
            public string DepartmentName { get; set; }

        }
        public class ActiveTrainingsSessions_mdlForPDF
        {
            public int TNAID { get; set; }
            public string TNAName { get; set; }
            public int TrainingID { get; set; }
            public string TrainingName { get; set; }
            public string Descriptions { get; set; }
            public int Days { get; set; }
            public int SessionID { get; set; }
            public string SessionName { get; set; }          
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public int Participants { get; set; }
            public string Session_Start { get; set; }
            public string Session_End { get; set; }
            public string TrainerName { get; set; }



        }
        public class ActiveAssignedDepartmentsEmployee_mdl
        {
            public int DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public int UserID { get; set; }
            public string EmployeeName { get; set; }
            public int TotalCount { get; set; }

        }
        public class SessionParticipants_mdl
        {
            public int TNAID { get; set; }
            public int TrainingID { get; set; }
            public int SessionID { get; set; }
            public int DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public int UserID { get; set; }
            public string EmployeeName { get; set; }
           // public bool PublishEmail { get; set; }

        }

        #region Line Manager

        public class Feedback {
            public double Rate1 { get; set; }
            public double Rate2 { get; set; }
            public double Rate3 { get; set; }
            public double Rate4 { get; set; }
            public double Rate5 { get; set; }
            public double Rate6 { get; set; }
            public int DeptID { get; set; }
            public int SessionID { get; set; }
            public int TNAID { get; set; }
            public int TrainingID { get; set; }
            public int UserID { get; set; }
           
        }
        public class GetAllTrainings_LM
        {
            public int TnaId { get; set; }
            public string TNAName { get; set; }
            public int TrainingId { get; set; }
            public string TrainingName { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public DateTime LastDateforLM { get; set; }
            public string Description { get; set; }
            public int DepartmentId { get; set; }
            public int ParticipantsCount { get; set; }
            public int LM_Limit { get; set; }
        }

        public class Add_or_Remove_Participants {
            public int [] Participants_IDs { get; set; }
            public int TNA_ID { get; set; }
            public int Training_ID { get; set; }
            public int Department_Id { get; set; }
            public int LM_ID { get; set; }

        }
        public class Remove_Participants_From_Session
        {
            public int SPID { get; set; }
            public int SessionID { get; set; }
            public int TNAID { get; set; }
            public int TrainingID { get; set; }
            public int ReporteeID { get; set; }
            public int DeptID { get; set; }
            public int LMID { get; set; }

        }

        public class TNATrainee_mdl
        {
            public int DepartmentId { get; set; }
            public int TNAID { get; set; }
            public int TrainingID { get; set; }
            public int UserID { get; set; }
            public string UserName { get; set; }
            public string Location { get; set; }
            public int RoleID { get; set; }
            public int EmpID { get; set; }
            public int SessionID { get; set; }

        }

        public class GetDepartLM
        {
            public int LMID { get; set; }
            public string LMName { get; set; }
            public List<GetDepartUsers> ListDepartUsers { get; set; }

        }
        public class GetDepartUsers
        {
            public int ReporteeID { get; set; }
            public string ReporteeName { get; set; }
            public string DepartmentName { get; set; }

        }


        public class GetTrainingDepartList
        {
            public int TrainingID { get; set; }
            public string TrainingName { get; set; }
            public bool Training_IsSelected { get; set; }
            public int DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public bool Department_IsActive { get; set; }
            public int ParentID  { get; set; }
        }

        public class TNA_Trainings
        {
            public int TrainingID { get; set; }
            public string TrainingName { get; set; }
            public bool Training_IsSelected { get; set; }
            public List<TNA_TrainingDepartment> Departs { get; set; }
        }

        public class TNA_TrainingDepartment
        {
            public int DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public bool Department_IsLocked { get; set; }
            public string TrainingName { get; set; }

        }

        public class GetTrainingDepartment
        {
            public int TrainingID { get; set; }
            public int TNAID { get; set; }
        }

        public class SubmitTrainingDeaprts
        {
            public int TrainingID { get; set; }
            public int TNAID { get; set; }
            public List<TNA_Details> DepartGrid { get; set; }
        }
        public class TNA_Details
        {
            public int TNA_ID { get; set; }
            public int TrainingID { get; set; }
            public string TrainingName { get; set; }
            public int DepartmentID { get; set; }
            public string DepartmentName { get; set; }
            public bool Department_IsLocked { get; set; }
        }
        #endregion


        public class HarmonyUsers
        {
            public int ID { get; set; }
            public int EmpID { get; set; }
            public string Name { get; set; }
            public int ReporteeID { get; set; }
            public string ReporteeName { get; set; }
            public string DepartmentName { get; set; }
            public string Department { get; set; }
            public string LoginID { get; set; }
            public string Email { get; set; }
            public string Grade { get; set; }
            public string Designation { get; set; }
            public string Location { get; set; }
            public string OrgUnit { get; set; }
            public string Division { get; set; }
            public bool IsUpdate { get; set; }
            public bool IsActive { get; set; }
            public string workDayId { get; set; }
        }

    }
}



//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Web;

//namespace TNA_ViewModel.Model
//{
//    public class TNA_VM
//    {
//        public class ResponseHeader
//        {
//            public bool Status { get; set; }
//            public string Message { get; set; }
//        }
//        public class LoginForm
//        {
//            public bool Status { get; set; }
//            public string Message { get; set; }
//            [Required(ErrorMessage ="User Name is Required")]
//            public string UserName { get; set; }
//            [Required(ErrorMessage = "Password is Required")]
//            public string Password { get; set; }
//        }

//        public class TopicForm
//        {
//            public int ID { get; set; }
//            public string TopicName { get; set; }
//            public string Description { get; set; }
//            public bool IsActive { get; set; }
//            public int[] Departments { get; set; }
//            public bool Status { get; set; }
//            public string Message { get; set; }
//        }
//        public class TopicDepartments
//        {
//            public int TopicID { get; set; }
//            public int[] DepartmentID { get; set; }
//        }

//        public class TrainingForm
//        {
//            public int ID { get; set; }
//            public int TopicID { get; set; }
//            public string TrainingName { get; set; }
//            public int CourseOwnerID { get; set; }
//            public string Description { get; set; }
//            public int ModeID { get; set; }
//            public string iLearnURL { get; set; }
//            public int TypeID { get; set; }
//            public decimal CostPer { get; set; }
//            public int Days { get; set; }
//            public bool IsActive { get; set; }
//            public int CreatedBy { get; set; }
//            public int[] CourseMaterial { get; set; }
//            public string[] Attachments { get; set; }
//            public HttpFileCollectionBase[] filesattachment { get; set; }
//            public bool Status { get; set; }
//            public string Message { get; set; }
//        }
//        public class TrainingsAttachments
//        {
//            public int TrainingID { get; set; }
//            public string Attachment { get; set; }
//            public string DocType { get; set; }
//            public string Path { get; set; }
//        }
//        public class TrainingsCourseMaterial
//        {
//            public int TrainingID { get; set; }
//            public int CourseMaterialID { get; set; }
//        }


//        public class UserForm
//        {
//            public int ID { get; set; }
//            public string LoginID { get; set; }
//            public int RoleID { get; set; }
//            public int? TypeID { get; set; }
//            public int[] Geography { get; set; }
//            public string Email { get; set; }
//            public string FullName { get; set; }
//            public int EmployeeID { get; set; }
//            public int DepartmentID { get; set; }
//            public int HarmonyID { get; set; }
//            public int WorkdayID { get; set; }
//            public bool IsActive { get; set; }
//            public bool Status { get; set; }
//            public string Message { get; set; }
//        }

//        public class HarmonyUser
//        {
//            public bool Status { get; set; }
//            public string Message { get; set; }
//            public int EmployeeID { get; set; }
//            public int DepartmentID { get; set; }
//            public string FullName { get; set; }
//        }

//        public class UserGeography
//        {
//            public int UserID { get; set; }
//            public int GeographyID { get; set; }
//        }

//        public class TNAForm
//        {
//            public int ID { get; set; }
//            public string TNAName { get; set; }
//            public string StartDate { get; set; }
//            public string EndDate { get; set; }
//            public int[] TrainingIDs { get; set; }
//            public bool IsActive { get; set; }
//            public bool Status { get; set; }
//            public string Message { get; set; }
//        }

//        public class TNADetails
//        {
//            public int TNA_ID { get; set; }
//            public int Training_ID { get; set; }
//        }

//        public class SessionForm
//        {
//            public int ID { get; set; }
//            public int TNAID { get; set; }
//            public int TrainingID { get; set; }
//            public string SessionName { get; set; }
//            public bool Multimedia { get; set; }
//            public bool Stationary { get; set; }
//            public bool Hall { get; set; }
//            public string Other { get; set; }
//            public int TrainerID { get; set; }
//            public string StartDate { get; set; }
//            public string EndDate { get; set; }
//            public string FeedbackDate { get; set; }
//            public string Venue { get; set; }
//            public decimal CostPerhead { get; set; }
//            public int GeographicID { get; set; }
//            public int Participants { get; set; }
//            public bool Status { get; set; }
//            public string Message { get; set; }
//        }

//        public class AssignedUserGrid
//        {
//            public int ID { get; set; }
//            public int SessionID { get; set; }
//            public int UserID { get; set; }
//            public string UserName { get; set; }
//            public bool Attendence { get; set; }
//            public string Feedback { get; set; }
//        }



//        #region Select Box Classes

//        public class SelectBoxResponse
//        {
//            public bool Status { get; set; }
//            public string Message { get; set; }
//            public List<SelectBoxProperties> data { get; set; }

//        }

//        public class SelectBoxProperties
//        {
//            public int ID { get; set; }
//            public string Value { get; set; }
//        }




//        #endregion

//        #region Line Manager
//        public class GetAllTrainings_LM
//        {
//            public int TnaId { get; set; }
//            public string TNAName { get; set; }
//            public int TrainingId { get; set; }
//            public string TrainingName { get; set; }
//            public DateTime StartDate { get; set; }
//            public DateTime EndDate { get; set; }
//            public string Description { get; set; } 
//            public int DepartmentId { get; set; }
//        }

//        public class TNATrainee_mdl
//        {
//            public int DepartmentId { get; set; }
//            public int TNAID { get; set; }
//            public int TrainingID { get; set; }
//            public int UserID { get; set; }
//            public string UserName { get; set; }

//        }

//        #endregion
//    }
//}
