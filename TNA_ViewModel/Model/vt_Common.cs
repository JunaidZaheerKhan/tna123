﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using System.Globalization;
using System.Web;
using System.Xml;
using System.Data;
using System.Security.Cryptography.X509Certificates;
using System.Net.Sockets;
using System.Net.Security;
using System.Diagnostics;
using System.DirectoryServices;
using System.Web.Configuration;
using DAL.DBEntities;
using System.Reflection;
using Newtonsoft.Json;
using static TNA_ViewModel.Model.TNA_VM;
using System.Data.SqlClient;
using TNA_ViewModel.Model;

namespace TNAApp_ViewModel.Model
{
    public class vt_Common
    {
        public static  string Con =Convert.ToString(ConfigurationManager.AppSettings["ConnStr"]);

        #region Active Directory

        public static bool ValidateUser(string path, string str_Username, string str_Password)
        {
            try
            {
                //return true;

                using (DirectoryEntry adsEntry = new DirectoryEntry(path, str_Username, str_Password))
                {
                    using (DirectorySearcher adsSearcher = new DirectorySearcher(adsEntry))
                    {
                        //adsSearcher.Filter = "(&(objectClass=user)(objectCategory=person))";
                        adsSearcher.Filter = "(sAMAccountName=" + str_Username.ToLower() + ")";
                        try
                        {
                            SearchResult adsSearchResult = adsSearcher.FindOne();
                            return true;


                        }
                        catch (Exception)
                        {
                            // Failed to authenticate. Most likely it is caused by unknown user
                            // id or bad strPassword.
                            return false;
                        }
                        finally
                        {
                            adsEntry.Close();
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool ValidateUser(string path, string strAccountId, string strPassword, string chkAccountId)
        {
            try
            {
                using (DirectoryEntry adsEntry = new DirectoryEntry(WebConfigurationManager.AppSettings["ActiveDirectoryPath"].ToString(), WebConfigurationManager.AppSettings["ActiveDirectory_ID"].ToString(), WebConfigurationManager.AppSettings["ActiveDirectory_Pass"].ToString()))
                {
                    using (DirectorySearcher adsSearcher = new DirectorySearcher(adsEntry))
                    {
                        //adsSearcher.Filter = "(&(objectClass=user)(objectCategory=person))";
                        adsSearcher.Filter = "(sAMAccountName=" + chkAccountId.ToLower() + ")";
                        try
                        {
                            SearchResult adsSearchResult = adsSearcher.FindOne();
                            if (adsSearchResult != null)
                            {
                                return true;
                            }
                            return false;
                        }
                        catch (Exception)
                        {
                            // Failed to authenticate. Most likely it is caused by unknown user
                            // id or bad strPassword.
                            return false;
                        }
                        finally
                        {
                            adsEntry.Close();
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Security

        public static string DecryptPassword(string Password)
        {
            return Decrypt(Password);
        }

        public static string EncryptPassword(string Password)
        {
            return Encrypt(Password);
        }

        public static string Encrypt(string originalString)
        {
            return Encrypt(originalString, getKey);
        }

        public static byte[] getKey
        {
            get
            {
                return ASCIIEncoding.ASCII.GetBytes(ConfigurationManager.AppSettings["EncryptKey"].ToString());
            }
        }
        public static string Encrypt(string originalString, byte[] bytes)
        {
            try
            {
                if (String.IsNullOrEmpty(originalString))
                {
                    throw new ArgumentNullException("The string which needs to be encrypted can not be null.");
                }

                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream memoryStream = new MemoryStream();
                CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateEncryptor(bytes, bytes), CryptoStreamMode.Write);

                StreamWriter writer = new StreamWriter(cryptoStream);
                writer.Write(originalString);
                writer.Flush();
                cryptoStream.FlushFinalBlock();
                writer.Flush();


                //return Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                var Converting = Convert.ToBase64String(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                Converting = Regex.Replace(Converting, "/", "-");
                Converting = Regex.Replace(Converting, "[+]", "_");
                return Converting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Decrypt(string originalString)
        {
            return Decrypt(originalString, getKey);
        }

        public static string Decrypt(string cryptedString, byte[] bytes)
        {
            try
            {
                if (String.IsNullOrEmpty(cryptedString))
                {
                    throw new ArgumentNullException("The string which needs to be decrypted can not be null.");
                }
                cryptedString = Regex.Replace(cryptedString, "-", "/");
                cryptedString = Regex.Replace(cryptedString, "[ ]", "+");
                cryptedString = Regex.Replace(cryptedString, "_", "+");

                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(cryptedString));
                CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoProvider.CreateDecryptor(bytes, bytes), CryptoStreamMode.Read);
                StreamReader reader = new StreamReader(cryptoStream);

                return reader.ReadToEnd();
            }
            catch (Exception)
            {
                return "";
            }
        }




        #endregion


        #region Emails

        public static string SendingEmailToParticipants(string EmailTo, string Subject, string msgBody)
        {
            try
            {
                using (MailMessage message = new MailMessage())
                {
                    message.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"].ToString(), ConfigurationManager.AppSettings["MailFromName"].ToString());
                    message.To.Add(new MailAddress(EmailTo));
                    message.IsBodyHtml = true;
                    message.Subject = Subject; //"HumQadum " + PlanTitle + " Confirmation" + Date;
                    message.Body = msgBody;
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.UseDefaultCredentials = true;
                        smtp.Host = ConfigurationManager.AppSettings["SmtpServer"].ToString();


                        if (bool.Parse(ConfigurationManager.AppSettings["UseCredentials"]))
                        {
                            smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["UserEmail"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                        }
                        else
                        {
                            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                            smtp.UseDefaultCredentials = false;
                        }
                        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"].ToString());
                        smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["isSSL"].ToString());
                        try
                        {
                            smtp.Send(message);
                        }
                        catch (Exception ex)
                        {
                            return "Exception: " + ex.Message.ToString();
                        }
                    }
                    return "Successfully send email.";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        public static string SessionPublishEmailBody(DataRow dr,string body)
        {
            string EmailBody = @"<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
                  <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'></table>
                  <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-complete deviceWidth' bgcolor='' style='border: 3px solid #f8ae00;background-color: #fff;border-radius: 5px;box-shadow: 1px 4px 10px 8px gainsboro;'>
                <tbody>
                    <tr>
                        <td width='100%'>
                    <span class='im'>
            <div style = 'text-align: center;'>
                <h2> Welcome to TNA</h2>
                <h2> Announcement </h2>   
                <hr style = 'width:36%;background-color:#f8ae00;height:2px;border:none'>  
            </div>
        </span>
    
                    <table width='600px;' border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth' style='padding-left:12px;padding-right:12px;'>
                    <tbody>
                        <tr>
                            <td id='logo' align='left'>
                                <h3 style='margin-top: 23px;font-size: 18px;letter-spacing: 1px;'> Dear [EmployeeName] </h3>
                            </td>
                        </tr>
                        <tr>
                            <td align='left' style='margin-top:10px;'>
    
                                  <p> [BodyText] </p>
                                
                            </td>
                        </tr>
						
						  <tr>
                    <td style = 'height:60px;'>
                    </td>
                </tr>
                <tr>
                    <td align='left' style='margin-top:10px;'>
                        <h4 style = 'padding-top:0px;font-weight:600;margin-bottom:4px;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                          Regards </h4>

                        <h4 style = 'font-weight:600;margin-top:0!important;margin-bottom:0;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                         TNA Administrator</h4>
                        <a href = '#' style='display: inline-block;width: 100%;text-align: center;'>
                            <img src = 'http://tna.viftechuat.com/assets/images/dark-logo.png' alt='' border='0' style='text-align:left;margin-top:10px;margin-bottom:20px;'>
                        </a>
                    </td>

                </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>";
            var BodyText = string.Empty;
             BodyText = body.Replace("\n", "<br>");
            EmailBody = EmailBody.Replace("[BodyText]", BodyText);
            var EmployeeName = string.Empty;
            var TrainingName = string.Empty;
            var TrainerName = string.Empty;
            var StartDate = string.Empty;
            var EndDate = string.Empty;
            var StartTime = string.Empty;
            var EndTime = string.Empty;
            var Venue = string.Empty;


            if (dr["EmployeeName"] != null && dr["EmployeeName"] != DBNull.Value)
                EmployeeName = dr["EmployeeName"].ToString();
            if (dr["TrainingName"] != null && dr["TrainingName"] != DBNull.Value)
                TrainingName = dr["TrainingName"].ToString();
            if (dr["TrainerName"] != null && dr["TrainerName"] != DBNull.Value)
                TrainerName = dr["TrainerName"].ToString();
            if (dr["StartDate"] != null && dr["StartDate"] != DBNull.Value)
                StartDate = dr["StartDate"].ToString();
            if (dr["EndDate"] != null && dr["EndDate"] != DBNull.Value)
                EndDate = dr["EndDate"].ToString();
            if (dr["StartTime"] != null && dr["StartTime"] != DBNull.Value)
                StartTime = dr["StartTime"].ToString();
            if (dr["EndTime"] != null && dr["EndTime"] != DBNull.Value)
                EndTime = dr["EndTime"].ToString();
          
            if (dr["Venue"] != null && dr["Venue"] != DBNull.Value)
                Venue = dr["Venue"].ToString();

            EmailBody = EmailBody.Replace("[EmployeeName]", EmployeeName);
            EmailBody = EmailBody.Replace("[TrainingName]", TrainingName);
            EmailBody = EmailBody.Replace("[TrainerName]", TrainerName);
            EmailBody = EmailBody.Replace("[StartDate]", StartDate);
            EmailBody = EmailBody.Replace("[EndDate]", EndDate);
            EmailBody = EmailBody.Replace("[StartTime]", StartTime);
            EmailBody = EmailBody.Replace("[EndTime]", EndTime);
            EmailBody = EmailBody.Replace("[Venue]", Venue);

            return EmailBody;
        }

        public static string SessionCancelEmailBody(DataRow dr, string body)
        {
            string EmailBody = @"<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
                  <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'></table>
                  <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-complete deviceWidth' bgcolor='' style='border: 3px solid #f8ae00;background-color: #fff;border-radius: 5px;box-shadow: 1px 4px 10px 8px gainsboro;'>
                <tbody>
                    <tr>
                        <td width='100%'>
 <span class='im'>
            <div style = 'text-align: center;'>
                <h2> Welcome to TNA</h2>
                <h2> Announcement </h2>   
                <hr style = 'width:36%;background-color:#f8ae00;height:2px;border:none'>  
            </div>
        </span>
                    
    
                    <table width='600px;' border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth' style='padding-left:12px;padding-right:12px;'>
                    <tbody>
                        <tr>
                            <td id='logo' align='left'>
                                <h3 style='margin-top: 23px;font-size: 18px;letter-spacing: 1px;'> Dear [EmployeeName] </h3>
                            </td>
                        </tr>
                        <tr>
                            <td align='left' style='margin-top:10px;'>
    
                                  <p>[BodyText]</p>
                                
                            </td>
                        </tr>
						
						  <tr>
                    <td style = 'height:60px;'>
                    </td>
                </tr>
                <tr>
                    <td align='left' style='margin-top:10px;'>
                        <h4 style = 'padding-top:0px;font-weight:600;margin-bottom:4px;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                          Regards </h4>

                        <h4 style = 'font-weight:600;margin-top:0!important;margin-bottom:0;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                         TNA Administrator</h4>
                        <a href = '#' style='display: inline-block;width: 100%;text-align: center;'>
                            <img src = 'http://tna.viftechuat.com/assets/images/dark-logo.png' alt='' border='0' style='text-align:left;margin-top:10px;margin-bottom:20px;'>
                        </a>
                    </td>

                </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>";
            var BodyText = string.Empty;
            BodyText = body.Replace("\n", "<br>");
            EmailBody = EmailBody.Replace("[BodyText]", BodyText);
            var EmployeeName = string.Empty;
            var SessionName = string.Empty;
            if (dr["ReporteeName"] != null && dr["ReporteeName"] != DBNull.Value)
                EmployeeName = dr["ReporteeName"].ToString();
            if (dr["SessionName"] != null && dr["SessionName"] != DBNull.Value)
                SessionName = dr["SessionName"].ToString();

            //if (dr["TrainingName"] != null && dr["TrainingName"] != DBNull.Value)
            //    TrainingName = dr["TrainingName"].ToString();
            //if (dr["TrainerName"] != null && dr["TrainerName"] != DBNull.Value)
            //    TrainerName = dr["TrainerName"].ToString();
            //if (dr["StartDate"] != null && dr["StartDate"] != DBNull.Value)
            //    StartDate = dr["StartDate"].ToString();
            //if (dr["EndDate"] != null && dr["EndDate"] != DBNull.Value)
            //    EndDate = dr["EndDate"].ToString();
            //if (dr["StartTime"] != null && dr["StartTime"] != DBNull.Value)
            //    StartTime = dr["StartTime"].ToString();
            //if (dr["EndTime"] != null && dr["EndTime"] != DBNull.Value)
            //    EndTime = dr["EndTime"].ToString();


            EmailBody = EmailBody.Replace("[EmployeeName]", EmployeeName);
            EmailBody = EmailBody.Replace("[SessionName]", SessionName);
            //EmailBody = EmailBody.Replace("[TrainingName]", TrainingName);
            //EmailBody = EmailBody.Replace("[TrainerName]", TrainerName);
            //EmailBody = EmailBody.Replace("[StartDate]", StartDate);
            //EmailBody = EmailBody.Replace("[EndDate]", EndDate);
            //EmailBody = EmailBody.Replace("[StartTime]", StartTime);
            //EmailBody = EmailBody.Replace("[EndTime]", EndTime);
            //EmailBody = EmailBody.Replace("[VenueName]", Venue);

            return EmailBody;
        }
        public static string SessionPublishEmailBodyDept(DataRow dr, string TrainingList,string body)
        {


            string EmailBody = @"<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
</head>
<body>
         <table width = '100%' border = '0' cellpadding = '0' cellspacing = '0' align = 'center' ></table>
         <table width = '600' border = '0' cellpadding = '0' cellspacing = '0' align = 'center' class='border-complete deviceWidth' bgcolor='' style='border: 3px solid #f8ae00;background-color: #fff;border-radius: 5px;box-shadow: 1px 4px 10px 8px gainsboro;'>
    <tbody>
    <tr>
        <td width = '100%'>
          <span class='im'>
            <div style = 'text-align: center;'>
                <h2> Welcome to TNA</h2>
                <h2> Announcement </h2>   
                <hr style = 'width:36%;background-color:#f8ae00;height:2px;border:none'>  
            </div>
        </span>

                 <table width='600px;' border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth' style='padding-left:12px;padding-right:12px;'>
                <tbody>
                <tr>
                    <td id = 'logo' align='left'>
                        <h3 style = 'margin-top: 23px;font-size:='' 18px;letter-spacing:='' 1px;'=''> Dear [EmployeeName], </h3>
                    </td>
                </tr>
                <tr>
                    <td align = 'left' style='margin-top:10px;'>

                        <h4 style = 'padding-top: 0;font-weight: 600;font-size: 15px;letter-spacing: 1px;'>
                         [Header]</h4>
                    </td>
                </tr>

                <tr>
                    <td style = 'height:30px;'>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border='0' cellpadding='0' cellspacing='0' class=''>
                            <tbody>
                            <tr>
                                <td>
                                    <ul style = 'padding-left: 20px;'>
                                        [TrainingList]                                       
                                     </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4 style = 'padding-top: 0;font-weight: 600; font-size:15px; letter-spacing: 1px;'>
                                     [Footer]</h4>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style = 'height:60px;'>
                    </td>
                </tr>
                <tr>
                    <td align='left' style='margin-top:10px;'>
                        <h4 style = 'padding-top:0px;font-weight:600;margin-bottom:4px;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                          Regards </h4>

                        <h4 style = 'font-weight:600;margin-top:0!important;margin-bottom:0;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                        TNA Administrator</h4>
                        <a href = '#' style='display: inline-block;width: 100%;text-align: center;'>
                            <img src = 'http://tna.viftechuat.com/assets/images/dark-logo.png' alt='' border='0' style='text-align:left;margin-top:10px;margin-bottom:20px;'>
                        </a>
                    </td>

                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>";
            var BodyComponents = body.Split(new string[] { "[TrainingList]" },StringSplitOptions.None);

            var Header = BodyComponents[0].Replace("\n", "<br>");
            var Footer = BodyComponents[1].Replace("\n","<br>");
            EmailBody = EmailBody.Replace("[Header]", Header);
            EmailBody = EmailBody.Replace("[Footer]", Footer);
            var EmployeeName = string.Empty;
            var TNAName = string.Empty;
            var TrainingName = string.Empty;
            var Department = string.Empty;
            var StartDate = string.Empty;
            var EndDate = string.Empty;



            if (dr["EmployeeName"] != null && dr["EmployeeName"] != DBNull.Value)
                EmployeeName = dr["EmployeeName"].ToString();
            if (dr["TNAName"] != null && dr["TNAName"] != DBNull.Value)
                TNAName = dr["TNAName"].ToString();
            //if (dr["TrainingName"] != null && dr["TrainingName"] != DBNull.Value)
            //    TrainingName = dr["TrainingName"].ToString();
            if (dr["DepartmentName"] != null && dr["DepartmentName"] != DBNull.Value)
                Department = dr["DepartmentName"].ToString();
            if (dr["StartDate"] != null && dr["StartDate"] != DBNull.Value)
                StartDate = dr["StartDate"].ToString();
            if (dr["EndDate"] != null && dr["EndDate"] != DBNull.Value)
                EndDate = dr["EndDate"].ToString();
            if (TrainingList != null)
            {
                var ListofTraining = TrainingList.Split(',');
                string appendtraininig = "";
                for (int i = 0; i < ListofTraining.Length; i++)
                {
                     appendtraininig+= "<li style = 'line-height: 30px; text-transform: capitalize;' >" + ListofTraining[i] + "</li >";
                }
                EmailBody = EmailBody.Replace("[TrainingList]", appendtraininig);
            }

            EmailBody = EmailBody.Replace("[EmployeeName]", EmployeeName);
            EmailBody = EmailBody.Replace("[TNAName]", TNAName);

            EmailBody = EmailBody.Replace("[DepartmentName]", Department);
            //EmailBody = EmailBody.Replace("[FromDate]", StartDate);
            //EmailBody = EmailBody.Replace("[ToDate]", EndDate);


            return EmailBody;
        }

        public static string EmailToTrainer(EmailToTrainer trainerDetail, string body)
        {
            string EmailBody = @"<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
                  <table width='100%' border='0' cellpadding='0' cellspacing='0' align='center'></table>
                  <table width='600' border='0' cellpadding='0' cellspacing='0' align='center' class='border-complete deviceWidth' bgcolor='' style='border: 3px solid #f8ae00;background-color: #fff;border-radius: 5px;box-shadow: 1px 4px 10px 8px gainsboro;'>
                <tbody>
                    <tr>
                        <td width='100%'>
                    <span class='im'>
            <div style = 'text-align: center;'>
                <h2> Welcome to TNA</h2>
                <h2> Announcement </h2>   
                <hr style = 'width:36%;background-color:#f8ae00;height:2px;border:none'>  
            </div>
        </span>
    
                    <table width='600px;' border='0' cellpadding='0' cellspacing='0' align='left' class='deviceWidth' style='padding-left:12px;padding-right:12px;'>
                    <tbody>
                        <tr>
                            <td id='logo' align='left'>
                                <h3 style='margin-top: 23px;font-size: 18px;letter-spacing: 1px;'> Dear [Receptor] </h3>
                            </td>
                        </tr>
                        <tr>
                            <td align='left' style='margin-top:10px;'>
    
                                  <p> [BodyText] </p>
                                
                            </td>
                        </tr>
						
						  <tr>
                    <td style = 'height:60px;'>
                    </td>
                </tr>
                <tr>
                    <td align='left' style='margin-top:10px;'>
                        <h4 style = 'padding-top:0px;font-weight:600;margin-bottom:4px;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                          Regards </h4>

                        <h4 style = 'font-weight:600;margin-top:0!important;margin-bottom:0;font-size:='' 15px;letter-spacing:='' 1px;'=''>
                         TNA Administrator</h4>
                        <a href = '#' style='display: inline-block;width: 100%;text-align: center;'>
                            <img src = 'http://tna.viftechuat.com/assets/images/dark-logo.png' alt='' border='0' style='text-align:left;margin-top:10px;margin-bottom:20px;'>
                        </a>
                    </td>

                </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>";
            var BodyText = string.Empty;
            BodyText = body.Replace("\n", "<br>");
            EmailBody = EmailBody.Replace("[BodyText]", BodyText);

            var Receptor = string.Empty;
            var TNAName = string.Empty;
            var TrainingName = string.Empty;
            var SessionName = string.Empty;
            var NoOfParticipants = string.Empty;



            if (trainerDetail.Receptor != null && trainerDetail.Receptor !="")
                Receptor = trainerDetail.Receptor;

            if (trainerDetail.TNAName != null && trainerDetail.TNAName != "")
                TNAName = trainerDetail.TNAName;

            if (trainerDetail.TrainingName != null && trainerDetail.TrainingName != "")
                TrainingName = trainerDetail.TrainingName;

            if (trainerDetail.SessionName != null && trainerDetail.SessionName != "")
                SessionName = trainerDetail.SessionName;
          
                NoOfParticipants = trainerDetail.NoOfParticipants.ToString();


            EmailBody = EmailBody.Replace("[Receptor]", Receptor);
            EmailBody = EmailBody.Replace("[TNAName]", TNAName);
            EmailBody = EmailBody.Replace("[TrainingName]", TrainingName);
            EmailBody = EmailBody.Replace("[SessionName]", SessionName);
            EmailBody = EmailBody.Replace("[NoOfParticipants]", NoOfParticipants);

            return EmailBody;
        }

        #endregion


        #region Audit Log

        public static string CreateAuditLog(int UserID, string ViewName, string ChangeData, string TableName)
        {
            vt_TNAEntity db = new vt_TNAEntity();
            db.Database.Connection.ConnectionString = Convert.ToString(ConfigurationManager.AppSettings["ConnStr"]);
            try
            {
                if (TableName.Contains("vt_Topic"))
                {
                    var NewValues= JsonConvert.DeserializeObject<TopicForm>(ChangeData);
                    var PreviousValues = db.vt_Topics.Where(x => x.ID == NewValues.ID).FirstOrDefault();
                    var previousDeptValues = db.vt_TopicTypes.Where(y => y.TopicID == NewValues.ID).ToList();
                   
                    if (!String.Equals(NewValues.TopicName, PreviousValues.TopicName))
                    {
                        InsertAudit(UserID, ViewName, PreviousValues.TopicName, NewValues.TopicName, "TopicName", "N/A", NewValues.ID.ToString());
                    }

                    if (!String.Equals(NewValues.Description, PreviousValues.Description))
                    {
                        InsertAudit(UserID, ViewName, PreviousValues.Description, NewValues.Description, "Description", "N/A", NewValues.ID.ToString());
                    }

                    if(NewValues.IsActive!=PreviousValues.IsActive)
                    {
                        InsertAudit(UserID, ViewName, PreviousValues.IsActive.ToString(), NewValues.IsActive.ToString(), "IsActive", "N/A", NewValues.ID.ToString());
                    }

                    string PreviousDep = ""; string NewDepValues = "";
                    if (previousDeptValues.Count!= NewValues.Departments.Length)
                    {
                        for (int i = 0; i < previousDeptValues.Count; i++)
                        {
                           
                            SqlParameter[] param = { new SqlParameter("@DepartID", previousDeptValues[i].DepartmentID) };

                            DataTable dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDepartmentNamebyID", param);
                           

                                if (dt.Rows.Count>0)
                            {
                                if (i == 0)
                                {
                                    PreviousDep += dt.Rows[0]["DepartmentName"].ToString();
                                }
                                else
                                {
                                    PreviousDep += "," + dt.Rows[0]["DepartmentName"].ToString();
                                }
                            }
                           

                            
                        }
                        
                        for (int j = 0; j < NewValues.Departments.Count(); j++)
                        {
                            
                            SqlParameter[] param = { new SqlParameter("@DepartID", NewValues.Departments[j]) };

                            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDepartmentNamebyID", param);

                            if (j == 0)
                            {
                                NewDepValues += dt.Rows[0]["DepartmentName"].ToString();
                            }
                            else
                            {
                                NewDepValues += "," + dt.Rows[0]["DepartmentName"].ToString();
                            }
                           
                        }
                        InsertAudit(UserID, ViewName, PreviousDep, NewDepValues, "Department", "N/A", NewValues.ID.ToString());

                    }
                    else
                    {
                        if (previousDeptValues.Count == NewValues.Departments.Length) {

                            bool isExist = false;
                            for (int i = 0; i < previousDeptValues.Count; i++)
                            {
                                for (int j = 0; j < NewValues.Departments.Count(); j++)
                                {
                                    if (previousDeptValues[i].DepartmentID == NewValues.Departments[j])
                                    {
                                        isExist = true;
                                        break;
                                    }

                                    else
                                    {
                                        isExist = false;
                                    }

                                    

                                }

                               
                            }
                            if (isExist == false)
                            {
                                for (int i = 0; i < previousDeptValues.Count; i++)
                                {
                                   SqlParameter[] param = {new SqlParameter("@DepartID",  previousDeptValues[i].DepartmentID )};

                                    var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDepartmentNamebyID", param);
                                    
                                    if (i==0)
                                    {
                                        PreviousDep += dt.Rows[0]["DepartmentName"].ToString();
                                    }
                                    else
                                    {
                                        PreviousDep += ","+dt.Rows[0]["DepartmentName"].ToString();
                                    }
                                   
                                }

                                for (int j = 0; j < NewValues.Departments.Count(); j++)
                                {
                                    SqlParameter[] param = { new SqlParameter("@DepartID", NewValues.Departments[j]) };

                                    var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetDepartmentNamebyID", param);

                                    if (j == 0)
                                    {
                                        NewDepValues += dt.Rows[0]["DepartmentName"].ToString();
                                    }
                                    else
                                    {
                                        NewDepValues += "," + dt.Rows[0]["DepartmentName"].ToString();
                                    }

                                   
                                   
                                }
                                InsertAudit(UserID, ViewName, PreviousDep, NewDepValues, "Department", "N/A", NewValues.ID.ToString());
                            }
                        }
                   
                        
                        
                    }

                }

                else if (TableName.Contains("vt_Trainings"))
                {
                    var NewValues = JsonConvert.DeserializeObject<TrainingForm>(ChangeData);
                    var PreviousValues = db.vt_Trainings.Where(x => x.ID == NewValues.ID).FirstOrDefault();

                    Type myType = NewValues.GetType();
                    IList<PropertyInfo> props = new List<PropertyInfo>(myType.GetProperties());       

                    var propertiesArray = props.Select(propertyInfo => propertyInfo.Name).ToArray();

                    for (int x = 0; x < propertiesArray.Length; x++)
                    {
                        string val = propertiesArray[x];
                        switch (val)
                        {
                            case ("TrainingName"):
                                if (!string.Equals(NewValues.TrainingName,PreviousValues.TrainingName))
                                {
                                    InsertAudit(UserID, ViewName, PreviousValues.TrainingName, NewValues.TrainingName, "TrainingName", NewValues.ID.ToString(), "N/A" );
                                }
                                break;
                            case ("Description"):
                                if (!string.Equals(NewValues.Description, PreviousValues.Description))
                                {
                                    InsertAudit(UserID, ViewName, PreviousValues.Description, NewValues.Description, "Description", NewValues.ID.ToString(), "N/A");
                                }
                                break;
                            case ("iLearnURL"):
                                if (!string.Equals(NewValues.iLearnURL, PreviousValues.iLearnURL))
                                {
                                    InsertAudit(UserID, ViewName, PreviousValues.iLearnURL, NewValues.iLearnURL, "iLearnURL", NewValues.ID.ToString(), "N/A");
                                }
                                break;
                            case ("CostPer"):
                                if (NewValues.CostPer!=PreviousValues.CostPerHead)
                                {
                                    InsertAudit(UserID, ViewName, Convert.ToString(PreviousValues.CostPerHead), Convert.ToDouble( NewValues.CostPer).ToString("N0"), "CostPerHead", NewValues.ID.ToString(), "N/A");
                                }
                                break;
                            case ("Days"):
                                if (NewValues.Days!=PreviousValues.Days)
                                {
                                    InsertAudit(UserID, ViewName, Convert.ToString(PreviousValues.Days), Convert.ToString(NewValues.Days), "Days", NewValues.ID.ToString(), "N/A");
                                }
                                break;
                            case ("TopicID"):
                               
                                if (NewValues.TopicID!=PreviousValues.TopicID)
                                {
                                    var getNewTopicName = db.vt_Topics.Where(a => a.ID == NewValues.TopicID).FirstOrDefault();
                                    var getPreTopicName = db.vt_Topics.Where(a => a.ID == PreviousValues.TopicID).FirstOrDefault();

                                    InsertAudit(UserID, ViewName, getPreTopicName.TopicName, getNewTopicName.TopicName, "TopicName", NewValues.ID.ToString(), "N/A");
                                }
                                break;
                            case ("ModeID"):
                                if (NewValues.ModeID!= PreviousValues.ModeID)
                                {
                                    var getNewModeName = db.vt_Training_Mode.Where(a => a.ID == NewValues.ModeID).FirstOrDefault();
                                    var getPreModeName = db.vt_Training_Mode.Where(a => a.ID == PreviousValues.ModeID).FirstOrDefault();
                                    InsertAudit(UserID, ViewName, getPreModeName.ModeOfTraining, getNewModeName.ModeOfTraining, "ModeName", NewValues.ID.ToString(), "N/A");
                                }
                                break;

                            //case ("TypeID"):
                            //    if (NewValues.TypeID != PreviousValues.TrainingTypeID)
                            //    {
                            //        var getNewTypeName = db.vt_Training_Type.Where(a => a.ID == NewValues.TypeID).FirstOrDefault();
                            //        var getPreTypeName = db.vt_Training_Type.Where(a => a.ID == PreviousValues.TrainingTypeID).FirstOrDefault();
                            //        InsertAudit(UserID, ViewName, getPreTypeName.TrainingType, getNewTypeName.TrainingType, "TrainingType", NewValues.ID.ToString(), "N/A");
                            //    }
                            //    break;

                            case ("CourseOwnerID"):
                                if (NewValues.CourseOwnerID != PreviousValues.CourseOwnerID)
                                {
                                    var getNewCourseOwnerName = db.vt_UserProfile.Where(a => a.ID == NewValues.CourseOwnerID).FirstOrDefault();
                                    var getPreCourseOwnerName = db.vt_UserProfile.Where(a => a.ID == PreviousValues.CourseOwnerID).FirstOrDefault();
                                    InsertAudit(UserID, ViewName, getPreCourseOwnerName.FullName, getNewCourseOwnerName.FullName, "CourseOwner", NewValues.ID.ToString(), "N/A");
                                }
                                break;

                            case ("IsActive"):
                                if (NewValues.IsActive != PreviousValues.IsActive)
                                {                                    
                                    InsertAudit(UserID, ViewName, Convert.ToString(PreviousValues.IsActive), Convert.ToString(NewValues.IsActive), "IsActive", NewValues.ID.ToString(), "N/A");
                                }
                                break;

                            case ("CourseMaterial"):
                                string PreviousCourseMaterial = ""; string NewCourseMaterial = "";
                                var listofPreCourseMaterial = db.vt_Training_CourseMaterial.Where(z => z.TrainingID == NewValues.ID).ToList();
                                if (NewValues.CourseMaterial.Count() != listofPreCourseMaterial.Count)
                                {

                                    for (int i = 0; i < listofPreCourseMaterial.Count; i++)
                                    {
                                         SqlParameter[] param = { new SqlParameter("@CourseID", listofPreCourseMaterial[i].CourseMaterialID) };
                                         var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetCourseMaterialNamebyID", param);
                                        if (i == 0)
                                        {
                                            dt.Rows[0]["CourseMaterial"].ToString();
                                            PreviousCourseMaterial += dt.Rows[0]["CourseMaterial"].ToString();
                                        }
                                        else
                                        {
                                            PreviousCourseMaterial += "," + dt.Rows[0]["CourseMaterial"].ToString();
                                        }
                                    }

                                    for (int j = 0; j < NewValues.CourseMaterial.Count(); j++)
                                    {
                                        SqlParameter[] param = { new SqlParameter("@CourseID", Convert.ToInt32(NewValues.CourseMaterial[j])) };
                                        var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetCourseMaterialNamebyID", param);
                                        if (j == 0)
                                        {
                                            NewCourseMaterial += dt.Rows[0]["CourseMaterial"].ToString();
                                        }

                                        else
                                        {
                                            NewCourseMaterial += "," + dt.Rows[0]["CourseMaterial"].ToString();
                                        }
                                    }
                                    InsertAudit(UserID, ViewName, PreviousCourseMaterial, NewCourseMaterial, "CourseMaterial", NewValues.ID.ToString(), "N/A");
                                    break;
                                }
                                else
                                {
                                    bool isExist = false;
                                    for (int i = 0; i < listofPreCourseMaterial.Count; i++)
                                    {
                                        for (int j = 0; j < NewValues.CourseMaterial.Count(); j++)
                                        {
                                            if (listofPreCourseMaterial[i].CourseMaterialID == NewValues.CourseMaterial[j])
                                            {
                                                isExist = true;
                                                break;
                                            }

                                            else
                                            {
                                                isExist = false;
                                            }
                                        }
                                    }

                                    if (isExist == false)
                                    {
                                        for (int i = 0; i < listofPreCourseMaterial.Count; i++)
                                        {
                                            SqlParameter[] param = { new SqlParameter("@CourseID", listofPreCourseMaterial[i].CourseMaterialID) };
                                            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetCourseMaterialNamebyID", param);
                                            if (i == 0)
                                            {
                                                PreviousCourseMaterial += dt.Rows[0]["CourseMaterial"].ToString();
                                            }
                                            else
                                            {
                                                PreviousCourseMaterial += "," + dt.Rows[0]["CourseMaterial"].ToString();
                                            }

                                        }

                                        for (int j = 0; j < NewValues.CourseMaterial.Count(); j++)
                                        {
                                            SqlParameter[] param = { new SqlParameter("@CourseID", Convert.ToInt32(NewValues.CourseMaterial[j])) };
                                            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetCourseMaterialNamebyID", param);
                                            if (j == 0)
                                            {
                                                NewCourseMaterial += dt.Rows[0]["CourseMaterial"].ToString();
                                            }
                                            else
                                            {
                                                NewCourseMaterial += "," + dt.Rows[0]["CourseMaterial"].ToString();
                                            }



                                        }
                                        InsertAudit(UserID, ViewName, PreviousCourseMaterial, NewCourseMaterial, "CourseMaterial", NewValues.ID.ToString(), "N/A");
                                        
                                    }

                                    break;



                                }

                            case ("Attachments"):
                                string PreviousAttachments = ""; string NewAttachments = "";
                                var listofPreAttachments = db.vt_Training_Attachments.Where(z => z.TrainingID == NewValues.ID).ToList();
                                if (NewValues.Attachments != null) {
                                    if (NewValues.Attachments.Count() != listofPreAttachments.Count)
                                    {

                                        for (int i = 0; i < listofPreAttachments.Count; i++)
                                        {
                                            SqlParameter[] param = { (new SqlParameter("@Attachment", listofPreAttachments[i].Attachment)) };
                                            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAttachmentNamebyAttachment", param);
                                            //var pAttachments = db.vt_Training_Attachments.Where(u => u.Attachment == listofPreAttachments[i].Attachment).FirstOrDefault();
                                            if (i == 0)
                                            {


                                                PreviousAttachments += dt.Rows[0]["Attachment"].ToString().Split('_')[0].ToString();
                                            }
                                            else
                                            {
                                                PreviousAttachments += "," + dt.Rows[0]["Attachment"].ToString().Split('_')[0].ToString();
                                            }
                                        }

                                        for (int j = 0; j < NewValues.Attachments.Count(); j++)
                                        {
                                            // SqlParameter[] param = { (new SqlParameter("@Attachment", Convert.ToString(NewValues.Attachments[j]))) };
                                            // var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAttachmentNamebyAttachment", param);
                                            // var NewAttachmentsName = db.vt_Training_Attachments.Where(u => u.Attachment == NewValues.Attachments[j]).FirstOrDefault();
                                            if (j == 0)
                                            {

                                                NewAttachments += NewValues.Attachments[j].Split('_')[0].ToString();
                                            }

                                            else
                                            {
                                                NewAttachments += "," + NewValues.Attachments[j].Split('_')[0].ToString();
                                            }
                                        }
                                        InsertAudit(UserID, ViewName, PreviousAttachments, NewAttachments, "Attachment", NewValues.ID.ToString(), "N/A");
                                        break;
                                    }

                                    else
                                    {
                                        bool isExist = false;
                                        for (int i = 0; i < listofPreAttachments.Count; i++)
                                        {
                                            for (int j = 0; j < NewValues.Attachments.Count(); j++)
                                            {
                                                if (listofPreAttachments[i].Attachment.Split('_')[0].ToString() == NewValues.Attachments[j].Split('_')[0].ToString())
                                                {
                                                    isExist = true;
                                                    break;
                                                }

                                                else
                                                {
                                                    isExist = false;
                                                }
                                            }
                                        }

                                        if (isExist == false)
                                        {
                                            for (int i = 0; i < listofPreAttachments.Count; i++)
                                            {
                                                SqlParameter[] param = { (new SqlParameter("@Attachment", listofPreAttachments[i].Attachment)) };
                                                var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAttachmentNamebyAttachment", param);
                                                //var pAttachments = db.vt_Training_Attachments.Where(u => u.Attachment == listofPreAttachments[i].Attachment).FirstOrDefault();
                                                if (i == 0)
                                                {



                                                    PreviousAttachments += dt.Rows[0]["Attachment"].ToString().Split('_')[0].ToString();
                                                }
                                                else
                                                {
                                                    PreviousAttachments += "," + dt.Rows[0]["Attachment"].ToString().Split('_')[0].ToString();
                                                }

                                            }

                                            for (int j = 0; j < NewValues.Attachments.Count(); j++)
                                            {
                                                SqlParameter[] param = { (new SqlParameter("@Attachment", Convert.ToString(NewValues.Attachments[j]))) };
                                                var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAttachmentNamebyAttachment", param);
                                                //var NewAttachmentsName = db.vt_Training_Attachments.Where(u => u.Attachment == NewValues.Attachments[j]).FirstOrDefault();
                                                if (j == 0)
                                                {

                                                    NewAttachments += NewValues.Attachments[j].Split('_')[0].ToString();
                                                }

                                                else
                                                {
                                                    NewAttachments += "," + NewValues.Attachments[j].Split('_')[0].ToString();
                                                }



                                            }
                                            InsertAudit(UserID, ViewName, PreviousAttachments, NewAttachments, "Attachment", NewValues.ID.ToString(), "N/A");

                                        }

                                        break;



                                    }
                                }
                                else
                                {                                   
                                    if (listofPreAttachments!=null)
                                    {
                                        for (int i = 0; i < listofPreAttachments.Count; i++)
                                        {
                                            SqlParameter[] param = { (new SqlParameter("@Attachment", listofPreAttachments[i].Attachment)) };
                                            var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_GetAttachmentNamebyAttachment", param);
                                            //var pAttachments = db.vt_Training_Attachments.Where(u => u.Attachment == listofPreAttachments[i].Attachment).FirstOrDefault();
                                            if (i == 0)
                                            {


                                                PreviousAttachments += dt.Rows[0]["Attachment"].ToString().Split('_')[0].ToString();
                                            }
                                            else
                                            {
                                                PreviousAttachments += "," + dt.Rows[0]["Attachment"].ToString().Split('_')[0].ToString();
                                            }
                                        }
                                        InsertAudit(UserID, ViewName, PreviousAttachments, "", "Attachment", NewValues.ID.ToString(), "N/A");
                                        break;
                                    }
                                    else
                                    {
                                        InsertAudit(UserID, ViewName, "", "", "Attachment", NewValues.ID.ToString(), "N/A");
                                        break;
                                    }
                                }
                               
                              


                        }
                    }
                    


                }


                return "";
            }
            catch (Exception ex)
            {
                throw new Exception("Error Creating Audit Log Try Again");
               
            }


        }

        public static string InsertAudit(int UserID, string ViewName, string PreviousData, string ChangeData, string FieldName, string TrainingID, string TopicID)
        {
           
            try
            {
            SqlParameter[] param = {
            new SqlParameter("@UserID",  UserID ),
            new SqlParameter("@ViewName",  ViewName ),
            new SqlParameter("@PreviousData", PreviousData),
            new SqlParameter("@ChangeData", ChangeData),
            new SqlParameter("@FieldName", FieldName),
            new SqlParameter("@TrainingID", TrainingID),
            new SqlParameter("@TopicID", TopicID),
            new SqlParameter("@CreatedAt", DateTime.Now),
                               };

                var dt = Entity_Common.get_DataTable_WithPara(ConfigurationManager.AppSettings["ConnStr"], "sp_InsertAuditLog", param);
                return "Sucess";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        #endregion

        //#region Custome

        //public static int ToInt(object value)
        //{
        //    int parseVal;
        //    return ((value == null) || (value == DBNull.Value)) ? 0 : int.TryParse(value.ToString(), out parseVal) ? parseVal : 0;
        //}
        public static DateTime GetCurrentDateTime()
        {
            string TimeZoneID = "Pakistan Standard Time";

            DateTime dt = DateTime.Now;
            DateTime currentTime = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById(TimeZoneID));

            return currentTime;
        }

        public static DateTime ConvertDateString(string Date)
        {
            string[] formats = {
                 "d/M/yyyy h:mm:ss tt",
                         "d/M/yyyy h:mm tt",
                         "dd/MM/yyyy hh:mm:ss",
                         "dd/MM/yyyy hh:mm:ss tt",
                          "dd/MM/yyyy HH:mm:ss tt",
                           "dd/MM/yyyy HH:mm:ss",
                         "d/M/yyyy h:mm:ss",
                         "d/M/yyyy hh:mm tt",
                         "d/M/yyyy hh tt",
                         "d/M/yyyy h:mm",
                         "d/M/yyyy h:mm",
                         "dd/MM/yyyy hh:mm",
                         "dd/MM/yyyy hh:mm:ss",
                         "dd/M/yyyy hh:mm",
                         "d/MM/yyyy HH:mm:ss.ffffff",
                         "d/M/yyyy",
                          "d/MM/yyyy",
                          "dd/MM/yyyy",
                          "dd/M/yyyy",
                          "yyyy/MM/dd",
                "M/d/yyyy h:mm:ss tt",
                           "M/d/yyyy h:mm tt",
                         "MM/dd/yyyy hh:mm:ss",
                          "MM/dd/yyyy hh:mm:ss tt",
                          "MM/dd/yyyy HH:mm:ss tt",
                           "MM/dd/yyyy HH:mm:ss",
                          "M/d/yyyy h:mm:ss",
                         "M/d/yyyy hh:mm tt",
                        "M/d/yyyy hh tt",
                         "M/d/yyyy h:mm",
                         "M/d/yyyy h:mm",
                         "MM/dd/yyyy hh:mm",
                         "MM/dd/yyyy hh:mm:ss",
                         "M/dd/yyyy hh:mm",
                         "MM/d/yyyy HH:mm:ss.ffffff",
                           "MM/d/yyyy",
                           "M/d/yyyy",
                           "yyyy/MM/dd"
                        };


            return DateTime.ParseExact(Date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
        }

        public static string ReplaceSpecialCharacters(string Para)
        {
            var strReplaced = Regex.Replace(Para, "[ *'\",_&#^@]", string.Empty);

            return strReplaced;
        }

        public static string GetPathPrefix()
        {
            return WebConfigurationManager.AppSettings["TNA_URL_Prefix"].ToString();
        }

        //public static TimeSpan ConvertTimeString(double hour, double minute, double second)
        //{
        //    string strHour = "00";
        //    string strMinute = "00";
        //    string strSecond = "00";

        //    if (hour < 10)
        //        strHour = "0" + hour.ToString();
        //    else
        //        strHour = hour.ToString();

        //    if (minute < 10)
        //        strMinute = "0" + minute.ToString();
        //    else
        //        strMinute = minute.ToString();

        //    if (second < 10)
        //        strSecond = "0" + second.ToString();
        //    else
        //        strSecond = second.ToString();

        //    string strTotalTime = strHour + ":" + strMinute + ":" + strSecond;



        //    return ConvertStringTime(strTotalTime);
        //}

        //public static TimeSpan ConvertStringTime(string Time)
        //{
        //    DateTime dt_TotalTime;
        //    DateTime.TryParseExact(Time, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt_TotalTime);
        //    return dt_TotalTime.TimeOfDay;
        //}
        //public static int CalculateAge(DateTime dOB)
        //{
        //    int age = 0;
        //    age = vt_Common.GetCurrentDateTime().Year - dOB.Year;
        //    if (vt_Common.GetCurrentDateTime().DayOfYear < dOB.DayOfYear)
        //        age = age - 1;

        //    return age;
        //}


        //#endregion



        //#region Link_Security
        //public static void UpdateGuid(string Guid)
        //{
        //    Bal_Layer bal = new Bal_Layer();
        //    bal.updateLinkSecurity(Guid);
        //}

        //public static void InsertGuid(string Guid)
        //{
        //    Bal_Layer bal = new Bal_Layer();
        //    bal.insertLinkSecurity(Guid);
        //}

        //public static DataTable GetGuid(string Guid)
        //{
        //    Bal_Layer bal = new Bal_Layer();
        //    return bal.getLinkGuid(Guid);
        //}

        //#endregion


        //#region ExceptionLog

        //public static void Log(int Userid, string ViewName, string MethodName, string Message, bool isException)
        //{
        //    ExceptionLog(Userid, ViewName, MethodName, Message, isException, vt_Common.GetCurrentDateTime());
        //}


        //public static void ExceptionLog(int Userid, string ViewName, string MethodName, string Message, bool isException, DateTime CreatedDate)
        //{
        //    Message = "isLiveException: " + ConfigurationManager.AppSettings["isLive"].ToString() + " " + Environment.NewLine + " " + Message;
        //    vt_Common.DbLog(Userid, ViewName, MethodName, Message, isException, CreatedDate);
        //    vt_Common.EmailLog(Userid, ViewName, MethodName, Message, isException, CreatedDate);
        //    vt_Common.SmsLog(Userid, ViewName, MethodName, Message, isException, CreatedDate);
        //    vt_Common.TxtFileLog(Userid, ViewName, MethodName, Message, isException, CreatedDate);
        //}

        //public static void DbLog(int Userid, string ViewName, string MethodName, string Message, bool isException, DateTime CreatedDate)
        //{
        //    //Bal_Layer bal = new Bal_Layer();
        //    vt_Log log = new vt_Log();

        //    log.UserId = Userid;
        //    log.ViewName = ViewName;
        //    log.MethodName = MethodName;
        //    log.Message = Message;
        //    log.isException = true;
        //    log.CreatedDate = CreatedDate;

        //    //bal.InsertDBLog(log);
        //}

        ////To Create the email body
        //public static void EmailLog(int Userid, string ViewName, string MethodName, string Message, bool isException, DateTime CreatedDate)
        //{
        //    string html = @"<html>
        //            <head>
        //                <title></title>

        //                <style>
        //                    table {
        //                        border-collapse: collapse;
        //                        border: 0;
        //                    }
        //                </style>
        //            </head>
        //            <body>
        //                <table style='width: 600px; margin: 0 auto; padding: 15px;'>
        //                    <tbody>
        //                        <tr>
        //                            <td>
        //                                <table style='width: 100%;'>
        //                                    <tr>
        //                                        <td align='center' style='background-color: #f2f5f9; padding: 15px;'>
        //                                            <img src='https://app.superdrive.com.au/Content/SDImage/logo.png' width='200px'>
        //                                            <h1 style='font-weight: 500;'>Super Drive Exception<br/>

        //                                   <span style='width: 100px; height: 2px; background-color: #333; display: inline-block; margin-top: 4px;'></span>

        //                                            </h1>
        //                                         </td>
        //            </tr>
        //            <td style='background-color: #f2f5f9; padding: 15px;'>
        //                                            <span style='text-align:left'>" + "<b>Super Drive Exception Log:</b><br>" + @"</span>
        //                                        <p style='font-size: 17px; line-height: 30px; text-align: left; color: #0949f6; padding: 10px 40px;'>
        //                                        UserID: " + Userid + @" <br>
        //                                        View: " + ViewName + @" <br>
        //                                        Method: " + MethodName + @" <br>
        //                                        Message: " + Message + @" <br>
        //                                        CreatedDate: " + CreatedDate + @" <br>

        //                                            </p>
        //                                    <br>
        //                                <span style='text-align:left'>
        //                            Regards, <br>
        //                                The SuperDrive Crew <br>
        //                                “Super Crew! Assemble!”<br>
        //                                <img src='https://app.superdrive.com.au/Content/SDImage/signature.jpg' width='150px' height='50px'>
        //                                </span>



        //                                        </td>
        //                                    </tr>

        //                                </table>
        //                                <table width='100 % '>
        //                                    <tr>
        //                                        <td style='height: 20px;'></td>
        //                                    </tr>

        //                                </table>
        //                            </td>
        //                        </tr>
        //                    </tbody>
        //                </table> 
        //            </body>
        //            </html>";

        //    //Sending Error Email
        //    SendLogEmail(html);

        //}

        ////To create the sms body
        //public static void SmsLog(int Userid, string ViewName, string MethodName, string Message, bool isException, DateTime CreatedDate)
        //{
        //    string smsbody = "SuperDrive Exception Log: UserID: " + Userid + ", View: " + ViewName + ", Method: " + MethodName + ", Message: " + Message + ", isException: " + isException + ", CreatedDate: " + CreatedDate + ".";

        //    SendLogSMS(smsbody);
        //}

        ////To create the txt body
        //public static void TxtFileLog(int Userid, string ViewName, string MethodName, string Message, bool isException, DateTime CreatedDate)
        //{
        //    string txtbody = "SuperDrive Exception Log: UserID: " + Userid + " View: " + ViewName + " Method: " + MethodName + " Message: " + Message + " isException: " + isException + " CreatedDate: " + CreatedDate + ".";

        //    ErrorHandling.WriteError(txtbody);
        //}

        ////public static DataTable GetAllEmailandPhone()
        ////{
        ////    //Bal_Layer bal = new Bal_Layer();
        ////    //return bal.getEmailTable();
        ////}

        //public static string SendLogEmail(string body)
        //{
        //    try
        //    {

        //        //Get All records of Emails from db using sp in DataTable
        //        //Loop thorugh al record and concatenate record with ',' like email1,email2,email3.....

        //        string cc = "";
        //        DataTable dt = GetAllEmailandPhone();
        //        foreach (DataRow row in dt.Rows)
        //        {
        //            cc += row[1] + ",";
        //        }

        //        cc = cc.TrimEnd(',');

        //        //MailTo = ConfigurationSettings.AppSettings["MailTo"].ToString();

        //        string to = "", bcc = "", subject = "", ReplyTo = "", attachment = "";

        //        to = ConfigurationSettings.AppSettings["MailTo"].ToString();

        //        //string[] ToEmails = to.Split(',');
        //        using (MailMessage message = new MailMessage())
        //        {
        //            string[] Add = to.ToString().Split(',');
        //            message.From = new MailAddress(ConfigurationSettings.AppSettings["MailFrom"].ToString(), ConfigurationSettings.AppSettings["MailFromName"].ToString());
        //            if (Add.Length > 1)
        //            {
        //                for (int a = 0; a < Add.Length; a++)
        //                {
        //                    message.To.Add(new MailAddress(Add[a].ToString()));
        //                }
        //            }
        //            else { message.To.Add(new MailAddress(to)); }
        //            if (cc != null && cc != "")
        //            {
        //                if (cc.ToString().Contains(","))
        //                {
        //                    string[] Add_cc = cc.ToString().Split(',');
        //                    if (Add_cc.Length > 1)
        //                    {
        //                        for (int a = 0; a < Add_cc.Length; a++)
        //                        {
        //                            message.CC.Add(new MailAddress(Add_cc[a]));
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    message.CC.Add(new MailAddress(cc));
        //                }
        //            }
        //            if (bcc != null && bcc != "")
        //            {
        //                message.Bcc.Add(new MailAddress(bcc));
        //            }
        //            if (ReplyTo != null && ReplyTo != "")
        //            {
        //                message.ReplyToList.Add(new MailAddress(ReplyTo));
        //            }
        //            message.IsBodyHtml = true;


        //            message.Subject = "Superdrive Exception";
        //            message.Body = body;



        //            using (SmtpClient smtp = new SmtpClient())
        //            {
        //                smtp.UseDefaultCredentials = true;
        //                smtp.Host = ConfigurationSettings.AppSettings["SmtpServer"].ToString();


        //                if (bool.Parse(ConfigurationSettings.AppSettings["UseCredentials"]))
        //                {
        //                    string EmailPass = (ConfigurationSettings.AppSettings["Password"].ToString());
        //                    smtp.Credentials = new NetworkCredential(ConfigurationSettings.AppSettings["UserEmail"].ToString(), EmailPass);

        //                }
        //                else
        //                {
        //                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        //                    smtp.UseDefaultCredentials = false;

        //                }
        //                smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["Port"].ToString());
        //                smtp.EnableSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["isSSL"].ToString());


        //                try
        //                {
        //                    smtp.Send(message);
        //                }
        //                catch (Exception ex)
        //                {
        //                    return "Exception: " + ex.Message.ToString();
        //                }
        //            }
        //            //if (type == "Forgot Password")
        //            //    return "Successfully email send for reset pass";
        //            //if (type == "Set Password")
        //            //    return "Successfully email send for set pass";
        //            //else
        //            return "email sent successfully";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message.ToString());
        //    }
        //}

        //public static void SendLogSMS(string Body)
        //{
        //    //Get All records of PhoneNumber from db using sp in DataTable
        //    //Loop thorugh al record and concatenate record with ',' like email1,email2,email3.....

        //    string phoneNumber = "";

        //    DataTable dt = GetAllEmailandPhone();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        phoneNumber += row[2] + ",";
        //    }

        //    phoneNumber = phoneNumber.TrimEnd(',');

        //    string ACCOUNT_SID = ConfigurationManager.AppSettings["ACCOUNT_SID"].ToString(); //"AC51942f59dfc8549581649307c913a91d";
        //    string AUTH_TOKEN = ConfigurationManager.AppSettings["AUTH_TOKEN"].ToString(); //"6205b8a5743322e25b3207606fee7457";
        //    TwilioClient.Init(ACCOUNT_SID, AUTH_TOKEN);


        //    string[] splitNo = phoneNumber.Split(',');
        //    if (splitNo.Length > 1)
        //    {
        //        for (int i = 0; i < splitNo.Length; i++)
        //        {
        //            var to = new PhoneNumber(splitNo[i]);
        //            try
        //            {

        //                var message = MessageResource.Create(
        //                    to,
        //                    ////from: new PhoneNumber("+13025261595"),

        //                    from: new PhoneNumber(ConfigurationManager.AppSettings["SMSFrom"].ToString()),
        //                    // statusCallback: new Uri(callbackurl),
        //                    body: Body);
        //            }
        //            catch (Exception ex)
        //            {
        //                ErrorHandling.WriteError(ex.ToString());
        //            }
        //        }

        //    }
        //    else
        //    {

        //    }


        //}
        //#endregion

    }
}
