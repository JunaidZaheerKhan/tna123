﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TNA_ViewModel.Model
{
 public class ModelClass
    {
        

    }
    public class EmailToTrainer
    {
        public string Receptor { get; set; }
        public string SessionName { get; set; }
        public int NoOfParticipants { get; set; }
        public string TNAName { get; set; }
        public string TrainingName { get; set; }
        public string Email { get; set; }
    }
}
