﻿using DAL.DBEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TNA_ViewModel.Model
{
    public class TNA_Session
    {
        public vt_UserProfile User { get; set; }
        public List<vt_RoleMenu> MenuList { get; set; }
        public List<vt_RolePages> PageList { get; set; }
    }

    //public class ActiveTnaAndTrainings_mdl
    //{
    //    public string TNAName { get; set; }
    //    public string TrainingName { get; set; }
    //    public string Descriptions { get; set; }
    //    public int Days { get; set; }
    //    public int TrainingID { get; set; }
    //}

    //public class ActiveTrainingsSessions_mdl
    //{
    //    public int TNAID { get; set; }
    //    public string TNAName { get; set; }
    //    public int TrainingID { get; set; }
    //    public string TrainingName { get; set; }
    //    public string Descriptions { get; set; }
    //    public int Days { get; set; }
    //    public int SessionID { get; set; }
    //    public string SessionName { get; set; }
    //    public DateTime SessionStart { get; set; }
    //    public DateTime SessionEnd { get; set; }
    //    public int Participants { get; set; }

    //}
    //public class ActiveAssignedDepartmentsEmployee_mdl
    //{
    //    public int DepartmentID { get; set; }
    //    public string DepartmentName { get; set; }
    //    public int UserID { get; set; }
    //    public string EmployeeName { get; set; }

    //}
    //public class SessionParticipants_mdl
    //{
    //    public int TNAID { get; set; }
    //    public int TrainingID { get; set; }
    //    public int SessionID { get; set; }
    //    public int DepartmentID { get; set; }
    //    public string DepartmentName { get; set; }
    //    public int UserID { get; set; }
    //    public string EmployeeName { get; set; }

    //}
}





//using DAL.DBEntities;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace TNA_ViewModel.Model
//{
//    public class TNA_Session
//    {
//        public vt_UserProfile User { get; set; }
//        public List<vt_RoleMenu> MenuList { get; set; }
//        public List<vt_RolePages> PageList { get; set; }
//    }
//}
